<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
<meta name="_csrf" content="${_csrf.token}"/>
    <!-- default header name is X-CSRF-TOKEN -->
    <meta name="_csrf_header" content="${_csrf.headerName}"/>
    <title>CommissionTracker</title>
    
     <!-- calender CSS -->
    <link href="${pageContext.request.contextPath}/layout/css/calender.css" rel="stylesheet">
    
    <!-- datatable.min.css -->
    <link href="${pageContext.request.contextPath}/layout/css/datatable.min.css" rel="stylesheet">

 <!-- fixedColumns.dataTables.min.css -->
    <link href="${pageContext.request.contextPath}/layout/css/fixedColumns.dataTables.min.css" rel="stylesheet">

 <!-- fixedColumns.dataTables.min.css -->
    <link href="${pageContext.request.contextPath}/layout/css/font-awesome.css" rel="stylesheet">


    <!-- Bootstrap Core CSS -->
    <link href="${pageContext.request.contextPath}/layout/css/bootstrap.min.css" rel="stylesheet">
    
    <!-- Jquey Ui Core CSS -->
    <link href="${pageContext.request.contextPath}/layout/css/jquery-ui-1.10.4.custom.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="${pageContext.request.contextPath}/layout/css/plugins/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="${pageContext.request.contextPath}/layout/css/index.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="${pageContext.request.contextPath}/layout/font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    
    <!-- DataTables CSS -->
        <link href="${pageContext.request.contextPath}/layout/css/plugins/dataTables.bootstrap.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/layout/images/favicon.ico" rel="icon">
         <!-- Alert Links -->
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/layout/css/sweet-alert.css">
    
     <link href="${pageContext.request.contextPath}/layout/css/redactor.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

   
    
    <div id="wrapper">

    <%--    <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="  img-responsive logo-exide"></div>
                <div class=" img-responsive logo-360degree"></div>
            <div class="navbar-header">
           		 
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    
                </button>
                
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li id="employee_also" style="display: none"><a href="${pageContext.request.contextPath}/employee"><i class="fa fa-user fa-fw"></i> Employee </a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="${pageContext.request.contextPath}/logout"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
            </ul>
            <!-- /.navbar-top-links -->

           <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li id="dashboard" class="maind maindashboard" data-toggle="tooltip tab" data-original-title="Details">&nbsp;&nbsp;<i class="fa fa-dashboard fa-fw"></i> 
                                <span class="content-hide">Dashboard</span>
                        </li>
                        <li id="drivercreation" class="maind" data-toggle="tooltip tab" data-original-title="Drivers"> &nbsp;&nbsp;<i class="fa fa-question fa-fw"></i>
                                <span class="content-hide">Question and Drivers</span>
                        </li>
                        <li id="emailtemplate" class="maind" data-toggle="tooltip tab" data-original-title="Email_Templats">&nbsp;&nbsp;<i class="fa fa-tags fa-fw"></i>
                                <span class="content-hide">Communication Template</span>
                        </li>
                        <li id="addsession" class="maind" data-toggle="tooltip tab" data-original-title="add/delete_session">&nbsp;&nbsp;<i class="fa fa-clock-o fa-fw"></i>
                                <span class="content-hide">Session Management</span>
                        </li>
                        <li id="uploadexcel" class="maind" data-toggle="tooltip tab" data-original-title="Excel_Upload">&nbsp;&nbsp;<i class="fa fa-file-excel-o fa-fw"></i> 
                                <span class="content-hide">Data upload</span>
                        </li>
                        <li id="participants_id" class="maind" data-toggle="tooltip tab" data-original-title="Participant">&nbsp;&nbsp;<i class="fa fa-group fa-fw"></i> 
                                <span class="content-hide">Participants Management</span>
                        </li>
                        <li id="listofexception" class="maind" data-toggle="tooltip tab" data-original-title="Exception"> &nbsp;&nbsp;<i class="fa fa-reorder fa-fw"></i>
                                <span class="content-hide">Exception List</span>
                        </li>
                        <li id="usermanage" class="maind" data-toggle="tooltip tab" data-original-title="Manage_User">&nbsp;&nbsp;<i class="fa fa-user fa-fw"></i> 
                                <span class="content-hide">Manage Admin</span>
                        </li>
                         <li id="reports" class="maind" data-toggle="tooltip tab" data-original-title="Report Generation">&nbsp;&nbsp;<i class="fa fa-file-pdf-o fa-fw"></i> 
                                <span class="content-hide">Generate Reports</span>
                        </li>
                        
                        
                        <li class="maindashboard">
                            <span id="menu-toggle" href="#menu-toggle"></span>
                        </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>
 --%>
	 <div class="header-area">
			<div class="container">
			<nav class="navbar">
				<div class="navbar-header">
				  <div class="brand-img">
					<img src="../../../images/logo.jpg" alt="">
				  </div>
				</div>
			</nav>
			</div>
		</div>
       
    </div>
    <!-- /#wrapper -->
    
    <div id="pageloading">
        <div id="loading"></div>
    </div>

   

    <!-- jQuery Version 1.11.0 -->
    <script src="${pageContext.request.contextPath}/layout/js/vendors/jquery-1.11.0.js"></script>
    
    <script type="text/javascript">
		$(document).ready(function(){
			 var token = $("meta[name='_csrf']").attr("content");
		        var header = $("meta[name='_csrf_header']").attr("content");
		        $(document).ajaxSend(function(e, xhr, options) {
		            xhr.setRequestHeader(header, token);
		        });

			});
    </script>
    
    <!-- jQuery UI version 1.8 -->
    <script src="${pageContext.request.contextPath}/layout/js/vendors/jquery-ui-1.10.4.custom.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="${pageContext.request.contextPath}/layout/js/vendors/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="${pageContext.request.contextPath}/layout/js/plugins/metisMenu/metisMenu.min.js"></script>
    
    
    <!-- underscrore backbone JavaScript -->
    <script src="${pageContext.request.contextPath}/layout/js/vendors/underscore.js"></script>
    
    
    <!-- backbone JavaScript -->
    <script src="${pageContext.request.contextPath}/layout/js/vendors/backbone.js"></script>
    
    <!-- forms JavaScript -->
    <script src="${pageContext.request.contextPath}/layout/js/vendors/jquery.form.js"></script>
    
     
    <!-- richtext JavaScript -->
    <script src="${pageContext.request.contextPath}/layout/js/vendors/redactor.min.js"></script>
    
    
     <!--dataTables javascript-->
    
    <script src="${pageContext.request.contextPath}/layout/js/plugins/dataTables/jquery.dataTables.js"></script>
    <script src="${pageContext.request.contextPath}/layout/js/plugins/dataTables/dataTables.bootstrap.js"></script>
    
   
</body>

</html>