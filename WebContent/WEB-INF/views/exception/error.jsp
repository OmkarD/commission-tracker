<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>


 <h2>Application Error, please contact support.</h2>
 
<h3>Debug Information:</h3>
 
Requested URL= ${url}<br><br>
 
Exception= ${exception.message}<br><br>
 
<strong>Exception Stack Trace</strong><br>
<c:forEach items="${exception.stackTrace}" var="ste">
    ${ste}
</c:forEach>
    <script type="text/javascript">
		$(document).ready(function(){
			 var token = $("meta[name='_csrf']").attr("content");
		        var header = $("meta[name='_csrf_header']").attr("content");
		        $(document).ajaxSend(function(e, xhr, options) {
		            xhr.setRequestHeader(header, token);
		        });

			});
    </script>