<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html lang="en">

<head>

    
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
<meta name="_csrf" content="${_csrf.token}"/>
    <!-- default header name is X-CSRF-TOKEN -->
    <meta name="_csrf_header" content="${_csrf.headerName}"/>
    <title>CommissionTracker</title>
 
    <!-- Bootstrap Core CSS -->
    <link href="${pageContext.request.contextPath}/layout/css/bootstrap.min.css" rel="stylesheet">
    
    <!-- DatePicker CSS -->
    <!-- <link href="${pageContext.request.contextPath}/layout/css/datepicker3.css" rel="stylesheet"> -->
    
    <!-- Jquey Ui Core CSS -->

    <!-- MetisMenu CSS -->
    <link href="${pageContext.request.contextPath}/layout/css/plugins/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="${pageContext.request.contextPath}/layout/css/index.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="${pageContext.request.contextPath}/layout/font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    
    <!-- DataTables CSS -->
        <!-- <link href="../css/plugins/dataTables.bootstrap.css" rel="stylesheet"> -->
         <link href="${pageContext.request.contextPath}/layout/css/datatable.min.css" rel="stylesheet">
         <link href="${pageContext.request.contextPath}/layout/css/fixedColumns.dataTables.min.css" rel="stylesheet">
         
    
    <!-- TimePicker CSS -->
    
    <!-- Favicon CSS -->
        <link href="${pageContext.request.contextPath}/layout/images/favicon.ico" rel="icon">
    
    <!-- Favicon CSS -->
        <link href="${pageContext.request.contextPath}/layout/css/redactor.css" rel="stylesheet" type="text/css">
    
    <!-- Alert Links -->
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/layout/css/sweet-alert.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/layout/css/jquery-ui-1.10.4.custom.min.css">

    <link rel="stylesheet" href="${pageContext.request.contextPath}/layout/css/alertifyjs/alertify.min.css"/>
    <!-- Default theme -->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/layout/css/alertifyjs/default.min.css"/>
    <!-- Semantic UI theme -->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/layout/css/alertifyjs/semantic.min.css"/>
    

</head>

<body class="backgroungImg">
    
    <div id="wrapper" class="full-content">
    </div>

    <script src="${pageContext.request.contextPath}/layout/js/vendors/jquery-1.11.0.js"></script>
    
    <!-- jQuery UI version 1.8 -->
    <script src="${pageContext.request.contextPath}/layout/js/vendors/jquery-ui-1.10.4.custom.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="${pageContext.request.contextPath}/layout/js/vendors/bootstrap.min.js"></script>

    <!-- underscrore backbone JavaScript -->
    <script src="${pageContext.request.contextPath}/layout/js/vendors/underscore.js"></script>
    
    <!-- backbone JavaScript -->
    <script src="${pageContext.request.contextPath}/layout/js/vendors/backbone.js"></script>
    
    <!-- forms JavaScript -->
    <script src="${pageContext.request.contextPath}/layout/js/vendors/jquery.form.js"></script>

    <!--dataTables javascript-->
    <script src="${pageContext.request.contextPath}/layout/js/plugins/dataTables/jquery.dataTables.js"></script>
    <script src="${pageContext.request.contextPath}/layout/js/plugins/dataTables/dataTables.fixedColumns.min.js"></script>
    <script src="${pageContext.request.contextPath}/layout/js/plugins/alertifyjs/alertify.min.js"></script>
    

    
    
    <!-- richtext JavaScript -->
    <script src="${pageContext.request.contextPath}/layout/js/vendors/redactor.min.js"></script>


    <script src="${pageContext.request.contextPath}/layout/js/user/controller/controller.js"></script>

     <script src="${pageContext.request.contextPath}/layout/js/user/view/test.js"></script>


    <script src="${pageContext.request.contextPath}/layout/js/user/routes/route.js"></script>


    <script src="${pageContext.request.contextPath}/layout/js/user/view/listofcycles.js"></script>

    <script src="${pageContext.request.contextPath}/layout/js/user/view/listofagents.js"></script>

    <script src="${pageContext.request.contextPath}/layout/js/user/view/validatedlistofagent.js"></script>

    <script src="${pageContext.request.contextPath}/layout/js/user/view/listofcommission.js"></script>


    <script src="${pageContext.request.contextPath}/layout/js/user/index.js"></script>


<div class="lds-css" id="pageloading">
  <div class="lds-dual-ring">
    <div></div>
  </div>
    <div class="loadTxt">
    please wait we are processing...
    </div>
</div>


     

 
<script type="text/javascript">
		$(document).ready(function(){
			 var token = $("meta[name='_csrf']").attr("content");
		        var header = $("meta[name='_csrf_header']").attr("content");
		        $(document).ajaxSend(function(e, xhr, options) {
		            xhr.setRequestHeader(header, token);
		        });

			});
    </script>
 
    
</body>

</html>

