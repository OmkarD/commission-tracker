<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>CommissionTracker</title>

    <!-- Bootstrap Core CSS -->
    <link href="${pageContext.request.contextPath}/layout/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="${pageContext.request.contextPath}/layout/css/plugins/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="${pageContext.request.contextPath}/layout/css/index.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="${pageContext.request.contextPath}/layout/font/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    
    <!-- DataTables CSS -->
        <link href="${pageContext.request.contextPath}/layout/css/plugins/dataTables.bootstrap.css" rel="stylesheet">
 <link href="${pageContext.request.contextPath}/layout/images/favicon.ico" rel="icon">
       
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        
       <!-- Navigation -->
      <!--   <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0;">
        <div class="  img-responsive logo-exide"></div>
                <div class=" img-responsive logo-360degree"></div>
            <div class="navbar-header">
           		 
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    
                </button>
                
            </div>
            /.navbar-header

            <ul class="nav navbar-top-links navbar-right">
                
            </ul>
            /.navbar-top-links

            
        </nav> -->
        
         <div class="header-area">
			<div class="container">
			<nav class="navbar">
				<div class="navbar-header">
				  <div class="brand-img">
					<img src="./images/logo.jpg" alt="">
				  </div>
				</div>
			</nav>
			</div>
		</div>


        <!-- Page Content -->
        <div id="page-wrapper"  style="margin: 0 ">
        
        
        
       <%--  <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title" align="center">Please Sign In</h3>
                    </div>
                    <div class="panel-body">
                        <form role="form" action="<c:url value='j_spring_security_check'   />" method="post">
                        <input type="hidden"    name="${_csrf.parameterName}"    value="${_csrf.token}"/>
                            <fieldset>
                                <div class="form-group">
                                    <input class="form-control"  name="j_username" type="text" autofocus="" placeholder="Username">
                                </div>
                                <div class="form-group">
                                    <input id="password" class="form-control"  name="j_password" type="password" placeholder="Password" >
                                </div>
                             
                                <!-- Change this to a button or input when using this as a form -->
                                <input type="submit" class="btn btn-lg btn-success btn-block" value="Login">
                                <div class="checkbox" align="center">
                                    <label  style="color:#f01e1e;">
                                        ${errormessage}
                                    </label>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div> --%>
        
        <div class="body-area">
				<div class="container">
				  <div class="row">
					<div class="welcome-container">Welcome To Commission Tracker</div>
					<div class="Absolute-Center is-Responsive">
					 <!--  <div class="login-container">Login</div> -->
					  <div class="col-sm-12 col-md-10 col-md-offset-1">
					  
							<form role="form" action="<c:url value='j_spring_security_check'/>" method="post">
							
							  <div class="form-group input-group">
								<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span> 
								<input class="form-control"  name="j_username" type="text" autocomplete="off" placeholder="User Name">					
							  </div>
							  
							  <div class="form-group input-group">
								<span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
								<input id="password" class="form-control"  name="j_password" type="password" autocomplete="off" placeholder="Password" >	
							  </div>
							  
							  <div class="form-group">
								<button type="submit" class="btn btn-def btn-block">Login</button>
							  </div>
							  <div  align="center">
                                    <label style="color:#f01e1e;">
                                        ${errormessage}
                                    </label>
                                </div>
							
							</form>     
							   
					  </div>  
					</div>    
				  </div>
				</div>
			</div>
        
         <!--   <div align="center"><b>Note:-</b> Please Enter your Credentials (Username and Password) same as your Window's Login Credentials</b></div> -->
        	
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery Version 1.11.0 -->
    <script src="${pageContext.request.contextPath}/layout/js/vendors/jquery-1.11.0.js"></script>
  <script type="text/javascript">
		$(document).ready(function(){
			 var token = $("meta[name='_csrf']").attr("content");
		        var header = $("meta[name='_csrf_header']").attr("content");
		        $(document).ajaxSend(function(e, xhr, options) {
		            xhr.setRequestHeader(header, token);
		        });

			});
    </script>
    <!-- Bootstrap Core JavaScript -->
    <script src="${pageContext.request.contextPath}/layout/js/vendors/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="${pageContext.request.contextPath}/layout/js/plugins/metisMenu/metisMenu.min.js"></script>
    
    
    <!-- underscrore backbone JavaScript -->
    <script src="${pageContext.request.contextPath}/layout/js/vendors/underscore.js"></script>
    
    
    <!-- backbone JavaScript -->
    <script src="${pageContext.request.contextPath}/layout/js/vendors/backbone.js"></script>
    
     <!--dataTables javascript-->
    
    <script src="${pageContext.request.contextPath}/layout/js/plugins/dataTables/jquery.dataTables.js"></script>
    <script src="${pageContext.request.contextPath}/layout/js/plugins/dataTables/dataTables.bootstrap.js"></script>
    
   <script>
/* $(document).ready(function(){
	if()
	alert("Currently there is no active session, please contact Administartor");
}); */

   </script>
</body>

</html>

