var listview,email, alertView, actionableView, aggregateview, feedbackview, stakeholdersCount, peerCount, selectingFlag, isparticipantFlag, directManagerReporteeFlag, surveySummaryFlag, gradeLTFlag, stackholdersexpFlag, questionDisplayFlag, ltFirstFlag, maxlimit, minlimit;

$(document).ready(function(e){
    
    questionDisplayFlag = false;
    surveySummaryFlag = false;
    isparticipantFlag = true;
    selectingFlag = false;
    directManagerReporteeFlag = false;
    gradeLTFlag = false;
    stackholdersexpFlag = false;
    stakeholdersCount = 0;
    peerCount = 0;
    ltFirstFlag = true;
    maxlimit = 0;
    minlimit = 0;
    email=null;
    
    $(document).attr('unselectable','on') 
             .css({'-moz-user-select':'-moz-none', 
                   '-moz-user-select':'none', 
                   '-o-user-select':'none', 
                   '-khtml-user-select':'none', /* you could also put this in a class */
                   '-webkit-user-select':'none',/* and add the CSS class here instead */
                   '-ms-user-select':'none', 
                   'user-select':'none'
             }).bind('selectstart', function(){ return false; }); 
    $("html").attr('unselectable','on') 
             .css({'-moz-user-select':'-moz-none', 
                   '-moz-user-select':'none', 
                   '-o-user-select':'none', 
                   '-khtml-user-select':'none', /* you could also put this in a class */
                   '-webkit-user-select':'none',/* and add the CSS class here instead */
                   '-ms-user-select':'none', 
                   'user-select':'none'
             }).bind('selectstart', function(){ return false; }); 
    $("*").attr('unselectable','on') 
             .css({'-moz-user-select':'-moz-none', 
                   '-moz-user-select':'none', 
                   '-o-user-select':'none', 
                   '-khtml-user-select':'none', /* you could also put this in a class */
                   '-webkit-user-select':'none',/* and add the CSS class here instead */
                   '-ms-user-select':'none', 
                   'user-select':'none'
             }).bind('selectstart', function(){ return false; }); 
    $("span").attr('unselectable','on') 
             .css({'-moz-user-select':'-moz-none', 
                   '-moz-user-select':'none', 
                   '-o-user-select':'none', 
                   '-khtml-user-select':'none', /* you could also put this in a class */
                   '-webkit-user-select':'none',/* and add the CSS class here instead */
                   '-ms-user-select':'none', 
                   'user-select':'none'
             }).bind('selectstart', function(){ return false; });
    
    selfFeedBackView = new SelfFeedBackView({ el : $("#selffb_wrapper")});
    instructionView = new InstructionView({ el : $("#employee-wrapper")});
	aggregateview = new AggregateView({ el : $("#employee-wrapper")});
	actionableView = new ActionableView({ el : $("#employee-wrapper")});
	listview = new ListView({ el : $("#employee-wrapper")});
	alertView = new AlertView({ el : $("#employee-wrapper")});
	allParticipentView = new AllParticipentView({ el : $("#listofparticipent")});
	feedbackview = new FeedBackView({ el : $("#feedback_wrapper")});
    
    instructionView.render();
    
    $.ajax({
        url:"employee/isadmin",
        type: "POST",
    error:function(res,ioArgs){
    	if(res.status == 403){
    		window.location.reload();
    	}
    },
        dataType : ""
    })
    .done(function(data, textStatus, jqXHR){
        try{
            if(data == 1){
                $("#admin_also").show();
            } else{
                $("#admin_also").hide();
            }
        }catch(e){}
    });
    
    Backbone.ajax({
        dataType: "json",
        url: "employee/minmaxset",
        data: "",
        type: "POST",
        success: function(data, textStatus, jqXHR){
             try{
                 for(var i = 0; i < data.length; i++){
                    if(data[i].id == "maxvalue"){
                        maxlimit = data[i].value;
                    } else if (data[i].id == "minvalue"){
                        minlimit = data[i].value;
                    }else if (data[i].id == "email"){
                        email = data[i].value;
                    }
                 }
             } catch(e){}
        },
        error:function(res,ioArgs){
        	if(res.status == 403){
        		window.location.reload();
        	}
        }
    });
});

$(function() {

    $('#side-menu').metisMenu();

});

//Loads the correct sidebar on window load,
//collapses the sidebar on window resize.
// Sets the min-height of #employee-wrapper to window size
$(function() {
    $(window).bind("load resize", function() {
        topOffset = 50;
        width = (this.window.innerWidth > 0) ? this.window.innerWidth : this.screen.width;
        if (width < 768) {
            $('div.navbar-collapse').addClass('collapse');
            topOffset = 100; // 2-row-menu
        } else {
            $('div.navbar-collapse').removeClass('collapse');
        }

        height = (this.window.innerHeight > 0) ? this.window.innerHeight : this.screen.height;
        height = height - topOffset;
        if (height < 1) height = 1;
        if (height > topOffset) {
            $("#employee-wrapper").css("min-height", (height) + "px");
        }
    });
});

/*function CustomAlert(){
	this.render = function(dialog){
		var winW = window.innerWidth;
	    var winH = window.innerHeight;
		var dialogoverlay = document.getElementById('dialogoverlay');
	    var dialogbox = document.getElementById('dialogbox');
		dialogoverlay.style.display = "block";
	    dialogoverlay.style.height = winH+"px";
		dialogbox.style.left = (winW/2) - (550 * .3)+"px";
	    dialogbox.style.top = "100px";
	    dialogbox.style.display = "block";
		//document.getElementById('dialogboxhead').innerHTML = "Acknowledge This Message";
	    document.getElementById('dialogboxbody').innerHTML = dialog;
		document.getElementById('dialogboxfoot').innerHTML = '<button class="btn btn-success" onclick="Alert.ok()" style="padding: 2px 30px">OK</button>';
	}
	this.ok = function(){
		document.getElementById('dialogbox').style.display = "none";
		document.getElementById('dialogoverlay').style.display = "none";
	}
}
var Alert = new CustomAlert();*/
