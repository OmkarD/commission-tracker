var dashboardView , participantView , addSessionView , createDriversView , manageAdminView , exceptionListView , excelUploadView , emailTemplateView, adminFeedBackView, maxlimit, minlimit, reportGeneratView;

$(document).ready(function(e){
    
    maxlimit = 0;
    minlimit = 0;
    
    dashboardView = new DashboardView({ el : $("#page-wrapper")});
    participantView = new ParticipantView({ el : $("#page-wrapper")});
    addSessionView = new AddSessionView({ el : $("#page-wrapper")});
    createDriversView = new CreateDriversView({ el : $("#page-wrapper")});
    manageAdminView = new ManageAdminView({ el : $("#page-wrapper")});
    exceptionListView = new ExceptionListView({ el : $("#page-wrapper")});
    excelUploadView = new ExcelUploadView({ el : $("#page-wrapper")});
    emailTemplateView = new EmailTemplateView({ el : $("#page-wrapper")});
    reportGeneratView = new ReportGeneratView({ el : $("#page-wrapper")});
    adminFeedBackView = new AdminFeedBackView({ el : $("#admin_feedback")});
    selectingEmployeeView = new SelectingEmployeeView({ el : $("#selecting_employee")});
    
    participantView.render();
    
    $.ajax({
        url:"isemployee",
        type: "POST",
    error:function(res,ioArgs){
    	if(res.status == 403){
    		window.location.reload();
    	}
    },
        dataType : ""
    })
    .done(function(data, textStatus, jqXHR){
        try{
            if(data == 1){
                $("#employee_also").show();
            } else{
                $("#employee_also").hide();
            }
        }catch(e){}
    });
    
    Backbone.ajax({
        dataType: "json",
        url: "minmaxset",
        data: "",
        type: "POST",
        success: function(data, textStatus, jqXHR){
             try{
                 for(var i = 0; i < data.length; i++){
                    if(data[i].id == "maxvalue"){
                        maxlimit = data[i].value;
                    } else if (data[i].id == "minvalue"){
                        minlimit = data[i].value;
                    }
                 }
             } catch(e){}
        },
        error:function(res,ioArgs){
        	if(res.status == 403){
        		window.location.reload();
        	}
        }
    });
});


    $( ".maind" ).on( "click", function(e) {
    var x = e.currentTarget.id;
    if(x=="dashboard"){
        dashboardView.render();
    } else if(x=="participants_id"){
        participantView.render();
    } else if(x=="addsession"){
        addSessionView.render();
    } else if(x=="drivercreation"){
        createDriversView.render();
    } else if(x=="listofexception"){
        exceptionListView.render();
    } else if(x=="usermanage"){
        manageAdminView.render();
    } else if(x=="uploadexcel"){
        excelUploadView.render();
    } else if(x=="emailtemplate"){
        emailTemplateView.render();
    } else if(x=="reports"){
        reportGeneratView.render();
    }
    
    
    $(e.currentTarget).parent().children(".maindashboard").removeClass("maindashboard");
    $(e.currentTarget).addClass("maindashboard");
});

/****************offcanvas js**********************/
$("#menu-toggle").click(function(e) {
        e.preventDefault();
		$(".sidebar-nav").toggleClass("toggled");
		$("#menu-toggle").toggleClass("toggled");
		$("#page-wrapper").toggleClass("toggled");
	    $(".content-hide").toggleClass("toggled");
	    $(".sidebar-nav li").toggleClass("toggled"); 
	   $(".navbar-default.sidebar").toggleClass("toggled"); 
    });
		
$(".sidebar-nav li").tooltip({
        placement : 'right'
});