var OptionModel = Backbone.Model.extend({
    defaults:{
        "1":"A = Rarely displays this behavior. Would display this behavior in less than 4 cases out of 10",
        "2":"B = Occasionally displays this behavior. Would display this behavior in 4-5 cases out of 10",
        "3":"C = Often displays this behavior. Would display this behavior in 6-8 cases out of 10",
        "4":"D = Almost always displays this behavior. Would display this behavior in 9 or more cases out of 10",
        "5":"N = No interaction on this parameter"
	}
});

var optionModel=new OptionModel();