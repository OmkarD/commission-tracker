

var appcontroller = Backbone.View.extend({

    
    events: {

   'click #listofcycle':"listofcycle",
   "click #commsionbtn":"listofcommision",
   "click #agentbtn":"listofagents",
   "click #golistofcycle":"listofcycle",

    },
        
    tagName: 'div',
    
    id: 'wrapper',
        
    initialize: function () {
        _.bindAll(this, 'render');
        this.render();
    },
        
    render: function () {
        this.$el.html(


            '<div class="header-area">'+
            '<div class="container">'+
            '<nav class="navbar">'+
                '<div class="navbar-header">'+
                  '<div class="brand-img">'+
                    '<img src="../layout/images/logo.jpg" alt="" class="brand-imglogo">'+
                  '</div>'+
                '</div>'+
                '<div class="navbar-header" id="swichuser">'+
                  '<div class="switchbtn">'+
                    'Switch to Admin'+
                  '</div>'+
                '</div>'+
                '<div class="projName">'+
                'COMMISSION TRACKER'+
                '</div>'+
                    '<ul class="nav navbar-top-links navbar-right">'+
                    '<li  class="dropdown">'+
                        '<a class="dropdown-toggle" data-toggle="dropdown" href="#">'+
                            '<div class="userlogoutbtn"><i class="fa fa-user fa-fw"></i><span id="useridname"> </span><i class="fa fa-caret-down"></i></div>'+
                        '</a>'+
                        '<ul class="dropdown-menu dropdown-user">'+
                            '<li id="admin_also" style="display: none"><a href="#"><i class="fa fa-user fa-fw"></i> Admin </a>'+
                            '</li>'+
                            '<li class="divider"></li>'+
                            '<li><a href="../logout"><i class="fa fa-sign-out fa-fw"></i> Logout</a>'+
                            '</li>'+
                        '</ul>'+
                    '</li>'+
                    '</ul>'+

            '</nav>'+
            '</div>'+
        '</div>'+
          '<div class="topbar">'+
            '<div class="container">'+

            /*................btn................*/
                '<div id="golistofcycle" style="margin-right: 10px;" class="btn btn-primary btn-sm mainopt" data-toggle="toggle" data-target="#golistofcycle">Home'+
                '</div>'+
            /*................btn................*/
            /*................btn................*/
                '<div id="agentbtn" class="btn btn-primary btn-sm mainopt" data-toggle="toggle" data-target="#agents">Agent'+
                '</div>'+
            /*................btn................*/

                '<!-- Responsive Arrow Progress Bar -->'+
                '<div class="arrow-steps clearfix" id = "agents">'+
                  '<div class="step step1 current" id="step1"> <span> <a  >List Of Advisors</a></span> </div>'+
                  '<div class="step step2 " id="step2"> <span><a >Agent Level Validation</a></span> </div>'+
                  /*'<div class="step step3 " id="step3"> <span><a href="#" >Upload Agent Tata</a></span> </div>'+*/
                '</div>'+
                '<!-- end Responsive Arrow Progress Bar -->'+
                /*................btn................*/
                '<div id="commsionbtn" style="margin-left: 10px;" class="btn btn-primary btn-sm mainopt" data-toggle="toggle" data-target="#commision">Commision'+
                '</div>'+
            /*................btn................*/

                '<!-- Responsive Arrow Progress Bar -->'+
                '<div class="arrow-steps clearfix" id = "commision">'+
                  '<div class="step step1 current" id="step1"> <span> <a  >List Of Commision</a></span> </div>'+
                  '<div class="step step2 " id="step2"> <span><a  >Commision Level Validation</a></span> </div>'+
                  /*'<div class="step step3 " id="step3"> <span><a href="#" >Upload Agent Tata</a></span> </div>'+*/
                '</div>'+
                '<!-- end Responsive Arrow Progress Bar -->'+
            '</div>'+
            
          '</div>'+


        '<!-- Page Content -->'+
        '<div id="employee-wrapper"></div>'+
        '<!-- /#employee-wrapper -->'+
        '<div id="feedback_wrapper"></div>'+
        '<div id="listofparticipent"></div>'+
        '<div id="selffb_wrapper"></div>'
           
        );
        this.common();
       
      

        

  },


listofcommision:function(){

landingroutes.navigate('listofcommission',{trigger: true});

},     
listofagents:function(){

landingroutes.navigate('listofagents',{trigger: true});

},         


common:function(selector){
    this.userroll();
/*  $("[data-toggle='toggle']").click(function() {
        var selector = $(this).data("target");
        $('.clearfix').removeClass('in')
        $(selector).toggleClass('in');
        });*/

        $('.clearfix').removeClass('in')
        $(selector).toggleClass('in');
},



 userroll: function () {
          Backbone.ajax({
            url: "userroles",
            data: "",
            type: "GET",
            beforeSend: function(xhr) {
                /*xhr.setRequestHeader(header, token);*/
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-Type", "application/json");
            },
            success: function (data, textStatus, jqXHR) {
                
                if(data == 2) {
                
                    $("#swichuser").show();

                }
            },
            error: function (res, ioArgs) {
            },
            complete: function() {
            }
        });
      },




topbarhide:function(){
  $(".topbar").hide();
},  

topbarshow:function(){
  $(".topbar").show();
}, 

stepschange:function(level){
    if(level==1){
    $(".step1").addClass('current');
    $(".step2").removeClass('current');
    $(".step3").removeClass('current');
    }
    if(level==2){
    $(".step2").addClass('current');
    $(".step1").removeClass('current').addClass('done');
    $(".step3").removeClass('current');
    }
    if(level==3){
    $(".step3").addClass('current');
    $(".step1").removeClass('current').addClass('done');
    $(".step2").removeClass('current').addClass('done');
    }
},

listofcycle:function(){

   landingroutes.navigate('',{trigger: true});
}

});