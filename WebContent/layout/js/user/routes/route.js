
  var  approute = Backbone.Router.extend({
      routes: {
    	  '':'index',
         'app/demo/:id/:modulename':'admin',
         'listofagents':'totalagentlist',
         'validagents': 'validlistagent',
         'listofcommission': 'totalcommisionlist',
     
      },

      initialize: function() {

  },
  
      
     index:function(){

        listofcycle= new app_cycle.Views.allcycle();
        $("#employee-wrapper").empty().append(listofcycle.el);
        listofcycle.render();
  

     },
    
     admin:function(id){

      if(id === "jintu")
      {
        apptest = new app_test.Views.header();
        $("#employee-wrapper").empty().append(apptest.el);
        apptest.render(id);
      }
     },

    totalagentlist:function(ids){

        allagentlist = new app_listofagetns.Views.header();
        $("#employee-wrapper").empty().append(allagentlist.el);
        allagentlist.render();
     },

     validlistagent:function(){

        listofagent = new app_validatedagentlist.Views.header();
        $("#employee-wrapper").empty().append(listofagent.el);
        listofagent.render();
     },

    totalcommisionlist:function(){

        listofcommision = new app_listofcommision.Views.header();
        $("#employee-wrapper").empty().append(listofcommision.el);
        listofcommision.render();
     },


      default: function (other) {
          alert("Doesn't know this url " + other + "");
      },


     
  
  });
