var tableelement,colelement, aggreCount, pendingShowFlag;

var AggregateView = Backbone.View.extend({

    events:{
        "click .pendingAction":"clickPendingAction",
        "click .selcting_action":"clickSelctingAction",
        "click .aggregateSent":"clickSent",
        "click .aggregateRecived":"clickRecived",
        "click .remark_action": "clickRemark"
    },
 
	initialize:function(){
        _.bindAll(this,'render');
        i=1;
        pendingShowFlag = false;
	},

	render:function(){
      this.$el.empty().append(
        '<div class="row">'+
            '<div class="col-lg-12">'+
                '<h1 class="page-header">Survey Summary</h1>'+
            '</div>'+
        '</div>'+  
        '<div class="col-lg-12">'+
            '<ul class="nav nav-tabs">'+
                '<li class="aggregateRecived active" data-toggle="tab">'+
                    '<div id="recivied" data-toggle="tab">Requests Received'+
                        '<span></span>'+
                    '</div>'+
                '</li> '+
                '<li class="aggregateSent" style="display:none" data-toggle="tab">'+
                    '<div id="sent">Requests Sent'+
                        '<span></span>'+
                    '</div>'+
                '</li>'+
            '</ul>'+
            '<div id="aggreTables" class="tab-content">'+
            '</div>'+
            '<div id="notehere"></div>' +
        '</div>' +
        '<div class="modal fade" id="remark_modal" role="dialog" aria-labelledby="remark_modal" aria-hidden="true" data-backdrop="static" style="margin-top:5%">'+
            '<div class="modal-dialog instrution-popup">'+
                '<div class="modal-content" style="border:10px solid #ccc">'+
                    '<div class="modal-header">'+
                        '<div class="close" data-dismiss="modal" ></div>'+
                        '<h4 class="remark_heading"></h4>'+
                    '</div>'+
                    '<div class="modal-body" style="text-align: justify;">'+
                        '<div class="row">'+
                            '<div class="table-responsive">'+
                                '<table id="remarkTable" class="table table-hover">'+
                                    '<thead>'+
                                        '<tr>'+
                                            '<th>Name</th>'+
                                            '<th>Employee Id</th>'+
                                            /*'<th>Position</th>'+*/
                                            '<th>Status</th>'+
                                            '<th class="rmcls">Remarks</th>'+
                                        '</tr>'+
                                    '</thead>'+
                                    '<tbody>'+
                                    '</tbody>'+
                                '</table>'+
                            '</div>'+
                        '</div>'+
                    '</div>'+
                    '<div class="modal-footer">'+
                        '<div class="btn btn-success remark_ok"  align="center" data-dismiss="modal">Ok</div>'+
                    '</div>'+
                '</div>'+
            '</div>'+
        '</div>'
        );
        this.first();
 	},
    
    first:function(){
        if(isparticipantFlag){
            $(".aggregateSent").show();
        } else {
            $(".aggregateSent").remove();
        }
        Backbone.$("#aggreTables").empty().append(
            '<div class="tab-pane fade in active">'+
                '<p>'+ 
                    '<div class="panel-body">'+
                        '<div class="table-responsive">'+
                            '<table id="aggregationTable" class="table table-striped table-bordered table-hover">'+
                                '<thead>'+
                                '</thead>'+
                                '<tbody>'+
                                '</tbody>'+
                            '</table>'+
                        '</div>'+
                    '</div>'+
                '</p>'+  
            '</div>');
        aggregateview.clickRecived();
    },
    
     clickRecived:function(){
         $("#notehere").empty().append(
            '<p><b>Note:</b> Please click on the hyperlinked numbers in column \'Number of Requests Received\' to view list of requests</p>'
        );
         $("#aggregationTable").children("thead").empty().append(
            '<tr>'+
                '<th>Request received from</th>'+
                '<th>Number of requests received</th>'+
                '<th>Accepted</th>'+
                '<th>Denied</th>'+
                '<th>Pending</th>'+
                '<th>Completed</th>'+
            '</tr>');
         $("#pageloading").show();
        Backbone.ajax({
            dataType:"json",
            url:"employee/aggregatesummary",
            data:"",
            type: "POST",
            success:function(data, textStatus, jqXHR){
                tableelement = $("#aggregationTable").children("tbody").empty();
                try{
                    tableelement.append(
    '<tr>'+
        '<td>Direct Manager</td>'+
        '<td>'+
            '<a id="pendingManager" class="pendingAction">'+data.reciveRequest.manager.total+''+
                '<i class="fa fa-hand-o-up"></i>'+
            '</a>'+
        '</td>'+
        '<td>'+data.reciveRequest.manager.accept+'</td>'+
        '<td>'+data.reciveRequest.manager.deny+'</td>'+
        '<td>'+data.reciveRequest.manager.pending+'</td>'+
        '<td>'+data.reciveRequest.manager.close+'</td>'+
    '</tr>'+                    
    '<tr>'+
        '<td>Direct Reportee</td>'+
        '<td>'+
            '<a id="pendingReportee" class="pendingAction">'+data.reciveRequest.directReport.total+''+
                '<i class="fa fa-hand-o-up"></i>'+
            '</a>'+
        '</td>'+
        '<td>'+data.reciveRequest.directReport.accept+'</td>'+
        '<td>'+data.reciveRequest.directReport.deny+'</td>'+
        '<td>'+data.reciveRequest.directReport.pending+'</td>'+
        '<td>'+data.reciveRequest.directReport.close+'</td>'+
    '</tr>'+                 
    '<tr>'+
        '<td>Peer</td>'+
        '<td>'+
            '<a id="pendingPeer" class="pendingAction">'+data.reciveRequest.peer.total+''+
                '<i class="fa fa-hand-o-up"></i>'+
            '</a>'+
        '</td>'+
        '<td>'+data.reciveRequest.peer.accept+'</td>'+
        '<td>'+data.reciveRequest.peer.deny+'</td>'+
        '<td>'+data.reciveRequest.peer.pending+'</td>'+
        '<td>'+data.reciveRequest.peer.close+'</td>'+
    '</tr>'+
    '<tr>'+
        '<td>Stakeholders</td>'+
        '<td>'+
            '<a id="pendingStakeholder" class="pendingAction">'+data.reciveRequest.stakeHolder.total+''+
                '<i class="fa fa-hand-o-up"></i>'+
            '</a>'+
        '</td>'+
        '<td>'+data.reciveRequest.stakeHolder.accept+'</td>'+
        '<td>'+data.reciveRequest.stakeHolder.deny+'</td>'+
        '<td>'+data.reciveRequest.stakeHolder.pending+'</td>'+
        '<td>'+data.reciveRequest.stakeHolder.close+'</td>'+
    '</tr>');
                }catch(e){}
                $("#pageloading").hide();
            },
            error:function(res,ioArgs){
            	if(res.status == 403){
            		window.location.reload();
            	}
            }
        });
         $("#pageloading").show();
         Backbone.ajax({
            dataType:"json",
            url:"employee/aggregatesummarycount",
            data:"",
            type: "POST",
            success:function(data, textStatus, jqXHR){
                var aggreCountRecived = $(".aggregateRecived").children("div").children("span");
                var aggreCountSent = $(".aggregateSent").children("div").children("span");
                try{
                    aggreCountRecived.empty().append("<span class='badge' >"+data.recivedCount+"</span>");
                    aggreCountSent.empty().append("<span class='badge'>"+data.sentCount+"</span>");
                }catch(e){}
                $("#pageloading").hide();
            },
            error:function(res,ioArgs){
            	if(res.status == 403){
            		window.location.reload();
            	}
            }
         });
    },
    
    clickRemark: function(e) {
        var r = e.currentTarget.id;
        $("#pageloading").show();
        if(r == "remarkPE"){
            pendingShowFlag = false;
            $(".remark_heading").text("Remarks");
            $("#remark_modal").modal('show');
            Backbone.ajax({
                dataType:"json",
                url:"employee/selectpeer",
                data:"",
                type: "POST",
                success:function(data, textStatus, jqXHR){
                    remarktableelement = Backbone.$("#remarkTable").children("tbody").empty();
                    aggregateview.generateRemarkTable(data);
                },
                error:function(res,ioArgs){
                    if(res.status == 403){
                        window.location.reload();
                    }
                }
            });
        } else if(r == "remarkSH"){
            pendingShowFlag = false;
            $(".remark_heading").text("Remarks");
            $("#remark_modal").modal('show');
            Backbone.ajax({
                dataType:"json",
                url:"employee/selectstakeholder",
                data:"",
                type: "POST",
                success:function(data, textStatus, jqXHR){
                    remarktableelement = Backbone.$("#remarkTable").children("tbody").empty();
                    aggregateview.generateRemarkTable(data);
                },
                error:function(res,ioArgs){
                    if(res.status == 403){
                        window.location.reload();
                    }
                }
            });
        } else if(r == "pendingDM"){
            pendingShowFlag = true;
            $(".remark_heading").text("Direct Manager");
            $("#remark_modal").modal('show');
            Backbone.ajax({
                dataType:"json",
                url:"employee/selectdirman",
                data:"",
                type: "POST",
                success:function(data, textStatus, jqXHR){
                    remarktableelement = Backbone.$("#remarkTable").children("tbody").empty();
                    aggregateview.generateRemarkTable(data);
                },
                error:function(res,ioArgs){
                    if(res.status == 403){
                        window.location.reload();
                    }
                }
            });
        } else if(r == "pendingDR"){
            pendingShowFlag = true;
            $(".remark_heading").text("Direct Reportee");
            $("#remark_modal").modal('show');
            Backbone.ajax({
                dataType:"json",
                url:"employee/selectdirrep",
                data:"",
                type: "POST",
                success:function(data, textStatus, jqXHR){
                    remarktableelement = Backbone.$("#remarkTable").children("tbody").empty();
                    aggregateview.generateRemarkTable(data);
                },
                error:function(res,ioArgs){
                    if(res.status == 403){
                        window.location.reload();
                    }
                }
            });
        } else if(r == "pendingPE"){
            pendingShowFlag = true;
            $(".remark_heading").text("Peers");
            $("#remark_modal").modal('show');
            Backbone.ajax({
                dataType:"json",
                url:"employee/selectpeer",
                data:"",
                type: "POST",
                success:function(data, textStatus, jqXHR){
                    remarktableelement = Backbone.$("#remarkTable").children("tbody").empty();
                    aggregateview.generateRemarkTable(data);
                },
                error:function(res,ioArgs){
                    if(res.status == 403){
                        window.location.reload();
                    }
                }
            });
        } else if(r == "pendingSH"){
            pendingShowFlag = true;
            $(".remark_heading").text("Stakeholders");
            $("#remark_modal").modal('show');
            Backbone.ajax({
                dataType:"json",
                url:"employee/selectstakeholder",
                data:"",
                type: "POST",
                success:function(data, textStatus, jqXHR){
                    remarktableelement = Backbone.$("#remarkTable").children("tbody").empty();
                    aggregateview.generateRemarkTable(data);
                },
                error:function(res,ioArgs){
                    if(res.status == 403){
                        window.location.reload();
                    }
                }
            });
        }
    },
    
    clickSent:function(){
        $("#pageloading").show();
        $("#notehere").empty().append(
            '<p><b>Note:</b> Users are not allowed to change or reconfigure Direct Managers/Reportees</p>'
        );
        $("#aggregationTable").children("thead").empty().append(
            '<tr> '+
                '<th>Request Sent To</th>'+
                '<th>Number of requests sent</th>'+
                '<th>Accepted</th>'+
                '<th>Denied</th>'+
                '<th>Pending</th>'+
                '<th>Completed</th>'+
            '</tr>');
        Backbone.ajax({
            dataType:"json",
            url:"employee/aggregatesummary",
            data:"",
            type: "POST",
            success:function(data, textStatus, jqXHR){
                colelement = $("#aggregationTable").children("tbody").empty();
                try{
                    tableelement.append(
    '<tr>'+
        '<td>Direct Manager</td>'+
        '<td>'+
            '<a id="selectingDM" class="selcting_action">'+data.sendRequest.manager.total+''+
                '<i class="fa fa-hand-o-up"></i>'+
            '</a>'+
        '</td>'+
        '<td>'+data.sendRequest.manager.accept+'</td>'+
        '<td>'+data.sendRequest.manager.deny+'</td>'+
        '<td>'+
            '<a id="pendingDM" class="remark_action">'+data.sendRequest.manager.pending+''+
                '<i class="fa fa-hand-o-up"></i>'+            
        '</td>'+
        '<td>'+data.sendRequest.manager.close+'</td>'+
    '</tr>'+                    
    '<tr>'+
        '<td>Direct Reportee</td>'+
        '<td>'+
            '<a id="selectingDR" class="selcting_action">'+data.sendRequest.directReport.total+''+
                '<i class="fa fa-hand-o-up"></i>'+
            '</a>'+
        '</td>'+
        '<td>'+data.sendRequest.directReport.accept+'</td>'+
        '<td>'+data.sendRequest.directReport.deny+'</td>'+
        '<td>'+
            '<a id="pendingDR" class="remark_action">'+data.sendRequest.directReport.pending+''+
                '<i class="fa fa-hand-o-up"></i>'+            
        '</td>'+
        '<td>'+data.sendRequest.directReport.close+'</td>'+
    '</tr>'+                    
    '<tr>'+
        '<td>Peer</td>'+
        '<td>'+
            '<a id="selectingPeer" class="selcting_action">'+data.sendRequest.peer.total+''+
                '<i class="fa fa-hand-o-up"></i>'+
            '</a>'+
        '</td>'+
        '<td>'+data.sendRequest.peer.accept+'</td>'+
        '<td>'+
            '<a id="remarkPE" class="remark_action">'+data.sendRequest.peer.deny+''+
                '<i class="fa fa-hand-o-up"></i>'+            
        '</td>'+
        '<td>'+
            '<a id="pendingPE" class="remark_action">'+data.sendRequest.peer.pending+''+
                '<i class="fa fa-hand-o-up"></i>'+            
        '</td>'+
        '<td>'+data.sendRequest.peer.close+'</td>'+
    '</tr>'+                    
    '<tr>'+
        '<td>Stakeholder</td>'+
        '<td>'+
            '<a id="selctingStakeholder" class="selcting_action">'+data.sendRequest.stakeHolder.total+''+
                '<i class="fa fa-hand-o-up"></i>'+
            '</a>'+
        '</td>'+
        '<td>'+data.sendRequest.stakeHolder.accept+'</td>'+
        '<td>'+
            '<a id="remarkSH" class="remark_action">'+data.sendRequest.stakeHolder.deny+''+
                '<i class="fa fa-hand-o-up"></i>'+            
        '</td>'+
        '<td>'+
            '<a id="pendingSH" class="remark_action">'+data.sendRequest.stakeHolder.pending+''+
                '<i class="fa fa-hand-o-up"></i>'+            
        '</td>'+
        '<td>'+data.sendRequest.stakeHolder.close+'</td>'+
    '</tr>');
                    if(gradeLTFlag){
                     $("#selctingStakeholder").parent().parent().hide();   
                    }
                }catch(e){}
                $("#pageloading").hide();
            },
            error:function(res,ioArgs){
            	if(res.status == 403){
            		window.location.reload();
            	}
            }
        });
        $("#pageloading").show();
        Backbone.ajax({
            dataType:"json",
            url:"employee/aggregatesummarycount",
            data:"",
            type: "POST",
            success:function(data, textStatus, jqXHR){
                var aggreCountRecived = $(".aggregateRecived").children("div").children("span");
                var aggreCountSent = $(".aggregateSent").children("div").children("span");
                try{
                    aggreCountRecived.empty().append("<span class='badge' >"+data.recivedCount+"</span>");
                    aggreCountSent.empty().append("<span class='badge'>"+data.sentCount+"</span>");
                }catch(e){}
                $("#pageloading").hide();
            },
            error:function(res,ioArgs){
            	if(res.status == 403){
            		window.location.reload();
            	}
            }
         });
            
    },
    
    clickSelctingAction:function(e){
        listview.render();
        surveySummaryFlag = true;
        aggregateview.instructionHide();
        $("#pageloading").show();
        ltFirstFlag = false;
        var selactn = e.currentTarget.id;
        if(selactn=="selectingDM"){
            $("#pageloading").hide();
            listview.selectDM();
        } else if(selactn=="selectingDR"){
            $("#pageloading").hide();
            listview.selectDR();
        } else if(selactn=="selectingPeer"){
            $("#pageloading").hide();
            listview.selectPr();
        } else if(selactn=="selctingStakeholder"){
            $("#pageloading").hide();
            if(gradeLTFlag){
                if(ltFirstFlag){
                    $("#cnt_SS").removeClass("disabal");
                    listview.submitSelection();
                } else {
                    aggregateview.render();
                }   
            } else{
                $("#pageloading").hide();
                stackholdersexpFlag = true;
                listview.selectSH();
            }
        }
    },
    
    instructionHide:function(){
        $(".drcls").hide();
        $(".dmcls").hide();
        $(".prcls").hide();
        $(".shcls").hide();
    },
    
    generateRemarkTable: function(data){
        for(var i=0; i< data.length; i++){
            remarktableelement.append(aggregateview.createPeerShRow(data[i]));
        }
        $("#pageloading").hide();
    },
    
    createPeerShRow:function(rowObject){
        
        if(pendingShowFlag){
            $(".rmcls").hide();
            if(rowObject.status == 0){
                try{
                    /*var trElement = "<tr id="+rowObject.pid+"><td>"+rowObject.participantName+"</td><td>"+rowObject.pid+"</td><td>"+rowObject.designation+"</td><td>Pending</td>";*/
                    var trElement = "<tr id="+rowObject.pid+"><td>"+rowObject.participantName+"</td><td>"+rowObject.pid+"</td><td>Pending</td>";
                    return trElement+"</tr>";
                }catch(e){}
            } else{
                return null;
            }
        } else {
            $(".rmcls").show();
            if(rowObject.status == 2){
                try{
                   /* var trElement = "<tr id="+rowObject.pid+"><td>"+rowObject.participantName+"</td><td>"+rowObject.pid+"</td><td>"+rowObject.designation+"</td><td>Denied</td><td>"+rowObject.remark+"</td>";*/
                	 var trElement = "<tr id="+rowObject.pid+"><td>"+rowObject.participantName+"</td><td>"+rowObject.pid+"</td><td>Denied</td><td>"+rowObject.remark+"</td>";
                    return trElement+"</tr>";
                }catch(e){}
            } else{
                return null;
            }
        }
    },
    
    clickPendingAction:function(e){
        actionableView.render();
        $("#pageloading").show();
        var penactn = e.currentTarget.id;
        if(penactn=="pendingManager"){
            directManagerReporteeFlag = true;
            $("#action_header").text("Direct Manager");
            actionableView.pendingAcionableTabDM(e);
        } else if(penactn=="pendingPeer"){
            directManagerReporteeFlag = false;
            $("#action_header").text("Peers");
            actionableView.pendingAcionableTabPeer(e);
        } else if(penactn=="pendingReportee"){
            directManagerReporteeFlag = true;
            $("#action_header").text("Direct Reportee(s)");
            actionableView.pendingAcionableTabDR(e);
        } else if(penactn=="pendingStakeholder"){
            directManagerReporteeFlag = false;
            $("#action_header").text("Stakeholders");
            actionableView.pendingAcionableTabSH(e);
        }
    }
    
});

