(function () {
    
    window.app_validatedagentlist = {
        Models: {},
        Views: {},
        Routes: {},
        Collection: {}
    };

   app_validatedagentlist.Views.header=Backbone.View.extend({

    events:{
      "click #validAgents":"getValidatedgentlist",
      "click #invalidAgents":"getInValidatedgentlist",
      

    },
    
    initialize:function(){
    _.bindAll(this,'render');
/*this.render();*/
    },

    render:function(e){

        this.$el.empty().append(
        '<div class="body-area">'+
            '<div class="container body-content">'+
                '<div class="col-xs-12 col-md-12">'+
                    '<div class="col-xs-6 col-md-6 nopadding">'+
                        '<div class="tableavailheader currentarrow" id="validAgents">Valid Agents</div>'+
                    '</div>'+
                    '<div class="col-xs-6 col-md-6 nopadding">'+
                        '<div class="tableavailheader"  id="invalidAgents">Invalid Agents</div>'+
                    '</div>'+
                    '</div>'+
                    '<div class="col-xs-12 col-md-12">'+
                        '<div class="agentTable">'+


                        /*....................................*/
                            '<div class="datatableBox">' +


                                        '<a href="excelforvalidagentlist" id="excelforvalidagentlist" target="_blank"><div class="alladentdownloaddata">Click here to download' +
                                        '</div> </a>' +
                                        '<a href="excelforinvalidagentlist" id="excelforinvalidagentlist" target="_blank"><div class="alladentdownloaddata">Click here to download' +
                                        '</div> </a>' +

                                        
                                '<div class="col-xs-12">' +
                                                '<div class="table-responsive" id="advisorTable">' +
                                                '</div>' +
                                '</div>' +
                            '</div>' +
                        /*....................................*/




                        '</div>'+
                    '</div>'+
            '</div>'+
        '</div>'
      );
        this.finished();




    },



    finished:function(){
    	appcontroller.topbarshow();
        appcontroller.stepschange(2);
        listofagent.getValidatedgentlist();
        var selector = "#agents";
        appcontroller.common(selector);
    },



    getValidatedgentlist:function(){
        $('#validAgents').addClass('currentarrow');
        $('#invalidAgents').removeClass('currentarrow');
        $('#excelforinvalidagentlist').hide();
        $('#excelforvalidagentlist').show();
        var url = "validagentslist";
        listofagent.allValidadvisorsListrequst(url);
    },

    getInValidatedgentlist:function(){
        $('#invalidAgents').addClass('currentarrow');
        $('#validAgents').removeClass('currentarrow');
        $('#excelforvalidagentlist').hide();
        $('#excelforinvalidagentlist').show();
        var url = "invalidagentslist";
        listofagent.allInValidadvisorsListrequst(url);
    },

            allValidadvisorsListrequst: function(urls) {
            $("#pageloading").show();
            Backbone.$("#advisorTable").empty().append(
                    '<table id="allValidAgents" class="table table-hover" >' +
                        '<thead>' +
                            '<tr>' +
                            '<td><b>AGNTNUM</b></td>' +
                            '<td><b>AGENTNAME</b></td>' +
                            '<td><b>SALUTL</b></td>' +
                            '<td><b>DTEAPP</b></td>' +
                            '<td><b>CLTSEX</b></td>' +
                            '<td><b>AGENTADDRESS</b></td>' +
                            '<td><b>ZBROKAGT</b></td>' +
                            '<td><b>ZDATELIC</b></td>' +
                            '<td><b>CLTDOB</b></td>' +
                            '<td><b>AGENT_BRANCH_CODE</b></td>' +
                            '<td><b>AGENT_UNIT_CODE</b></td>' +
                            '<td><b>AGTYPE</b></td>' +
                            '<td><b>ARACDE</b></td>' +
                            '<td><b>AGENT_CLASS</b></td>' +
                            '<td><b>DTETRM</b></td>' +
                            '<td><b>PAYCLT</b></td>' +
                            '<td><b>CLNTNUM</b></td>' +
                            '<td><b>PHONENO1</b></td>' +
                            '<td><b>PHONENO2</b></td>' +
                            '<td><b>REPORTAG</b></td>' +
                            '<td><b>NBUSALLW</b></td>' +
                            '<td><b>MINSTA</b></td>' +
                            '<td><b>REASONCD</b></td>' +
                            '<td><b>EFFDATE</b></td>' +
                            '<td><b>PROPTION</b></td>' +
                            '<td><b>MOBILEPHONE</b></td>' +
                            '<td><b>EMPLOYEECODE</b></td>' +
                            '<td><b>MAILID</b></td>' +
                            '<td><b>PANNo</b></td>' +
                            '<td><b>CHANNEL</b></td>' +
                            '<td><b>PAYMETHOD</b></td>' +
                                /*'<td><b>Add Students</b><input type="checkbox"  class="useAll-check"><button id="addStudentCheck" type="button" class=" btn btn-success btn-circle"> <i class="fa fa-plus" aria-hidden="true"/> </button></td>' +*/
                                /*'<td></td>' +
                                '<td></td>' +*/
                               '</tr>' +
                        '</thead>' +
                        '<tbody>' +
                        '</tbody>' +
                    '</table>'
                );
            
            
            
  
            var agentTable;
            var cls = "test";
            agentTable = jQuery("#allValidAgents").dataTable({
            "scrollY":"290px",
            "scrollX":true,
            "scrollCollapse":true,
            "paging":true,
            "iDisplayLength": 10,
            "bProcessing" : true,
            "bServerSide" : true,
            "bSort" : false,
            "sAjaxSource" :urls,
            "sServerMethod": "GET",
            "fixedColumns":   {
            "leftColumns": 2,
            },
      
             
            "aoColumns" : [
            	{ "mData": "agentNum" },
				   { "mData": "agentName" },
				   { "mData": "salutl" },
				   { "mData": "dateApp" },
				   { "mData": "clientSex" },
					{ "mData": "agentAddr" },
					{ "mData": "zBrokAgnt" },
					{ "mData": "zDateLic" },
					{ "mData": "clientDob" },
					{ "mData": "agentBranchCode" },
					{ "mData": "agentUnitCode" },
					{ "mData": "agentType" },
					{ "mData": "aracde" },
					{ "mData": "agentClass" },
					{ "mData": "dateTrm" },
					{ "mData": "payClient" },
					{ "mData": "clientNumber" },
					{ "mData": "phoneNum1" },
					{ "mData": "phoneNum2" },
					{ "mData": "reporting" },
					{ "mData": "nbusallw" },
					{ "mData": "minsta" },
					{ "mData": "reasoncd" },
					{ "mData": "effectiveDate" },
					{ "mData": "proption" },
					{ "mData": "mobilePhone" },
					{ "mData": "employeeCode" },
					{ "mData": "mailId" },
					{ "mData": "panNum" },
					{ "mData": "channel" },
					{ "mData": "paymentMethod" }


                          
            ],

            "initComplete": function(settings, json) {
                  $("#pageloading").hide();
            },
                
            rowCallback: function ( row, data ) {
                $('user-check', row).prop( 'checked', data.addStudentCheck == 1 );
            },
            
            'fnCreatedRow': function (nRow, data, iDataIndex) {
                    $(nRow).attr('id', 'module' + data.uniId); // or whatever you choose to set as the id
                   
            },
             
        });
    },

               allInValidadvisorsListrequst: function(urls) {
            $("#pageloading").show();
            Backbone.$("#advisorTable").empty().append(
                    '<table id="allInValidAgents" class="table table-hover" >' +
                        '<thead>' +
                            '<tr>' +
                            '<td><b>AGNTNUM</b></td>' +
                            '<td><b>AGENTNAME</b></td>' +
                            '<td><b>SALUTL</b></td>' +
                            '<td><b>DTEAPP</b></td>' +
                            '<td><b>CLTSEX</b></td>' +
                            '<td><b>AGENTADDRESS</b></td>' +
                            '<td><b>ZBROKAGT</b></td>' +
                            '<td><b>ZDATELIC</b></td>' +
                            '<td><b>CLTDOB</b></td>' +
                            '<td><b>AGENT_BRANCH_CODE</b></td>' +
                            '<td><b>AGENT_UNIT_CODE</b></td>' +
                            '<td><b>AGTYPE</b></td>' +
                            '<td><b>ARACDE</b></td>' +
                            '<td><b>AGENT_CLASS</b></td>' +
                            '<td><b>DTETRM</b></td>' +
                            '<td><b>PAYCLT</b></td>' +
                            '<td><b>CLNTNUM</b></td>' +
                            '<td><b>PHONENO1</b></td>' +
                            '<td><b>PHONENO2</b></td>' +
                            '<td><b>REPORTAG</b></td>' +
                            '<td><b>NBUSALLW</b></td>' +
                            '<td><b>MINSTA</b></td>' +
                            '<td><b>REASONCD</b></td>' +
                            '<td><b>EFFDATE</b></td>' +
                            '<td><b>PROPTION</b></td>' +
                            '<td><b>MOBILEPHONE</b></td>' +
                            '<td><b>EMPLOYEECODE</b></td>' +
                            '<td><b>MAILID</b></td>' +
                            '<td><b>PANNo</b></td>' +
                            '<td><b>CHANNEL</b></td>' +
                            '<td><b>PAYMETHOD</b></td>' +
                            '<td><b>REMARKS</b></td>' +
                                /*'<td><b>Add Students</b><input type="checkbox"  class="useAll-check"><button id="addStudentCheck" type="button" class=" btn btn-success btn-circle"> <i class="fa fa-plus" aria-hidden="true"/> </button></td>' +*/
                                /*'<td></td>' +
                                '<td></td>' +*/
                               '</tr>' +
                        '</thead>' +
                        '<tbody>' +
                        '</tbody>' +
                    '</table>'
                );
            
            
            
  
            var agentTable;
            var cls = "test";
            agentTable = jQuery("#allInValidAgents").dataTable({
            "scrollY":"290px",
            "scrollX":true,
            "scrollCollapse":true,
            "paging":true,
            "iDisplayLength": 10,
            "bProcessing" : true,
            "bServerSide" : true,
            "bSort" : false,
            "sAjaxSource" :urls,
            "sServerMethod": "GET",
            "fixedColumns":   {
            "leftColumns": 2,
            "rightColumns": 1,
            },
      
             
            "aoColumns" : [
            	{ "mData": "agentNum" },
				   { "mData": "agentName" },
				   { "mData": "salutl" },
				   { "mData": "dateApp" },
				   { "mData": "clientSex" },
					{ "mData": "agentAddr" },
					{ "mData": "zBrokAgnt" },
					{ "mData": "zDateLic" },
					{ "mData": "clientDob" },
					{ "mData": "agentBranchCode" },
					{ "mData": "agentUnitCode" },
					{ "mData": "agentType" },
					{ "mData": "aracde" },
					{ "mData": "agentClass" },
					{ "mData": "dateTrm" },
					{ "mData": "payClient" },
					{ "mData": "clientNumber" },
					{ "mData": "phoneNum1" },
					{ "mData": "phoneNum2" },
					{ "mData": "reporting" },
					{ "mData": "nbusallw" },
					{ "mData": "minsta" },
					{ "mData": "reasoncd" },
					{ "mData": "effectiveDate" },
					{ "mData": "proption" },
					{ "mData": "mobilePhone" },
					{ "mData": "employeeCode" },
					{ "mData": "mailId" },
					{ "mData": "panNum" },
					{ "mData": "channel" },
					{ "mData": "paymentMethod" },
					{ "mData": "remarks" }
					


                          
            ],

            "initComplete": function(settings, json) {
                  $("#pageloading").hide();
            },
                
            rowCallback: function ( row, data ) {
                $('user-check', row).prop( 'checked', data.addStudentCheck == 1 );
            },
            
            'fnCreatedRow': function (nRow, data, iDataIndex) {
                    $(nRow).attr('id', 'module' + data.uniId); // or whatever you choose to set as the id
                   
            },
             
        });
    },

  
});
    

    
})();