var liElement, queElement, ansElement, rid,num,flag;

var FeedBackView = Backbone.View.extend({
  
    events:
    {
        "click .answer":"ansClick",
		"click .fbsubmit":"submitAns",
		"click .fbprevious":"previousAns",
		"click #selfansId1":"addColor1",
		"click #selfansId2":"addColor2",
		"click #selfansId3":"addColor3",
		"click #selfansId4":"addColor4",
		"click .finalsubmit":"feedbackanswerfinalsubmit"
    },
    initialize:function(){
        _.bindAll(this,'render')
        var pid;
        driverarr=[];
        feedansarr=[];
        didrow =0;
        preFlag=false;
        preNum=0;
        ansNum = 0;
        ansId=0;
        preAnsNum=0;
        anscount=1;
    },

    render:function(id)
    {
		this.pid=id;
		rid=id;
		 anscount=1;
		 driverarr=[];
	        feedansarr=[];
	        didrow =0;
        this.$el.empty().append(
            '<div class="modal" id="feedBackModal" tabindex="-1" role="dialog" aria-labelledby="feedBackModal" aria-hidden="true" data-backdrop="static">'+
	            '<div class="modal-dialog feedback-popup" >'+
	            // Content part
	            '<div class="modal-content" id="Selffeedpopup">'+
	            '<div class="modal-header">'+
	            	'<div class="close" data-dismiss="modal"></div>'+
	            '</div>'+
	            '<div class="modal-body">' +
                    '<div id="feedBackBox" class="row">'+
                        '<div class="col-xs-7" id="driverName">'+
                            '<div class="panel">'+
                                '<div class="panel-heading">'+
                                    '<h3 class="panel-title">Feedback for <span id="partyname"></span> '+
                                        '<div class="questionCount"><span id="ansCount"></span> / <span id="qunCount"></span></div>'+
                                    '</h3>'+
                                '</div>'+
                                '<div class="panel-body">'+
                                    '<div class="row driverDisplay">'+
                                        '<span class="label">Driver : </span>'+
                                        '<span id="drName"> </span>'+
                                    '</div>'+
                                    '<div class="row queDisply">'+
                                        '<span class="label">Behavior : </span><br>'+
                                        '<span id="quName"> </span>'+
                                    '</div>'+
                                '</div>'+
                            '</div>'+
                        '</div>'+
                                                   
                        '<div class="col-xs-5"   id="options" >'+
                            '<div class="row">'+  
                                '<div class="col-xs-5" >'+  
                                    '<div id="selfansId1" class="optionOne" ></div>'+
                                    '<div id="opta">Recruit</div>' +
                                '</div>' +
                                
                                '<div class="col-xs-7" id="firstone">'+
                                    '<h5 >Beginner, has a long way to go</h5>'+
                                '</div>'+
                                 '</div>' +
                           
                            '<div class="row">'+  
                                '<div class="col-xs-5">'+   
                                    '<div id="selfansId2" class="optionTwo" ></div>'+
                                    '<div id="optb">Apprentice</div>' +
                                '</div>'+
                                '<div class="col-xs-7"  id="secondone">'+
                                    '<h5>Student, has a few gaps</h5>'+
                               '</div>'+
                                 
                            '</div>'+
                          
                            '<div class="row">'+  
                                '<div class="col-xs-5" >'+   
                                    '<div id="selfansId3" class="optionThree"></div>'+
                                     '<div id="optc">Jedi</div>' +
                                '</div>'+
                                '<div class="col-xs-7" id="thirdone">'+
                                    '<h5>A keen practitioner of the behavior</h5>'+
                               '</div>'+
                               
                            '</div>'+
                           
                            '<div class="row">'+  
                                '<div class="col-xs-5">'+
                                    '<div id="selfansId4" class="optionFour"></div>'+
                                     '<div id="optd">Yoda</div>' +
                                '</div>'+
                                '<div class="col-xs-7" id="fourthone">'+
                                    '<h5>Impressive mastery of the behavior</h5>'+
                                '</div>'+
                                
                            '</div>'+

                        '</div>'+
                    '</div>'+
                '</div>'+
	    // Footer part 
                		'<div class="modal-footer " id="nextPage" >'+
		                    '<div class="col-xs-7">'+
		                        'Read the behaviors displayed above and choose an appropriate response from the right hand side box'+
		                    '</div>'+
		                            
		                    '<div class="col-xs-5" id="btns">'+
		                        '<button class="btn btn-success fbprevious" align="center"><i class="fa fa-angle-left" aria-hidden="true"></i></button>'+
		                        '<button class="btn btn-success fbsubmit" align="center"><i class="fa fa-angle-right" aria-hidden="true"></i></button>'+
		                        '<button type="button" class="btn btn-success finalsubmit" align="center">Submit</button>'+
		                    '</div>'+
	                    '</div>'+
	            '</div>'+
            '</div>');
		this.feedFirst();
    },
    
    feedFirst:function(e){
		var prtcpant=$($("#"+this.pid+"").children()[0]).text()
        $("#partyname").text(prtcpant);	
            Backbone.ajax({
                dataType:"json",
                url:"employee/driversnew",
                type: "POST",
                success:function(data, textStatus, jqXHR){
                    if(data.length == 0){
                        sweetAlert("Sorry!", "Please logout there is No 'Drivers' active");
                    } else {
                        feedbackview.generateLi(data);
                    }
                },
                error:function(res,ioArgs){
                	if(res.status == 403){
                		window.location.reload();
                	}
                }
            });
    },
    
    generateLi:function(data){/*
        liElement = $("#feedbackTab").children("ul").empty();
        var fbdidrow = 0;
        //get table id from jquery
        for(var i=0; i< data.length; i++){
            try{
                if(i==0){
                    questionDisplayFlag = true;
                liElement.append("<li id='fbdid"+fbdidrow+"' class='fbdriver active' value='"+data[i].did+"'><a>"+data[i].desc+"</a></li>");
                } else {
                    questionDisplayFlag = false;
                    liElement.append("<li id='fbdid"+fbdidrow+"' class='fbdriver disabled' value='"+data[i].did+"'><a>"+data[i].desc+"</a></li>");
                }
            }catch(e){
            }
            queElement = $("#feedbackTab").children("div");
            feedbackview.generateQuestion(data[i].questions, fbdidrow);
            fbdidrow++;
        }
        this.setAnswer(data[0].did);
    */

        
        /* selfLElement = $("#selfFbTab").children("ul").empty();*/
         //get table id from jquery
         
         for(var i=0; i< data.length; i++){
             try{/*
                 if(i==0){
                     questionDisplayFlag = true;
                 selfLElement.append("<li id='did"+didrow+"' class='driver	 active'><a>"+data[i].desc+"</a></li>");
                 } else {
                     questionDisplayFlag = false;
                     selfLElement.append("<li id='did"+didrow+"' class='driver disabled'><a>"+data[i].desc+"</a></li>");
                 }
             */
               	driverarr[i]=data[i];
             	}catch(e){
             }
 /*            selfqElement = $("#selfFbTab").children("div");
             didrow++;
 */			
         }
         $("#qunCount").text(driverarr.length);
         this.setAnswer(data[0].qid);
         flag=true;
         feedbackview.generateQuestion(driverarr,didrow);
     
    },
    
    generateQuestion:function(queObj, fbdrow){
       
    	
    	$("#drName").text(driverarr[didrow].desc);
    	$("#quName").text(driverarr[didrow].question);
    	$("#ansCount").text(anscount);
    	 $('#selfansId1').addClass('img1');
    	 $('#selfansId2').addClass('img2');
    	 $('#selfansId3').addClass('img3');
    	 $('#selfansId4').addClass('img4');
        $("#pageloading").hide();
    },
    
    chackPreviewsAns:function(){
    	num=0;
		++didrow;
		ansId=didrow;
		if(didrow == driverarr.length){
			$(".fbsubmit").hide();
    		$(".finalsubmit").show();
		}else{
			$("#drName").text(driverarr[didrow].desc);
	    	$("#quName").text(driverarr[didrow].question);
	    	$("#ansCount").text(++anscount);
	    	$(".fbprevious").show();
	    	feedbackview.setAnswer(driverarr[didrow].qid);
		}
    	
    },
    
    loadPage:function(){
    	flag=true;
		preFlag=false;
		$('#selfansId1').removeClass();
		$('#selfansId2').removeClass();
		$('#selfansId3').removeClass();
   	 	$('#selfansId4').removeClass();
   	 	$('#selfansId1').addClass('img1');
   	 	$('#selfansId2').addClass('img2');
   	 	$('#selfansId3').addClass('img3');
   	 	$('#selfansId4').addClass('img4');
    },
    
    generateNewQuestion:function(e){
   	 
    	if(didrow == driverarr.length){
    		/*selfFeedBackView.feedbackanswerfinalsubmit();*/
    		/*$(".fbsubmit").hide();
    		$(".finalsubmit").show();*/
    	}else{
    		
        	if(typeof feedansarr[ansId] !== 'undefined'){
        		if(preFlag){
            		if(feedansarr.length > 0){
                		if(feedansarr[ansId] == 1){
                			if(ansNum != 1 & preNum != 1){
                				$('#selfansId1').removeClass('img1-block').addClass('img1Click');
                    			$('#selfansId'+preNum).removeClass('img'+preNum+'Click').addClass('img'+preNum+'-block');
                    			preNum=1;
                    			ansNum=1;
                			}else if(preNum == 1){
                				ansNum=1;
                			}
                			
                		}else if(feedansarr[ansId] == 2){
                			if(ansNum != 2  & preNum != 2){
                				$('#selfansId2').removeClass('img2-block').addClass('img2Click');
                    			$('#selfansId'+preNum).removeClass('img'+preNum+'Click').addClass('img'+preNum+'-block');
                    			preNum=2;
                    			ansNum=2;
                			}else if(preNum == 2){
                				ansNum=2;
                			}
                			
                		}else if(feedansarr[ansId] == 3){
                			if(ansNum != 3  & preNum != 3){
                				$('#selfansId3').removeClass('img3-block').addClass('img3Click');
                    			$('#selfansId'+preNum).removeClass('img'+preNum+'Click').addClass('img'+preNum+'-block');
                    			preNum=3;
                    			ansNum=3
                			}else if(preNum == 3){
                				ansNum=3;
                			}
                			
                		}else {
                			if(ansNum != 4  & preNum != 4){
                				$('#selfansId4').removeClass('img4-block').addClass('img4Click');
                    			$('#selfansId'+preNum).removeClass('img'+preNum+'Click').addClass('img'+preNum+'-block');
                    			preNum=4;
                    			ansNum=4;
                			}else if(preNum == 4){
                				ansNum=4;
                			}
                			
                		}
                			
                	}
            	
            	}
        	}
        	else{
        		flag=true;
        		preFlag=false;
        	$('#selfansId1').removeClass();
           	 $('#selfansId2').removeClass();
           	 $('#selfansId3').removeClass();
           	 $('#selfansId4').removeClass();
           	 $('#selfansId1').addClass('img1');
           	 $('#selfansId2').addClass('img2');
           	 $('#selfansId3').addClass('img3');
           	 $('#selfansId4').addClass('img4');
        	}
        	 
        	
        	 
    	}
    	
    },
    
    addColor1:function(e){
    	 num=1;
	   	 preAnsNum=1;
	   	 ansNum = 1;
	   	 flag=false;
	   	feedbackview.loadPage();
	    $('#selfansId1').removeClass('img1').addClass('img1Click');
  	   	 $('#selfansId2').removeClass('img2').addClass('img2-block');
  	   	 $('#selfansId3').removeClass('img3').addClass('img3-block');
  	   	 $('#selfansId4').removeClass('img4').addClass('img4-block');
  	   if(driverarr.length == anscount){
			$(".fbsubmit").hide();
			$(".finalsubmit").show();
			feedbackview.submitAns();
		}
    	
        ansNum = 1;
    },
    
    addColor2:function(e){
    	
    	num=2;
	   	 preAnsNum=2;
	   	 ansNum = 2;
	   	 flag=false;
	   	feedbackview.loadPage();
	   	$('#selfansId1').removeClass('img1').addClass('img1-block');
  	   	 $('#selfansId2').removeClass('img2').addClass('img2Click');
  	   	 $('#selfansId3').removeClass('img3').addClass('img3-block');
  	   	 $('#selfansId4').removeClass('img4').addClass('img4-block');
  	   	 
  	   	 
  	   if(driverarr.length == anscount){
  		   	$(".fbsubmit").hide();
			$(".finalsubmit").show();
			feedbackview.submitAns();
		}
    	 
    },
    
    addColor3:function(e){
    	num=3;
	   	 preAnsNum=3;
	   	 ansNum = 3;
	   	 flag=false;
	   	feedbackview.loadPage();
	    $('#selfansId1').removeClass('img1').addClass('img1-block');
  	   	 $('#selfansId2').removeClass('img2').addClass('img2-block');
  	   	 $('#selfansId3').removeClass('img3').addClass('img3Click');
  	   	 $('#selfansId4').removeClass('img4').addClass('img4-block');
  	   if(driverarr.length == anscount){
  		   	$(".fbsubmit").hide();
			$(".finalsubmit").show();
			feedbackview.submitAns();
		}
    },
    
    addColor4:function(e){
    	num=4;
	   	 preAnsNum=4;
	   	 ansNum = 4;
	   	 flag=false;
	   	feedbackview.loadPage();
	    $('#selfansId1').removeClass('img1').addClass('img1-block');
  	   	 $('#selfansId2').removeClass('img2').addClass('img2-block');
  	   	 $('#selfansId3').removeClass('img3').addClass('img3-block');
  	   	 $('#selfansId4').removeClass('img4').addClass('img4Click');
  	   	 
  	   if(driverarr.length == anscount){
  		   	$(".fbsubmit").hide();
			$(".finalsubmit").show();
			feedbackview.submitAns();
		}
    },
    
    ansClick:function(e){/*
        var ansParentID = e.currentTarget.parentElement.parentNode.id;
        var ansID = e.currentTarget.id;
        var ansNum = (ansID.slice(5));
        var autoAns = $("#"+ansParentID+"").children('p').empty().html(optionModel.get(ansNum));
        
        $(e.currentTarget).parent().children(".replay").removeClass("replay");
        $(e.currentTarget).addClass("replay");
    */},
    
    submitAns:function(e){
		try{
			
			
			var q= driverarr[didrow].qid;
			a = ansNum;
			ansNum =0;
			
			if(a != 0){
				$("#pageloading").show();
				feedansarr[didrow]=a;
				json={"qid":q,"ans":a,"pid":this.pid,"eid":"self"};
				$.ajax({
					url: "employee/feedbackanswer",
					data: JSON.stringify([json]),
					type: "POST",
					beforeSend: function(xhr) {
						xhr.setRequestHeader("Accept", "application/json");
						xhr.setRequestHeader("Content-Type", "application/json");
						},
						success: function(smartphone) {
							if(smartphone==1){
								$("#pageloading").hide();
								if(driverarr.length != anscount){
									feedbackview.chackPreviewsAns();
								}
							}else{
                                sweetAlert("Oops!", "There is some problem while saving data!");
		                	}
							
						},
				            error:function(res,ioArgs){
				            	if(res.status == 403){
				            		window.location.reload();
				            	}
				            }
						});
			}else {

				$('#feedBackModal').modal('hide');
                swal({
                    title:"Oops!", 
                    text:"Please select at least 1 response",
                },
                     function(){
                        setTimeout(function(){ $('#feedBackModal').modal('show'); }, 0);
                });
            }
		}catch(e){
			
		}
    },
    
    feedbackanswerfinalsubmit:function(){
    	
    	Backbone.ajax({
			url:"employee/feedbackanswerfinalsubmitrespondent?rid="+rid,
			type: "POST",
            success:function(data, textStatus, jqXHR){
            	if(data==1){
                    $("#feedBackModal").modal('hide');
                    feedbackview.reloadActionPage();
                    sweetAlert("Feedback saved", "Continue to give feedback for other participants");
            	}else{
                    sweetAlert("Oops!", "There is some problem while saving data!");
            	}
			
            }
        });

    
    },
    
	previousAns:function(){
		$(".fbsubmit").show();
		 flag=false;
			preFlag=true;
			if(didrow !=0){
				--didrow;
				feedbackview.setAnswer(driverarr[didrow].qid);
				ansId=didrow;
				$("#drName").text(driverarr[didrow].desc);
		    	$("#quName").text(driverarr[didrow].question);
		    	$("#ansCount").text(--anscount);
		    	$(".finalsubmit").hide();
		    	if(didrow == 0)
		    		$(".fbprevious").hide();
		    	var ans =feedansarr[didrow];
		    	if(ans == 1){
		    		ansNum=ans;
		    		preNum=ans;
	    			num=ans;
	    			feedbackview.loadPage();
	    			$('#selfansId1').removeClass('img1').addClass('img1Click');
	    			$('#selfansId2').removeClass('img2').addClass('img2-block');
	    			$('#selfansId3').removeClass('img3').addClass('img3-block');
	    			$('#selfansId4').removeClass('img4').addClass('img4-block');
	    			
		    		
		    		 
	    			
		    	}else if(ans == 2){
		    		ansNum=ans;
		    		preNum=ans;
    				num=ans;
    				feedbackview.loadPage();
    				$('#selfansId2').removeClass('img2').addClass('img2Click');
	    			$('#selfansId1').removeClass('img1').addClass('img1-block');
	    			$('#selfansId3').removeClass('img3').addClass('img3-block');
	    			$('#selfansId4').removeClass('img4').addClass('img4-block');
		    		
		    	}else if(ans == 3){
		    		ansNum=ans;
		    		preNum=ans;
    				num=ans;
    				feedbackview.loadPage();
    				$('#selfansId3').removeClass('img3').addClass('img3Click');
	    			$('#selfansId1').removeClass('img1').addClass('img1-block');
	    			$('#selfansId2').removeClass('img2').addClass('img2-block');
	    			$('#selfansId4').removeClass('img4').addClass('img4-block');
	    			
		    		
		    	}else{
		    		ansNum=ans;
		    		preNum=ans;
    				num=ans;
		    		feedbackview.loadPage();
		    		
		    		$('#selfansId4').removeClass('img4').addClass('img4Click');
	    			$('#selfansId1').removeClass('img1').addClass('img1-block');
	    			$('#selfansId2').removeClass('img2').addClass('img2-block');
	    			$('#selfansId3').removeClass('img3').addClass('img3-block');
	    			
		    		

		    	}
			}
	},
	
	 reloadActionPage:function(){
         $("#pageloading").show();
         if($("#action_header").text()=="Direct Manager")
             actionableView.pendingAcionableTabDM();
         else if($("#action_header").text()=="Peers")
             actionableView.pendingAcionableTabPeer();
         else if($("#action_header").text()=="Direct Reportee(s)")
             actionableView.pendingAcionableTabDR();
         else if($("#action_header").text()=="Stakeholders")
             actionableView.pendingAcionableTabSH();
     },
    
	setAnswer:function(qid){
		$.ajax({
			url: "employee/getfeedbackanswernew?pid="+this.pid+"&qid="+qid+"",
			success: function(data) {
                if(data.length !=0){
            		ans = data.ans;
            		if(ans == 1){
                    	ansNum = 1;
                    	feedbackview.loadPage();
            			feedbackview.addColor1();
            		}else if(ans == 2){
                    	ansNum = 2;
                    	feedbackview.loadPage();
            			feedbackview.addColor2();
            		}else if(ans == 3){
                    	ansNum = 3;
                    	feedbackview.loadPage();
            			feedbackview.addColor3();
            		}else if(ans == 4){
                    	ansNum = 4;
                    	feedbackview.loadPage();
            			feedbackview.addColor4();
            		} else if(ans == "reseted"){
            			feedbackview.loadPage();
            		}
            		
            	}else{
            		feedbackview.generateNewQuestion();
            	}
              
			},
            error:function(res,ioArgs){
            	if(res.status == 403){
            		window.location.reload();
            	}
            }
		});	
		
	}
    
});