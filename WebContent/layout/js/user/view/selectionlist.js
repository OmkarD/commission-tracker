var generateTable, listtableelement, row, removeShowFlag;

var notChecked = [], checked = [];

var ListView = Backbone.View.extend({
    events:{
        "click #addpeer":"clickpeer",
	    "click #addstakeholder":"clickStakeHolder",
	    "click .removePeSh":"removePESH",
        "click #continue_dr":"selectDR",
        "click #continue_peer":"selectPr",
        "click #continue_SH":"selectSH",
        "click #sumsur":"clickSumSur",
        "click #cnt_SS":"submitSelection",
        "click .instruction_ok":"selectionInstructionOk"
    },
    initialize:function(){
    	
        _.bindAll(this,'render');
        var listType;
        removeShowFlag = false;
    },
    render:function(){
        this.$el.empty().append(
            '<div class="row list_select">'+
                '<div class="col-lg-12">'+
                    '<h1 class="page-header">Selection Menu'+
                    '<div id="sumsur" class="pull-right btn btn-default tollexp" title="Back to Survey Summery"><a> Back</a></div>'+
                    '</h1>'+
                '</div>'+
                '<!-- /.col-lg-12 -->'+
            '</div>'+
            '<!-- /.row -->'+
            '<div class="row">'+
                '<div class="col-lg-12">'+
                    '<div class="panel">'+
                        '<div class="panel-header">'+
                            '<h3 id="selection_header"></h3>'+
                        '</div>'+
                        '<div class="panel-body">'+
				            '<h5 class="addStakeholder"><strong>Click to </strong> <div id="addstakeholder" class="btn btn-success">Add Stakeholders</div> </h5>'+
				            '<h5 class="addPeer"><strong>Click to </strong><div id="addpeer" class="btn btn-success">Add Peers</div></h5>'+
                            '<div class="table-responsive">'+
                                '<table id="selectTable" class="table table-hover">'+
                                    '<thead>'+
                                        '<tr>'+
                                            '<th>Name</th>'+
                                            '<th>Employee Id</th>'+
                                            /*'<th>Position</th>'+*/
                                            '<th>Status</th>'+
                                            '<th class="removePeSh1">Edit</th>'+
                                        '</tr>'+
                                    '</thead>'+
                                    '<tbody>'+
                                    '</tbody>'+
                                '</table>'+
                            '</div>'+
                        '</div>'+
                        '<div class="panel-footer" style="text-align: justify;">'+
                            '<div class="dmcls"><i class="fa fa-hand-o-right col-xs-1"></i> <h5 class="col-xs-11"> Your Direct Manager name would be prefilled. In case of any modifications please write to '+
                                '<a href="mailto:"'+email+'">"'+email+'"</a>'+
                            '</h5></div>'+
                            '<div class="dmcls"><i class="fa fa-hand-o-right col-xs-1"></i> <h5 class="col-xs-11"> Please note: Skip level manager cannot be entered as a respondent under any category. If you have dual reporting please write to the administrator at '+
                                '<a href="mailto:"'+email+'">"'+email+'"</a>'+
                            '</h5></div>'+
                            '<div class="dmcls"><i class="fa fa-hand-o-right col-xs-1"></i> <h5 class="col-xs-11">'+
                                '<a id="continue_dr">Click here</a>'+
                                    ' to move to next section if Direct Manager details are correct '+
                            '</h5></div>'+
                            '<div class="drcls"><i class="fa fa-hand-o-right col-xs-1"></i> <h5 class="col-xs-11"> Your Direct Reportee names would be prefilled. Please check if <strong>ALL</strong> Direct Reportee names are appearing as respondents under this category. In case of any modifications please write to '+
                                '<a href="mailto:"'+email+'">"'+email+'"</a>'+
                            '</h5></div>'+
                            '<div class="drcls"><i class="fa fa-hand-o-right col-xs-1"></i> <h5 class="col-xs-11"> L6,L7 & L8 level reportees have been excluded '+
                            '</h5></div>'+
                            '<div class="drcls"><i class="fa fa-hand-o-right col-xs-1"></i> <h5 class="col-xs-11"> If the number of Direct Reportee is <=2, the responses shall be clubbed along with Peer feedback. In case the participant is in an individual contributor role and does not have any Direct Reportee, he/she can choose to leave the section blank. '+
                            '</h5></div>'+
                            '<div class="drcls"><i class="fa fa-hand-o-right col-xs-1"></i>'+
                                '<h5 class="col-xs-11"><a id="continue_peer">Click here</a> to move to the next section, if the Direct Reportee(s) details are correct '+
                            '</h5></div>'+
                           /* '<div class="prcls"><i class="fa fa-hand-o-right col-xs-1"></i> <h5 class="col-xs-11"> <strong>M4/M5 to select peers within M4/M5 only and SMT to select peers within SMT</strong> '+*/
                            '</h5></div>'+
                            '<div class="prcls"><i class="fa fa-hand-o-right col-xs-1"></i> <h5 class="col-xs-11"> Peers could be within or outside the function based on the frequency and nature of interaction.  '+
                            '</h5></div>'+
                            '<div class="prcls"><i class="fa fa-hand-o-right col-xs-1"></i>'+
                                '<h5 class="col-xs-11"><a id="continue_SH" class="btn disabal" style="background-color: #fff">Click here</a> <span>to move to next section to add stakeholders if you have entered the minimum required peers</span> '+
                            '</h5></div>'+
                            '<div class="shcls"><i class="fa fa-hand-o-right col-xs-1"></i> <h5 class="col-xs-11"> Stakeholders would be internal stakeholders (with whom there is close working relationship /dependency). '+
                            '</h5></div>'+
                            '<div class="shcls"><i class="fa fa-hand-o-right col-xs-1"></i>'+
                                '<h5 class="col-xs-11"><a id="cnt_SS" class="btn disabal" style="background-color: #fff">Click here</a> if you have completed adding stakeholders '+
                            '</h5></div>'+
                        '</div>'+
                    '</div>'+
                '</div><!-- /.col-lg-12 end -->'+
            '</div><!-- /.row end -->'+
        '<div class="modal fade" id="instrution_modal" tabindex="-1" role="dialog" aria-labelledby="instrution_modal" aria-hidden="true" data-backdrop="static" style="margin-top:5%">'+
            '<div class="modal-dialog instrution-popup">'+
                '<div class="modal-content" style="border:10px solid #ccc">'+
                    '<div class="modal-header">'+
                        '<h4>Instructions</h4>'+
                    '</div>'+
                    '<div class="modal-body" style="text-align: justify;">'+
                        '<div class="row">'+
                        '<i class="fa fa-hand-o-right col-xs-1"></i><h5 class="col-xs-11"> Once the self-assessment is completed and respondents have been assigned in all the various categories, an email will be triggered to the reporting manager informing him/her of the same.</h5>'+
                        '<i class="fa fa-hand-o-right col-xs-1"></i><h5 class="col-xs-11"> In case the reporting manager would like to add or delete any respondents, he/she can send a mail directly to  you (participant).</h5>'+
                        '<i class="fa fa-hand-o-right col-xs-1"></i><h5 class="col-xs-11"> Auto mailers shall be sent to all the respondents selected by the participants with the link to input their responses.</h5>'+
                        '<i class="fa fa-hand-o-right col-xs-1"></i><h5 class="col-xs-11"> We request all participants to please pro-actively follow up with the respondents whom they have selected for completion of responses.</h5>'+
                        '<i class="fa fa-hand-o-right col-xs-1"></i><h5 class="col-xs-11"> The report, based on the responses received will consist of overall summary report based on the average scores in all the different categories.</h5>'+
                        '<i class="fa fa-hand-o-right col-xs-1"></i><h5 class="col-xs-11"> The feedback is anonymous (except from the Direct Manager) and the recipient will receive a \'collated\' report showing all the feedback segregated as Direct Manager, Direct Reportee(s), Peers and Stakeholders. The qualitative comments will be provided as is to the recipient. In case the total number of responses across any category is <= 2, the responses will be clubbed together to retain anonymity. Please feel free to reach '+' <a href="mailto:"'+email+'"> "'+email+'" </a> '+' in case of any clarifications.</h5>'+
                        '</div>'+
                    '</div>'+
                    '<div class="modal-footer">'+
                        '<div class="btn btn-success instruction_ok"  align="center" data-dismiss="modal">Ok</div>'+
                    '</div>'+
                '</div>'+
            '</div>'+
        '</div>');
    },

    
    listOfParticipentClickDM:function(e){
        if(!surveySummaryFlag){
            $(".dmcls").show();
        } else if(surveySummaryFlag){
            $(".dmcls").hide();
            $("#sumsur").show();
        }
        removeShowFlag = false;
        $("#selection_header").text("Direct Manager");
        $("#pageloading").show();
        Backbone.ajax({
            dataType:"json",
            url:"employee/selectdirman",
            data:"",
            type: "POST",
            success:function(data, textStatus, jqXHR){
                listtableelement = Backbone.$("#selectTable").children("tbody").empty();
                listview.generateTable(data);
            },
            error:function(res,ioArgs){
            	if(res.status == 403){
            		window.location.reload();
            	}
            }
        });
    },
    
    listOfParticipentClickPeer:function(e){
        $("#selection_header").text("Peers");
        removeShowFlag = false;
        if(!surveySummaryFlag){
            removeShowFlag = true;
            $(".prcls").show();
        } else if(surveySummaryFlag){
            $("#sumsur").show();
        }
        if(gradeLTFlag){
            removeShowFlag = false;
            $(".addPeer").hide();
            $("#continue_SH").parent().parent().children("h5").children("span").text("to move to next section, if you have entered the minimum required peers");
            var urlgt;
            if(ltFirstFlag){
                urlgt = "employee/selectpeerlist";
            } else {
                urlgt = "employee/selectpeer";
            }
            $("#pageloading").show();
            Backbone.ajax({
            dataType:"json",
            url:urlgt,
            data:"",
            type: "POST",
            success:function(data, textStatus, jqXHR){
                try{
                    peerCount = data.length;
                    listtableelement = Backbone.$("#selectTable").children("tbody").empty();
                    listview.generateTable(data);
                    $("#continue_SH").removeClass("disabal");
                } catch(e){}
            },
            error:function(res,ioArgs){
            	if(res.status == 403){
            		window.location.reload();
            	}
            }
            });
        } else{
            $(".addPeer").show();
            $("#pageloading").show();
            Backbone.ajax({
            dataType:"json",
            url:"employee/selectpeer",
            data:"",
            type: "POST",
            success:function(data, textStatus, jqXHR){
                try{
                    peerCount = data.length;
                    listtableelement = Backbone.$("#selectTable").children("tbody").empty();
                    listview.generateTable(data);
                    
                    Backbone.ajax({
                        dataType:"json",
                        url:"employee/ispidinex",
                        data:"",
                        type: "POST",
                        success:function(data, textStatus, jqXHR){
                            try{
                                if(data == 2){
                                    selectingFlag = false;
                                    $("#continue_SH").removeClass("disabal");
                                    
                                } else {
                                    selectingFlag = true;
                                    if(peerCount >= minlimit || gradeLTFlag){
                                        $("#continue_SH").removeClass("disabal");
                                    }else {
                                        $("#continue_SH").addClass("disabal");
                                    }
                                }
                            } catch(e){}
                        }
                    });
                } catch(e){}
            },
            error:function(res,ioArgs){
            	if(res.status == 403){
            		window.location.reload();
            	}
            }
            });
        }
    },
    
    listOfParticipentClickDR:function(e){
        $("#selection_header").text("Direct Reportee(s)");
          if(!surveySummaryFlag){
            $(".drcls").show();
        } else if(surveySummaryFlag){
            $(".drcls").hide();
            $("#sumsur").show();
        }
        removeShowFlag = false;
        $("#pageloading").show();
        Backbone.ajax({
            dataType:"json",
            url:"employee/selectdirrep",
            data:"",
            type: "POST",
            success:function(data, textStatus, jqXHR){
                listtableelement = Backbone.$("#selectTable").children("tbody").empty();
                listview.generateTable(data);
            },
            error:function(res,ioArgs){
            	if(res.status == 403){
            		window.location.reload();
            	}
            }
        });
    },
    
    listOfParticipentClickSH:function(e){
        $("#selection_header").text("Stakeholders");
        removeShowFlag = false;
        if(!surveySummaryFlag){
            $(".shcls").show();
            removeShowFlag = true;
        } else if(surveySummaryFlag){
            $("#sumsur").show();
        }
        
        if(gradeLTFlag){
            $("#cnt_SS").removeClass("disabal");
            listview.submitSelection();
            removeShowFlag = false;
        } else{
            $(".addStakeholder").show();
            $("#pageloading").show();
            Backbone.ajax({
            dataType:"json",
            url:"employee/selectstakeholder",
            data:"",
            type: "POST",
            success:function(data, textStatus, jqXHR){
                try{
                    stakeholdersCount = data.length;
                    listtableelement = Backbone.$("#selectTable").children("tbody").empty();
                    listview.generateTable(data);
                    
                    Backbone.ajax({
                        dataType:"json",
                        url:"employee/ispidinex",
                        data:"",
                        type: "POST",
                        success:function(data, textStatus, jqXHR){
                            try{
                                if(data == 2){
                                    selectingFlag = false;
                                    $("#cnt_SS").removeClass("disabal");
                                   
                                } else {
                                    selectingFlag = true;
                                    if(stakeholdersCount >= minlimit || gradeLTFlag){
                                        $("#cnt_SS").removeClass("disabal");
                                    }else {
                                        $("#cnt_SS").addClass("disabal");
                                    }
                                }
                            } catch(e){}
                        }
                    });
                } catch(e){}
            },
            error:function(res,ioArgs){
            	if(res.status == 403){
            		window.location.reload();
            	}
            }
            });
        }
    },
    
    selectDM:function(){
        listview.listOfParticipentClickDM();
    },
    
    selectDR:function(){
        $(".dmcls").hide();
        listview.listOfParticipentClickDR();
    },
    
    selectPr:function(){
        $(".drcls").hide();
        listview.listOfParticipentClickPeer();
    },
    
    selectSH:function(){
    	if(stackholdersexpFlag){
    		stackholdersexpFlag = false;
    		$(".prcls").hide();
            listview.listOfParticipentClickSH();
            $(".addPeer").hide();
    	} else {
	        if($("#continue_SH").parent().children(".disabal").text() == "Click here"){
	            sweetAlert("Oops!", "You haven't selected minimum no of peers");
	        } else {
                if(gradeLTFlag){
                    $("#cnt_SS").removeClass("disabal");
                    listview.submitSelection();
                } else {
    	            $(".prcls").hide();
    	            $(".addPeer").hide();
                    listview.listOfParticipentClickSH();
                }
	        }
    	}
    },
    
    submitSelection:function(){
        if($("#cnt_SS").parent().children(".disabal").text() == "Click here"){
            sweetAlert("Oops!", "You haven't selected minimum no of stakeholders");
        } else {
            $("#instrution_modal").modal({ keyboard: false });
            $("#instrution_modal").modal('show');
        }
    },
    
    selectionInstructionOk:function(){
        $("#instrution_modal").on('hidden.bs.modal', function() {
        $("#pageloading").show();
            Backbone.ajax({
                dataType:"json",
                url:"employee/feedbackrequestsend",
                data:"",
                type: "POST",
                success:function(data, textStatus, jqXHR){
                    $("#pageloading").hide();
                    try{
                        if(data == 1){
                            swal({
                                title: "Respondent selection complete",
                                text: "To submit your survey to respondents",
                                confirmButtonColor: "#003264",
                                confirmButtonText: "Click here",
                                closeOnConfirm: true
                            },
                                 function(){
                                    aggregateview.render();
                            });
                        } else {
                            sweetAlert("Oops!", "There is some problem while saving data!");
                        }
                    } catch(e){}
                },
                error:function(res,ioArgs){
                	if(res.status == 403){
                		window.location.reload();
                	}
                }
            });
        });
    },
    
    clickpeer: function(e){
        if(peerCount>=maxlimit && selectingFlag){
            sweetAlert("Sorry...", "You already have maximum Peers");
        } else if (peerCount>=maxlimit){
            sweetAlert("Sorry...", "You already have maximum Peers");
        } else {
            $("#selectionModal").remove();
            allParticipentView.render();
            allParticipentView.clickMyPeerTable();
            $("#selectionModal").modal('show');
        }
    },
    
    clickStakeHolder: function(e){
        if(stakeholdersCount>=maxlimit && selectingFlag){
            sweetAlert("Sorry...", "You already have maximum Stakeholders");
        } else if(stakeholdersCount>=maxlimit){
            sweetAlert("Sorry...", "You already have maximum Stakeholders");
        } else {
            $("#selectionModal").remove();
            allParticipentView.render();
            allParticipentView.clickMyStakeHolderTable();
            $("#selectionModal").modal('show');
        }
    },
    
    clickSumSur: function(){
    	if($("#sumsur").parent().children(".disabal").children().text() == " Back"){
            sweetAlert("Oops!", "You haven't selected minimum of respondents");
        } else {
        	$("#pageloading").show();
        	aggregateview.render();
        }
    },
    
    generateTable: function(data){
        for(var i=0; i< data.length; i++){
            /*listtableelement.append('<tr id=listpid'+data[i].pid+'><td>'+data[i].participantName+'</td><td>'+data[i].pid+'</td><td>'+data[i].designation+'</td><td class="statusClss"></td></tr>');*/
        	listtableelement.append('<tr id=listpid'+data[i].pid+'><td>'+data[i].participantName+'</td><td>'+data[i].pid+'</td><td class="statusClss"></td></tr>');
            if(data[i].status == 0){
                $("#listpid"+data[i].pid+"").children(".statusClss").text("Pending");
            } else if(data[i].status == 1){
                $("#listpid"+data[i].pid+"").children(".statusClss").text("Accepted");
            } else if(data[i].status == 2){
                $("#listpid"+data[i].pid+"").children(".statusClss").text("Denied");
            } else if(data[i].status == 3){
                $("#listpid"+data[i].pid+"").children(".statusClss").text("Completed");
            }
            
            $('.removePeSh1').hide();
            $('.removePeSh').hide();
            if(removeShowFlag){
                $("#listpid"+data[i].pid+"").append('<td><div class="removePeSh btn btn-default">Remove</div></td>');
                $('.removePeSh1').show();
                $('.removePeSh').css({"display": "inline-block"});
            }
        }
        
        $("#pageloading").hide();
    },
    
    removePESH:function(e){
    	swal({
            title: "Are you sure",
            text: "Do you want remove this respondent",
            showCancelButton: true,
            confirmButtonColor: "#F01E1E",
            confirmButtonText: "Yes",
            cancelButtonColor: "#003264",
            cancelButtonText: "No"
        },
         function(isConfirm){
        if(isConfirm){
            var pid = e.currentTarget.parentElement.parentElement.id.slice(7);
            Backbone.ajax({
                url:"employee/removerespondent?rid="+pid,
                 type: "POST",
                 success:function(data, textStatus, jqXHR){
                     if(($("#selection_header").text())==("Peers")){
                         listview.listOfParticipentClickPeer();
                     } else if(($("#selection_header").text())==("Stakeholders")){
                         listview.listOfParticipentClickSH();
                     }
                 },
                 error:function(res,ioArgs){
                    if(res.status == 403){
                        window.location.reload();
                    }
                 }
             });
        }
    });
    }

});