var listofParticipentElement, row, checkTest, selectingValue, participentCount;
var json = [];

var AllParticipentView = Backbone.View.extend({
  
    events:
    {
        "click .myCheck":"checkAlert",
        "click .selectBox":"selectedBox"
    },
    initialize:function(){
        _.bindAll(this,'render');
        selectingValue=0;
    },

    render:function()
    {
        this.$el.empty().append(
            '<div class="modal fade" id="selectionModal" tabindex="-1" role="dialog" aria-labelledby="selectionModal" aria-hidden="true" data-backdrop="static">'+
                        '<div class="modal-dialog">'+
                            '<div class="modal-content">'+
                                '<div class="modal-header">'+
                                    '<div class="close" data-dismiss="modal" ></div>'+
                                    '<h4 id="sel_header" class="modal-title"></h4>'+
                                    '<h5>Select Minimum '+minlimit+' respondents & Maximum '+maxlimit+' respondents from  the below list</h5>'+
                                '<b><span class="checkedcount"></span>/'+maxlimit+': selection made</b>'+
                                '</div>'+
                                '<div class="modal-body">'+
                                    '<div class="panel-body">'+
                                        '<div class="table-responsive">'+
                                            '<table id="selectingTable" class="table table-hover">'+
                                                '<thead>'+
                                                    '<tr>'+
                                                        '<th>Name</th>'+
                                                        /*'<th>Position</th>'+*/
                                                        '<th>Select</th>'+
                                                    '</tr>'+
                                                '</thead>'+
                                                '<tbody>'+
                                                '</tbody>'+
                                            '</table>'+
                                        '</div>'+
                                    '</div>'+
                                '</div>'+
                                '<div class="modal-footer">'+
                                    '<div class="selectBox btn btn-success disabal">Submit</div>'+
                                    '<div class="cancelBox btn btn-success" align="center" data-dismiss="modal">Cancel</div>'+
                                '</div>'+
                            '</div>'+
                        '</div>'+
                    '</div>');
    },
    
    clickMyPeerTable:function(){
        json = [];
        $("#sel_header").text("Peers List");
        selectingValue = 1;
        participentCount = peerCount;
        checkTest = participentCount;
        row=0;
        $("#pageloading").show();
        Backbone.ajax({
            dataType : "json",
            url:"employee/selectpeerlist",
            data:"",
            type: "POST",
            success:function(data, textStatus, jqXHR){
                $("#pageloading").hide();
                listofParticipentElement = $("#selectingTable").children("tbody").empty();
                allParticipentView.generateParticipentTable(data);
            },
            error:function(res,ioArgs){
            	if(res.status == 403){
            		window.location.reload();
            	}
            }
        });
    },
    
     clickMyStakeHolderTable:function(){
         json = [];
         $("#sel_header").text("Stakeholders List");
         selectingValue = 2;
         participentCount = stakeholdersCount;
         checkTest = participentCount;
         row=0;
         $("#pageloading").show();
         Backbone.ajax({
             dataType : "json",
             url:"employee/selectstakeholderlist",
             data:"",
             type: "POST",
             success:function(data, textStatus, jqXHR){
                 $("#pageloading").hide();
                 listofParticipentElement = $("#selectingTable").children("tbody").empty();
                 allParticipentView.generateParticipentTable(data);
             },
             error:function(res,ioArgs){
             	if(res.status == 403){
             		window.location.reload();
             	}
             }
         });
     },
    
    generateParticipentTable:function(data){
        $(".checkedcount").text(checkTest);
        $("#pageloading").show();
        //get table id from jquery
        for(var i=0; i< data.length; i++){
            listofParticipentElement.append(allParticipentView.createRow(data[i]));
        }
        $("#pageloading").hide();
        $("#listofparticipent").show();
        $("#selectingTable").dataTable();
    },
    
    createRow:function(rowObject){
        try{
            /*var trElement = "<tr id='"+rowObject.pid+"'><td>"+rowObject.participantName+"</td><td>"+rowObject.designation+"</td><td><input type=checkbox class=myCheck id='myCheck"+row+"'></td>";*/
            var trElement = "<tr id='"+rowObject.pid+"'><td>"+rowObject.participantName+"</td><td><input type=checkbox class=myCheck id='myCheck"+row+"'></td>";
            row++;
            return trElement+"</tr>";
        }catch(e){}
    },
    
    checkAlert:function(e){
        var selectioncheckId = e.currentTarget.id;            
        if(e.currentTarget.checked){
            try{
                checkTest++;
                json.push({"pid":e.currentTarget.parentElement.parentElement.id});
            }catch(e){} 
        } else {
            try{
                checkTest--;
                this.removeA(e.currentTarget.parentElement.parentElement.id);
            }catch(e){}  
        }
        
        if(selectingFlag){
            if((checkTest >= minlimit) && (checkTest <= maxlimit)){
                Backbone.$(".selectBox").removeClass("disabal");
            } else if(checkTest > maxlimit){
                checkTest--;
                sweetAlert("Sorry...", "You have selected maximum of "+maxlimit+" respondents");
                Backbone.$("#"+selectioncheckId+"").prop("checked", false);
                this.removeA(e.currentTarget.parentElement.parentElement.id);
            } else if(checkTest < minlimit){
                Backbone.$(".selectBox").addClass("disabal");
            }
            if(checkTest <= participentCount){
                Backbone.$(".selectBox").addClass("disabal");
            }
        } else {
            if(checkTest <= participentCount){
                Backbone.$(".selectBox").addClass("disabal");
            } else if(checkTest > maxlimit){
                checkTest--;
                sweetAlert("Sorry...", "You have selected maximum of "+maxlimit+" respondents");
                Backbone.$("#"+selectioncheckId+"").prop("checked", false);
                this.removeA(e.currentTarget.parentElement.parentElement.id);
            } else {
                Backbone.$(".selectBox").removeClass("disabal");
            }
        }
        
        $(".checkedcount").text(checkTest);
    },
    
    selectedBox:function(e){
        if($(".selectBox").parent().children(".disabal").text() == "Submit"){
            $("#selectionModal").modal('hide');
            swal({
                title:"Oops!", 
                text:"Please select Min "+minlimit+" & Max "+maxlimit+" respondents from the list",
            },
                 function(){
                    $("#selectionModal").modal('show');
            });
        }
        else{
            $("#pageloading").show();
            var urlv;
            
            if(selectingValue==1){
        
                urlv="employee/selectpeerlistsave";
            } else if(selectingValue==2){

                urlv="employee/selectstakeholderlistsave";
            }

            Backbone.ajax({
                url: urlv,
                data: JSON.stringify(json),
                type: "POST",
                beforeSend: function(xhr) {
                    xhr.setRequestHeader("Accept", "application/json");
                    xhr.setRequestHeader("Content-Type", "application/json");
                },
                success: function(smartphone) {
                    $("#pageloading").hide();
                    if(smartphone == 1){
                        $("#selectionModal").modal('hide');
                        $("#selectionModal").on('hidden.bs.modal', function(){
                            if(($("#selection_header").text())==("Peers")){
                                listview.render();
                                listview.listOfParticipentClickPeer();
                            } else if(($("#selection_header").text())==("Stakeholders")){
                                listview.render();
                                listview.listOfParticipentClickSH();
                            }
                        });
                    } else {
                        $("#selectionModal").modal('hide');
                        $("#selectionModal").on('hidden.bs.modal', function(){
                            sweetAlert("Error..!","Error in submiting please Try again!");
                        });
                    }
                },
                error:function(res,ioArgs){
                	if(res.status == 403){
                		window.location.reload();
                	}
                }
            });
        }
    },
    
    removeA: function(v) {  
        for(var i=0;i<json.length;i++){ 
            if(json[i].pid == v){
                json.splice(i, 1);
            }
        }
    }
    
});