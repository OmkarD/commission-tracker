var selfLElement, selfqElement, selfansElement,num,flag;

var SelfFeedBackView = Backbone.View.extend({
  
    events:
    {
        "mousedown .answer":"ansClick",
		/*"click .selfsubmit":"submitAns",*/
		"click .selfnext":"submitAns",
		"click .selfprevious":"previousAns",
		"click #selfansId1":"addColor1",
		"click #selfansId2":"addColor2",
		"click #selfansId3":"addColor3",
		"click #selfansId4":"addColor4",
		"click .selffinalsubmit":"feedbackanswerfinalsubmit"
    },
    initialize:function(){
        _.bindAll(this,'render');
        driverarr=[];
        ansarr=[];
        didrow =0;
        preFlag=false;
        preNum=0;
        ansNum = 0;
        ansId=0;
        anscount=1;
       this.render();
    },

    render:function()
    {
    	anscount=1;
    	 driverarr=[];
         ansarr=[];
         didrow =0;
        $("#pageloading").show();
        this.$el.empty().append(
            '<div class="modal" id="SelffeedBackModal" tabindex="-1" role="dialog" aria-labelledby="SelffeedBackModal" aria-hidden="true" data-backdrop="static">'+
                '<div class="modal-dialog feedback-popup" >'+
                // Content part
                    '<div class="modal-content" id="Selffeedpopup">'+
                        '<div class="modal-body">' +
                            '<div id="feedBackBox" class="row">'+
                                '<div class="col-xs-7" id="driverName">'+
                                    '<div class="panel">'+
                                        '<div class="panel-heading">'+
                                            '<h3 class="panel-title">Self Assessment '+
                                                '<div class="questionCount"><span id="ansCount"></span> / <span id="qunCount"></span></div>'+
                                            '</h3>'+
                                        '</div>'+
                                        '<div class="panel-body">'+
                                            '<div class="row driverDisplay">'+
                                                '<span class="label">Driver : </span>'+
                                                '<span id="drName"> </span>'+
                                            '</div>'+
                                            '<div class="row queDisply">'+
                                                '<span class="label">Behavior : </span><br>'+
                                                '<span id="quName"> </span>'+
                                            '</div>'+
                                        '</div>'+
                                    '</div>'+
                                '</div>'+
                                                           
                                '<div class="col-xs-5"   id="options" >'+
                                    '<div class="row">'+  
                                        '<div class="col-xs-5" >'+  
                                            '<div id="selfansId1" class="optionOne" ></div>'+
                                            '<div id="opta">Recruit</div>' +
                                        '</div>' +
                                        
                                        '<div class="col-xs-7" id="firstone">'+
                                            '<h5 >Beginner, has a long way to go</h5>'+
                                        '</div>'+
                                         '</div>' +
                                   
                                    '<div class="row">'+  
                                        '<div class="col-xs-5">'+   
                                            '<div id="selfansId2" class="optionTwo" ></div>'+
                                            '<div id="optb">Apprentice</div>' +
                                        '</div>'+
                                        '<div class="col-xs-7"  id="secondone">'+
                                            '<h5>Student, has a few gaps</h5>'+
                                       '</div>'+
                                         
                                    '</div>'+
                                  
                                    '<div class="row">'+  
                                        '<div class="col-xs-5" >'+   
                                            '<div id="selfansId3" class="optionThree"></div>'+
                                             '<div id="optc">Jedi</div>' +
                                        '</div>'+
                                        '<div class="col-xs-7" id="thirdone">'+
                                            '<h5>A keen practitioner of the behavior</h5>'+
                                       '</div>'+
                                       
                                    '</div>'+
                                   
                                    '<div class="row">'+  
                                        '<div class="col-xs-5">'+
                                            '<div id="selfansId4" class="optionFour"></div>'+
                                             '<div id="optd">Yoda</div>' +
                                        '</div>'+
                                        '<div class="col-xs-7" id="fourthone">'+
                                            '<h5>Impressive mastery of the behavior</h5>'+
                                        '</div>'+
                                        
                                    '</div>'+

                                '</div>'+
                            '</div>'+
                        '</div>'+
                  //  
        // Footer part 
                    '<div class="modal-footer " id="nextPage" >'+
                        '<div class="col-xs-7">'+
                            'Read the behaviors displayed above and choose an appropriate response from the right hand side box'+
                        '</div>'+
                                
                        '<div class="col-xs-5" id="btns">'+
                            '<button class="btn btn-success selfprevious" align="center"><i class="fa fa-angle-left" aria-hidden="true"></i></button>'+
                            '<button class="btn btn-success selfnext" align="center"><i class="fa fa-angle-right" aria-hidden="true"></i></button>'+
                            '<button type="button" class="btn btn-success selffinalsubmit" align="center">Submit</button>'+
                        '</div>'+
                    '</div>'+
                '</div>'+
            '</div>' 

        );
		 this.feedFirst();
    },
    
        feedFirst:function(e){
            Backbone.ajax({
                dataType:"json",
                url:"employee/driversnew",
                type: "POST",
                success:function(data, textStatus, jqXHR){
                	if(data.length == 0){
                        sweetAlert("Sorry!", "Please logout there is No 'Drivers' active");
                    } else {
                    	 selfFeedBackView.generateLi(data);
                    }
                   
                },
                error:function(res,ioArgs){
                    if(res.status == 403){
                        window.location.reload();
                    }
                }
            });
    },
    
    feedFirst:function(e){
		var prtcpant=$($("#"+this.pid+"").children()[0]).text()
        $("#partyname").text(prtcpant);	
            Backbone.ajax({
                dataType:"json",
                url:"employee/driversnew",
                type: "POST",
                success:function(data, textStatus, jqXHR){
                    if(data.length == 0){
                        sweetAlert("Sorry!", "Please logout there is No 'Drivers' active");
                    } else {
                        selfFeedBackView.generateLi(data);
                    }
                },
                error:function(res,ioArgs){
                	if(res.status == 403){
                		window.location.reload();
                	}
                }
            });
    },
    
    generateLi:function(data){/*
        liElement = $("#feedbackTab").children("ul").empty();
        var fbdidrow = 0;
        //get table id from jquery
        for(var i=0; i< data.length; i++){
            try{
                if(i==0){
                    questionDisplayFlag = true;
                liElement.append("<li id='fbdid"+fbdidrow+"' class='fbdriver active' value='"+data[i].did+"'><a>"+data[i].desc+"</a></li>");
                } else {
                    questionDisplayFlag = false;
                    liElement.append("<li id='fbdid"+fbdidrow+"' class='fbdriver disabled' value='"+data[i].did+"'><a>"+data[i].desc+"</a></li>");
                }
            }catch(e){
            }
            queElement = $("#feedbackTab").children("div");
            feedbackview.generateQuestion(data[i].questions, fbdidrow);
            fbdidrow++;
        }
        this.setAnswer(data[0].did);
    */

        
        /* selfLElement = $("#selfFbTab").children("ul").empty();*/
         //get table id from jquery
         
         for(var i=0; i< data.length; i++){
             try{/*
                 if(i==0){
                     questionDisplayFlag = true;
                 selfLElement.append("<li id='did"+didrow+"' class='driver	 active'><a>"+data[i].desc+"</a></li>");
                 } else {
                     questionDisplayFlag = false;
                     selfLElement.append("<li id='did"+didrow+"' class='driver disabled'><a>"+data[i].desc+"</a></li>");
                 }
             */
               	driverarr[i]=data[i];
             	}catch(e){
             }
 /*            selfqElement = $("#selfFbTab").children("div");
             didrow++;
 */			
         }
         $("#qunCount").text(driverarr.length);
         flag=true;
         selfFeedBackView.generateQuestion(driverarr,didrow);
     
    },
    
    generateQuestion:function(queObj, fbdrow){
       /* for(var j=0; j< queObj.length ;j++){
            try{
                if(questionDisplayFlag) {
                    queElement.append("<div id='question"+queObj[j].qid+"' class='tab-pane fade in active question fbdid"+fbdrow+"'><a data-toggle='collapse' data-parent='#feedBackQuestion' href='#collapseOne' class='nav question-num1 change-parent' style='cursor: default'><h5 style='font-weight:bold'>"+queObj[j].question+"</h5><div id='ansId1' class='answer question"+queObj[j].qid+"'><p>A</p></div><div id='ansId2' class='answer question"+queObj[j].qid+"'><p>B</p></div><div id='ansId3' class='answer question"+queObj[j].qid+"'><p>C</p></div><div id='ansId4' class='answer question"+queObj[j].qid+"'><p>D</p></div><div id='ansId5' class='answer question"+queObj[j].qid+"'><p>N</p></div><br></a><p></p></div>");
                }
                else {
                    queElement.append("<div id='question"+queObj[j].qid+"' class='tab-pane question fbdid"+fbdrow+"'><a data-toggle='collapse' data-parent='#feedBackQuestion' href='#collapseOne' class='nav question-num1 change-parent' style='cursor: default'><h5 style='font-weight:bold'>"+queObj[j].question+"</h5><div id='ansId1' class='answer question"+queObj[j].qid+"'><p>A</p></div><div id='ansId2' class='answer question"+queObj[j].qid+"'><p>B</p></div><div id='ansId3' class='answer question"+queObj[j].qid+"'><p>C</p></div><div id='ansId4' class='answer question"+queObj[j].qid+"'><p>D</p></div><div id='ansId5' class='answer question"+queObj[j].qid+"'><p>N</p></div><br></a><p></p></div>");
                }
                if(!directManagerReporteeFlag){
                	$(".drdmhide").show();
                    if(queObj[j].nflag == 1){
                        $("#question"+queObj[j].qid+"").children("a").children("#ansId5").show();
                    } else {
                          $("#question"+queObj[j].qid+"").children("a").children("#ansId5").hide();  
                    }
                }
            }catch(e){
            }
        }
        
        $("#pageloading").hide();*/
    	
    	$("#drName").text(driverarr[didrow].desc);
    	$("#quName").text(driverarr[didrow].question);
    	$("#ansCount").text(anscount);
    	 $('#selfansId1').addClass('img1');
    	 $('#selfansId2').addClass('img2');
    	 $('#selfansId3').addClass('img3');
    	 $('#selfansId4').addClass('img4');
        $("#pageloading").hide();
    },
    
    chackPreviewsAns:function(){
    	num=0;
		++didrow;
		ansId=didrow;
		
			$("#drName").text(driverarr[didrow].desc);
	    	$("#quName").text(driverarr[didrow].question);
	    	$("#ansCount").text(++anscount);
	    	$(".selfprevious").show();
	    	selfFeedBackView.setAnswer(driverarr[didrow].qid);
    	
    	
    },
    
    loadPage:function(){
    	flag=true;
		preFlag=false;
		$('#selfansId1').removeClass();
		$('#selfansId2').removeClass();
		$('#selfansId3').removeClass();
   	 	$('#selfansId4').removeClass();
   	 	$('#selfansId1').addClass('img1');
   	 	$('#selfansId2').addClass('img2');
   	 	$('#selfansId3').addClass('img3');
   	 	$('#selfansId4').addClass('img4');
    },
    
    generateNewQuestion:function(e){
   	 
    	if(didrow == driverarr.length){
    		/*selfFeedBackView.feedbackanswerfinalsubmit();*/
    		/*$(".selfnext").hide();
    		$(".selffinalsubmit").show();*/
    	}else{
    		
        	if(typeof ansarr[ansId] !== 'undefined'){
        		if(preFlag){
            		if(ansarr.length > 0){
                		if(ansarr[ansId] == 1){
                			if(ansNum != 1 & preNum != 1){
                				$('#selfansId1').removeClass('img1-block').addClass('img1Click');
                    			$('#selfansId'+preNum).removeClass('img'+preNum+'Click').addClass('img'+preNum+'-block');
                    			preNum=1;
                    			ansNum=1;
                			}else if(preNum == 1){
                				ansNum=1;
                			}
                			
                		}else if(ansarr[ansId] == 2){
                			if(ansNum != 2  & preNum != 2){
                				$('#selfansId2').removeClass('img2-block').addClass('img2Click');
                    			$('#selfansId'+preNum).removeClass('img'+preNum+'Click').addClass('img'+preNum+'-block');
                    			preNum=2;
                    			ansNum=2;
                			}else if(preNum == 2){
                				ansNum=2;
                			}
                			
                		}else if(ansarr[ansId] == 3){
                			if(ansNum != 3  & preNum != 3){
                				$('#selfansId3').removeClass('img3-block').addClass('img3Click');
                    			$('#selfansId'+preNum).removeClass('img'+preNum+'Click').addClass('img'+preNum+'-block');
                    			preNum=3;
                    			ansNum=3
                			}else if(preNum == 3){
                				ansNum=3;
                			}
                			
                		}else {
                			if(ansNum != 4  & preNum != 4){
                				$('#selfansId4').removeClass('img4-block').addClass('img4Click');
                    			$('#selfansId'+preNum).removeClass('img'+preNum+'Click').addClass('img'+preNum+'-block');
                    			preNum=4;
                    			ansNum=4;
                			}else if(preNum == 4){
                				ansNum=4;
                			}
                			
                		}
                			
                	}
            	
            	}
        	}
        	else{
        		flag=true;
        		preFlag=false;
        	$('#selfansId1').removeClass();
           	 $('#selfansId2').removeClass();
           	 $('#selfansId3').removeClass();
           	 $('#selfansId4').removeClass();
           	 $('#selfansId1').addClass('img1');
           	 $('#selfansId2').addClass('img2');
           	 $('#selfansId3').addClass('img3');
           	 $('#selfansId4').addClass('img4');
        	}
        	 
        	
        	 
    	}
    	
    },
    
    addColor1:function(e){
    	 num=1;
	   	 preAnsNum=1;
	   	 ansNum = 1;
	   	 flag=false;
	   	 ansNum = 1;
	   	selfFeedBackView.loadPage();
	    $('#selfansId1').removeClass('img1').addClass('img1Click');
  	   	 $('#selfansId2').removeClass('img2').addClass('img2-block');
  	   	 $('#selfansId3').removeClass('img3').addClass('img3-block');
  	   	 $('#selfansId4').removeClass('img4').addClass('img4-block');
	   	
  	   if(driverarr.length == anscount){
			$(".selfnext").hide();
			$(".selffinalsubmit").show();
			selfFeedBackView.submitAns();
		}
       
    },
    
    addColor2:function(e){
    	
    	num=2;
	   	 preAnsNum=2;
	   	 ansNum = 2;
	   	 flag=false;
	   	selfFeedBackView.loadPage();
	   	$('#selfansId1').removeClass('img1').addClass('img1-block');
  	   	 $('#selfansId2').removeClass('img2').addClass('img2Click');
  	   	 $('#selfansId3').removeClass('img3').addClass('img3-block');
  	   	 $('#selfansId4').removeClass('img4').addClass('img4-block');
  	   	 
  	   if(driverarr.length == anscount){
			$(".selfnext").hide();
			$(".selffinalsubmit").show();
			selfFeedBackView.submitAns();
		}
  		
    	 
    },
    
    addColor3:function(e){
    	num=3;
	   	 preAnsNum=3;
	   	 ansNum = 3;
	   	 flag=false;
	   	selfFeedBackView.loadPage();
	    $('#selfansId1').removeClass('img1').addClass('img1-block');
  	   	 $('#selfansId2').removeClass('img2').addClass('img2-block');
  	   	 $('#selfansId3').removeClass('img3').addClass('img3Click');
  	   	 $('#selfansId4').removeClass('img4').addClass('img4-block');
  	   	 
  	   if(driverarr.length == anscount){
			$(".selfnext").hide();
			$(".selffinalsubmit").show();
			selfFeedBackView.submitAns();
		}
 	
    },
    
    addColor4:function(e){
    	num=4;
	   	 preAnsNum=4;
	   	 ansNum = 4;
	   	 flag=false;
	   	selfFeedBackView.loadPage();
	    $('#selfansId1').removeClass('img1').addClass('img1-block');
  	   	 $('#selfansId2').removeClass('img2').addClass('img2-block');
  	   	 $('#selfansId3').removeClass('img3').addClass('img3-block');
  	   	 $('#selfansId4').removeClass('img4').addClass('img4Click');
  	   	 
  	   if(driverarr.length == anscount){
			$(".selfnext").hide();
			$(".selffinalsubmit").show();
			selfFeedBackView.submitAns();
		}
    },
    
    ansClick:function(e){/*
        var ansParentID = e.currentTarget.parentElement.parentNode.id;
        var ansID = e.currentTarget.id;
        var ansNum = (ansID.slice(5));
        var autoAns = $("#"+ansParentID+"").children('p').empty().html(optionModel.get(ansNum));
        
        $(e.currentTarget).parent().children(".replay").removeClass("replay");
        $(e.currentTarget).addClass("replay");
    */},
    
    submitAns:function(e){
		try{
			var q= driverarr[didrow].qid;
			a = ansNum;
			ansNum =0;
			if(a != 0){
				$("#pageloading").show();
				ansarr[didrow]=a;
				json.push({"qid":q,"ans":a,"pid":"self","eid":"self"});
				$.ajax({
					url: "employee/feedbackanswer",
					data: JSON.stringify(json),
					type: "POST",
					beforeSend: function(xhr) {
						xhr.setRequestHeader("Accept", "application/json");
						xhr.setRequestHeader("Content-Type", "application/json");
						},
						success: function(smartphone) {
							if(smartphone==1){
								$("#pageloading").hide();
								if(driverarr.length != anscount){
									selfFeedBackView.chackPreviewsAns();
								}
							}else{
                                sweetAlert("Oops!", "There is some problem while saving data!");
		                	}
							
							
							/*
							if(smartphone==1){
								$(".fbprevious").removeClass("disabled");
								if($("#fbdid"+(++d)+".fbdriver").length){
									$("#fbdid"+(--d)+".fbdriver").removeClass("active").addClass("disabled");
									qarry=$(".fbdid"+d+".active");
									for(var i=0;i<qarry.length;++i){
										$(qarry[i]).removeClass("active").addClass("disabled");
										}
									$("#fbdid"+(++d)+".fbdriver").removeClass("disabled").addClass("active");
									qarry=$(".fbdid"+d+"");
									for(var i=0;i<qarry.length;++i){
										$(qarry[i]).removeClass("disabled").addClass("active");
										}
									var drsid = $(".fbdriver.active").attr('value');
									feedbackview.setAnswer(drsid);
									}else{
										$.ajax({
											url:"employee/feedbackanswerfinalsubmitrespondent?rid="+rid,
											type: "POST",
							                success:function(data, textStatus, jqXHR){
							                	if(data==1){
	                                                $("#feedBackModal").modal('hide');
	                                                feedbackview.reloadActionPage();
	                                                sweetAlert("Feedback saved", "Continue to give feedback for other participants");
							                	}else{
	                                                sweetAlert("Oops!", "There is some problem while saving data!");
							                	}
											
							                }
							            });
									}
									 
								 }else{
	                                 sweetAlert("Oops!", "There is some problem while saving data!");
								 }
							*/},
				            error:function(res,ioArgs){
				            	if(res.status == 403){
				            		window.location.reload();
				            	}
				            }
						});
			}else {
                $('#SelffeedBackModal').modal('hide');
                swal({
                    title:"Oops!", 
                    text:"Please select at least 1 respondent",

                },
                     function(){
                        setTimeout(function(){ $('#SelffeedBackModal').modal('show'); }, 0);
                });
            }
		}catch(e){
			
		}
    },
    
    feedbackanswerfinalsubmit:function(){ 
    	Backbone.ajax({
            url:"employee/feedbackanswerfinalsubmit",
            type: "POST",
            success:function(data, textStatus, jqXHR){
                if(data==1){
                    $("#SelffeedBackModal").modal('hide');
                    swal({
                        title: "Self-assessment is submitted",
                        text: "To move to the next section",
                        confirmButtonColor: "#003264",
                        confirmButtonText: "Click here",
                        closeOnConfirm: true
                    }, 
                        function(){
                            Backbone.ajax({
                                url:"employee/stepstatus",
                                type: "POST",
                                dataType : "json",
                                success:function(data, textStatus, jqXHR){
                                    try{
                                        if(data == 1){
                                            selfFeedBackView.selectionMenu();
                                        } else{
                                            aggregateview.render();
                                        }
                                    }catch(e){}
                                }
                            }); 
                    });
	        	}else{
                        sweetAlert("Oops!", "There is some problem while saving data!");
	        	}
            }
        });
},
    
	previousAns:function(){/*
		var d=($(".fbdriver.active").attr("id").split("did")[1]);
		if($("#fbdid"+(--d)+".fbdriver").length){
			$("#fbdid"+(++d)+".fbdriver").removeClass("active").addClass("disabled");
			qarry=$(".fbdid"+d+".active");
			for(var i=0;i<qarry.length;++i){
					$(qarry[i]).removeClass("active").addClass("disabled");
			}
			$("#fbdid"+(--d)+".fbdriver").removeClass("disabled").addClass("active");
			qarry=$(".fbdid"+d+"");
			for(var i=0;i<qarry.length;++i){
					$(qarry[i]).removeClass("disabled").addClass("active");
			}
			if($("#fbdid"+(--d)+".fbdriver").length){}else{
			 $(".fbprevious").addClass("disabled");
			}
		}else{
			
		}
			
	*/
		$(".selfnext").show();
		
		 flag=false;
			preFlag=true;
			if(didrow !=0){
				--didrow;
				ansId=didrow;
				$("#drName").text(driverarr[didrow].desc);
		    	$("#quName").text(driverarr[didrow].question);
		    	$("#ansCount").text(--anscount);
		    	$(".selffinalsubmit").hide();
		    	if(didrow == 0)
		    		$(".selfprevious").hide();
		    	var ans =ansarr[didrow];
		    	if(ans == 1){
		    		ansNum=ans;
		    		preNum=ans;
	    			num=ans;
	    			selfFeedBackView.loadPage();
	    			$('#selfansId1').removeClass('img1').addClass('img1Click');
	    			$('#selfansId2').removeClass('img2').addClass('img2-block');
	    			$('#selfansId3').removeClass('img3').addClass('img3-block');
	    			$('#selfansId4').removeClass('img4').addClass('img4-block');
	    			
		    		/*if(num == 0){
		    			if(preNum == 0 | preNum == 1){
		    				$('#selfansId1').removeClass('img1').addClass('img1Click');
			    			$('#selfansId2').removeClass('img2').addClass('img2-block');
			    			$('#selfansId3').removeClass('img3').addClass('img3-block');
			    			$('#selfansId4').removeClass('img4').addClass('img4-block');
			    			preNum=ans;
		    			}else if(preNum != 1){
		    				$('#selfansId'+preNum).removeClass('img'+preNum+'Click').addClass('img'+preNum+'-block');
		    				$('#selfansId1').removeClass('img1-block').addClass('img1Click');
		    				preNum=ans;
		    				num=ans;
		    			}
		    			
		    		}else if(num !=1){
		    			$('#selfansId1').removeClass('img1-block').addClass('img1Click');
		    			$('#selfansId'+num).removeClass('img'+num+'Click').addClass('img'+num+'-block');
		    			preNum=ans;
		    			num=ans;
		    		}*/
		    		 
	    			
		    	}else if(ans == 2){
		    		ansNum=ans;
		    		preNum=ans;
    				num=ans;
    				selfFeedBackView.loadPage();
    				$('#selfansId2').removeClass('img2').addClass('img2Click');
	    			$('#selfansId1').removeClass('img1').addClass('img1-block');
	    			$('#selfansId3').removeClass('img3').addClass('img3-block');
	    			$('#selfansId4').removeClass('img4').addClass('img4-block');
		    		/*if(num == 0){
		    			if(preNum == 0 | preNum == 2){
		    				$('#selfansId2').removeClass('img2').addClass('img2Click');
			    			$('#selfansId1').removeClass('img1').addClass('img1-block');
			    			$('#selfansId3').removeClass('img3').addClass('img3-block');
			    			$('#selfansId4').removeClass('img4').addClass('img4-block');
			    			preNum=ans;
		    			}else if(preNum != 2){
		    				$('#selfansId'+preNum).removeClass('img'+preNum+'Click').addClass('img'+preNum+'-block');
		    				$('#selfansId2').removeClass('img2-block').addClass('img2Click');
		    				preNum=ans;
		    				num=ans;
		    			}
		    				
		    		}else if(num !=2){
		    			$('#selfansId2').removeClass('img2-block').addClass('img2Click');
		    			$('#selfansId'+num).removeClass('img'+num+'Click').addClass('img'+num+'-block');
		    			preNum=ans;
		    			num=ans;
		    		}
*/
		    	}else if(ans == 3){
		    		ansNum=ans;
		    		preNum=ans;
    				num=ans;
    				selfFeedBackView.loadPage();
    				$('#selfansId3').removeClass('img3').addClass('img3Click');
	    			$('#selfansId1').removeClass('img1').addClass('img1-block');
	    			$('#selfansId2').removeClass('img2').addClass('img2-block');
	    			$('#selfansId4').removeClass('img4').addClass('img4-block');
	    			
		    		/*if(num == 0){
		    			if(preNum == 0 | preNum == 3){
		    				$('#selfansId3').removeClass('img3').addClass('img3Click');
			    			$('#selfansId1').removeClass('img1').addClass('img1-block');
			    			$('#selfansId2').removeClass('img2').addClass('img2-block');
			    			$('#selfansId4').removeClass('img4').addClass('img4-block');
			    			preNum=ans;
		    			}else if(preNum != 3){
		    				$('#selfansId'+preNum).removeClass('img'+preNum+'Click').addClass('img'+preNum+'-block');
		    				$('#selfansId3').removeClass('img3-block').addClass('img3Click');
		    				preNum=ans;
		    				num=ans;
		    			}
		    			
		    		}else if(num !=3){
		    			$('#selfansId3').removeClass('img3-block').addClass('img3Click');
		    			$('#selfansId'+num).removeClass('img'+num+'Click').addClass('img'+num+'-block');
		    			preNum=ans;
		    			num=ans;
		    		}*/
		    		
		    	}else{
		    		ansNum=ans;
		    		preNum=ans;
    				num=ans;
    				selfFeedBackView.loadPage();
		    		
		    		$('#selfansId4').removeClass('img4').addClass('img4Click');
	    			$('#selfansId1').removeClass('img1').addClass('img1-block');
	    			$('#selfansId2').removeClass('img2').addClass('img2-block');
	    			$('#selfansId3').removeClass('img3').addClass('img3-block');
	    			
		    		/*if(num == 0){
		    			if(preNum == 0 | preNum == 4){
		    				$('#selfansId4').removeClass('img4').addClass('img4Click');
			    			$('#selfansId1').removeClass('img1').addClass('img1-block');
			    			$('#selfansId2').removeClass('img2').addClass('img2-block');
			    			$('#selfansId3').removeClass('img3').addClass('img3-block');
			    			preNum=ans;
		    			}else if(preNum != 4){
		    				$('#selfansId'+preNum).removeClass('img'+preNum+'Click').addClass('img'+preNum+'-block');
		    				$('#selfansId4').removeClass('img4-block').addClass('img4Click');
		    				preNum=ans;
		    				num=ans;
		    			}
		    			
		    		}else if(num !=4){
		    			$('#selfansId4').removeClass('img4-block').addClass('img4Click');
		    			$('#selfansId'+num).removeClass('img'+num+'Click').addClass('img'+num+'-block');
		    			preNum=ans;
		    			num=ans;
		    		}*/

		    	}
			}
	},
	
	 selectionMenu:function(){
	        $("#dialogbox").hide();
			$("#dialogoverlay").hide();
	        listview.render();
	        listview.listOfParticipentClickDM();
	    },
		show:function(){
			 $(".modal").css("display","block");
	         $("#selffb_wrapper").modal('show');
		},
		hide:function(){
			 $(".modal").css("display","none");
	         $("#selffb_wrapper").modal('hide');
		},
	
	
	setAnswer:function(qid){
		

 		ans = ansarr[didrow]
 		if(ans == 1){
         	ansNum = 1;
         	selfFeedBackView.loadPage();
 			selfFeedBackView.addColor1();
 		}else if(ans == 2){
         	ansNum = 2;
         	selfFeedBackView.loadPage();
 			selfFeedBackView.addColor2();
 		}else if(ans == 3){
         	ansNum = 3;
         	selfFeedBackView.loadPage();
 			selfFeedBackView.addColor3();
 		}else if(ans == 4){
         	ansNum = 4;
         	selfFeedBackView.loadPage();
 			selfFeedBackView.addColor4();
 		}else{
     		selfFeedBackView.generateNewQuestion();
     	}
	}
    
});