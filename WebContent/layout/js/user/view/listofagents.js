(function () {
    
    window.app_listofagetns = {
        Models: {},
        Views: {},
        Routes: {},
        Collection: {}
    };

   app_listofagetns.Views.header=Backbone.View.extend({

    events:{
        'click #validAgents':"displaylistofagents",
        'click #goForValidationOfAgents':"listofvalidagents",
        'click #synchronizeAgents':"synchronizeAgents",

    },
    
    initialize:function(){
    _.bindAll(this,'render');
    // this.render();

    },
    render:function(e){

        this.$el.empty().append(
        '<div class="body-area">'+
            '<div class="container body-content">'+
                '<div class="col-xs-12 col-md-12">'+
                        '<div class="tableavailheader currentarrow" id="validAgents">Agents List</div>'+
                   
                '</div>'+
                    '<div class="col-xs-12 col-md-12">'+
                        '<div class="agentTable">'+


                        /*....................................*/
                    '<div class="datatableBox">' +

                                        '<a href="excelforagentlist" target="_blank"><div class="alladentdownloaddata">Click here to download' +
                                        '</div></a>' +
                    	
                        '<div class="col-xs-12">' +

                                        '<div class="table-responsive" id="allAgentlist">' +
                                        '</div>' +
                        '</div>' +
                    '</div>' +
                        /*....................................*/


                        '</div>'+
                            
                            '<div class="nextbtn" id="goForValidationOfAgents">Go for validation  &raquo;</div>'+
                            '<div class="syncbtn" id="synchronizeAgents">Sync All Agents data</div>'+

                    '</div>'+
            '</div>'+
        '</div>'
      );
        allagentlist.finished();
          





    },

    finished:function(){

        appcontroller.topbarshow();
        appcontroller.stepschange(1);
        allagentlist.displaylistofagents();

        var selector = "#agents";
        appcontroller.common(selector);

        

      
    },

    listofvalidagents:function(){
        
        landingroutes.navigate('validagents',{trigger: true});

        },



    displaylistofagents:function(){
        var url = "listofagents";
        allagentlist.alladvisorsListrequst(url);
    },

            alladvisorsListrequst: function(urls) {

            $("#pageloading").show();
            
            Backbone.$("#allAgentlist").empty().append(
                    '<table id="allAgents" class="table table-hover" >' +
                        '<thead>' +
                            '<tr>' +
                                '<td><b>AGNTNUM</b></td>' +
                                '<td><b>AGENTNAME</b></td>' +
                                '<td><b>SALUTL</b></td>' +
                                '<td><b>DTEAPP</b></td>' +
                                '<td><b>CLTSEX</b></td>' +
                                '<td><b>AGENTADDRESS</b></td>' +
                                '<td><b>ZBROKAGT</b></td>' +
                                '<td><b>ZDATELIC</b></td>' +
                                '<td><b>CLTDOB</b></td>' +
                                '<td><b>AGENT_BRANCH_CODE</b></td>' +
                                '<td><b>AGENT_UNIT_CODE</b></td>' +
                                '<td><b>AGTYPE</b></td>' +
                                '<td><b>ARACDE</b></td>' +
                                '<td><b>AGENT_CLASS</b></td>' +
                                '<td><b>DTETRM</b></td>' +
                                '<td><b>PAYCLT</b></td>' +
                                '<td><b>CLNTNUM</b></td>' +
                                '<td><b>PHONENO1</b></td>' +
                                '<td><b>PHONENO2</b></td>' +
                                '<td><b>REPORTAG</b></td>' +
                                '<td><b>NBUSALLW</b></td>' +
                                '<td><b>MINSTA</b></td>' +
                                '<td><b>REASONCD</b></td>' +
                                '<td><b>EFFDATE</b></td>' +
                                '<td><b>PROPTION</b></td>' +
                                '<td><b>MOBILEPHONE</b></td>' +
                                '<td><b>EMPLOYEECODE</b></td>' +
                                '<td><b>MAILID</b></td>' +
                                '<td><b>PANNo</b></td>' +
                                '<td><b>CHANNEL</b></td>' +
                                '<td><b>PAYMETHOD</b></td>' +
                                /*'<td><b>Add Students</b><input type="checkbox"  class="useAll-check"><button id="addStudentCheck" type="button" class=" btn btn-success btn-circle"> <i class="fa fa-plus" aria-hidden="true"/> </button></td>' +*/
                                /*'<td></td>' +
                                '<td></td>' +*/
                               '</tr>' +
                        '</thead>' +
                        '<tbody>' +
                        '</tbody>' +
                    '</table>'
                );
            
            
            
  
            var agentTable;
            var cls = "test";
            agentTable = jQuery("#allAgents").dataTable({
            "scrollY":"290px",
            "scrollX":true,
            "scrollCollapse":true,
            "paging":true,
            "iDisplayLength": 10,
            "bProcessing" : true,
            "bServerSide" : true,
            "bSort" : false,
            "sAjaxSource" :urls,
            "sServerMethod": "GET",
            "fixedColumns":   {
            "leftColumns": 2,
            },
      
             
            "aoColumns" : [
						   { "mData": "agentNum" },
						   { "mData": "agentName" },
						   { "mData": "salutl" },
						   { "mData": "dateApp" },
						   { "mData": "clientSex" },
							{ "mData": "agentAddr" },
							{ "mData": "zBrokAgnt" },
							{ "mData": "zDateLic" },
							{ "mData": "clientDob" },
							{ "mData": "agentBranchCode" },
							{ "mData": "agentUnitCode" },
							{ "mData": "agentType" },
							{ "mData": "aracde" },
							{ "mData": "agentClass" },
							{ "mData": "dateTrm" },
							{ "mData": "payClient" },
							{ "mData": "clientNumber" },
							{ "mData": "phoneNum1" },
							{ "mData": "phoneNum2" },
							{ "mData": "reporting" },
							{ "mData": "nbusallw" },
							{ "mData": "minsta" },
							{ "mData": "reasoncd" },
							{ "mData": "effectiveDate" },
							{ "mData": "proption" },
							{ "mData": "mobilePhone" },
							{ "mData": "employeeCode" },
							{ "mData": "mailId" },
							{ "mData": "panNum" },
							{ "mData": "channel" },
							{ "mData": "paymentMethod" }
							



                          
            ],
                
            
            "initComplete": function(settings, json) {
                  $("#pageloading").hide();
                  

                  
                  if(json.aaData[0].cycleId == 0){
                        $("#goForValidationOfAgents").hide()
                        $("#synchronizeAgents").show()
                  }
                  else{
                        $("#synchronizeAgents").hide()
                        $("#goForValidationOfAgents").show()
                  }
            },
             
        });
    },

      synchronizeAgents: function () {
          $("#pageloading").show();
          Backbone.ajax({
            url: "syncagentlist",
            data: "",
            type: "POST",
            beforeSend: function(xhr) {
                /*xhr.setRequestHeader(header, token);*/
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-Type", "application/json");
            },
            success: function (data, textStatus, jqXHR) {
                
                if(data == 1) {
                

                    alertify.notify('Successfully Sync the data', 'success', 3 );
                    allagentlist.displaylistofagents();

                }
                if(data == 2) {
                

                    alertify.notify('Some promble happend. Please try again', 'error', 3 );

                }
            },
            error: function (res, ioArgs) {
                 
                if (res.status === 440) {
                    window.location.reload();
                }
                else{
                    alertify.notify('Some promble happend. Please try again', 'error', 3 );
                }
            },
            complete: function() {
                $("#pageloading").hide();
            }
        });
      },
    

  
   });
    

    
})();