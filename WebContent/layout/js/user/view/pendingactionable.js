var generateTable, actiontableelement, acceptId;  
var ActionableView = Backbone.View.extend({
    events:
    {
        "click .feedback":"clickFeedBack",
        "click .accept1":"clickedId",
        "click .reject1":"clickedId",
        "click #feedbacksummary":"clickBackFeedBack",
        "click .notificationsubmit":"clicknotificationsubmit"
    },
    initialize:function(){
        _.bindAll(this,'render');
    },

    render:function()
    {
        this.$el.empty().append( 
            '<div class="row list_select">'+
                '<div class="col-lg-12">'+
                    '<h1 class="page-header">Pending Actionables'+
                        '<div id="feedbacksummary" class="pull-right btn btn-default tollexp" title="Back to Survey Summery"><a> Back</a></div>'+
                    '</h1>'+
                '</div>'+
                '<!-- /.col-lg-12 -->'+
            '</div>'+
            '<!-- /.row -->'+
            '<div class="row">'+
                '<div class="col-lg-12">'+
                    '<div class="panel">'+
                        '<div class="panel-header">'+
                            '<h3 id="action_header">Pending Actionables</h3>'+
                            '<span id="spinfo2"></span>'+
                        '</div>'+
                        '<div class="panel-body">'+
                           '<div class="table-responsive">'+
                                '<table id="actiontable" class="table table-hover">'+
                                    '<thead>'+
                                        '<tr>'+
                                            '<th>Participant Name</th>'+
                                            '<th>Department Name</th>'+
                                           /* '<th>Participant Group</th>'+*/
                                            '<th>Actions</th>'+
                                            '<th>Feedback</th>'+
                                            '<th>Status</th>'+
                                        '</tr>'+
                                    '</thead>'+
                                    '<tbody>'+
                                    '</tbody>'+
                                '</table>'+
                            '</div>'+
                        '</div>'+
                    '</div>'+
                '</div>'+
            '</div>'+
            '<div class="modal fade" id="rejected" tabindex="-1" role="dialog" aria-labelledby="rejected" aria-hidden="true" data-backdrop="static" style="margin-top:10%">'+
        '<div class="modal-dialog">'+
            '<div class="modal-content" style="border:10px solid #ccc">'+
                '<div class="modal-header">'+
                    '<div class="close" data-dismiss="modal" ></div>'+
                '</div>'+
                '<div class="modal-body">'+
                    
                    '<div class="panel-body">'+
                        '<div id="text_feedback" class="text_feedback" id="textarea" maxlength="400"> <!--your content start-->  '+
                            '<p> Please enter rejection reason here... '+
                                '<textarea id="remark_deny" style="width:450px;height:150px" rows="6" cols="50" placeholder="Maximum of 400 character..."></textarea></p>'+
                            '<br><br>'+
                        '</div>'+
                    '</div>'+
                '</div>'+
                '<div class="modal-footer">'+
                    '<div class="notificationsubmit btn btn-success" align="center" data-dismiss="modal" >Submit</div>'+
                '</div>'+
            '</div>'+
        '</div>'+
    '</div>'+
    
    '<div class="modal fade" id="accepted" tabindex="-1" role="dialog" aria-labelledby="accepted" aria-hidden="true" data-backdrop="static" style="margin-top:10%">'+
        '<div class="modal-dialog">'+
            '<div class="modal-content" style="border:10px solid #ccc">'+
                '<div class="modal-header">'+
                    '<div class="close" data-dismiss="modal" ></div>'+
                '</div>'+
                '<div class="modal-body">'+
                    '<div class="panel-body">'+
                        '<div id="text_feedback" class="text_feedback" id="textarea" maxlength="400" > <!--your content start--> '+
                            '<p> You have successfully accepted 360 feedback for selected participant.'+
                                '<br>'+
                                'Please proceed to give feedback </p>'+
                            '<br>'+
                            '<br>'+

                        '</div>'+
                    '</div>'+
                '</div>'+
                '<div class="modal-footer">'+
                    '<div class="notificationsubmit btn btn-success" align="center" data-dismiss="modal">Ok</div>'+
                '</div>'+
            '</div>'+
        '</div>'+
    '</div>');
    },
    
    pendingAcionableTabDM:function(e){
        Backbone.ajax({
            dataType:"json",
            url:"employee/penactdirman",
            data:"",
            type: "POST",
            success:function(data, textStatus, jqXHR){
                actiontableelement = Backbone.$("#actiontable").children("tbody").empty();
                actionableView.generateTable(data);
            },
            error:function(res,ioArgs){
            	if(res.status == 403){
            		window.location.reload();
            	}
            }
        });
    },
    pendingAcionableTabPeer:function(e){
        Backbone.ajax({
            dataType:"json",
            url:"employee/penactpeer",
            data:"",
            type: "POST",
            success:function(data, textStatus, jqXHR){
                actiontableelement = Backbone.$("#actiontable").children("tbody").empty();
                actionableView.generateTable(data);
            },
            error:function(res,ioArgs){
            	if(res.status == 403){
            		window.location.reload();
            	}
            }
        });
    },
    pendingAcionableTabDR:function(e){
        Backbone.ajax({
            dataType:"json",
            url:"employee/penactdirrep",
            data:"",
            type: "POST",
            success:function(data, textStatus, jqXHR){
                actiontableelement = Backbone.$("#actiontable").children("tbody").empty();
                actionableView.generateTable(data);
            },
            error:function(res,ioArgs){
            	if(res.status == 403){
            		window.location.reload();
            	}
            }
        });
    },
    pendingAcionableTabSH:function(e){
        Backbone.ajax({
            dataType:"json",
            url:"employee/penactstakeholder",
            data:"",
            type: "POST",
            success:function(data, textStatus, jqXHR){
                actiontableelement = Backbone.$("#actiontable").children("tbody").empty();
                actionableView.generateTable(data);
            },
            error:function(res,ioArgs){
            	if(res.status == 403){
            		window.location.reload();
            	}
            }
        });
    },

    clickFeedBack:function(e){
        if($("#"+e.currentTarget.id+"").children(".disabal").text() == "Feed Back"){
        	
        } else {
            $("#"+e.currentTarget.id+"").parent().parent().children(".feedbackaction").children().remove();
            feedbackview.render($(e.currentTarget).parent().parent().attr("id"));
            $("#feedBackModal").modal('show');
        }
    },
    
    clickedId:function(e){
        acceptId=e.currentTarget.parentElement.parentElement.parentElement.parentElement.id;
    },
    
    clicknotificationsubmit:function(e){
        $("#pageloading").show();
        var submitId = e.currentTarget.parentElement.parentElement.parentElement.parentElement.id;
        if(submitId == "accepted"){
            var json = [];
            try{
                json.push({"pid":acceptId,"action":1});
            }catch(e){}
            $.ajax({
				url: "employee/alertaction",
				data: JSON.stringify(json),
				type: "POST",
				 
				beforeSend: function(xhr) {
					xhr.setRequestHeader("Accept", "application/json");
					xhr.setRequestHeader("Content-Type", "application/json");
				},
				success: function(data) {
                    $("#pageloading").hide();
                    if(data == 1){
                    	feedbackview.reloadActionPage();
                    } else if(data == 2){
                        sweetAlert("Oops!", "There is some problem while saving data!");
                    } else{
                        sweetAlert("Oops!", data);
                    }
                },
                error:function(res,ioArgs){
                	if(res.status == 403){
                		window.location.reload();
                	}
                }
            });
        } else if(submitId == "rejected"){
            var json = [];
            try{
                    json.push({"pid":acceptId,"action":2,"remark":$("#remark_deny").val()});
                }catch(e){}
            $.ajax({
				url: "employee/alertaction",
				data: JSON.stringify(json),
				type: "POST",
				 
				beforeSend: function(xhr) {
					xhr.setRequestHeader("Accept", "application/json");
					xhr.setRequestHeader("Content-Type", "application/json");
				},
				success: function(data) {
                    $("#pageloading").hide();
                    if(data == 1){
                    	feedbackview.reloadActionPage();
                    } else if(data == 2){
                        sweetAlert("Oops!", "There is some problem while saving data!");
                    } else{
                        sweetAlert("Oops!", data);
                    }
                },
                error:function(res,ioArgs){
                	if(res.status == 403){
                		window.location.reload();
                	}
                }
            });
        }
        $("#pageloading").hide();
    },
    
    
    generateTable:function(data){
        $("#pageloading").show();
        //get table id from jquery
        for(var i=0; i< data.length; i++){
            try{
                /*actiontableelement.append("<tr id='"+data[i].pid+"'><td>"+data[i].participantName+"</td><td>"+data[i].functionGroup+"</td><td>"+data[i].designation+"</td><td class='feedbackaction'><div class='dropdown btn btn-success'><div id='dLable' role='button' data-toggle='dropdown'>Action<span class='caret'></span></div><ul class='dropdown-menu' role='menu' aria-labelledby='dLabel'><li class='accept1'><div data-toggle='modal' data-target='#accepted' style='color: #f01e1e;'>Accept</div></li><li class='divider'></li><li class='reject1'><div data-toggle='modal' data-target='#rejected' style='color: #f01e1e;'>Reject</div></li></ul></div></td><td><div id='feedbackid"+data[i].pid+"' class='feedback'><div class='btn btn-success' title='Click Here To Give Feedback'>Feed Back</div></div></td><td><span id='prtstatus"+data[i].pid+"'>status</span></td></tr>");*/
                actiontableelement.append("<tr id='"+data[i].pid+"'><td>"+data[i].participantName+"</td><td>"+data[i].functionGroup+"</td><td class='feedbackaction'><div class='dropdown btn btn-success'><div id='dLable' role='button' data-toggle='dropdown'>Action<span class='caret'></span></div><ul class='dropdown-menu' role='menu' aria-labelledby='dLabel'><li class='accept1'><div data-toggle='modal' data-target='#accepted' style='color: #f01e1e;'>Accept</div></li><li class='divider'></li><li class='reject1'><div data-toggle='modal' data-target='#rejected' style='color: #f01e1e;'>Reject</div></li></ul></div></td><td><div id='feedbackid"+data[i].pid+"' class='feedback'><div class='btn btn-success' title='Click Here To Give Feedback'>Feed Back</div></div></td><td><span id='prtstatus"+data[i].pid+"'>status</span></td></tr>");
                
                if(directManagerReporteeFlag){
                    $(".reject1").hide();
                }
                
                if(data[i].status==0){
                    $("#feedbackid"+data[i].pid+"").removeClass("feedback");
                    $("#feedbackid"+data[i].pid+"").children().addClass("disabal");
                    $("#prtstatus"+data[i].pid+"").text("Pending");
                } else if(data[i].status==1){
                    $("#"+data[i].pid+"").children(".feedbackaction").children().removeClass("dropdown").remove();
                    $("#feedbackid"+data[i].pid+"").children().removeClass("disabal");
                    $("#prtstatus"+data[i].pid+"").text("Accepted");
                } else if(data[i].status==2){
                    $("#"+data[i].pid+"").children(".feedbackaction").children().removeClass("dropdown").remove();
                    $("#feedbackid"+data[i].pid+"").removeClass("feedback");
                    $("#feedbackid"+data[i].pid+"").children().addClass("disabal");
                    $("#prtstatus"+data[i].pid+"").text("Rejected");
                } else if(data[i].status==3){
                    $("#"+data[i].pid+"").children(".feedbackaction").children().removeClass("dropdown").remove();
                    $("#feedbackid"+data[i].pid+"").children().addClass("disabal");
                    $("#prtstatus"+data[i].pid+"").text("Completed");
                }
            }catch(e){
            }
        }
        $("#pageloading").hide();
    },
    
    
    clickBackFeedBack:function(e){
        $("#pageloading").show();
        aggregateview.render();
    }
    
});
                          
                      
                      