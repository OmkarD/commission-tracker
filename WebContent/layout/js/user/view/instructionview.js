
var InstructionView = Backbone.View.extend({

    events:{
        "click #continueid":"continueToSurvey",
        "click #selfAssesmentClick": "selfAssesmentStart"
    },
 
	initialize:function(){
        _.bindAll(this,'render');
	},

	render:function(){
	$("#pageloading").show();
      this.$el.empty().append(
        '<div class="row">'+
            '<div class="col-md-12">'+
                '<div class="panel">'+
                    '<div class="panel-heading">'+
                        '<h4>Dear <span id="welomeid"></span></h4><br>'+
                        '<h5>Welcome to the 360 Degree Feedback tool. Please read the instructions carefully before you start the feedback survey.</h5>'+
                    '</div>'+
                    '<!-- .panel-heading -->'+
                    '<div class="panel-body" style="text-align: justify;">'+
                        '<h5 style="color:#f01e1e">Instructions</h5><br>'+
                        '<i class="fa fa-hand-o-right col-xs-1 participantinstruction"></i> <h5 class="col-xs-11 participantinstruction">The purpose of this exercise is for you to understand how you are perceived by others with respect to the values of the organization (Dependable, Dynamic, Responsive and Foresighted) and desired behaviors linked to these values. This will be collated in a report where the emphasis will be on looking for common feedback themes and applying any strengths and development areas to your Personal Development Plan.</h5>'+
                                    '<i class="fa fa-hand-o-right col-xs-1 participantinstruction"></i><h5 class="col-xs-11 participantinstruction"> Your raters (whom you will choose from the various categories listed below) will be required to complete and rate the following areas for you, based on their observations and interactions with you over the past year.</h5>'+
                                    '<i class="fa fa-hand-o-right col-xs-1 participantinstruction"></i><h5 class="col-xs-11 participantinstruction"> The feedback is anonymous (except from the Direct Manager) and you will receive a \'collated\' report showing all the feedback segregated as  Direct Reportee(s), Peers and Stakeholders. In case the total number of responses for you across all categories is <= 2, the responses will be clubbed together to retain anonymity.</h5>'+
                                    '<i class="fa fa-hand-o-right col-xs-1 participantinstruction"></i><h5 class="col-xs-11 participantinstruction"> The report is confidential and will be made available only to you, your manager and HR.</h5>'+
                                    '<i class="fa fa-hand-o-right col-xs-1 participantinstruction"></i><h5 class="col-xs-11 participantinstruction"> You are also required to provide a self-feedback on all the listed behaviors.</h5>'+
                                    '<i class="fa fa-hand-o-right col-xs-1 participantinstruction"></i><h5 class="col-xs-11 participantinstruction"> Scoring instructions: For each of the behaviors listed below, there are scoring options to describe how often you have been seen demonstrating this behavior. Please think over your behavior against these behaviors for the last one year and choose the option that you feel describes your behavior most frequently.  </h5>'+
                                    
                                    '<i class="fa fa-hand-o-right col-xs-1 respondentinstruction"></i><h5 class="col-xs-11 respondentinstruction"> The purpose of this exercise is for you to understand how you are perceived by others with respect to the values of the organization (Dependable, Dynamic, Responsive and Foresighted) and desired behaviors linked to these values. This will be collated in a report where the emphasis will be on looking for common feedback themes and applying any strengths and development areas to your Personal Development Plan.</h5>'+
                                    '<i class="fa fa-hand-o-right col-xs-1 respondentinstruction"></i><h5 class="col-xs-11 respondentinstruction"> You are required to complete and rate the following areas for your colleague based on your observations and interactions with them. It is useful to try and think of specific situations when completing the feedback form.</h5>'+
                                    '<i class="fa fa-hand-o-right col-xs-1 respondentinstruction"></i><h5 class="col-xs-11 respondentinstruction"> The feedback is anonymous (except from the Direct Manager) and the recipient will receive a \'collated\' report showing all the feedback segregated as Direct Reportee(s), Peers and Stakeholders. In case the total number of responses across all categories is <= 2, the responses will be clubbed together to retain anonymity.</h5>'+
                                    '<i class="fa fa-hand-o-right col-xs-1 respondentinstruction"></i><h5 class="col-xs-11 respondentinstruction"> The report is confidential and will be made available only to the recipient of feedback, his manager and HR.</h5>'+
                                    '<i class="fa fa-hand-o-right col-xs-1 respondentinstruction"></i><h5 class="col-xs-11 respondentinstruction"> Scoring instructions: For each of the behaviors listed below, there are scoring options to describe how often you have been seen demonstrating this behavior. Please think over your behavior against these behaviors for the last one year and choose the option that you feel describes your behavior most frequently.  </h5>'+
                    '</div>'+
                    '<!-- .panel-body -->'+
                    '<div class="panel-footer">'+
                        '<div id="selffeedbackid" style="display:none"> <b><a id="selfAssesmentClick" class="btn SlffedbckMdl">Click Here</a> to take your Self-Assessment</b>'+
                            '</div>'+
                        '</div>'+
                        '<div class="afterSelfAssesment" style="display:none"> <a id="continueid" class="btn SlffedbckMdl"> Click Here </a> to Continue'+
                        '</div>'+
                    '</div>'+
                '</div>'+
            '</div>'+
        '</div>'
      );
        this.iswelcomePage();
 	},
    
 	iswelcomePage:function(){
        $('.fa').hide();
        Backbone.ajax({
            url:"employee/welcomepage",
            type: "POST",
            dataType : "json",
            success:function(data, textStatus, jqXHR){
                try{
                    Backbone.$("#welomeid").empty().append(''+data.name+'');
                    Backbone.$("#useridname").empty().append(''+data.name+'');
                    if(data.grade == "LT"){
                         gradeLTFlag = true;
                    }
                }catch(e){}
                instructionView.ifisparticipant();
            },
            error:function(res,ioArgs){
            	if(res.status == 403){
            		window.location.reload();
            	}
            }
        });
    },
 	
    ifisparticipant:function(){
        $('.fa').show();
        Backbone.ajax({
            url:"employee/isparticipant",
            type: "POST",
            dataType : "json",
            success:function(data, textStatus, jqXHR){
                try{
                    if(data == 1){
                        instructionView.ifisselffeedbackanswered();
                    } else if(data == 2){
                        isparticipantFlag = false;
                        $(".participantinstruction").hide();
                        $(".respondentinstruction").hide();
                        $(".afterSelfAssesment").hide();
                        instructionView.continueToSurvey();
                    }
                }catch(e){}
            },
            error:function(res,ioArgs){
            	if(res.status == 403){
            		window.location.reload();
            	}
            }
        });
    },
        
   ifisselffeedbackanswered:function(){
        
        Backbone.ajax({
            url:"employee/isselffeedbackanswer",
            type: "POST",
            dataType : "json",
            success:function(data, textStatus, jqXHR){
                try{
                    if(data == 1){
                        swal({
                            title: "Please Note...!",
                            text: "Before providing feedback for others you will have to complete your Self-Assessment followed by selecting respondents for your own survey",
                            confirmButtonColor: "#003264",
                            confirmButtonText: "Click here, to take your Self-Assessment"
                        },
                            function(){
                            $("#selffeedbackid").show();
                            $(".afterSelfAssesment").hide();
                            isparticipantFlag = true;
                            $(".participantinstruction").show();
                            $(".respondentinstruction").hide();
                        });
                    } else {
                        Backbone.ajax({
                        url:"employee/stepstatus",
                        type: "POST",
                        dataType : "json",
                        success:function(data, textStatus, jqXHR){
                            try{
                                if(data == 1){
                                    listview.render();
                                    listview.listOfParticipentClickDM();
                                } else{
                                    $(".participantinstruction").show();
                                    $(".respondentinstruction").hide();
                                    $(".afterSelfAssesment").show();
                                    instructionView.continueToSurvey();
                                }
                            }catch(e){}
                        }
                        });
                    }
                }catch(e){}
                $("#pageloading").hide();
            },
            error:function(res,ioArgs){
            	if(res.status == 403){
            		window.location.reload();
            	}
            }
        });
  },
    
    selfAssesmentStart:function(){
        $("#SelffeedBackModal").modal({ keyboard: false });
        $("#SelffeedBackModal").modal('show');
    },
    
    continueToSurvey:function(){
        $("#pageloading").show();
        aggregateview.render();
    },
});

