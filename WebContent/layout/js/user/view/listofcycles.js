(function () {
    var Fullcycletlist;
    
    window.app_cycle = {
        Models: {},
        Views: {},
        Routes: {},
        Collection: {}
    };

   app_cycle.Views.allcycle=Backbone.View.extend({

    events:{
        'click #createcycle':"navigatetocreate",
        'click .addcyclebtn':"clearCreatepopup",
        /*'click .dataTable tbody tr'*/

    },
    
    initialize:function(){
      _.bindAll(this,'render');

    },
    render:function(){

        this.$el.empty().append(
            
            /*'<div>'+
                 '<h1>Hlooooooo</hi>'+
                 '<button id="ttt">kooiii'+
                 '</button>'+
            '</div>' */
'<div class="body-area ">'+
''+
''+
'<div class="container">'+
'<div class="row">'+
''+
'<div class=" listpage">'+
''+
'<div class="col-sm-0 col-md-8 col-lg-8">'+
''+
'</div>'+
''+
'<div class="col-sm-12 col-md-4 col-lg-4 middlepage">'+
'<div class="noitembtn addcyclebtn" data-toggle="modal" data-target="#createmodal">'+
'Add A New Cycle'+
'</div> '+
''+
'</div>'+
''+
  '<div class="col-sm-12 col-md-12 col-lg-12 middlepage">'+
    '<div class="cyclelistbox">'+
      '<div class="listcycletablehead">List Of Cycle</div>'+
      '<div class="cyclelistTable">'+






                      /*....................................*/
                  '<div class="datatableBox">' +

                      '<div class="col-xs-12">' +

                                      '<div class="table-responsive" id="allcyclelist">' +
                                      '</div>' +
                      '</div>' +
                  '</div>' +
                      /*....................................*/






      '</div>'+
    '</div> '+
  '</div> '+
''+
'</div>    '+
'</div>'+

'</div>'+

'</div>'+


''+
'<!-- modal new cycle -->'+
''+
'<div class="modal fade" id="createmodal" role="dialog">'+
'<div class="modal-dialog">'+
''+
'<!-- Modal content-->'+
'<div class="modal-content">'+
'<div class="modal-header">'+
'<button type="button" class="close" data-dismiss="modal">x</button>'+
'<h4 class="modal-title">Add New Cycle</h4>'+
'</div>'+
''+
''+
'<div class="modal-body">'+
'<form>'+
'<div class="row">'+
'<div class=" col-sm-12 col-md-6 col-lg-6 form-group">'+
'<label for="datefrom" class="col-form-label">Date from:</label>'+
'<input type="text" class="form-control datefrom" id="datepickerfrom" name="date" readonly>'+
'</div>'+
'<div class=" col-sm-12 col-md-6 col-lg-6 form-group">'+
'<label for="dateto" class="col-form-label">Date to:</label>'+
'<input type="text" class="form-control dateto" id="datepickerto" readonly>'+
'</div>'+
'<div class="  col-sm-12 col-md-12 col-lg-12 form-group">'+
'<label for="message-text" class="col-form-label">Cycle Name:</label>'+
'<input type="text" class="form-control" id="cyclename">'+
'</div>'+
'<div class="  col-sm-12 col-md-12 col-lg-12 form-group">'+
'<label for="message-text" class="col-form-label">Cycle Discription:</label>'+
'<input type="text" class="form-control" id="cycleDiscription">'+
'</div>'+
'</div>'+
'</form>'+
'</div>'+
'<div class="modal-footer">'+
'<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>'+
'<button type="button" class="btn btn-primary" id="createcycle">Proceed</button>'+
'</div>'+
'</div>'+
''+
'</div>'+
'</div>'+
''+
'<!-- modal end -->'




 );

this.readyfunction();





    },

    readyfunction: function () {
        appcontroller.topbarhide();
        appcontroller.common();
        $("#pageloading").show();


            $('#datepickerfrom').datepicker({
            	dateFormat : 'yy-mm-dd',
            	changeYear: true,
            	changeMonth: true,

              onClose: function (selected) {
                var dt = new Date(selected);
                dt.setDate(dt.getDate() + 1);
                $("#datepickerto").datepicker("option", "minDate", dt);
            }

            });


            $('#datepickerto').datepicker({
            	dateFormat : 'yy-mm-dd',
            	changeYear: true,
            	changeMonth: true,

                  onClose: function (selected) {
                    var dt = new Date(selected);
                    dt.setDate(dt.getDate() + 1);
                    $("#datepickerfrom").datepicker("option", "maxDate", dt);
                }
            });

        listofcycle.displaylistofcycle();
    },



    navigatetocreate:function(){
      var fromDate= $("#datepickerfrom").val();
      var toDate= $("#datepickerto").val();
      var cycleName= $("#cyclename").val();
      var cycleDescription= $("#cycleDiscription").val();


      if(!fromDate || !toDate || !cycleName || !cycleDescription){
        alertify.notify('All filrds are mandatory', 'error', 3 );
        return false;

      }
      






      $("#createmodal").modal('hide');
      $('body').removeClass('modal-open');
      $('.modal-backdrop').remove();
      $("#pageloading").show();
        var json;
      	json = { "fromDate":fromDate, "toDate":toDate, "cycleName":cycleName,"cycleDescription":cycleDescription};
        Backbone.ajax({
          url: "createcycle",
          data: JSON.stringify(json),
          type: "POST",
          beforeSend: function(xhr) {
              /*xhr.setRequestHeader(header, token);*/
              xhr.setRequestHeader("Accept", "application/json");
              xhr.setRequestHeader("Content-Type", "application/json");
          },
          success: function (data, textStatus, jqXHR) {
              
              if(data == 1) {
                /*alertify.success('Successfully Added');*/
                alertify.notify('Successfully Added', 'success', 3 );
                listofcycle.displaylistofcycle();


              }
          },
          error: function (res, ioArgs) {
               
              if (res.status === 440) {
                  window.location.reload();
              }
          },
          complete: function() {
              /*$("#pageloading").hide();*/
          }
      });

    },

    
    clearCreatepopup:function(){
        $("#datepickerfrom").val("");
        $("#datepickerto").val("");
        $("#cyclename").val("");
        $("#cycleDiscription").val("");
      },


    displaylistofcycle:function(){
      $("#pageloading").show();
      var url = "listofcycles";
      listofcycle.alladvisorsListrequst(url);
    },

              alladvisorsListrequst: function(urls) {
            
            Backbone.$("#allcyclelist").empty().append(
                    '<table id="allcyc" class="table table-hover" >' +
                        '<thead>' +
                            '<tr>' +
                                '<td><b>Cycle Name</b></td>' +
                                '<td><b>From Date</b></td>' +
                                '<td><b>To Date</b></td>' +
                                '<td><b>Cycle Description</b></td>' +
                                '<td><b>Created By</b></td>' +
                                '<td><b>Created On</b></td>' +
                                
                                /*'<td><b>Add Students</b><input type="checkbox"  class="useAll-check"><button id="addStudentCheck" type="button" class=" btn btn-success btn-circle"> <i class="fa fa-plus" aria-hidden="true"/> </button></td>' +*/
                                /*'<td></td>' +
                                '<td></td>' +*/
                               '</tr>' +
                        '</thead>' +
                        '<tbody>' +
                        '</tbody>' +
                    '</table>'
                );
            
            
            
  
            var agentTable;
            var cls = "test";
            /*agentTable = jQuery("#allcyc").dataTable({*/
            Fullcycletlist = jQuery('#allcyc').DataTable({
            "scrollY":"250px",
            /*"scrollX":true,
            "scrollCollapse":true,*/
            "paging":true,
            "iDisplayLength": 10,
            "bProcessing" : true,
            "bServerSide" : true,
            "bSort" : false,
            "sAjaxSource" :"listofcycles",
            "sServerMethod": "GET",
            /*"fixedColumns":   {
            "leftColumns": 3,
            },*/
      
             
            "aoColumns" : [
                            { "mData": "cycleName" },
                            { "mData": "fromDate" },
                            { "mData": "toDate" },
                            { "mData": "cycleDescription" },
                            { "mData": "createdBy" },
                            { "mData": "criatedOn" }

                          
            ],

            "initComplete": function(settings, json) {
              listofcycle.datattableclick();
              $("#pageloading").hide();
            },
                
            rowCallback: function ( row, data ) {
                $('user-check', row).prop( 'checked', data.addStudentCheck == 1 );
                
            },
            
            'fnCreatedRow': function (nRow, data, iDataIndex) {
                    $(nRow).attr('id', 'module' + data.uniId); // or whatever you choose to set as the id
                   
            },
             
        });
    },

    datattableclick:function(){
      var rowdata
      $('#allcyc tbody').on( 'click', 'tr', function () {
      rowdata = ( Fullcycletlist.row( this ).data());


        if(rowdata){
          var id = rowdata.cycleId
            listofcycle.setcycleIDinBackend(id , rowdata)


      }
      else{
        console.log(error);
        $("#pageloading").hide();
      }




      } );


  },

  setcycleIDinBackend: function ( id , rowdata) {
          $("#pageloading").show();
          Backbone.ajax({
            url: "addvariable",
            data: JSON.stringify( id ),
            type: "POST",
            beforeSend: function(xhr) {
                /*xhr.setRequestHeader(header, token);*/
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-Type", "application/json");
            },
            success: function (data, textStatus, jqXHR) {
                
                if(data == 1) {
                   /*call for nxr pafe start*/

                         if(rowdata.validflag==1){
                          var ids=rowdata.cycleId;
                          landingroutes.navigate('listofagents',{trigger: true});

                        }
                        if(rowdata.validflag==2){
                          var ids=rowdata.cycleId;
                          landingroutes.navigate('validagents',{trigger: true});

                        }

                     /*call for nxr pafe end*/



                }
            },
            error: function (res, ioArgs) {
                 
                if (res.status === 440) {
                    window.location.reload();
                }
            },
            complete: function() {
                $("#pageloading").hide();
            }
        });
      },


   });
    

    
})();