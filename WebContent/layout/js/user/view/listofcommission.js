(function () {
    
    window.app_listofcommision = {
        Models: {},
        Views: {},
        Routes: {},
        Collection: {}
    };

   app_listofcommision.Views.header=Backbone.View.extend({

    events:{
        'click #validCommision':"displaylistofcommission",
        'click #goForValidationOfAgents':"listofvalidagents",

    },
    
    initialize:function(){
    _.bindAll(this,'render');
    // this.render();

    },
    render:function(e){

        this.$el.empty().append(
        '<div class="body-area">'+
            '<div class="container body-content">'+
                '<div class="col-xs-12 col-md-12">'+
                        '<div class="tableavailheader currentarrow" id="validCommision">Commision List</div>'+
                   
                '</div>'+
                    '<div class="col-xs-12 col-md-12">'+
                        '<div class="agentTable">'+


                        /*....................................*/
                    '<div class="datatableBox">' +

                                        '<div class="alladentdownloaddata uploadbar">'+
                                        '<input style="float:left;" type=file ><button class="btn btn-primary btn-sm smallbutton">Click here to upload</button>' +
                                        '</div>' +

                        '<div class="col-xs-12">' +

                                        '<div class="table-responsive" id="allCommisionlist">' +
                                        '</div>' +
                        '</div>' +
                    '</div>' +
                        /*....................................*/


                        '</div>'+
                            
                            '<div class="nextbtn" id="goForValidationOfCommisssion">Go for validation  &raquo;</div>'+
                            '<div class="syncbtn" id="synchronizecommision">Sync All Commission data</div>'+

                    '</div>'+
            '</div>'+
        '</div>'
      );
        this.finished();
          





    },

    finished:function(){

        appcontroller.topbarshow();
        appcontroller.stepschange(1);
        listofcommision.displaylistofcommission();
        var selector = "#commision";
        appcontroller.common(selector);

        $("#pageloading").hide();

        

      
    },

    listofvalidagents:function(){
        
        landingroutes.navigate('validagents',{trigger: true});

        },


    displaylistofcommission:function(){
        var url = "listofagents";
        listofcommision.alladvisorsListrequst(url);
    },

            alladvisorsListrequst: function(urls) {

             $("#pageloading").hide();
            
            Backbone.$("#allCommisionlist").empty().append(
                    '<table id="allCommision" class="table table-hover" >' +
                        '<thead>' +
                            '<tr>' +
                                '<td><b>Agent number</b></td>' +
                                '<td><b>Name</b></td>' +
                                '<td><b>E-mail</b></td>' +
                                '<td><b>Branch Code</b></td>' +
                                '<td><b>Branch Mapping</b></td>' +
                                '<td><b>Branch Mapping</b></td>' +
                                
                                '<td><b>Branch Mapping</b></td>' +
                                '<td><b>Branch Mapping</b></td>' +
                                '<td><b>Branch Mapping</b></td>' +
                                '<td><b>Branch Mapping</b></td>' +
                                '<td><b>Branch Mapping</b></td>' +
                                '<td><b>Branch Mapping</b></td>' +
                                '<td><b>Branch Mapping</b></td>' +
                                '<td><b>Branch Mapping</b></td>' +
                                '<td><b>Branch Mapping</b></td>' +
                                '<td><b>Branch Mapping</b></td>' +
                                '<td><b>Branch Mapping</b></td>' +
                                '<td><b>Branch Mapping</b></td>' +
                                '<td><b>Branch Mapping</b></td>' +
                                '<td><b>Branch Mapping</b></td>' +
                                '<td><b>Branch Mapping</b></td>' +
                                '<td><b>Branch Mapping</b></td>' +
                                '<td><b>Branch Mapping</b></td>' +
                                '<td><b>Branch Mapping</b></td>' +
                                '<td><b>Branch Mapping</b></td>' +
                                '<td><b>Branch Mapping</b></td>' +
                                '<td><b>Branch Mapping</b></td>' +
                                '<td><b>Branch Mapping</b></td>' +
                                '<td><b>Branch Mapping</b></td>' +
                                '<td><b>Branch Mapping</b></td>' +
                                '<td><b>Branch Mapping</b></td>' +
                                /*'<td><b>Add Students</b><input type="checkbox"  class="useAll-check"><button id="addStudentCheck" type="button" class=" btn btn-success btn-circle"> <i class="fa fa-plus" aria-hidden="true"/> </button></td>' +*/
                                /*'<td></td>' +
                                '<td></td>' +*/
                               '</tr>' +
                        '</thead>' +
                        '<tbody>' +
                        '</tbody>' +
                    '</table>'
                );
            
            
            
  
            var commsionTable;
            commsionTable = jQuery("#allCommision").dataTable({
            "scrollY":"290px",
            "scrollX":true,
            "scrollCollapse":true,
            "paging":true,
            "iDisplayLength": 10,
            "bProcessing" : true,
            "bServerSide" : true,
            "bSort" : false,
            "sAjaxSource" :urls,
            "sServerMethod": "GET",
            "fixedColumns":   {
            "leftColumns": 2,
            },
      
             
            "aoColumns" : [
                           { "mData": "agentNum" },
                           { "mData": "agentName" },
                           { "mData": "dateApp" },
                           { "mData": "clientSex" },
                           { "mData": "salutl" },
                            { "mData": "agentAddr" },
                            { "mData": "zBrokAgnt" },
                            { "mData": "zDateLic" },
                            { "mData": "agentBranchCode" },
                            { "mData": "agentUnitCode" },
                            { "mData": "agentType" },
                            { "mData": "aracde" },
                            { "mData": "agentClass" },
                            { "mData": "dateTrm" },
                            { "mData": "payClient" },
                            { "mData": "clientNumber" },
                            { "mData": "phoneNum1" },
                            { "mData": "phoneNum2" },
                            { "mData": "reporting" },
                            { "mData": "nbusallw" },
                            { "mData": "minsta" },
                            { "mData": "reasoncd" },
                            { "mData": "effectiveDate" },
                            { "mData": "proption" },
                            { "mData": "mobilePhone" },
                            { "mData": "employeeCode" },
                            { "mData": "mailId" },
                            { "mData": "panNum" },
                            { "mData": "channel" },
                            { "mData": "paymentMethod" },
                            { "mData": "clientDob" }


                          
            ],
                
            "initComplete": function(settings, json) {
                $("#pageloading").hide();
            },
                
            rowCallback: function ( row, data ) {
                $('user-check', row).prop( 'checked', data.addStudentCheck == 1 );
            },
            
            'fnCreatedRow': function (nRow, data, iDataIndex) {
                    $(nRow).attr('id', 'module' + data.uniId); // or whatever you choose to set as the id
                   
            },
             
        });
    },
    

  
   });
    

    
})();