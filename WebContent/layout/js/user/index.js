// /*** Views File variable Declare General ***/
var appcontroller;
// /*** Routes variable ***/
var approute,allagentlist,listofcycle,listofagent,listofcommision;

(function () {
    
    window.app_common = {
        Models: {},
        Views: {},
        Routes: {},
        Collection: {}
    };



    /*** Routes for the Program common Page ***/
   app_common.Routes.programe = approute;
   
   app_common.Views.programe = appcontroller;
    
    appcontroller = new app_common.Views.programe ();
    $("#wrapper").append(appcontroller.el);

    /*** Initializing Program common Routes ***/
    landingroutes = new app_common.Routes.programe();
    Backbone.history.start();
    
})();