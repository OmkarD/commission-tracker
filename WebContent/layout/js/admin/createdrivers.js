var  creatingDriverElement, creatingQuestionInAdmin, queid, drID, drivid;

var CreateDriversView = Backbone.View.extend({

    events:{
        "click .dques":"dquestionClick",
        "click .addquestion":"addNewQuestion",
        "mousedown .adddriversubmit":"addNewDriverBeforeSubmitted",
        "mouseup .adddriversubmit":"addNewDriver",
        "mousedown .addnewquestionsubmit":"addNewQuestionBeforeSubmitted",
        "mouseup .addnewquestionsubmit":"addNewQuestionSubmitted",
        "click .editquestion":"editQuestion",
        "click .editdriver":"editDriver",
        "mousedown .editsubmit":"editDriverBeforeSubmitted",
        "mouseup .editsubmit":"editsubmitClick",
        "click .enabledriverBox":"enableDriver",
        "click .disabledriverBox":"disableDriver",
        "click .enableBox":"enableQuestion",
        "click .disableBox":"disableQuestion"
    },
 
	initialize:function(){
        _.bindAll(this,'render');
	},

	render:function(){
      this.$el.empty().append(
          '<div class="row">'+
            '<div class="col-lg-12">'+
                '<h1 class="page-header">Drivers</h1>'+					
            '</div>'+
        '</div>'+
        '<div class="row">'+
            '<div class="col-lg-12">'+
                '<div class="panel panel-default">'+
                    '<div class="panel-heading">'+
                            '<h4>Driver Group</h4>'+
                    '</div>'+
                    '<!-- .panel-heading -->'+
                    '<div class="panel-body">'+
                        '<div class="btn" data-toggle="modal" data-target="#adddrive">'+
                            '<button class="adddriver btn btn-default" type="button">'+
                                '<i class="fa fa-list-alt"></i> Add new driver'+
                            '</button>'+
                        '</div>'+
                        '<div class="panel-group" id="createaccordion">'+
                        '</div>'+
                    '</div>'+
                    '<!-- .panel-body -->'+
                '</div>'+
                '<!-- /.panel -->'+
            '</div>'+
            '<!-- /.col-lg-12 -->'+
        '</div>'+
        '<div class="modal fade" id="adddrive" tabindex="-1" role="dialog" aria-labelledby="adddrive" aria-hidden="true" data-backdrop="static" style="margin-top:10%">'+
            '<div class="modal-dialog">'+
                '<div class="modal-content" style="border:10px solid #ccc">'+
                    '<div class="modal-header">'+
                        '<div class="close" data-dismiss="modal" ></div>'+
                        '<h4 class="modal-title">Add Driver</h4>'+
                    '</div>'+
                    '<div class="modal-body">'+
                        '<div class="panel-body">'+
                            '<p>Add new driver name: <input id="adddrivernew" type="text" class="form-control" placeholder="Driver"></p>'+
                            '<br><br>'+
                        '</div>'+
                    '</div>'+
                    '<div class="modal-footer">'+
                        '<div class="adddriversubmit btn btn-default" align="center" data-dismiss="modal">Submit</div>'+
                    '</div>'+
                '</div>'+
            '</div>'+
        '</div>'+
        '<div class="modal fade" id="addquestionnew" tabindex="-1" role="dialog" aria-labelledby="addquestionnew" aria-hidden="true" data-backdrop="static" style="margin-top:10%">'+
            '<div class="modal-dialog">'+
                '<div class="modal-content" style="border:10px solid #ccc">'+
                    '<div class="modal-header">'+
                        '<div class="close" data-dismiss="modal" ></div>'+
                        '<h4 class="modal-title">Add question for this driver</h4>'+
                    '</div>'+
                    '<div class="modal-body">'+
                        '<div class="panel-body">'+
                            '<p>Add new question: <input id="addanewquestion" type="text" class="form-control" placeholder="Question"></p>'+
                            '<br><br>'+
                            '<div><input type="checkbox" class="nFlagCheck"> To set \'N\' scoring on this Question</div>'+
                        '</div>'+
                    '</div>'+
                    '<div class="modal-footer">'+
                        '<div class="addnewquestionsubmit btn btn-default" align="center" data-dismiss="modal">Submit</div>'+
                    '</div>'+
                '</div>'+
            '</div>'+
        '</div>'+
        '<div class="modal fade" id="editdriv" tabindex="-1" role="dialog" aria-labelledby="editdriv" aria-hidden="true" data-backdrop="static" style="margin-top:10%">'+
            '<div class="modal-dialog">'+
                '<div class="modal-content" style="border:10px solid #ccc">'+
                    '<div class="modal-header">'+
                        '<div class="close" data-dismiss="modal" ></div>'+
                        '<h4 class="modal-title">Edit Driver Name</h4>'+
                    '</div>'+
                    '<div class="modal-body">'+
                        '<div class="panel-body">'+
                            '<div id="driver_name">'+
                                '<p>Edit Driver Name: <input id="driveChange" type="text" class="form-control"></p>'+
                            '<br><br>'+
                            '</div>'+
                        '</div>'+
                    '</div>'+
                    '<div class="modal-footer">'+
                        '<div class="editsubmit btn btn-default" align="center" data-dismiss="modal" >Submit</div>'+
                    '</div>'+
                '</div>'+
            '</div>'+
          '</div>'+
          '<div class="modal fade" id="editques" tabindex="-1" role="dialog" aria-labelledby="editques" aria-hidden="true" data-backdrop="static" style="margin-top:10%">'+
            '<div class="modal-dialog">'+
                '<div class="modal-content" style="border:10px solid #ccc">'+
                    '<div class="modal-header">'+
                        '<div class="close" data-dismiss="modal" ></div>'+
                        '<h4 class="modal-title">Edit Question</h4>'+
                    '</div>'+
                    '<div class="modal-body">'+
                        '<div class="panel-body">'+
                            '<div id="driver_name">'+
                                '<p>Edit Question Name: <input id="quesChange" type="textarea" class="form-control"></p>'+
                            '<br><br>'+
                            '</div>'+
                            '<div><input type="checkbox" class="nFlagEditCheck"> To set \'N\' scoring on this Question</div>'+
                        '</div>'+
                    '</div>'+
                    '<div class="modal-footer">'+
                        '<div class="editsubmit btn btn-default" align="center" data-dismiss="modal" >Submit</div>'+
                    '</div>'+
                '</div>'+
            '</div>'+
          '</div>');
        this.createDriverQuestion();
 	},
    
    createDriverQuestion:function(){
        $("#pageloading").show();
        Backbone.ajax({
            dataType:"json",
            url:"drivers",
            data:"",
            type: "POST",
            success:function(data, textStatus, jqXHR){
                creatingDriverElement = Backbone.$("#createaccordion").empty();
                if(data.length == 0){
                    $("#pageloading").hide();
                } else {
                    createDriversView.generateDriverTable(data);
                }
            },
            error:function(res,ioArgs){
            	if(res.status == 403){
            		window.location.reload();
            	}
            }
        });
    },
        
    generateDriverTable:function(data){
        //get table id from jquery
        for(var i=0; i< data.length; i++){
            try{
                creatingDriverElement.append(
                    '<div class="panel panel-default">'+
                        '<div class="panel-heading">'+
                            '<div id="d'+data[i].did+'" class="panel-title dques">'+
                                '<span>'+data[i].desc+'</span>'+
                                '<span id="enabledriver'+data[i].did+'" class="enabledriverBox btn btn-default" align="center">Enable</span>'+
                                '<span id="disabledriver'+data[i].did+'" class="disabledriverBox btn btn-default" align="center">Disable</span>'+
                                '<span class="editdriver btn btn-default" align="center" data-toggle="modal" data-target="#editdriv">Edit</span>'+
                            '</div>'+
                        '</div>'+
                    '</div>'+
                    '<div id="q'+data[i].did+'" class="panel-body quesHide">'+
                        '<div id="driversid'+data[i].did+'" class="questiondrivers">'+
                            '<span class="btn addquestion" data-toggle="modal" data-target="#addquestionnew">'+
                                '<button class="btn btn-default" type="button">'+
                                    '<i class="fa fa-question"></i> Add new question'+
                                '</button>'+
                            '</span>'+
                        '</div>'+
                    '</div>');
            if(data[i].status==1){
                $("#enabledriver"+data[i].did+"").addClass("disabal");
                $("#disabledriver"+data[i].did+"").removeClass("disabal");
            } else if(data[i].status==2){
                $("#disabledriver"+data[i].did+"").addClass("disabal");
                $("#enabledriver"+data[i].did+"").removeClass("disabal");
            }
            creatingQuestionInAdmin = Backbone.$("#q"+data[i].did+"").children(".questiondrivers");
            createDriversView.questionGeneration(data[i].questions);
            }catch(e){}
        }
    },
    
    questionGeneration:function(qObj){
        for(var j=0; j< qObj.length ;j++){
            try{
                    creatingQuestionInAdmin.append("<div id='question"+qObj[j].qid+"' value='"+qObj[j].nflag+"' class='quedrive'><span style='float:left'>"+qObj[j].question+"</span><div id='enablequestion"+qObj[j].qid+"' class='enableBox btn btn-default' align='center'>Enable</div><div id='disablequestion"+qObj[j].qid+"' class='disableBox btn btn-default' align='center'>Disable</div><div class='editquestion btn btn-default' align='center' data-toggle='modal' data-target='#editques'>Edit</div></div><br>");
            }catch(e){
            }
            if(qObj[j].status==1){
                $("#enablequestion"+qObj[j].qid+"").addClass("disabal");
                $("#disablequestion"+qObj[j].qid+"").removeClass("disabal");
            } else if(qObj[j].status==2){
                $("#disablequestion"+qObj[j].qid+"").addClass("disabal");
                $("#enablequestion"+qObj[j].qid+"").removeClass("disabal");
            }
        }
        $("#pageloading").hide();
    },
    
    
    /*Accordion for Question Display acoording the Driver*/
    dquestionClick:function(e){
        var checkDriver = e.currentTarget.id;
        var quesDriv = checkDriver.slice(1)
        
        if($("#q"+quesDriv+"").attr('class')=="panel-body quesHide"){
            $("#q"+quesDriv+"").parent().parent().children().children(".quesShow").removeClass("quesShow").addClass("quesHide");
            $("#q"+quesDriv+"").removeClass("quesHide").addClass("quesShow");
            
        } else if($("#q"+quesDriv+"").attr('class')=="panel-body quesShow"){
            $("#q"+quesDriv+"").removeClass("quesShow").addClass("quesHide");
        }
    },
    
    addNewDriverBeforeSubmitted:function(e){
        if($("#adddrivernew").val()==0){
            sweetAlert("Please add driver name before submit");
        }
    },
    
    /*Adding new Driver for the List*/
    addNewDriver:function(e){
        $("#pageloading").show();
        var json;
        try{
            json={"desc":$("#adddrivernew").val()};
        }catch(e){}
        
        $.ajax({
            url: "adddriver",
            data: JSON.stringify(json),
            type: "POST",
            beforeSend: function(xhr) {
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-Type", "application/json");
            },
            success: function(data) {
                $("#pageloading").hide();
                if(data == 1){
                    $("#adddrivernew").val("");
                    createDriversView.createDriverQuestion();
                } else if(data == 2){
                    sweetAlert("Problem in adding wait for minute and try again!");
                } else{
                    sweetAlert(data);
                }
            },
            error:function(res,ioArgs){
            	if(res.status == 403){
            		window.location.reload();
            	}
            }
        });
    },
    
    /*Adding New Question for the Particular driver*/
    
    addNewQuestion:function(e){
        var newq = e.currentTarget.parentNode.id;
        drivid = newq.slice(9);
    },
    
    addNewQuestionBeforeSubmitted:function(e){
        if($("#addanewquestion").val()==0){
            sweetAlert("Please add question before submit");
        }
    },
    
    addNewQuestionSubmitted:function(e){
        var nflagset = 0;
        if($(".nFlagCheck").is(':checked')){
                nflagset = 1;
            }
        $("#pageloading").show();
        var json;
        try{
            json={"did":drivid,"question":$("#addanewquestion").val(),"nflag":nflagset};
        }catch(e){}
        
        $.ajax({
            url: "addquestion",
            data: JSON.stringify(json),
            type: "POST",
            beforeSend: function(xhr) {
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-Type", "application/json");
            },
            success: function(data) {
                $("#pageloading").hide();
                if(data==1){
                    $("#addanewquestion").val("");
                    createDriversView.createDriverQuestion();
                } else if(data==2){
                    sweetAlert("Problem in adding wait for minute and try again!");
                }else{
                    sweetAlert(data);
                }
            },
            error:function(res,ioArgs){
            	if(res.status == 403){
            		window.location.reload();
            	}
            }
        });
    },
    
    enableDriver:function(e){
        if($("#"+e.currentTarget.id+"").parent().children(".disabal").text() == "Enable"){
            sweetAlert("Oops!","This driver is already Enabled");
        } else {
            $("#pageloading").show();

            var didenable = e.currentTarget.parentNode.id;
            var didename = $($("#"+didenable+"").children()[0]).text();

            var json;
            try{
                json={"did":((didenable).slice(1)), "desc":didename, "status":1};
            }catch(e){}

            Backbone.ajax({
                url:"actdriver",
                type: "POST",
                data:JSON.stringify(json),
                beforeSend: function(xhr) {
                    xhr.setRequestHeader("Accept", "application/json");
                    xhr.setRequestHeader("Content-Type", "application/json");
                },
                success:function(data, textStatus, jqXHR){
                    $("#pageloading").hide();
                     if(data == 1){
                         createDriversView.createDriverQuestion();
                     } else if(data == 2){
                         sweetAlert("Problem in disabling wait for min and try again!");
                     } else{
                         sweetAlert(data);
                     }
                 },
                 error:function(res,ioArgs){
                 	if(res.status == 403){
                 		window.location.reload();
                 	}
                 }
             });
        }
    },
    
    disableDriver:function(e){
        if($("#"+e.currentTarget.id+"").parent().children(".disabal").text() == "Disable"){
            sweetAlert("Oops!","This driver is already Disable");
        } else {
            $("#pageloading").show();
            var diddisable = e.currentTarget.parentNode.id;
            var diddname = $($("#"+diddisable+"").children()[0]).text();

            var json;
            try{
                json={"did":((diddisable).slice(1)), "desc":diddname, "status":2};
            }catch(e){}

            Backbone.ajax({
                url:"deactdriver",
                type: "POST",
                data:JSON.stringify(json),
                beforeSend: function(xhr) {
                    xhr.setRequestHeader("Accept", "application/json");
                    xhr.setRequestHeader("Content-Type", "application/json");
                },
                success:function(data, textStatus, jqXHR){
                    $("#pageloading").hide();
                     if(data == 1){
                         createDriversView.createDriverQuestion();
                     } else if(data == 2){
                         sweetAlert("Problem in disabling wait for min and try again!");
                     } else{
                         sweetAlert(data);
                     }
                 },
                 error:function(res,ioArgs){
                 	if(res.status == 403){
                 		window.location.reload();
                 	}
                 }
             });
        }
    },
    
    /*Enabling the Question*/
    enableQuestion:function(e){
        if($("#"+e.currentTarget.id+"").parent().children(".disabal").text() == "Enable"){
            sweetAlert("Oops!","This question is already Enable");
        } else {
            $("#pageloading").show();
            var qidenable = e.currentTarget.parentNode.id;
            var quesename = $("#"+qidenable+"").children("span").text();

            var json;
            try{
                json={"qid":((qidenable).slice(8)), "question":quesename, "status":1};
            }catch(e){}


            Backbone.ajax({
                url:"actquestion",
                type: "POST",
                data:JSON.stringify(json),
                beforeSend: function(xhr) {
                    xhr.setRequestHeader("Accept", "application/json");
                    xhr.setRequestHeader("Content-Type", "application/json");
                },
                success:function(data, textStatus, jqXHR){
                    $("#pageloading").hide();
                     if(data == 1){
                         createDriversView.createDriverQuestion();
                     } else if(data == 2){
                         sweetAlert("Problem in disabling wait for min and try again!");
                     } else{
                         sweetAlert(data);
                     }
                 },
                 error:function(res,ioArgs){
                 	if(res.status == 403){
                 		window.location.reload();
                 	}
                 }
             });
        }
    },
    
    /*Disabling the Question*/
    disableQuestion:function(e){
        if($("#"+e.currentTarget.id+"").parent().children(".disabal").text() == "Disable"){
            sweetAlert("Oops!","This question is already Disable");
        } else {
            $("#pageloading").show();
            var qiddisable = e.currentTarget.parentNode.id;
            var quesdname = $("#"+qiddisable+"").children("span").text();

            var json;
            try{
                json={"qid":((qiddisable).slice(8)), "question":quesdname, "status":2};
            }catch(e){}

            Backbone.ajax({
                url:"deactquestion",
                type: "POST",
                data:JSON.stringify(json),
                beforeSend: function(xhr) {
                    xhr.setRequestHeader("Accept", "application/json");
                    xhr.setRequestHeader("Content-Type", "application/json");
                },
                success:function(data, textStatus, jqXHR){
                    $("#pageloading").hide();
                     if(data == 1){
                         createDriversView.createDriverQuestion();
                     } else if(data == 2){
                         sweetAlert("Problem in disabling wait for min and try again!");
                     } else{
                         sweetAlert(data);
                     }
                 },
                 error:function(res,ioArgs){
                 	if(res.status == 403){
                 		window.location.reload();
                 	}
                 }
             });
        }
    },
    
    /*Editting for the Quesion*/
    
    editQuestion:function(e){
        var quesid = e.currentTarget.parentElement.id;
        queid = quesid.slice(8);
        var disquesname = $("#"+quesid+"").children("span").text();
        Backbone.$("#quesChange").val(disquesname);
        if($("#"+quesid+"").attr('value') == 1){
            $(".nFlagEditCheck").prop("checked", true);
        } else {
            $(".nFlagEditCheck").prop("checked", false);
        }
    },
    
    /*Editing for the driver*/
    
    editDriver:function(e){
        var driveID = e.currentTarget.parentElement.id;
        drID = driveID.slice(1);
        var disdrivname = $($("#"+driveID+"").children()[0]).text();
        Backbone.$("#driveChange").val(disdrivname);
    },
    
    /*Validation for Edit Question and Driver*/
    
    editDriverBeforeSubmitted:function(e){
        var subCheckEdit = e.currentTarget.parentNode.parentNode.parentNode.parentNode.id;
        if(subCheckEdit == "editdriv"){
            if($("#driveChange").val() == 0){
                sweetAlert("Please add driver name before submit");
            }
        } else if(subCheckEdit == "editques"){
            if($("#quesChange").val() == 0){
                sweetAlert("Please add question before submit");
            }
        }
        
    },
    
    /*Editted Question and/or Driver Submitting to backend */
    
    editsubmitClick:function(e){
        $("#pageloading").show();
        var subCheckEdit = e.currentTarget.parentNode.parentNode.parentNode.parentNode.id;
        
        if(subCheckEdit == "editdriv"){
            var editted = $("#driveChange").val();
            var json;
            try{
                json={"did":drID,"desc":editted};
            }catch(e){}
            try{
                $.ajax({
                    url: "editdriver",
                    data: JSON.stringify(json),
                    type: "POST",
                    beforeSend: function(xhr) {
                        xhr.setRequestHeader("Accept", "application/json");
                        xhr.setRequestHeader("Content-Type", "application/json");
                    },
                    success: function(data) {
                        $("#pageloading").hide();
                        if(data==1){
                            createDriversView.createDriverQuestion();
                        } else if(data == 2){
                            sweetAlert("Problem in disabling wait for min and try again!");
                        } else{
                            sweetAlert(data);
                        }
                    },
                    error:function(res,ioArgs){
                    	if(res.status == 403){
                    		window.location.reload();
                    	}
                    }
                });
            }catch(e){}
        } else if(subCheckEdit == "editques"){
            var editted = $("#quesChange").val();
            var nflagset = 0;
            var json;
            
            if($(".nFlagEditCheck").is(':checked')){
                nflagset = 1;
            }
            try{
                json={"qid":queid,"question":editted,"nflag":nflagset};
            }catch(e){}
            try{
                $.ajax({
                    url: "editquestion",
                    data: JSON.stringify(json),
                    type: "POST",
                    beforeSend: function(xhr) {
                        xhr.setRequestHeader("Accept", "application/json");
                        xhr.setRequestHeader("Content-Type", "application/json");
                    },
                    success: function(data) {
                        $("#pageloading").hide();
                        if(data==1){
                            createDriversView.createDriverQuestion();
                        } else if(data == 2){
                            sweetAlert("Problem in disabling wait for min and try again!");
                        } else{
                            sweetAlert(data);
                        }
                    },
                    error:function(res,ioArgs){
                    	if(res.status == 403){
                    		window.location.reload();
                    	}
                    }
                });
            }catch(e){}
        }
    }
    
});