var generateExceptionTable, execeptiontableelement;

var ExceptionListView = Backbone.View.extend({

    events:{
        "click .removeexception":"removeException",
        "change .execptionfile":"exceptionUpload",
        "click #addexception":"addExceptionListPopup"
    },
 
	initialize:function(){
        _.bindAll(this,'render');
        i=1;
	},

	render:function(){
      this.$el.empty().append(
        '<div class="row">'+
            '<div class="col-lg-12">'+
                '<h1 class="page-header">Exception List</h1>'+					
            '</div>'+
          '</div>'+
        '<div class="row">'+
            '<div class="col-lg-3 col-md-6">'+
                '<button id="addexception" class="btn btn-default" type="button">'+
                    'Add Exception'+
                '</button>'+
            '</div>'+
            '<div class="col-lg-3 col-md-6">'+
                '<h5>Upload excel of exception list: </h5>'+
            '</div>'+
            '<div class="col-lg-3 col-md-6">'+
                '<form method="post" action="exceptionlistupload" enctype="multipart/form-data">'+
                    '<div class="btn btn-default btn-file">'+
                        '<i class="fa fa-upload">'+
                        '</i> &nbsp;Upload <input name="filename" type="file" class="execptionfile" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel,text/comma-separated-values, text/csv, application/csv">'+
                    '</div>'+
                '</form>'+
            '</div>'+
        '</div>'+
        '<div class="row">'+
            '<div class="col-lg-12">'+
                '<div class="panel">'+
                    '<div class="panel-body">'+
                        '<div class="table-responsive">'+
                            '<table id="exceptionTable" class="table table-hover">'+
                                '<thead>'+
                                    '<tr>'+
                                        '<th>Employee Id</th>'+
                                        '<th>Name</th>'+
                                        '<th>Email Id </th>'+
                                        '<th>Department</th>'+
                                        '<th>Grade</th>'+
                                        '<th>Action</th>'+
                                    '</tr>'+
                                '</thead>'+
                                '<tbody>'+
                                '</tbody>'+
                            '</table>'+
                        '</div>'+
                    '</div>'+
                '</div>'+
            '</div>'+
        '</div>'
      );
        exceptionListView.exceptionListTable();
 	},
    
    addExceptionListPopup:function(){
        selectingEmployeeView.render();
        selectingEmployeeView.clickparticipantList();
        $("#selectingRespondentModal").modal('show');
    },
    
    removeException:function(e){
        swal({   
            title: "Are you sure?",   
            text: "You want remove from exception list",
            showCancelButton: true,   
            confirmButtonColor: "#DD6B55",   
            confirmButtonText: "Yes, Remove it!",
        },
             function(isConfirm){
                if (isConfirm) {
                    var json;
                    try{
                        json={"eid":e.currentTarget.parentNode.parentNode.id};
                    }catch(e){}

                    Backbone.ajax({
                        dataType:"json",
                        url:"removeexception",
                        type: "POST",
                        data:JSON.stringify(json),
                        beforeSend: function(xhr) {
                            xhr.setRequestHeader("Accept", "application/json");
                            xhr.setRequestHeader("Content-Type", "application/json");
                        },
                         success:function(data, textStatus, jqXHR){
                        	 if(data=="1"){
                        		 exceptionListView.render();
                                 alert("Participant removed from Exception List sucessfully");
             				} else if(data=="2"){
                                 alert("Removing the Partcipant from Exception list failed");
             				} else{
                                 alert(data);
                             }
                         },
                         error:function(res,ioArgs){
                         	if(res.status == 403){
                         		window.location.reload();
                         	}
                         }
                    });
                }
        });
    },
    
    exceptionListTable:function(e){
        $("#pageloading").show();
        Backbone.ajax({
            dataType:"json",
            url:"exceptionlist",
            data:"",
            type: "POST",
            success:function(data, textStatus, jqXHR){
                execeptiontableelement = Backbone.$("#exceptionTable").children("tbody").empty();
                if(data.length == 0){
                    $("#pageloading").hide();
                } else {
                    exceptionListView.generateExceptionTable(data);
                    $("#exceptionTable").dataTable();
                }
            },
            error:function(res,ioArgs){
            	if(res.status == 403){
            		window.location.reload();
            	}
            }
        });
    },
    
    generateExceptionTable:function(data){
        //get table id from jquery
        for(var i=0; i< data.length; i++){
            execeptiontableelement.append(exceptionListView.createExceptionRow(data[i]));
        }
        $("#pageloading").hide();
    },
    
    createExceptionRow:function(rowObject){
        try{
            var trElement = "<tr id='"+rowObject.eid+"'><td>"+rowObject.eid+"</td><td>"+rowObject.name+"</td><td>"+rowObject.emailId+"</td><td>"+rowObject.department+"</td>><td>"+rowObject.grade+"</td><td><div 'style='color:#F01E1E' class='btn btn-default removeexception'>Remove</div></td>";
            return trElement+"</tr>";
        }catch(e){}
    },
    
    exceptionUpload:function(e){
        $("#pageloading").show();
        $(e.currentTarget).parent().parent().ajaxForm({
			success : function(data) {
				if(data=="1"){
                    exceptionListView.render();
                    sweetAlert("Exception List uploaded sucessfully");
				} else if(data=="2"){
                    sweetAlert("Exception List upload failed");
				} else{
                    sweetAlert(data);
                }
			},
            error:function(res,ioArgs){
            	if(res.status == 403){
            		window.location.reload();
            	}
            },
			dataType : "text"
		}).submit();
        $("#pageloading").hide();
    }
});