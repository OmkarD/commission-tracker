var sessionElement, updatesessiontemp, sessionUpdateFlag;

var AddSessionView = Backbone.View.extend({

    events:{
        "click .addsession":"addSessionModal",
        "click .updatesession":"upDateSession",
        "mousedown .sessionsubmit":"sessionBeforeSubmit",
        "mouseup .sessionsubmit":"sessionSubmit"
    },
 
	initialize:function(){
        _.bindAll(this,'render');
        sessionUpdateFlag=true;
	},

	render:function(){
      this.$el.empty().append(
        '<div class="row">'+
            '<div class="col-lg-12">'+
                '<h1 class="page-header">Add Session</h1>'+					
            '</div>'+
        '</div>'+
        '<div class="row">'+
            '<div class="col-lg-3">'+
                '<button class="addsession btn btn-default" type="button" data-toggle="modal" data-target="#descrip">'+
                    '<i class="fa fa-calendar"></i> Add Session'+
                '</button>'+
            '</div>'+
        '</div>'+
        '<div class="row">'+
            '<div class="col-lg-12">'+
                '<div class="panel">'+
                    '<div class="panel-body">'+
                        '<div class="table-responsive">'+
                            '<table id="sessiontable" class="table table-hover">'+
                                '<thead>'+
                                    '<tr>'+
                                        '<th>Description</th>'+
                                        '<th>Start Session</th>'+
                                        '<th>End Session</th>'+
                                        '<th>Update</th>'+
				                    '</tr>'+
				                '</thead>'+
				                '<tbody>'+
				                '</tbody>'+
				            '</table>'+
                        '</div>'+
                    '</div>'+
                '</div>'+
            '</div>'+
        '</div>'+
          '<div class="modal fade" id="descrip" tabindex="-1" role="dialog" aria-labelledby="descrip" aria-hidden="true" data-backdrop="static" style="margin-top:10%">'+
            '<div class="modal-dialog">'+
                '<div class="modal-content" style="border:10px solid #ccc">'+
                    '<div class="modal-header">'+
                        '<div class="close" data-dismiss="modal" ></div>'+
                        '<h4 class="modal-title">Add Session</h4>'+
                    '</div>'+
                    '<div class="modal-body">'+
                        '<div class="panel-body">'+
                            '<div class="row">'+
                                '<div class="col-lg-12">'+
                                    '<div class="input-group">'+
                                        '<h5>Session start:</h5>'+
                                        '<input type="text" name="selected_date" id="datepickerstart" readonly/>'+
                                    '</div>'+
                                '</div>'+
                                '<div class="col-lg-12">'+
                                    '<div class="input-group">'+
                                        '<h5>Session end:</h5>'+
                                        '<input type="text" name="selected_date" id="datepickerend" readonly/>'+
                                    '</div>'+
                                '</div>'+
                            '</div>'+
                            '<div id="descrip_id">'+
                                '<p>    Name for the session: <input id="description" type="textarea" class="form-control"></p>'+
                            '<br><br>'+
                            '</div>'+
                        '</div>'+
                    '</div>'+
                    '<div class="modal-footer">'+
                        '<div class="btn btn-default" align="center" data-dismiss="modal">Cancel</div>'+
                        '<div class="sessionsubmit btn btn-default" align="center">Submit</div>'+
                    '</div>'+
                '</div>'+
            '</div>'+
          '</div>');
        addSessionView.sessionTable();
        addSessionView.dateValidation();        
 	},
    
    addSessionModal: function(){
        sessionUpdateFlag = true;
        $("#description").val("");
        $("#datepickerstart").val("");
        $("#datepickerend").val(""); 
    },
    
    sessionTable:function(e){
        $("#pageloading").show();
        Backbone.ajax({
            dataType:"json",
            url:"sessionlist",
            data:"",
            type: "POST",
            success:function(data, textStatus, jqXHR){
                sessionElement = Backbone.$("#sessiontable").children("tbody").empty();
                addSessionView.generateSessionTable(data);
                $("#sessiontable").dataTable();
            },
            error:function(res,ioArgs){
            	if(res.status == 403){
            		window.location.reload();
            	}
            }
        });
    },
    
    generateSessionTable:function(data){
        //get table id from jquery
        for(var i=0; i< data.length; i++){
            sessionElement.append(addSessionView.createSessionRow(data[i]));
        }
        $("#pageloading").hide();
    },
    
    createSessionRow:function(rowObject){
        try{
            var trElement = "<tr id='session"+rowObject.id+"'><td>"+rowObject.description+"</td><td>"+rowObject.startTime+"</td><td>"+rowObject.endTime+"</td><td><div class='btn btn-default updatesession' data-toggle='modal' data-target='#descrip'>Update</div></td>";
            return trElement+"</tr>";
        }catch(e){}
    },
    
    
    sessionBeforeSubmit:function(){
        if($("#datepickerstart").val()==0){
            sweetAlert("Please provide start time");
        } else if($("#datepickerend").val()==0){
            sweetAlert("Please provide end time");
        }else if ($("#description").val()==0){
            sweetAlert("Add description for reffering the session");
        } 
    },
    
    upDateSession:function(e){
        sessionUpdateFlag = false;
        updatesessiontemp = e.currentTarget.parentNode.parentNode.id;
        $("#description").val($($("#"+updatesessiontemp+"").children("td")[0]).text());
        $("#datepickerstart").val($($("#"+updatesessiontemp+"").children("td")[1]).text());
        $("#datepickerend").val($($("#"+updatesessiontemp+"").children("td")[2]).text());
    },
    
    /*Adding discription with Session*/
    sessionSubmit:function(){
        if(sessionUpdateFlag){
            $("#pageloading").show();
            var json;
            try{
                json={"description":($("#description").val()),"startTime":($("#datepickerstart").val()),"endTime":($("#datepickerend").val())};
            }catch(e){}

            $.ajax({
                url: "addsession",
                data: JSON.stringify(json),
                type: "POST",
                beforeSend: function(xhr) {
                    xhr.setRequestHeader("Accept", "application/json");
                    xhr.setRequestHeader("Content-Type", "application/json");
                },
                success: function(data) {
                    $("#datepickerstart").val("");
                    $("#description").val("");
                    $("#datepickerend").val("");
                    if(data == 1){
                        $("#descrip").modal("hide");
                        $("#descrip").on('hidden.bs.modal', function() {
                            addSessionView.render();
                        });
                    } else if(data == 2){
                        sweetAlert("Problem in adding wait for minute and try again!");
                    }else if(data == 4){
                        sweetAlert("Cannot create another session if other session is Active!");
                    } else if(data == 5){
                        sweetAlert("Please check weather the drivers are present or enabled");
                    }  else{
                        sweetAlert(data);
                    }
                    
                    $("#pageloading").hide();
                },
                error:function(res,ioArgs){
                	if(res.status == 403){
                		window.location.reload();
                	}
                }
            });
        } else{
            sessionUpdateFlag=true;
            $("#pageloading").show();
            var json;
            try{
                json={"id":updatesessiontemp.slice(7),"description":($("#description").val()),"startTime":($("#datepickerstart").val()),"endTime":($("#datepickerend").val())};
            }catch(e){}

            $.ajax({
                url: "editsession",
                data: JSON.stringify(json),
                type: "POST",
                beforeSend: function(xhr) {
                    xhr.setRequestHeader("Accept", "application/json");
                    xhr.setRequestHeader("Content-Type", "application/json");
                },
                success: function(data) {
                    $("#datepickerstart").val("");
                    $("#description").val("");
                    $("#datepickerend").val("");
                    if(data == 1){
                        $("#descrip").modal("hide");
                        $("#descrip").on('hidden.bs.modal', function() {
                            addSessionView.render();
                        });
                    } else if(data == 2){
                        sweetAlert("Problem in adding wait for minute and try again!");
                    } else{
                        sweetAlert(data);
                    }
                    
                    $("#pageloading").hide();
                },
                error:function(res,ioArgs){
                	if(res.status == 403){
                		window.location.reload();
                	}
                }
            });
        }
    },
    
    dateValidation:function(){
        $("#datepickerstart").datepicker({
            dateFormat: "yy-m-dd",
            minDate: 0,
            onSelect: function (date) {
                var dt2 = $('#datepickerend');
                var startDate = $(this).datepicker('getDate');
                var minDate = $(this).datepicker('getDate');
                //dt2.datepicker('setDate', minDate);
                startDate.setDate(startDate.getDate() + 3000);
                //sets dt2 maxDate to the last day of 30 days window
                dt2.datepicker('option', 'maxDate', startDate);
                dt2.datepicker('option', 'minDate', minDate);
                $(this).datepicker('option', 'minDate', minDate);
            }
        });
        $('#datepickerend').datepicker({
            dateFormat: "yy-m-dd"
        });
    }
    
});