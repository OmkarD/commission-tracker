var listofParticipentElement, row, selectingValue, participentCount, participentid, filter_respondent;
var json = [];
var SelectingEmployeeView = Backbone.View.extend({
  
    events:
    {
        "click .addexectionsubmit":"addToExceptionList",
        "click .respondentCheck":"checkAlert",
        "click .selectemplyeeBox":"selectedBox"
    },
    initialize:function(){
        _.bindAll(this,'render');
        this.render();
        selectingValue=0;
    },

    render:function()
    {
        this.$el.empty().append(
            '<div class="modal fade" id="selectingRespondentModal" tabindex="-1" role="dialog" aria-labelledby="selectingRespondentModal" aria-hidden="true" data-backdrop="static">'+
                        '<div class="modal-dialog" style="width: 70%">'+
                            '<div class="modal-content">'+
                                '<div class="modal-header">'+
                                    '<div class="close" data-dismiss="modal" ></div>'+
                                    '<h4 class="modal-title headerlist">Employee List</h4>'+
                                '</div>'+
                                '<div class="modal-body">'+
                                    '<div class="panel-body">'+
                                        '<div class="table-responsive">'+
                                            '<table id="selectingEmployeeTable" class="table table-hover display">'+
                                                '<thead>'+
                                                    '<tr>'+
                                                        '<th>Emp id</th>'+
                                                        '<th>Name</th>'+
                                                        '<th>Position</th>'+
                                                        '<th>Grade</th>'+
                                                        '<th>Select</th>'+
                                                    '</tr>'+
                                                '</thead>'+
                                                '<tbody>'+
                                                '</tbody>'+
                                            '</table>'+
                                        '</div>'+
                                    '</div>'+
                                '</div>'+
                                '<div class="modal-footer">'+
                                    '<div class="addexectionsubmit btn btn-default disabal" style="display:none">Submit</div>'+
                                    '<div class="selectemplyeeBox btn btn-default disabal" style="display:none">Submit</div>'+
                                    '<div class="cancelemployeeBox btn btn-success" align="center" data-dismiss="modal">Cancel</div>'+
                                '</div>'+
                            '</div>'+
                        '</div>'+
                    '</div>');
    },
    
    clickparticipantList:function(){
        json = [];
        $("#pageloading").show();
        $(".selectemplyeeBox").hide();
        $(".addexectionsubmit").show();
        Backbone.$(".addexectionsubmit").addClass("disabal");
        Backbone.$(".selectemplyeeBox").addClass("disabal");
        Backbone.ajax({
            dataType:"json",
            url:"listparticipantexception",
            data:"",
            type: "POST",
            success:function(data, textStatus, jqXHR){
                listofParticipentElement = Backbone.$("#selectingEmployeeTable").children("tbody").empty();
                selectingEmployeeView.generateParticipentTable(data);
            },
            error:function(res,ioArgs){
            	if(res.status == 403){
            		window.location.reload();
            	}
            }
        });
    },
    
    clickEmployeeList:function(pratyid, filid){
        json = [];
        Backbone.$(".addexectionsubmit").addClass("disabal");
        Backbone.$(".selectemplyeeBox").addClass("disabal");
        $(".selectemplyeeBox").show();
        $(".addexectionsubmit").hide();
        participentid = pratyid;
        if(filid == "Managers"){
            $(".headerlist").text("Managers");
            filter_respondent = "DM";
        } else if(filid == "Reportee(s)"){
            $(".headerlist").text("Reportee(s)");
            filter_respondent = "DR";
        } else if(filid == "Peers"){
            $(".headerlist").text("Peers");
            filter_respondent = "PE";
        } else if(filid == "Stakeholders"){
            $(".headerlist").text("Stakeholders");
            filter_respondent = "SH";
        }
        
        row=0;
        $("#pageloading").show();
        Backbone.ajax({
            dataType : "json",
            url:"selectrespondent?pid="+participentid+"&relationship="+filter_respondent+"",
            data:"",
            type: "POST",
            success:function(data, textStatus, jqXHR){
                listofParticipentElement = $("#selectingEmployeeTable").children("tbody").empty();
                selectingEmployeeView.generateParticipentTable(data);
            },
            error:function(res,ioArgs){
            	if(res.status == 403){
            		window.location.reload();
            	}
            }
        });
    },
    
    generateParticipentTable:function(data){
            //get table id from jquery
        for(var i=0; i< data.length; i++){
            listofParticipentElement.append(selectingEmployeeView.createRow(data[i]));
        }
        $("#pageloading").hide();
        $("#selectingEmployeeTable").dataTable();
    },
    
    createRow:function(rowObject){
        try{
            var trElement = "<tr id='"+rowObject.eid+"'><td>"+rowObject.eid+"</td><td>"+rowObject.name+"</td><td>"+rowObject.department+"</td><td>"+rowObject.grade+"</td><td><input type=checkbox class=respondentCheck id='respondentCheck"+row+"'></td>";
            row++;
            return trElement+"</tr>";
        }catch(e){}
    },

    checkAlert:function(e){
        var selectioncheckId = e.currentTarget.id;
        var checkRespondent = $(".respondentCheck:checked").length;
            if(checkRespondent < 1){
                Backbone.$(".addexectionsubmit").addClass("disabal");
                Backbone.$(".selectemplyeeBox").addClass("disabal");
            } else {
                Backbone.$(".addexectionsubmit").removeClass("disabal");
                Backbone.$(".selectemplyeeBox").removeClass("disabal");
            }
        
        if(e.currentTarget.checked){
            try{
                json.push({"eid":e.currentTarget.parentElement.parentElement.id});
            }catch(e){} 
        } else {
            try{
                this.removeArray(e.currentTarget.parentElement.parentElement.id);
            }catch(e){}  
        }
    },
    
    selectedBox:function(e){
        if($(".selectemplyeeBox").attr('class') == "selectemplyeeBox btn btn-default disabal"){
            $("#selectingRespondentModal").modal('hide');
            swal({
                title:"Oops!", 
                text:"Please select at least 1 respondent",
            },
                 function(){
                    $("#selectingRespondentModal").modal('show');
            });
        } else { 
            $("#selectingRespondentModal").modal('hide');
            $("#pageloading").show();
            Backbone.ajax({
                url: "addrespondent?pid="+participentid+"&relationship="+filter_respondent+"",
                data: JSON.stringify(json),
                type: "POST",
                beforeSend: function(xhr) {
                    xhr.setRequestHeader("Accept", "application/json");
                    xhr.setRequestHeader("Content-Type", "application/json");
                },
                success: function(data) {
                     $("#pageloading").hide();
                     if(data==1){
                         participantView.loadRespondent(participentid);
                     } else if(data==2){
                         sweetAlert("please Try Again!!")
                     } else if(data==5){
                         sweetAlert("Selected More than "+maxlimit+" Plese unselect some respondent!")
                     }  else{
                         sweetAlert(data);
                     }
                },
                error:function(res,ioArgs){
                	if(res.status == 403){
                		window.location.reload();
                	}
                }
            });
        }
    },
    
    addToExceptionList:function(e){
        if($(".addexectionsubmit").attr('class') == "addexectionsubmit btn btn-default disabal"){
            $("#selectingRespondentModal").modal('hide');
            swal({
                title:"Oops!", 
                text:"Please select at least 1 participant",
            },
                 function(){
                    $("#selectingRespondentModal").modal('show');
            });
        } else {
            $("#selectingRespondentModal").modal('hide');
            $("#pageloading").show();
            Backbone.ajax({
                url: "addexception",
                data: JSON.stringify(json),
                type: "POST",
                beforeSend: function(xhr) {
                    xhr.setRequestHeader("Accept", "application/json");
                    xhr.setRequestHeader("Content-Type", "application/json");
                },
                success: function(data) {
                    $("#pageloading").hide();
                    if(data==1){
                        exceptionListView.render();
                        $("#exceptionadd").val("");
                    } else if(data==2){
                        sweetAlert("please Try Again!!")
                    } else{
                        sweetAlert(data);
                    }             
                },
                error:function(res,ioArgs){
                	if(res.status == 403){
                		window.location.reload();
                	}
                }
            });
        }
    },
    
    removeArray: function(v) {  
        for(var i=0;i<json.length;i++){ 
            if(json[i].eid == v){
                json.splice(i, 1);
            }
        }
    }
});