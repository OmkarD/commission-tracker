var participantTableElement, gradeAdminLTFlag = false, peCountLimit, shCountLimit;

var ParticipantView = Backbone.View.extend({
    events:
    {
        "click .partcls":"sendParticipantId",
        "click #back_participant":"comeBackParticipant",
        "click .dm_re_cls":"dmRespondent",
        "click .dr_re_cls":"drRespondent",
        "click .pe_re_cls":"peRespondent",
        "click .sh_re_cls":"shRespondent",
        "click .all_re_cls":"allRespondent",
        "click .del_respondent":"deleteRespondent",
        "click .add_respond":"addRespondent",
        "click #edit_respons":"editRespondent",
        "mousedown .reset_selfassesment":"clickResetSelfAssesment"
    },
    initialize:function(){
        _.bindAll(this,'render');
    },

    render:function(){
        this.$el.empty().append( 
            '<div class="row list_select">'+
                '<div class="col-lg-12">'+
                '<h1 id="participantview_header" class="page-header">Participant List</h1>'+
                '<div id="back_participant" class="pull-right btn btn-default tollexp" title="Back to participant list"><a> Back</a></div>'+
                '</div>'+
                '<!-- /.col-lg-12 -->'+
            '</div>'+
            '<!-- /.row -->'+
            '<div id="party_table"></div>'+
            '<div id="respons_table"></div>');
        
        this.tableCreateforParticipant();
    },
    
    tableCreateforParticipant:function(){
        Backbone.$("#party_table").empty().append(
            '<div class="row">'+
                '<div class="col-lg-12">'+
                    '<div class="panel">'+
                        '<div class="panel-body">'+
                           '<div class="table-responsive">'+
                                '<table id="participant_table" class="table table-hover">'+
                                    '<thead>'+
                                        '<tr>'+
                                            '<th>Employee Id</th>'+
                                            '<th>Name</th>'+
                                            '<th>Email Id </th>'+
                                            '<th>Department</th>'+
                                            '<th>Grade</th>'+
                                        '</tr>'+
                                    '</thead>'+
                                    '<tbody>'+
                                    '</tbody>'+
                                '</table>'+
                            '</div>'+
                        '</div>'+
                    '</div>'+
                '</div>'+
            '</div>');
        this.participantListTable();
    },
    
    participantListTable:function(e){
    	$("#participantview_header").text("Participant List");
        $("#pageloading").show();
        Backbone.ajax({
            dataType:"json",
            url:"participantlist",
            data:"",
            type: "POST",
            success:function(data, textStatus, jqXHR){
                participantTableElement = Backbone.$("#participant_table").children("tbody").empty();
                if(data.length == 0){
                    $("#pageloading").hide();
                } else {
                    participantView.generateParticipantTable(data);
                }
            },
            error:function(res,ioArgs){
            	if(res.status == 403){
            		window.location.reload();
            	}
            }
        });
    },
    
    generateParticipantTable:function(data){
        //get table id from jquery
        for(var i=0; i< data.length; i++){
            participantTableElement.append(participantView.createParticipantRow(data[i]));
        }
        $("#participant_table").dataTable({
            "paging":   false,
            "ordering": false,
            "info":     true,
            "scrollY":  "300",
            "scrollCollapse": true
        });
        $("#pageloading").hide();
        $("#back_participant").hide();
    },
    
    createParticipantRow:function(rowObject){
        try{
            var trElement = "<tr id='participanteid"+rowObject.eid+"' class='partcls'><td>"+rowObject.eid+"</td><td>"+rowObject.name+"</td><td>"+rowObject.emailId+"</td><td>"+rowObject.department+"</td><td>"+rowObject.grade+"</td>";
            return trElement+"</tr>";
        }catch(e){}
    },
    
    respondentsListTable:function(rowObj, respid){
        Backbone.$("#respons_table").empty().append(
            '<div class="row list_select">'+
                '<div class="col-lg-6">'+
                    '<p>Filter</p>'+
                    '<div id="filt_rel" class="dropdown btn btn-default" style="text-align: left; width: 200px; padding: 2px 12px">'+
                        '<div role="button" class="relation-dropdown" data-toggle="dropdown">'+
                            '<span>All</span>'+
                        '</div>'+
                            '<span class="caret" style="margin-top: -12px; float: right;"></span>'+
                        '<ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">'+
                            '<li>'+
                                '<a class="all_re_cls">All</a>'+
                            '</li>'+
                            '<li>'+
                                '<a class="dm_re_cls">Managers</a>'+
                            '</li>'+
                            '<li>'+
                                '<a class="dr_re_cls">Reportee(s)</a>'+
                            '</li>'+
                            '<li>'+
                                '<a class="pe_re_cls">Peers</a>'+
                            '</li>'+
                            '<li>'+
                                '<a class="sh_re_cls">Stakeholders</a>'+
                            '</li>'+
                        '</ul>'+
                    '</div>'+
                '</div>'+
                '<!-- /.col-lg-6 -->'+
                '<div><a id="resetself'+respid+'" class="reset_selfassesment"> Click here </a> in order to reset the self assesment for <span> </span></div>'+
                '<div class="col-lg-6 add_rpond" style="padding: 30px; display:none">'+
                    '<a id="add_respondent'+respid+'" class="add_respond">Click here</a> to add a new respondent'+
                '</div>'+
                '<!-- /.col-lg-6 -->'+
            '</div>'+
            '<!-- /.row -->'+
            '<div class="row">'+
                '<div class="col-lg-12">'+
                    '<div class="panel">'+
                        '<div class="panel-body">'+
                           '<div class="table-responsive">'+
                                '<table id="respondent_table" class="table table-hover">'+
                                    '<thead>'+
                                        '<tr>'+
                                            '<th>Name</th>'+
                                            '<th>Relationship</th>'+
                                            '<th>Status</th>'+
                                            '<th>Active/Deactive</th>'+
                                            '<th>Reset Feedback</th>'+
                                        '</tr>'+
                                    '</thead>'+
                                    '<tbody>'+
                                    '</tbody>'+
                                '</table>'+
                            '</div>'+
                        '</div>'+
                    '</div>'+
                '</div>'+
            '</div>');
        
        if(gradeAdminLTFlag){
             $(".sh_re_cls").hide();
        }
        
        var resopondentElement = Backbone.$("#respondent_table").children("tbody").empty();
        
        for(var i=0; i< rowObj.length; i++){
            resopondentElement.append(
                '<tr id="respondent'+rowObj[i].rid+'" class="responcls respon'+rowObj[i].relationship+'">'+
                    '<td>'+rowObj[i].rname+'</td>'+
                    '<td></td>'+
                    '<td></td>'+
                    '<td id="respart'+rowObj[i].rid+'"><div class="del_respondent btn btn-default" value='+rowObj[i].pid+'></div></td>'+
                    '<td></td>'+
                '</tr>'
            );
            
            if(rowObj[i].validFlag == "1"){
                $("#respart"+rowObj[i].rid+"").children(".del_respondent").text("Deactivate");
            } else if(rowObj[i].validFlag == "2"){
                $("#respart"+rowObj[i].rid+"").children(".del_respondent").text("Activate");
            }
            
            if(rowObj[i].relationship == "DM"){
                $($("#respondent"+rowObj[i].rid+"").children("td")[1]).text("Direct Manager");
            } else if(rowObj[i].relationship == "DR" ){
                $($("#respondent"+rowObj[i].rid+"").children("td")[1]).text("Direct Reportee");
            } else if(rowObj[i].relationship == "PE"){
                $($("#respondent"+rowObj[i].rid+"").children("td")[1]).text("Peer");
                if(rowObj[i].validFlag == "1"){
                    peCountLimit++;
                }
            } else if(rowObj[i].relationship == "SH"){
                $($("#respondent"+rowObj[i].rid+"").children("td")[1]).text("Stakeholders");
                if(rowObj[i].validFlag == "1"){
                    shCountLimit++;
                }
            } 
            
            if(rowObj[i].status == 0){
                $($("#respondent"+rowObj[i].rid+"").children("td")[2]).text("Pending");
            } else if(rowObj[i].status == 1){
                $($("#respondent"+rowObj[i].rid+"").children("td")[2]).text("Accepted");
            } else if(rowObj[i].status == 2){
                $($("#respondent"+rowObj[i].rid+"").children("td")[2]).text("Rejected");
            } else if(rowObj[i].status == 3){
                $($("#respondent"+rowObj[i].rid+"").children("td")[4]).empty().append(
                    '<div id="edit_respons" class="btn btn-default">Reset</div>'
                );
                $($("#respondent"+rowObj[i].rid+"").children("td")[2]).text("Completed");
            }
        }
        $("#pageloading").hide();
        $("#back_participant").show();
        $(".reset_selfassesment").parent().children("span").text($("#participantview_header").text());
    },
    
    sendParticipantId:function(e){
        gradeAdminLTFlag = false;
        var pid = (e.currentTarget.id).slice(14);
        var nameparty = $($("#"+e.currentTarget.id+"").children("td")[1]).text();
        $("#participantview_header").text(nameparty);
        participantView.checkStepStatusResponednt(pid);
        if ($($("#"+e.currentTarget.id+"").children("td")[4]).text() == "LT"){
            gradeAdminLTFlag = true;
        }
    },
    
    checkStepStatusResponednt: function(reisd){
        Backbone.ajax({
            url:"isselectioncompleted?pid="+reisd+"",
            type: "POST",
            dataType:"json",
             success:function(data, textStatus, jqXHR){
                 if(data == 1){
                     participantView.loadRespondent(reisd);
                     Backbone.$("#respons_table").show();
                 } else {
                     $("#participantview_header").text("Participant List");
                    Backbone.$("#respons_table").hide();
                    sweetAlert("Oops!", "Participant has not selected his/her respondents yet");
                 }
             }
        });
    },
    
    loadRespondent:function(respid){
        peCountLimit = 0;
        shCountLimit = 0;
        Backbone.ajax({
            url:"respondentsforparticipant?pid="+respid+"",
            type: "POST",
            dataType:"json",
             success:function(data, textStatus, jqXHR){
                if(data == 1){
                    sweetAlert("Oops!", "There is some problem while saving data!");
                } else if(data == 0){
                    Backbone.$("#party_table").hide();
                    $("#pageloading").show();
                    participantView.respondentsListTable(data, respid);
                    $("#respondent_table").dataTable({
                        "paging":   false,
                        "ordering": false,
                        "info":     true,
                        "scrollY":  "300",
                        "scrollCollapse": true
                    });
                } else {
                    Backbone.$("#party_table").hide();
                    $("#pageloading").show();
                    participantView.respondentsListTable(data, respid);
                    $("#respondent_table").dataTable({
                        "paging":   false,
                        "ordering": false,
                        "info":     true,
                        "scrollY":  "300",
                        "scrollCollapse": true
                    });
                }
             },
             error:function(res,ioArgs){
             	if(res.status == 403){
             		window.location.reload();
             	}
             }
         });   
    },
    
    comeBackParticipant:function(e){
        $("#participantview_header").text("Participant List");
        Backbone.$("#party_table").show();
        $("#back_participant").hide();
        Backbone.$("#respons_table").hide();
    },
    
    dmRespondent:function(){
        $(".responcls").hide();
        $(".responDM").show();
        $(".add_rpond").show();
        $($("#filt_rel").children("div").children("span")[0]).text("Managers");
    },
    
    drRespondent:function(){
        $(".responcls").hide();
        $(".responDR").show();
        $(".add_rpond").show();
        $($("#filt_rel").children("div").children("span")[0]).text("Reportee(s)");
    },
    
    peRespondent:function(){
        $(".responcls").hide();
        $(".responPE").show();
        $(".add_rpond").show();
        $($("#filt_rel").children("div").children("span")[0]).text("Peers");
    },
    
    shRespondent:function(){
        $(".responcls").hide();
        $(".responSH").show();
        $(".add_rpond").show();
        $($("#filt_rel").children("div").children("span")[0]).text("Stakeholders");
    },
    
    allRespondent:function(){
        $(".responcls").show();
        $(".add_rpond").hide();
        $($("#filt_rel").children("div").children("span")[0]).text("All");
    },
    
    deleteRespondent:function(e){
        var pid = $("#"+e.currentTarget.parentElement.id+"").children().attr('value');
        var id = (e.currentTarget.parentElement.parentElement.id).slice(10);
        var relatid = $("#"+e.currentTarget.parentElement.id+"").parent().attr('class').slice(16, 18);
        
        if (e.currentTarget.textContent == "Deactivate"){
                swal({   
                title: "Are you sure?",
                showCancelButton: true,   
                confirmButtonColor: "#DD6B55",   
                confirmButtonText: "Yes",
            },
                 function(isConfirm){
                    if (isConfirm) {
                        Backbone.ajax({
                            url: "deactrespondent?id="+id+"&pid="+pid+"&relationship="+relatid+"",
                            type: "POST",
                            dataType:"json",
                             success:function(data, textStatus, jqXHR){
                                if(data == 1){
                                    participantView.loadRespondent(pid);
                                } else { 
                                    sweetAlert("Oops!", "There is some problem while deactivating try again!");
                                }
                             },
                             error:function(res,ioArgs){
                             	if(res.status == 403){
                             		window.location.reload();
                             	}
                             }
                         });   
                    } 
            });
        } else if (e.currentTarget.textContent == "Activate"){
           swal({   
                title: "Are you sure?",
                showCancelButton: true,   
                confirmButtonColor: "#DD6B55",   
                confirmButtonText: "Yes",
            },
                 function(isConfirm){
                    if (isConfirm) {
                        Backbone.ajax({
                            url: "actrespondent?id="+id+"&pid="+pid+"&relationship="+relatid+"",
                            type: "POST",
                            dataType:"json",
                             success:function(data, textStatus, jqXHR){
                                if (data == 1){
                                    participantView.loadRespondent(pid);
                                } else if(data == 5) {
                                     alert("Maximum no off respondent already selected");
                                } else {
                                    alert("There is some problem while activate try again!");
                                }
                             },
                             error:function(res,ioArgs){
                             	if(res.status == 403){
                             		window.location.reload();
                             	}
                             }
                         });   
                    } 
            }); 
        }  
    },
    
    addRespondent:function(e){
         var pid = (e.currentTarget.id).slice(14);
         var filter_id = $($("#filt_rel").children("div").children("span")[0]).text();
         if( filter_id == "All"){
             sweetAlert("Oops!", "Please select filter to add respondent");
         } else if(filter_id == "Managers"){
             this.selectingBefore(pid, filter_id);
         } else if(filter_id == "Reportee(s)"){
             this.selectingBefore(pid, filter_id);
         } else if(filter_id == "Peers" ){
             if(peCountLimit >= maxlimit){
                 sweetAlert("Oops!", "Aleared "+maxlimit+" Respondent are active");
             } else {
                this.selectingBefore(pid, filter_id);
             }
         } else if(filter_id == "Stakeholders"){
             if(shCountLimit >= maxlimit){
                 sweetAlert("Oops!", "Aleared "+maxlimit+" Respondent are active");
             } else {
                this.selectingBefore(pid, filter_id);
             }
         }
    },
    
    selectingBefore: function(pid, filter_id){
        swal({   
            title: "Are you sure?",   
            text: "Do you want to add "+filter_id+" as respondent",
            showCancelButton: true,   
            confirmButtonColor: "#DD6B55",   
            confirmButtonText: "Yes",
        },
             function(isConfirm){
            if (isConfirm) {
                selectingEmployeeView.render();
                selectingEmployeeView.clickEmployeeList(pid, filter_id);
                $("#selectingRespondentModal").modal('show');   
            } 
        });
    },
    
    editRespondent:function(e){
        var pid = $($(e.currentTarget).parent().parent().children("td")[3]).children().attr('value');
        var id = (e.currentTarget.parentElement.parentElement.id).slice(10);
        swal({   
            title: "Are you sure?",
            showCancelButton: true,   
            confirmButtonColor: "#DD6B55",   
            confirmButtonText: "Yes",
        },
             function(isConfirm){
                if (isConfirm) {
                    Backbone.ajax({
                        url: "feedbackanswerreset?pid="+pid+"&rid="+id+"",
                        type: "POST",
                        dataType:"json",
                         success:function(data, textStatus, jqXHR){
                            if(data == 1){
                                swal({
                                    title: "Reseted...!!",
                                    text: "Feedback from this respondent is rested",
                                    timer: 1000 
                                });
                                participantView.loadRespondent(pid);
                            } else {
                                sweetAlert("Oops!", "There is some problem while reseting try again!");
                            }
                         },
                         error:function(res,ioArgs){
                         	if(res.status == 403){
                         		window.location.reload();
                         	}
                         }
                     });   
                } 
        });
    },
    
    clickResetSelfAssesment:function(e){
        var pid = (e.currentTarget.id).slice(9);
        swal({   
            title: "Are you sure?",   
            text: "You want to reset self assesment for "+$("#participantview_header").text()+"",
            showCancelButton: true,   
            confirmButtonColor: "#DD6B55",   
            confirmButtonText: "Yes",
        },
             function(isConfirm){
                if (isConfirm) {
                    Backbone.ajax({
                        url: "feedbackanswerreset?pid="+pid+"&rid="+pid+"",
                        type: "POST",
                        dataType:"json",
                         success:function(data, textStatus, jqXHR){
                            if(data == 1){
                                swal({
                                    title: "Reseted...!!",
                                    text: "Self assesment is reseted for "+$("#participantview_header").text()+"",
                                    timer: 1000 
                                });
                                participantView.loadRespondent(pid);
                            } else {
                                sweetAlert("Oops!", "There is some problem while Reseting try again!");
                            }
                         },
                         error:function(res,ioArgs){
                         	if(res.status == 403){
                         		window.location.reload();
                         	}
                         }
                     });    
                } 
        });
    }
    
});
                          
                      
                      