var adminTableElement, deleteAdminTemp;

var ManageAdminView = Backbone.View.extend({

    events:{
        "mousedown .adminstatus":"admitBeforeSubmit",
        "mouseup .adminstatus":"admitSubmit",
        "click .deleteadmin":"deleteAdmin",
        "click .deletesubmit":"deleteSubmitId",
        "click .activatebox":"activAdmin",
        "click .diactivatebox":"deActivAdmin"
    },
 
	initialize:function(){
        _.bindAll(this,'render');
        i=1;
	},

	render:function(){
      this.$el.empty().append(
          '<div class="row">'+
            '<div class="col-lg-12">'+
                '<h1 class="page-header">Users</h1>'+					
            '</div>'+
        '</div>'+
        '<div class="row">'+
            '<div class="col-lg-12">'+
                '<div class="panel">'+
                    '<div class="panel-body">'+
                        '<div class="btn btn-default btnaddadmin" data-toggle="modal" data-target="#addadmin">Add Admin</div>'+
                        '</div>'+
                        '<div class="table-responsive">'+
                            '<table id="adminusertable" class="table table-hover">'+
                                '<thead>'+
                                    '<tr>'+
                                        '<th style="text-align: center">Admin Id</th>'+
                                        '<th style="text-align: center">Actions</th>'+
				                    '</tr>'+
				                '</thead>'+
				                '<tbody>'+
				                '</tbody>'+
				            '</table>'+
                        '</div>'+
                    '</div>'+
                '</div>'+
            '</div>'+
        '</div>'+
      '<div class="modal fade" id="addadmin" tabindex="-1" role="dialog" aria-labelledby="addadmin" aria-hidden="true" data-backdrop="static" style="margin-top:10%">'+
            '<div class="modal-dialog">'+
                '<div class="modal-content" style="border:10px solid #ccc">'+
                    '<div class="modal-header">'+
                        '<div class="close" data-dismiss="modal" ></div>'+
                        '<h4 class="modal-title">Add Admin</h4>'+
                    '</div>'+
                    '<div class="modal-body">'+
                        '<div class="panel-body">'+
                            '<div id="driver_name">'+
                                '<p>Add Admin Id: <input id="adminid" type="textarea" class="form-control"></p>'+
                            '<br>'+
                            '<br>'+
                            '</div>'+
                        '</div>'+
                    '</div>'+
                    '<div class="modal-footer">'+
                        '<div class="adminstatus btn btn-default" align="center" data-dismiss="modal" >Submit</div>'+
                    '</div>'+
                '</div>'+
            '</div>'+
          '</div>'+
      '<div class="modal fade" id="deleteid" tabindex="-1" role="dialog" aria-labelledby="deleteid" aria-hidden="true" data-backdrop="static" style="margin-top:10%">'+
            '<div class="modal-dialog">'+
                '<div class="modal-content" style="border:10px solid #ccc">'+
                    '<div class="modal-header">'+
                        '<div class="close" data-dismiss="modal" ></div>'+
                        '<h3 class="modal-title">Delete Admin</h3>'+
                        'Are you sure, you want delete admin with Admin Id <span id="deletingid"></span>'+
                    '</div>'+
                    '<div class="modal-footer">'+
                        '<div class="deletesubmit btn btn-default" align="center" data-dismiss="modal">delete</div>'+
                    '</div>'+
                '</div>'+
            '</div>'+
          '</div>');
        manageAdminView.adminTable();
 	},
    
    adminTable:function(e){
        $("#pageloading").show();
        Backbone.ajax({
            dataType:"json",
            url:"adminlist",
            data:"",
            type: "POST",
            success:function(data, textStatus, jqXHR){
                adminTableElement = Backbone.$("#adminusertable").children("tbody").empty();
                manageAdminView.generateAdminTable(data);
            },
            error:function(res,ioArgs){
            	if(res.status == 403){
            		window.location.reload();
            	}
            }
        });
    },
    
    generateAdminTable:function(data){
        //get table id from jquery
        for(var i=0; i< data.length; i++){
            try{
                adminTableElement.append("<tr id='admin"+data[i].userId+"' style='text-align: center'><td>"+data[i].userId+"</td><td><div 'style='color:#F01E1E' class='btn btn-default deleteadmin' data-toggle='modal' data-target='#deleteid'>Delete</div></td></tr>");
                
                /*<div id='activeadmin"+data[i].userId+"' class='activatebox btn btn-default' align='center'>Activate</div><div id='deactiveadmin"+data[i].userId+"' class='diactivatebox btn btn-default' align='center'>Deactivate</div>if(data[i].status==1){
                    $("#activeadmin"+data[i].userId+"").addClass("disabal");
                    $("#deactiveadmin"+data[i].userId+"").removeClass("disabal");
                } else if(data[i].status==2){
                    $("#deactiveadmin"+data[i].userId+"").addClass("disabal");
                    $("#activeadmin"+data[i].userId+"").removeClass("disabal");
                }*/
            }catch(e){
                
            }
        }
        $("#pageloading").hide();
    },
    
    admitBeforeSubmit:function(){
        if($("#adminid").val()==0){
            sweetAlert("Please provide admin Id");
        } 
    },
    
    admitSubmit:function(e){
        
        $("#pageloading").show();
            var json;
            try{
                json={"userId":($("#adminid").val()),"status":2};
            }catch(e){}

            $.ajax({
                url: "addadmin",
                data: JSON.stringify(json),
                type: "POST",
                beforeSend: function(xhr) {
                    xhr.setRequestHeader("Accept", "application/json");
                    xhr.setRequestHeader("Content-Type", "application/json");
                },
                success: function(data) {
                    $("#pageloading").hide();
                    $("#adminid").val("");
                    if(data == 1){
                        manageAdminView.adminTable();
                    } else if(data == 2){
                        sweetAlert("Problem in adding wait for minute and try again!");
                    } else{
                        sweetAlert(data);
                    }
                },
                error:function(res,ioArgs){
                	if(res.status == 403){
                		window.location.reload();
                	}
                }
            });
    },
    
    deleteAdmin:function(e){
        deleteAdminTemp = e.currentTarget.parentNode.parentNode.id.slice(5);
        $("#deletingid").text(deleteAdminTemp)
    },
    
    deleteSubmitId:function(e){
        var json;
            try{
                json={"userId":deleteAdminTemp};
            }catch(e){}

            $.ajax({
                url: "deleteadmin",
                data: JSON.stringify(json),
                type: "POST",
                beforeSend: function(xhr) {
                    xhr.setRequestHeader("Accept", "application/json");
                    xhr.setRequestHeader("Content-Type", "application/json");
                },
                success: function(data) {
                    $("#pageloading").hide();
                    if(data == 1){
                        manageAdminView.adminTable();
                    } else if(data == 2){
                        sweetAlert("Problem in adding wait for minute and try again!");
                    } else{
                        sweetAlert(data);
                    }
                },
                error:function(res,ioArgs){
                	if(res.status == 403){
                		window.location.reload();
                	}
                }
            });
    },
    
   /* activAdmin:function(e){
        
        if($("#"+e.currentTarget.id+"").parent().children(".disabal").text() == "Activate"){
            sweetAlert("Oops!","This admin already activated");
        } else {
            $("#pageloading").show();
            var idActiveTemp = e.currentTarget.parentNode.parentNode.id.slice(5);

            var json;
            try{
                json={"userId":idActiveTemp, "status":1};
            }catch(e){}

            Backbone.ajax({
                url:"editadmin",
                type: "POST",
                data:JSON.stringify(json),
                beforeSend: function(xhr) {
                    xhr.setRequestHeader("Accept", "application/json");
                    xhr.setRequestHeader("Content-Type", "application/json");
                },
                success:function(data, textStatus, jqXHR){
                    $("#pageloading").hide();
                     if(data == 1){
                         manageAdminView.adminTable();
                     } else if(data == 2){
                         sweetAlert("Problem in disabling wait for min and try again!");
                     } else{
                         sweetAlert(data);
                     }
                 },
            error:function(res,ioArgs){
            	if(res.status == 403){
            		window.location.reload();
            	}
            }
             });
        }
    },
    */
    /*deActivAdmin:function(e){
        
        if($("#"+e.currentTarget.id+"").parent().children(".disabal").text() == "Deactivate"){
            sweetAlert("Oops!","This admin already deactivated");
        } else {
            $("#pageloading").show();
            var idDeActiveTemp = e.currentTarget.parentNode.parentNode.id.slice(5);

            var json;
            try{
                json={"userId":idDeActiveTemp, "status":2};
            }catch(e){}

            Backbone.ajax({
                url:"editadmin",
                type: "POST",
                data:JSON.stringify(json),
                beforeSend: function(xhr) {
                    xhr.setRequestHeader("Accept", "application/json");
                    xhr.setRequestHeader("Content-Type", "application/json");
                },
                success:function(data, textStatus, jqXHR){
                    $("#pageloading").hide();
                     if(data == 1){
                         manageAdminView.adminTable();
                     } else if(data == 2){
                         sweetAlert("Problem in disabling wait for min and try again!");
                     } else{
                         sweetAlert(data);
                     }
                 },
            error:function(res,ioArgs){
            	if(res.status == 403){
            		window.location.reload();
            	}
            }
             });
        }
    }*/
});