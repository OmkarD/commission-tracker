

var ExcelUploadView = Backbone.View.extend({

    events:{
        "change .file":"fileLoad",
        "change .filename":"fileLoadPeerandstakemap",
       
    },
 
	initialize:function(){
        _.bindAll(this,'render');
        i=1;
	},

	render:function(){
      this.$el.empty().append(
        '<div class="row">'+
            '<div class="col-lg-12">'+
                '<h1 class="page-header">Upload Excel File</h1>'+					
            '</div>'+
        '</div>'+
        '<div class="col-md-6">'+
            '<div class="col-md-6">'+
                '<div class="panel panel-pink">'+
                    '<div class="panel-heading">'+
                        '<div class="row">'+
                            '<div class="col-xs-3">'+
                                '<i class="fa fa-list-alt fa-5x"></i>'+
                            '</div>'+
                            '<div class="col-xs-9 text-right">'+
                                '<div>Employee list</div>'+
                            '</div>'+
                        '</div>'+
                    '</div>'+
                    '<div class="panel-footer">'+
                        '<form method="post" action="employeemasterupload" enctype="multipart/form-data">'+
                            '<div class="input-group-btn">'+
                                '<div class="btn panel-pink btn-file">'+
                                    '<i class="glyphicon glyphicon-folder-open">'+
                                    '</i> &nbsp;Browse  <input id="uploadempId" name="filename" type="file" class="file" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel,text/comma-separated-values, text/csv, application/csv">'+
                                '</div>'+
                            '</div>'+
                        '</form>'+
                    '</div>'+
                '</div>'+
            '</div>'+
   
             '<div class="col-md-6">'+
                '<div class="panel panel-primary">'+
                    '<div class="panel-heading">'+
                        '<div class="row">'+
                            '<div class="col-xs-3">'+
                                '<i class="fa fa-file-text-o fa-5x"></i>'+
                            '</div>'+
                            '<div class="col-xs-9 text-right">'+
                                '<div>Participant list</div>'+
                            '</div>'+
                        '</div>'+
                    '</div>'+
                    '<div class="panel-footer">'+
                        '<form method="post" action="participantmasterupload" enctype="multipart/form-data">'+
                            '<div class="input-group-btn">'+
                                '<div class="btn panel-primary btn-file">'+
                                    '<i class="glyphicon glyphicon-folder-open">'+
                                    '</i> &nbsp;Browse  <input id="uploadparticipantId" name="filename" type="file" class="file" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel,text/comma-separated-values, text/csv, application/csv">   '+
                                '</div>'+
                            '</div>'+
                        '</form>'+
                    '</div>'+
                '</div>'+
            '</div>'+
          '</div>'+
        '<div class="col-md-6">'+
            '<div class="panel panel-default">'+
                '<div class="panel-heading">'+
                    '<h4>Excel Template</h4>'+
                '</div>'+
                '<div class="panel-body">'+
                    '<div class="list-group-item">'+
                        '<a href="exlempexp">Click Here</a> to download "Employee Master" excel upload format'+
                    '</div>'+
                    '<div class="list-group-item">'+
                        '<a href="exlpartiexp">Click Here</a> to download "Participant Master" excel upload format'+
                    '</div>'+
                '</div>'+
            '</div>'+
        '</div>'+
  /*--------------------------------------- 05/01/2017--------------------------------------------------------------------*/
       '<div class="col-md-6">'+
            '<div class="col-md-6">'+
                '<div class="panel panel-pink">'+
                    '<div class="panel-heading">'+
                        '<div class="row">'+
                            '<div class="col-xs-3">'+
                                '<i class="fa fa-list-alt fa-5x"></i>'+
                            '</div>'+
                            '<div class="col-xs-9 text-right">'+
                                '<div>Peer map</div>'+
                            '</div>'+
                        '</div>'+
                    '</div>'+
                    '<div class="panel-footer">'+
                        '<form method="post" action="peerupload" enctype="multipart/form-data">'+
                            '<div class="input-group-btn">'+
                                '<div class="btn panel-pink btn-file">'+
                                    '<i class="glyphicon glyphicon-folder-open">'+
                                    '</i> &nbsp;Browse  <input id="uploadempId" name="filename" type="file" class="filename" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel,text/comma-separated-values, text/csv, application/csv">'+
                                '</div>'+
                            '</div>'+
                        '</form>'+
                    '</div>'+
                '</div>'+
            '</div>'+
             '<div class="col-md-6">'+
                '<div class="panel panel-primary">'+
                    '<div class="panel-heading">'+
                        '<div class="row">'+
                            '<div class="col-xs-3">'+
                                '<i class="fa fa-file-text-o fa-5x"></i>'+
                            '</div>'+
                            '<div class="col-xs-9 text-right">'+
                                '<div>Stakeholder map</div>'+
                            '</div>'+
                        '</div>'+
                    '</div>'+
                    '<div class="panel-footer">'+
                        '<form method="post" action="stakeupload" enctype="multipart/form-data">'+
                            '<div class="input-group-btn">'+
                                '<div class="btn panel-primary btn-file">'+
                                    '<i class="glyphicon glyphicon-folder-open">'+
                                    '</i> &nbsp;Browse  <input id="uploadparticipantId" name="filename" type="file" class="filename" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel,text/comma-separated-values, text/csv, application/csv">   '+
                                '</div>'+
                            '</div>'+
                        '</form>'+
                    '</div>'+
                '</div>'+
            '</div>'+
          '</div>'+
          '<div class="col-md-6">'+
          '<div class="panel panel-default">'+
              /*'<div class="panel-heading">'+
                  '<h4>Excel Template</h4>'+
              '</div>'+*/
              '<div class="panel-body">'+
                  '<div class="list-group-item">'+
                      '<a href="peeruploadtemplate">Click Here</a> to download "Peer Map" excel upload format'+
                  '</div>'+
                  '<div class="list-group-item">'+
                      '<a href="stakeuploadtemplate">Click Here</a> to download "Stakeholder Map" excel upload format'+
                  '</div>'+
              '</div>'+
          '</div>'+
      '</div>'
     /*--------------------------------------- end--------------------------------------------------------------------*/   
       );
 	},
    
    fileLoad:function(e){
         $("#pageloading").show();
        var excelname = $(e.currentTarget).parent().parent().parent().attr("action");
        $(e.currentTarget).parent().parent().parent().ajaxForm({
            contentType:'application/xsls; boundary=AaB03x',
			success : function(data) {
				if(data=="1"){
                    $("#pageloading").hide();
                    if(excelname=="participantmasterupload"){
                        swal({
                            title: "Successfull",
                            text: "Participant list uploaded successfully",
                            confirmButtonText: "Ok"
                        },
                        function(){
                            excelUploadView.render();
                        });
                    } else if(excelname=="employeemasterupload"){
                        swal({
                            title: "Successfull",
                            text: "Employee list uploaded successfully",
                            confirmButtonText: "Ok"
                        },
                        function(){
                            excelUploadView.render();
                        });
                    }
				} else if(data=="5"){
					$("#pageloading").hide();
					 sweetAlert("Check weather the Session is Active or Not");
				} else if(data=="2"){
                     $("#pageloading").hide();
                   if(excelname=="participantmasterupload"){
                        swal({
                            title: "Failed",
                            text: "Participant list upload failed",
                            confirmButtonText: "Ok"
                        },
                        function(){
                            excelUploadView.render();
                        });
                    } else if(excelname=="employeemasterupload"){
                        swal({
                            title: "Failed",
                            text: "Employee list upload failed",
                            confirmButtonText: "Ok"
                        },
                        function(){
                            excelUploadView.render();
                        });
                    }
				} else{
                    $("#pageloading").hide();
                    sweetAlert(data);
                }
			},
            error:function(res,ioArgs){
            	if(res.status == 403){
            		window.location.reload();
            	}
            },
			dataType : "text"
		}).submit();
    },
    
   /*------------------------------------------- 05/01/2017----------------------------------------------------------*/
    fileLoadPeerandstakemap:function(e){
        $("#pageloading").show();
       var excelname = $(e.currentTarget).parent().parent().parent().attr("action");
       $(e.currentTarget).parent().parent().parent().ajaxForm({
           contentType:'application/xsls; boundary=AaB03x',
			success : function(data) {
				if(data=="1"){
                   $("#pageloading").hide();
                   if(excelname=="stakeupload"){
                       swal({
                           title: "Successfull",
                           text: "Stakeholder map uploaded successfully",
                           confirmButtonText: "Ok"
                       },
                       function(){
                           excelUploadView.render();
                       });
                   } else if(excelname=="peerupload"){
                       swal({
                           title: "Successfull",
                           text: "Peer map uploaded successfully",
                           confirmButtonText: "Ok"
                       },
                       function(){
                           excelUploadView.render();
                       });
                   }
				} else if(data=="5"){
					$("#pageloading").hide();
					 sweetAlert("Check weather the Session is Active or Not");
				} else if(data=="2"){
                    $("#pageloading").hide();
                  if(excelname=="stakeupload"){
                       swal({
                           title: "Failed",
                           text: "Stakeholder map upload failed",
                           confirmButtonText: "Ok"
                       },
                       function(){
                           excelUploadView.render();
                       });
                   } else if(excelname=="peerupload"){
                       swal({
                           title: "Failed",
                           text: "Peer map upload failed",
                           confirmButtonText: "Ok"
                       },
                       function(){
                           excelUploadView.render();
                       });
                   }
				} else{
                   $("#pageloading").hide();
                   sweetAlert(data);
               }
			},
           error:function(res,ioArgs){
           	if(res.status == 403){
           		window.location.reload();
           	}
           },
			dataType : "text"
		}).submit();
   },
   

   /*-------------------------------------------end----------------------------------------------------------*/   
});
