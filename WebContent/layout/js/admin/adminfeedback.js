var liElement, queElement, ansElement, rid, rpid, questionDisplayFlag;

var AdminFeedBackView = Backbone.View.extend({
  
    events:
    {
        "click .answer":"ansClick",
		"click .fbsubmit":"submitAns",
		"click .fbprevious":"previousAns"
    },
    initialize:function(){
        _.bindAll(this,'render')
        var pid;
        var resid;
        questionDisplayFlag = false;
    },

    render:function(paid, repid)
    {
        this.resid = repid;
		this.pid = paid;
		rid = repid;
		rpid = paid;
        this.$el.empty().append(
            '<div class="modal" id="adminfBModal" tabindex="-1" role="dialog" aria-labelledby="adminfBModal" aria-hidden="true" data-backdrop="static">'+
                '<div class="modal-dialog feedback-popup">'+
                    '<div class="modal-content">'+
                        '<div class="modal-header">'+
                            '<div class="close" data-dismiss="modal" ></div>'+
                            '<h4 class="modal-title">Feedback For <span id="participantadminname"></span> from <span id="responadminname"></span></h4>'+
                        '</div>'+
                        '<div class="modal-body">'+
                            '<div id="feedbackTab" class="panel-body">'+
                            '<span>'+
                                '<h5><strong>Please click on one of the below listed rating</strong></h5>'+
                                '<h5>A = Rarely displays this behavior. Would display this behavior in less than 4 cases out of 10</h5>'+
                                '<h5>B = Occasionally displays this behavior. Would display this behavior in 4-5 cases out of 10</h5>'+
                                '<h5>C = Often displays this behavior. Would display this behavior in 6-8 cases out of 10</h5>'+
                                '<h5>D = Almost always displays this behavior. Would display this behavior in 9 or more cases out of 10</h5>'+
                                '<h5 class="drdmhide" style="display:none">N = No interaction on this parameter</h5>'+
                            '</span>'+
                                    '<ul class="nav nav-tabs" role="navigation">'+
                                        '<!--tabs -->'+
                                    '</ul>'+
                                '<div class="tab-content panel-group change-id" id="feedBackQuestion">'+
                                '</div>'+
                            '</div>'+
                        '</div>'+
                        '<div class="modal-footer">'+
                            '<p class="btn btn-success fbprevious disabled" align="center">Previous</p>'+
                            '<p class="fbsubmit btn btn-success " align="center">Next</p>'+
                        '</div>'+
                    '</div>'+
                '</div>'+
            '</div>');
		this.feedFirst();
    },
    
    feedFirst:function(e){
		var prtcpant=$($("#respondent"+this.resid+"").children()[0]).text()
		var nameparty = $("#participantview_header").text()
		$("#responadminname").text(prtcpant);
        $("#participantadminname").text(nameparty);
            Backbone.ajax({
                dataType:"json",
                url:"drivers",
                type: "POST",
                success:function(data, textStatus, jqXHR){
                    adminFeedBackView.generateLi(data);
                },
                error:function(res,ioArgs){
                	if(res.status == 403){
                		window.location.reload();
                	}
                }
            });
    },
    
    generateLi:function(data){
        liElement = $("#feedbackTab").children("ul").empty();
        //get table id from jquery
        for(var i=0; i< data.length; i++){
            try{
                if(i==0){
                    questionDisplayFlag = true;
                liElement.append("<li id='fbdid"+data[i].did+"' class='fbdriver active'><a  >"+data[i].desc+"</a></li>");
                } else {
                    questionDisplayFlag = false;
                    liElement.append("<li id='fbdid"+data[i].did+"' class='fbdriver disabled'><a  >"+data[i].desc+"</a></li>");
                }
            }catch(e){
            }
            queElement = $("#feedbackTab").children("div");
            adminFeedBackView.generateQuestion(data[i].questions);
        }
        this.setAnswer(data[0].did);
    },
    
    generateQuestion:function(queObj){
        for(var j=0; j< queObj.length ;j++){
            try{
                if(questionDisplayFlag) {
                    queElement.append("<div id='question"+queObj[j].qid+"' class='tab-pane fade in active question fbdid"+queObj[j].did+"'><a data-toggle='collapse' data-parent='#feedBackQuestion' href='#collapseOne' class='nav question-num1 change-parent' style='cursor: default'><h5 style='font-weight:bold'>"+queObj[j].question+"</h5><div id='ansId1' class='answer question"+queObj[j].qid+"'><p>A</p></div><div id='ansId2' class='answer question"+queObj[j].qid+"'><p>B</p></div><div id='ansId3' class='answer question"+queObj[j].qid+"'><p>C</p></div><div id='ansId4' class='answer question"+queObj[j].qid+"'><p>D</p></div><div id='ansId5' class='answer question"+queObj[j].qid+"'><p>N</p></div><br></a><p></p></div>");
                }
                else {
                    queElement.append("<div id='question"+queObj[j].qid+"' class='tab-pane question fbdid"+queObj[j].did+"'><a data-toggle='collapse' data-parent='#feedBackQuestion' href='#collapseOne' class='nav question-num1 change-parent' style='cursor: default'><h5 style='font-weight:bold'>"+queObj[j].question+"</h5><div id='ansId1' class='answer question"+queObj[j].qid+"'><p>A</p></div><div id='ansId2' class='answer question"+queObj[j].qid+"'><p>B</p></div><div id='ansId3' class='answer question"+queObj[j].qid+"'><p>C</p></div><div id='ansId4' class='answer question"+queObj[j].qid+"'><p>D</p></div><div id='ansId5' class='answer question"+queObj[j].qid+"'><p>N</p></div><br></a><p></p></div>");
                }
                if(!directManagerReporteeFlag){
                	$(".drdmhide").show();
                    if(queObj[j].nflag == 1){
                        $("#question"+queObj[j].qid+"").children("a").children("#ansId5").show();
                    } else {
                          $("#question"+queObj[j].qid+"").children("a").children("#ansId5").hide();  
                    }
                }
            }catch(e){
            }
        }
        
        $("#pageloading").hide();
    },
    
    ansClick:function(e){
        var ansParentID = e.currentTarget.parentElement.parentNode.id;
        var ansID = e.currentTarget.id;
        var ansNum = (ansID.slice(5));
        var autoAns = $("#"+ansParentID+"").children('p').empty().html(optionModel.get(ansNum));
        
        $(e.currentTarget).parent().children(".replay").removeClass("replay");
        $(e.currentTarget).addClass("replay");
    },
    
    submitAns:function(e){
		try{
			var json=[];
			var d=($(".fbdriver.active").attr("id").split("fbdid")[1]);
			var qarry=$(".fbdid"+d+".active");
			for(var i=0;i<qarry.length;++i){
				try{
				var q=$(qarry[i]).attr("id").split("question")[1];
				var a=$($(qarry[i]).children("a").children(".replay")).attr("id").split("ansId")[1];
					json.push({"qid":q,"ans":a,"pid":this.pid});
				}catch(e){
                    $("#adminfBModal").modal('hide');
                    swal({
                        title: "Feedback incomplete...!",
                        text: "Please answer all the questions before submitting",
                        confirmButtonText: "Ok"
                    },
                        function(){
                        $("#adminfBModal").modal('show');
                    });
					return;
				}
			}
			$.ajax({
						url: "feedbackansweradmin?id="+this.resid+"",
						data: JSON.stringify(json),
						type: "POST",
						 
						beforeSend: function(xhr) {
							xhr.setRequestHeader("Accept", "application/json");
							xhr.setRequestHeader("Content-Type", "application/json");
						},
						success: function(smartphone) {
							 if(smartphone==1){
								 $(".fbprevious").removeClass("disabled");
								if($("#fbdid"+(++d)+".fbdriver").length){
									
									$("#fbdid"+(--d)+".fbdriver").removeClass("active").addClass("disabled");
									
									qarry=$(".fbdid"+d+".active");
									for(var i=0;i<qarry.length;++i){
											$(qarry[i]).removeClass("active").addClass("disabled");
									}
									$("#fbdid"+(++d)+".fbdriver").removeClass("disabled").addClass("active");
									qarry=$(".fbdid"+d+"");
									for(var i=0;i<qarry.length;++i){
											$(qarry[i]).removeClass("disabled").addClass("active");
									}
									adminFeedBackView.setAnswer(d);
								}else{
									
                                    $.ajax({
						                url:"feedbackansweradminfinalsubmit?pid="+rpid+"&rid="+rid,
						                type: "POST",
						                success:function(data, textStatus, jqXHR){
						                	if(data==1){
                                                $("#adminfBModal").modal('hide');
                                                participantView.loadRespondent(rpid);
                                                sweetAlert("Feedback saved", "Continue to respondent menu");
						                	}else{
                                                sweetAlert("", "There is some problem while saving data!");
						                	}
										
						                }
						            });
								}
								 
							 }else{
                                 sweetAlert("Oops!", "There is some problem while saving data!");
							 }
						},
			            error:function(res,ioArgs){
			            	if(res.status == 403){
			            		window.location.reload();
			            	}
			            }
					});
					  
		}catch(e){
			
		}
    },
	previousAns:function(){
		var d=($(".fbdriver.active").attr("id").split("did")[1]);
		if($("#fbdid"+(--d)+".fbdriver").length){
			$("#fbdid"+(++d)+".fbdriver").removeClass("active").addClass("disabled");
			qarry=$(".fbdid"+d+".active");
			for(var i=0;i<qarry.length;++i){
					$(qarry[i]).removeClass("active").addClass("disabled");
			}
			$("#fbdid"+(--d)+".fbdriver").removeClass("disabled").addClass("active");
			qarry=$(".fbdid"+d+"");
			for(var i=0;i<qarry.length;++i){
					$(qarry[i]).removeClass("disabled").addClass("active");
			}
			if($("#fbdid"+(--d)+".fbdriver").length){}else{
			 $(".fbprevious").addClass("disabled");
			}
		}else{
			
		}
			
	},
    
	setAnswer:function(d){
		$.ajax({
			url: "getfeedbackansweradmin?pid="+this.pid+"&id="+this.resid+"&driver="+d+"",
			success: function(data) {
                $("#pageloading").hide();
                if(data == 1){
                    sweetAlert("Oops!", "There is some problem while saving data!");
                } else if(data == 2){
                    sweetAlert("Oops!", "There is some problem while saving data!");
                } else {
                	for(var i=0;i<data.length;i++){
                        $("#question"+data[i].qid).children("a").children("#ansId"+data[i].ans).addClass("replay");
                    }
                }
			},
            error:function(res,ioArgs){
            	if(res.status == 403){
            		window.location.reload();
            	}
            }
		});	
		
	}
    
});