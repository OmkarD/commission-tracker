    var generateEmailElementBody, generateEmailElementSubject, generateEmailType, typeid, typeofid;

    var EmailTemplateView = Backbone.View.extend({

        events:{
            "click .mailclick":"openMailModal",
            "click .submitemail":"onSubmitEmail",
            "click .cancelemail": "cancleEmailT"
        },

        initialize:function(){
            _.bindAll(this,'render');
        },

        render:function(){
          this.$el.empty().append(
            '<div class="row">'+
                '<div class="col-lg-12">'+
                    '<h1 class="page-header">Email Template List</h1>'+					
                '</div>'+
            '</div>'+
            '<div id="communicationtype" class="col-lg-12">'+
                '<div class="panel panel-default">'+
                    '<div class="panel-heading">'+
                        '<h4>Email Template</h4>'+
                    '</div>'+
                    '<div id="emailtypes" class="panel-body">'+
                        
                    '</div>'+
                '</div>'+
            '</div>'+
            '<div id="emailTemplateModal" style="display:none">'+
                '<div class="row">'+
                    '<div class="btn btn-default cancelemail" style="float:right">Back</div>'+
                '</div>'+
                '<div class="row">'+
                    '<div class="col-lg-4">'+
                        '<div class="panel">'+
                            '<div class="panel-header">'+
                                '<h5><b>In Order to add name and table use related Id below</b></h5>'+
                            '</div>'+
                            '<div class="panel-body">'+
                                '<table class="table table-hover display">'+
                                    '<tbody>'+
                                        '<tr><th>ID</th><th>Description</th></tr>'+
                                        '<tr class="mailsid participantname"><td>@@participantname@@</td><td>Name of participant</td></tr>'+
                                        '<tr class="mailsid respondentname"><td>@@respondentname@@</td><td> Name of Respondent</td></tr>'+
                                        '<tr class="mailsid managername"><td>@@managername@@</td><td>Name of Manager</p>'+
                                        '<tr class="mailsid managermail"><td>@@departmentmailtable@@</td><td>List of selected respondent from participant</td></tr>'+
                                        '<tr class="mailsid participantfollow"><td>@@participantfollowmailtable@@</td><td>List of Respondent chosen by the participant</td></tr>'+
                                        '<tr class="mailsid repondantfollow"><td>@@respondfollowmailtable@@</td><td>List of Repondents recived</td></tr>'+
                                    '</tbody>'+
                                '</table>'+
                            '</div>'+
                        '</div>'+
                    '</div>'+
                    '<div class="col-lg-8">'+
                        '<div class="panel">'+
                            '<div class="panel-header">'+
                                '<h4>Template</h4>'+
                            '</div>'+
                            '<div class="panel-body">'+
                                'Subject: <input type="text" id="templateSubject" style="height: 30px; width: 90%"><br><br><br>'+
                                'Body: <div id="templateBody" class="panel-body" style="min-height: 225px;"></div>'+
                            '</div>'+
                            '<div class="panel-footer">'+
                                '<div class="btn btn-default submitemail" style="float:right">Submit</div>'+
                                '<div class="btn btn-default cancelemail">Cancel</div>'+
                            '</div>'+
                        '</div>'+
                    '</div>'+
                '</div>'+
            '</div>'
          );
            this.emailTypeGenerate();
        },

        emailTypeGenerate:function(){
            $("#pageloading").show();
            Backbone.ajax({
                dataType:"json",
                url:"listemailtypes",
                data:"",
                type: "POST",
                success:function(data, textStatus, jqXHR){
                    generateEmailType = $("#emailtypes").empty();
                    try{
                        for(var i=0; i< data.length; i++){
                            generateEmailType.append(
                                '<div class="list-group-item">'+
                                    '<a id="emid'+data[i].id+'" class="mailclick" value="'+data[i].description+'">Click Here</a> to '+data[i].description+' template'+
                                '</div>'
                            );
                        }
                    }catch(e){}
                    
                    $("#pageloading").hide();
                },
                error:function(res,ioArgs){
                	if(res.status == 403){
                		window.location.reload();
                	}
                }
            });
        },
        
        openMailModal:function(e){
            $("#pageloading").show();
            $(".mailsid").hide();
            var idvalue = $("#"+e.currentTarget.id+"").attr('value');
            if (idvalue == "Participant Mail") {
                $(".participantname").show();
            } else if (idvalue == "Particicpant Follow-up Mail"){
                $(".participantname").show();
                $(".participantfollow").show();
            } else if (idvalue == "Manager Mail"){
                $(".managername").show();
                $(".participantname").show();
                $(".managermail").show();
            } else if (idvalue == "Respondent Mail"){
                $(".respondentname").show();
                $(".participantname").show();
            } else if (idvalue == "Respondent Follow-up Mail"){
                $(".repondantfollow").show();
                $(".participantname").show();
            }
            
            typeofid = 0;
            typeid = e.currentTarget.id.slice(4)
            Backbone.ajax({
                dataType:"json",
                url:"displaymailtemplate?id="+typeid+"",
                data:"",
                type: "POST",
                success:function(data, textStatus, jqXHR){
                    generateEmailElementBody = $("#templateBody").empty();
                    $("#templateSubject").val("");
                    try{
                        typeofid = data.id;
                        if(data.emailTypeId == typeid || data.id == typeid){
                            generateEmailElementBody.append(data.body);
                            $("#templateSubject").val(data.subject);
                        } else {
                            generateEmailElementSubject.append();
                            $("#templateSubject").val("");
                        }
                    }catch(e){}
                    $("#emailTemplateModal").show();
                    $('#templateBody').redactor();
                    $("#communicationtype").hide();
                    $("#pageloading").hide();
                },
                error:function(res,ioArgs){
                	if(res.status == 403){
                		window.location.reload();
                	}
                }
            });
        },
        
        onSubmitEmail:function(){
            var emailid;
            if(typeofid){
                emailid = typeofid;
            } else {
                emailid = 0;
            }
            var elesubject = $("#templateSubject").val();
            var elebody = $("#templateBody").html();
            $("#emailTemplateModal").hide();
            $("#communicationtype").show();
            var json = [];
            $("#pageloading").show();
            try{
                json={"id":emailid,"subject":elesubject,"body":elebody,"emailTypeId":typeid};
            }catch(e){}
            $.ajax({
                url: "editmailtemplate",
                data: JSON.stringify(json),
                type: "POST",
                beforeSend: function(xhr) {
                    xhr.setRequestHeader("Accept", "application/json");
                    xhr.setRequestHeader("Content-Type", "application/json");
                },
                success:function(data, textStatus, jqXHR){
                    $("#pageloading").hide();
                    try{
                        if(data == 1){
                            emailTemplateView.emailTypeGenerate();
                        } else if(data == 2){
                            sweetAlert("Oopps...!!","Problem in sending email template try again");
                        } else {
                            sweetAlert(data);
                        }
                    }catch(e){}
                },
                error:function(res,ioArgs){
                	if(res.status == 403){
                		window.location.reload();
                	}
                }
            });
        },
        
        cancleEmailT: function() {
            $("#emailTemplateModal").hide();
            $("#communicationtype").show();
        }
        
    });