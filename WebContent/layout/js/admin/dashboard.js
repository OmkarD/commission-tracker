/*
 *
 */

var gradeElement;
var DashboardView = Backbone.View.extend({
    events:
    {
        "click .grdcls":"displayParticipant"
    },
    
    initialize:function(){
        _.bindAll(this,'render')
    },

    render:function()
    {
        this.$el.empty().append(
            '<div class="row list_select">'+
                '<div class="col-lg-12">'+
                    '<h1 class="page-header">Dashboard</h1>'+
                '</div>'+
                '<!-- /.col-lg-12 -->'+
            '</div>'+
        '<div class="row">'+
            '<div class="col-lg-12">'+
                '<div class="panel-body">'+
                    '<div class="table-responsive">'+
                        '<table id="grade_table" class="table table-striped table-bordered table-hover">'+
                            '<thead>'+
                                '<tr>'+
                                    '<th>Participant Group</th>'+
                                    '<th>Participants</th>'+
                                    '<th>Self assessment completed</th>'+
                                    '<th>Full assessment completed</th>'+
                                '</tr>'+
                            '</thead>'+
                            '<tbody>'+
                            '</tbody>'+
                            '<tfoot>'+
                                '<tr>'+
                                    '<th>Total</th>'+
                                    '<td></td>'+
                                    '<td></td>'+
                                    '<td></td>'+
                                '</tr>'+
                            '</tfoot>'+
                        '</table>'+
                    '</div>'+
                '</div>'+
            '</div>'+
        '</div>'+
        '<div class="row">'+
            '<!-- /.col-lg-12 -->'+
            '<div class="col-md-12">'+
                '<a href="excelreport">Click Here</a> to download Participants report <br><br>'+
            '</div>'+
            '<!-- /.col-lg-12 -->'+
            '<div class="col-md-12">'+
                '<a href="responsereport">Click Here</a> to download Response report<br><br>'+
            '</div>'+
        '</div>');
        this.gradeUpdate();
    },
    
    gradeUpdate:function(){
        $("#pageloading").show();
        Backbone.ajax({
            dataType:"json",
            url:"participantwithgrade",
            data:"",
            type: "POST",
            success:function(data, textStatus, jqXHR){
                var sumtd0 = 0;
                var sumtd1 = 0;
                var sumtd2 = 0;
                gradeElement = $("#grade_table").children("tbody").empty();
                try{
                    for(var i=0; i< data.length; i++){
                    gradeElement.append('<tr>'+
                                            '<td>'+data[i].grade+'</td>'+
                                            '<td>'+data[i].total+'</td>'+
                                            '<td>'+data[i].saanswered+'</td>'+
                                            '<td>'+data[i].surveycompleted+'</td>'+
                                        '</tr>');
                        sumtd0 += data[i].total;
                        sumtd1 += data[i].saanswered;
                        sumtd2 += data[i].surveycompleted;
                    }
                }catch(e){}
                
                    $($("#grade_table").children("tfoot").children("tr").children("td")[0]).text(sumtd0);
                    $($("#grade_table").children("tfoot").children("tr").children("td")[1]).text(sumtd1);
                    $($("#grade_table").children("tfoot").children("tr").children("td")[2]).text(sumtd2);
                    $("#pageloading").hide();
            },
            error:function(res,ioArgs){
            	if(res.status == 403){
            		window.location.reload();
            	}
            }
        });
    },
    
    displayParticipant:function(e){
        var gradeId = e.currentTarget.id;
        var grd = gradeId.slice(3);
        var json = [];
         $("#pageloading").show();
        try{
            json.push({"grade":grd});
        }catch(e){}
        Backbone.ajax({
            url: "listgradewise",
            data: JSON.stringify(json),
            type: "POST",
            beforeSend: function(xhr) {
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-Type", "application/json");
            },
            success: function(data) {
                 $("#pageloading").hide();
                if(data == 1){
                    
                } else {
                    sweetAlert("Oops!", "Problem in saving. Please try again");
                }
            },
            error:function(res,ioArgs){
            	if(res.status == 403){
            		window.location.reload();
            	}
            }
        });
    },
    
});





