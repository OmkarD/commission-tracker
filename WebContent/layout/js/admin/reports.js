    var generateReportElements, reportElement;

    var ReportGeneratView = Backbone.View.extend({

        events:{
            'click .closeSession': 'closeSessionID',
            'click .emailSession': 'generateEmails',
            'click .viewSession': 'genarateParticipants',
            'click .goToLastPage': 'callLastLoadedPage',
            'click .sendAll-check': 'sendEmailAll',
            'click .send-check': 'removeCheckEmailAll'
        },

        initialize:function(){
            _.bindAll(this,'render');
        },

        render:function(){
            this.$el.empty().append(
            '<div class="row">'+
                '<div class="col-lg-12">'+
                    '<h1 class="page-header"> Generate Reports </h1>' +				
                '</div>'+
            '</div>'+
            '<div class="row">'+
                '<div class="col-lg-5">'+
                    '<div class="pull-left report-page"><a id="excelSessionReport">Click here</a> to download full session reports in excel</div>' +
                '</div>'+
                '<div class="col-lg-7">'+
                    //'<div class="pull-left report-page"><a id="pdfSessionReport">Click here</a> to download full session reports in pdf</div>' +
                    '<div class="btn goToLastPage report-page pull-right"><i class="fa fa-reply-all"></i> Back</div>' +
                '</div>'+
            '</div>'+
            '<div class="row">'+
                '<div class="col-lg-12">'+
                    '<div class="panel">'+
                        '<div class="panel-body">'+
                           '<div class="table-responsive">'+
                                '<table id="closed-sessions" class="table table-hover">'+
                                    '<thead>'+
                                        '<tr>'+
                                            '<th>Description</th>'+
                                            '<th>Start Time</th>'+
                                            '<th>End Time</th>'+
                                            '<th>Close Session</th>'+
                                        '</tr>'+
                                    '</thead>'+
                                    '<tbody>'+
                                    '</tbody>'+
                                '</table>'+
                                '<table id="view-participants" class="table table-hover">'+
                                    '<thead>'+
                                        '<tr>'+
                                            '<th>Employee Id</th>'+
                                            '<th>Name</th>'+
                                            '<th>Department</th>'+
                                            '<th>Grade</th>'+
                                            '<th>Status</th>'+
                                            '<th></th>'+
                                            '<th><input type="checkbox" class="sendAll-check" style="float: left;"><button type="button" class="emailSession btn btn-danger" style="float: right;">send mail</button></th>'+
                                        '</tr>'+
                                    '</thead>'+
                                    '<tbody>'+
                                    '</tbody>'+
                                '</table>'+
                            '</div>'+
                        '</div>'+
                    '</div>'+
                '</div>'+
            '</div>');
            reportGeneratView.sessionListTable();
        },
    
    /* User Send All */
        sendEmailAll: function(e) {
            if($('.sendAll-check').is(':checked')){
                $(".send-check").prop('checked',true);
            } else if(!$('.sendAll-check').is(':checked')) {
                $(".send-check").prop('checked',false);
            }
        },
        
    /* User Remove Send All */
        removeCheckEmailAll: function(e) {
            if(!e.currentTarget.checked){
                if($('.sendAll-check').is(':checked')){
                    $(".sendAll-check").prop('checked',false);
                }
            } else if(e.currentTarget.checked){
                var tempcount = 0;
                var userChecked = $(".send-check");
                for (var i = 0, length = userChecked.length; i < length; i++) {
                    if ($(userChecked[i]).is(':checked')) { 
                        tempcount++;
                    }
                }
                
                if(tempcount == $(".send-check").length){
                    $(".sendAll-check").prop('checked',true);
                }
            }
        },
        
        sessionListTable:function(e){
            $("#pageloading").show();
            Backbone.ajax({
                dataType:"json",
                url:"closedsessions",
                data:"",
                type: "POST",
                success:function(data, textStatus, jqXHR){
                    reportElement = Backbone.$("#closed-sessions").children("tbody").empty();
                    if(data.length == 0){
                        $("#pageloading").hide();
                    } else {
                        reportGeneratView.generateClosedSessionTable(data);
                    }
                },
                error:function(res,ioArgs){
                    if(res.status == 403){
                        window.location.reload();
                    }
                }
            });
        },
        
        generateClosedSessionTable: function( data ){
            for( var i = 0; i < data.length; i++){
                reportElement.append(
                    '<tr>' +
                        '<td>'+data[i].description+'</td>' +
                        '<td>'+data[i].startTime+'</td>' +
                        '<td>'+data[i].endTime+'</td>' +
                        '<td><button id="clos'+data[i].id+'" class="btn btn-default closeSession"></button></td>' +
                    '</tr>' 
                );
                if(data[i].freeze == 1) {
                    $("#email"+data[i].id+"").hide();
                    if(data[i].repGenStatus == 2) {
                        $("#clos"+data[i].id+"").addClass("closeSession");
                        $("#clos"+data[i].id+"").text("Close");
                        $("#clos"+data[i].id+"").prop('disabled', false);
                    } else {
                        $("#clos"+data[i].id+"").removeClass("closeSession").addClass("viewSession");
                        $("#clos"+data[i].id+"").text("View");
                        $("#clos"+data[i].id+"").prop('disabled', false);
                    }
                } else if(data[i].freeze == 2) {
                    $("#clos"+data[i].id+"").removeClass("closeSession").addClass("viewSession");
                    $("#clos"+data[i].id+"").prop('disabled', false);
                    if(data[i].repGenStatus == 2) {
                        $("#clos"+data[i].id+"").addClass("viewSession");
                        $("#clos"+data[i].id+"").text("View");
                        $("#clos"+data[i].id+"").prop('disabled', false);
                    } else if(data[i].repGenStatus == 3) {
                        $("#clos"+data[i].id+"").removeClass("viewSession");
                        $("#clos"+data[i].id+"").prop('disabled', true);
                        $("#clos"+data[i].id+"").text("Processing");
                    }
                }
            }
            $("#pageloading").hide();
        },
        
        closeSessionID: function(e) {
            $("#pageloading").show();
            var sesId = e.currentTarget.id.slice(4);
            Backbone.ajax({
                dataType:"json",
                url:"detailedreport?sessionId="+sesId,
                data:"",
                type: "POST",
                cache:false,
                success: function(data, textStatus, jqXHR){
                    $("#clos"+sesId+"").removeClass("closeSession");
                    $("#clos"+sesId+"").text("Closed");
                    $("#clos"+sesId+"").prop('disabled', true);
                },
                error: function(res,ioArgs){
                    if(res.status == 403){
                        window.location.reload();
                    }
                },
                always: function() {
                    $("#clos"+sesId+"").removeClass("closeSession");
                    $("#clos"+sesId+"").text("Closed");
                    $("#clos"+sesId+"").prop('disabled', true);
                },
                complete: function() {
                    $("#pageloading").hide();
                    $("#clos"+sesId+"").removeClass("closeSession");
                    $("#clos"+sesId+"").text("Closed");
                    $("#clos"+sesId+"").prop('disabled', true);
                }
            });
        },
        
        generateEmails: function(e) {
            if($(".send-check").is(':checked')){
                swal({   
                    title: "Are you sure?",   
                    text: "You want to send mail",
                    showCancelButton: true,   
                    confirmButtonColor: "#DD6B55",   
                    confirmButtonText: "Yes",
                },
                     function(isConfirm){
                    if (isConfirm) {
                        $("#pageloading").show();
                        var sessionsId = $("#view-participants").attr('value');
                        var sesId = [];
                        var userChecked = $(".send-check");
                        for (var i = 0, length = userChecked.length; i < length; i++) {
                            if ($(userChecked[i]).is(':checked')) { 
                                sesId.push(userChecked[i].value);
                            }
                        }
                        Backbone.ajax({
                            url:"triggerreports?sessionId="+sessionsId+"&pid="+sesId,
                            type:"POST",
                            dataType:"json",
                            cache:false,
                            data:"",
                            success: function(data){

                            },
                            error: function(res,ioArgs){
                                if(res.status == 403){
                                    window.location.reload();
                                }
                            },
                            complete: function() {
                                $("#pageloading").hide();
                                reportGeneratView.callLastLoadedPage();
                            },
                            always: function() {

                            }
                        });
                    }
                });
            } else {
                sweetAlert("Please select atleast one participant");
            }
        },
        
        genarateParticipants: function(e) {
            var ssid = e.currentTarget.id.slice(4);
            $("#pageloading").show();
            $("#excelSessionReport").attr('href', 'exceldriverwiserep?sessionId='+ssid+'');
            $("#pdfSessionReport").attr('href', 'downloadrep?sessionId='+ssid+'');
            $("#view-participants").attr('value', ssid);
            Backbone.$("#view-participants").children("tbody").empty();
            Backbone.ajax({
                url:"participantsforsession?sessionId="+ssid,
                type:"POST",
                dataType:"json",
                cache:false,
                data:"",
                success: function(data){
                    $("#closed-sessions").hide();
                    $("#view-participants").show();
                    $(".report-page").show();
                    if(data == 0){
                        Backbone.$("#view-participants").children("tbody").append('');
                    } else {
                        for( var i = 0; i < data.length; i++ ) {
                            Backbone.$("#view-participants").children("tbody").append(
                                '<tr id="particpantSe'+data[i].eid+'">' +
                                    '<td>'+data[i].eid+'</td>' +
                                    '<td>'+data[i].name+'</td>' +
                                    '<td>'+data[i].department+'</td>' +
                                    '<td>'+data[i].grade+'</td>' +                       
                                    '<td></td>' +
                                    '<td><a href="reportind?sessionId='+ssid+'&pid='+data[i].eid+'" target="_blank"> Report </a></td>' +
                                    '<td></td>' +
                                '</tr>'
                            );
                            if(data[i].status == 5){
                                $($("#particpantSe"+data[i].eid+"").children('td')[4]).text("Sent");
                                $($("#particpantSe"+data[i].eid+"").children('td')[6]).empty().append('<i class="fa fa-check-square-o"></i>');
                            } else if(data[i].status == 10){
                                $($("#particpantSe"+data[i].eid+"").children('td')[4]).text("Pending");
                                $($("#particpantSe"+data[i].eid+"").children('td')[6]).empty().append('<input type="checkbox" class="send-check" style="float: left;" value='+data[i].eid+'>');
                            }
                        }
                    }
                },
                error: function(res,ioArgs){
                    if(res.status == 403){
                        window.location.reload();
                    }
                },
                complete: function() {
                    $("#pageloading").hide();
                    $("#view-participants").dataTable({
                        "paging":   false,
                        "ordering": false,
                        "info":     true,
                        "scrollY":  "300",
                        "scrollCollapse": true
                    });
                },
            });
        },
        
        callLastLoadedPage: function(){
            $(".report-page").hide();
            reportGeneratView.render();
        },
    });