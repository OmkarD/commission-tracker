package com.rectrix.exide.service;

import java.util.List;

import com.rectrix.exide.form.models.CycleMasterForm;
import com.rectrix.exide.form.models.DataTableModel;
import com.rectrix.exide.form.models.DataTableResultModel;
import com.rectrix.exide.models.CycleMaster;
import com.rectrix.exide.models.LaAgentMaster;
import com.rectrix.exide.models.Zanc;

public interface UserService {

	

	/**
	 *  saving data for create new cycle
	 *  @return
	 */
	void saveNewCycle(CycleMaster cycleMaster);


	/**
	 * List of agents from LA400 (View Created in CT) Data Tales
	 *  @return
	 */
	DataTableResultModel paginGetListOfLaAgentDetails(DataTableModel model, long cycleId);

	/**
	 *  Viewing List of Cycles created, sorted by created first
	 *  @return
	 */
	DataTableResultModel paginGetListOfCyclesCreated(DataTableModel model);

	/**
	 * valid and invalid LA400 agents list by sp 
	 *  @return
	 */
	DataTableResultModel paginGetValidAgentDetals(DataTableModel model, long cycleId, int i);

	/**
	 * List Of La Agents 
	 *  @return
	 */
	List<LaAgentMaster> getListOfAgents();

	/**
	 * Returning Current Cycle Data
	 *  @return
	 */
	CycleMaster getCurrentCycleDetails();

	/**
	 * List Of valid and invalid agents list by cycleid and eligible 
	 * where eligible is 1 the its for valid and if its 2 then its for invalid
	 *  @return
	 */
	List<LaAgentMaster> getListOfValidAgentsListByCycleId(long cycleId, int i);

	/**
	 * Saving list of agent with cycleid
	 *  @return
	 */
	void saveAgentListForCycle(long cycleId);

	DataTableResultModel paginGetListOfAgPayUploadByCycleId(DataTableModel model, long cycleId);


}
