package com.rectrix.exide.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.rectrix.exide.dao.UserDao;
import com.rectrix.exide.form.models.CycleMasterForm;
import com.rectrix.exide.form.models.DataTableModel;
import com.rectrix.exide.form.models.DataTableResultModel;
import com.rectrix.exide.models.CycleMaster;
import com.rectrix.exide.models.LaAgentMaster;
import com.rectrix.exide.models.Zanc;
import com.rectrix.exide.service.UserService;

@Service
public class UserServiceImp implements UserService {
	
	@Autowired
	private UserDao userDao; 


	@Override
	public void saveNewCycle(CycleMaster cycleMaster) {
		 userDao.createNewCycleMaster(cycleMaster);
		
	}

	@Override
	public DataTableResultModel paginGetListOfLaAgentDetails(DataTableModel model,long cycleId) {
		return userDao.paginGetViewAgentMaster(model,cycleId);
	}

	@Override
	public DataTableResultModel paginGetListOfCyclesCreated(DataTableModel model) {
		return userDao.paginGetListOfCycleMaster(model);
	}
	
	//passing 1 to get valid agents
	//passing 2 to get invalid agents
	@Override
	public DataTableResultModel paginGetValidAgentDetals(DataTableModel model, long cycleId, int i) {
		return userDao.paginGetValidAndInvalideViewAgentMaster(model,cycleId,i); 
	}


	@Override
	public List<LaAgentMaster> getListOfAgents() {
		return userDao.getListOfAgentMaster();
	}

	@Override
	public CycleMaster getCurrentCycleDetails() {
		return userDao.getcurrentCycleMaster();
	}

	@Override
	public List<LaAgentMaster> getListOfValidAgentsListByCycleId(long cycleId, int eligibility) {
		return userDao.getListOfValidAndInvalidAgentsListByCycleId(cycleId, eligibility);
	}

	@Override
	public void saveAgentListForCycle(long cycleId) {
		userDao.saveAgentDataToTransactionMasterByCycleId(cycleId);
		
	}

	@Override
	public DataTableResultModel paginGetListOfAgPayUploadByCycleId(DataTableModel model, long cycleId) {
		return userDao.paginGetListOfAgPayUploadByCycleId(model,cycleId);
	}

	
	

}
