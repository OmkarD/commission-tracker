package com.rectrix.exide.dao;

import java.util.List;

import javax.sql.DataSource;

import com.rectrix.exide.models.AdminUserGroups;



public interface AdminUserGroupDao {

	List<AdminUserGroups> getAdminUserGroups();

	

}