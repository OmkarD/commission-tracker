package com.rectrix.exide.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.rectrix.exide.dao.AdminUserGroupDao;
import com.rectrix.exide.models.AdminUserGroups;
import com.rectrix.exide.rowmapper.AdminUserGroupMapper;

@Repository
@Transactional(readOnly=true)
public class AdminUserGroupDaoImpl implements AdminUserGroupDao {
	private static final Logger logger = Logger.getLogger(AdminUserGroupDaoImpl.class);
    
	private DataSource dataSource;
	private JdbcTemplate jdbcTemplateObject;
   
	@Autowired
   public void setDataSource(DataSource dataSource) {
      this.dataSource = dataSource;
      this.jdbcTemplateObject = new JdbcTemplate(dataSource);
   }

	@Override
	public List<AdminUserGroups> getAdminUserGroups() {
		
		List<AdminUserGroups> adminUserGroupsList = new ArrayList<AdminUserGroups>();
		
		try {
			String SQL = "SELECT * FROM TB_APP_ADMIN_USER_GROUP";
			adminUserGroupsList = jdbcTemplateObject.query(SQL, new AdminUserGroupMapper());
			return adminUserGroupsList;
		}catch(Exception e) {
			logger.error("Error While fetching data for Application Groups",e);
			e.printStackTrace();
			return adminUserGroupsList;
		}
		
	}

	
}
