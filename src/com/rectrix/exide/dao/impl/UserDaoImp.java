package com.rectrix.exide.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.TransactionException;
import org.springframework.transaction.annotation.Transactional;

import com.rectrix.exide.dao.UserDao;
import com.rectrix.exide.form.models.CycleMasterForm;
import com.rectrix.exide.form.models.DataTableModel;
import com.rectrix.exide.form.models.DataTableResultModel;
import com.rectrix.exide.models.CycleMaster;
import com.rectrix.exide.models.LaAgentMaster;
import com.rectrix.exide.models.TransactionAgentCycle;
import com.rectrix.exide.models.Zanc;
import com.rectrix.exide.rowmapper.AgentEligibilityMapper;
import com.rectrix.exide.rowmapper.CycleMasterMapper;
import com.rectrix.exide.rowmapper.TransactionAgentCycleMapper;
import com.rectrix.exide.rowmapper.ViewAgentMasterMapper;
import com.rectrix.exide.rowmapper.ZancMapper;

@Repository
@Transactional
public class UserDaoImp implements UserDao {
	
private static final Logger logger = Logger.getLogger(UserDaoImp.class);
    
	private DataSource dataSource;
	private JdbcTemplate jdbcTemplateObject;
	private SimpleJdbcCall storedProcedure;
   
	@Autowired
   public void setDataSource(DataSource dataSource) {
      this.dataSource = dataSource;
      this.jdbcTemplateObject = new JdbcTemplate(dataSource);
   }


	@Override
	public void createNewCycleMaster(CycleMaster cycleMaster) {
		
		CycleMaster master = getcurrentCycleMaster();
		
		String SQL1 = "UPDATE TB_CYCLE_MASTER SET VALID_FLAG = 2 WHERE CYCLE_ID =" + master.getCycleId();
		jdbcTemplateObject.update(SQL1);
		
		String SQL ="INSERT INTO TB_CYCLE_MASTER (CYCLE_NAME, FROM_DATE, TO_DATE, CYCLE_DESC, CREATED_BY, CREATED_ON, VALID_FLAG, AGENT_SYNC_STATUS) VALUES(?,?,?,?,?,?,?,?) ";
		
		try {
			jdbcTemplateObject.update(SQL, 	cycleMaster.getCycleName(),
											cycleMaster.getFromDate(),
											cycleMaster.getToDate(),
											cycleMaster.getCycleDescription(),
											cycleMaster.getCreatedBy(),
											cycleMaster.getCriatedOn(),
											cycleMaster.getValidflag(),
											cycleMaster.getAgentSyncStatus());
			
		}catch (Exception e) {
			logger.error("Error While saving new cycle creation data",e);
		}
		
	}

	@Override
	public DataTableResultModel paginGetViewAgentMaster(DataTableModel model,long cycleId) {
			
		try {
			
				String sql = "SELECT * FROM TB_CYCLE_MASTER WHERE CYCLE_ID=" + cycleId;
				List<CycleMaster> cycleMaster = jdbcTemplateObject.query(sql, new CycleMasterMapper());
	
				if(cycleMaster.get(0).getAgentSyncStatus() == 1) {
					/*
					 * Getting data from LA before Sinking to transaction master
					 */
					return getViewAgentMaster(model);
				}else {
					/*
					 * Getting sinking data from transaction master by cycle id
					 */
					return getTransactionMasterByCycleId(model,cycleId);
				}
		}catch (Exception e) {
			logger.error("Error While Getting List Of Agents ",e);
			return new DataTableResultModel(new ArrayList<LaAgentMaster>(), 0,0);
		}
			
	}

	private DataTableResultModel getTransactionMasterByCycleId(DataTableModel model, long cycleId) {
		
		List<TransactionAgentCycle> listOfLaAgentMaster = new ArrayList<TransactionAgentCycle>();
		int iTotalRecords = 0;
		int iTotalDisplayRecords =0;
		
		try {
		
		String SQL ="SELECT * FROM ( "
				+ "SELECT ROW_NUMBER() OVER(ORDER BY TEMP10.AGNTNUM ASC) AS ROW_CO, TEMP10.* FROM "
				+ "( "
				+ " SELECT * FROM TB_TRANSACTION_AGENT_CYCLE WHERE CYCLE_ID=" + cycleId
				+ " ) AS TEMP10 ";
			
			if(model.getSearchTearm()!=null&&model.getSearchTearm().length()>0){
					SQL = SQL+" "+" WHERE TEMP10.AGNTNUM LIKE '%"+model.getSearchTearm()+
	 							"%' OR TEMP10.AGENTNAME LIKE '%"+model.getSearchTearm()+
	 							"%' OR TEMP10.AGTYPE LIKE '%"+model.getSearchTearm()+
	 							"%' OR TEMP10.PANNo LIKE '%"+model.getSearchTearm()+"%'";
				
			}
			
			SQL = SQL+" "+") AS temp11 "
					+ "WHERE ROW_CO BETWEEN '"+model.getStart()+"' AND '"+(model.getStart()+model.getAmount())+"'";
			
			listOfLaAgentMaster =jdbcTemplateObject.query(SQL, new TransactionAgentCycleMapper());
			
			
			String COUNTSQL = "SELECT COUNT(*) FROM ( "
					+ "SELECT ROW_NUMBER() OVER(ORDER BY TEMP10.AGNTNUM ASC) AS ROW_CO, TEMP10.* FROM "
					+ "( "
					+ " SELECT * FROM TB_TRANSACTION_AGENT_CYCLE WHERE CYCLE_ID=" + cycleId
					+ " ) AS TEMP10 "
					+" "+") AS temp11 ";
			 iTotalRecords = jdbcTemplateObject.queryForInt(COUNTSQL);
			
			String COUNTDISPLAYSQL = "SELECT COUNT(*) FROM ( "
					+ "SELECT ROW_NUMBER() OVER(ORDER BY TEMP10.AGNTNUM ASC) AS ROW_CO, TEMP10.* FROM "
					+ "( "
					+ " SELECT * FROM TB_TRANSACTION_AGENT_CYCLE WHERE CYCLE_ID=" + cycleId
					+ " ) AS TEMP10 ";
			

				if(model.getSearchTearm()!=null&&model.getSearchTearm().length()>0){
					
					COUNTDISPLAYSQL = COUNTDISPLAYSQL+" "+" WHERE TEMP10.AGNTNUM LIKE '%"+model.getSearchTearm()+
														"%' OR TEMP10.AGENTNAME LIKE '%"+model.getSearchTearm()+
														"%' OR TEMP10.AGTYPE LIKE '%"+model.getSearchTearm()+
														"%' OR TEMP10.PANNo LIKE '%"+model.getSearchTearm()+"%'";
				}
				COUNTDISPLAYSQL = COUNTDISPLAYSQL+" "+") AS temp11 ";
				
				if(model.getSearchTearm()!=null&&model.getSearchTearm().length()>0){
					iTotalDisplayRecords = jdbcTemplateObject.queryForInt(COUNTDISPLAYSQL);
				}else{
					iTotalDisplayRecords=iTotalRecords;
				}	
		return new DataTableResultModel(listOfLaAgentMaster, iTotalRecords,iTotalDisplayRecords);
	}
	catch(Exception e){
		e.printStackTrace();
		logger.error("Error while displaying LA Agent from transaction master List ",e);
		return new DataTableResultModel(listOfLaAgentMaster, iTotalRecords,iTotalDisplayRecords);
		}
	}

	private DataTableResultModel getViewAgentMaster(DataTableModel model) {
		
		List<LaAgentMaster> listOfLaAgentMaster = new ArrayList<LaAgentMaster>();
		int iTotalRecords = 0;
		int iTotalDisplayRecords =0;
		
		try {
		
		String SQL ="SELECT * FROM ( "
				+ "SELECT ROW_NUMBER() OVER(ORDER BY TEMP10.AGNTNUM ASC) AS ROW_CO, TEMP10.* FROM "
				+ "( "
				+ "SELECT * FROM ViewAgentMaster"
				+ " ) AS TEMP10 ";
			
			if(model.getSearchTearm()!=null&&model.getSearchTearm().length()>0){
					SQL = SQL+" "+" WHERE TEMP10.AGNTNUM LIKE '%"+model.getSearchTearm()+
	 							"%' OR TEMP10.AGENTNAME LIKE '%"+model.getSearchTearm()+
	 							"%' OR TEMP10.AGTYPE LIKE '%"+model.getSearchTearm()+
	 							"%' OR TEMP10.PANNo LIKE '%"+model.getSearchTearm()+"%'";
				
			}
			
			SQL = SQL+" "+") AS temp11 "
					+ "WHERE ROW_CO BETWEEN '"+model.getStart()+"' AND '"+(model.getStart()+model.getAmount())+"'";
			
			listOfLaAgentMaster =jdbcTemplateObject.query(SQL, new ViewAgentMasterMapper());
			
			
			String COUNTSQL = "SELECT COUNT(*) FROM ( "
					+ "SELECT ROW_NUMBER() OVER(ORDER BY TEMP10.AGNTNUM ASC) AS ROW_CO, TEMP10.* FROM "
					+ "( "
					+ "SELECT * FROM ViewAgentMaster"
					+ " ) AS TEMP10 "
					+" "+") AS temp11 ";
			 iTotalRecords = jdbcTemplateObject.queryForInt(COUNTSQL);
			
			String COUNTDISPLAYSQL = "SELECT COUNT(*) FROM ( "
					+ "SELECT ROW_NUMBER() OVER(ORDER BY TEMP10.AGNTNUM ASC) AS ROW_CO, TEMP10.* FROM "
					+ "( "
					+ "SELECT * FROM ViewAgentMaster"
					+ " ) AS TEMP10 ";
			

				if(model.getSearchTearm()!=null&&model.getSearchTearm().length()>0){
					
					COUNTDISPLAYSQL = COUNTDISPLAYSQL+" "+" WHERE TEMP10.AGNTNUM LIKE '%"+model.getSearchTearm()+
														"%' OR TEMP10.AGENTNAME LIKE '%"+model.getSearchTearm()+
														"%' OR TEMP10.AGTYPE LIKE '%"+model.getSearchTearm()+
														"%' OR TEMP10.PANNo LIKE '%"+model.getSearchTearm()+"%'";
				}
				COUNTDISPLAYSQL = COUNTDISPLAYSQL+" "+") AS temp11 ";
				
				if(model.getSearchTearm()!=null&&model.getSearchTearm().length()>0){
					iTotalDisplayRecords = jdbcTemplateObject.queryForInt(COUNTDISPLAYSQL);
				}else{
					iTotalDisplayRecords=iTotalRecords;
				}	
		return new DataTableResultModel(listOfLaAgentMaster, iTotalRecords,iTotalDisplayRecords);
	}
	catch(Exception e){
		e.printStackTrace();
		logger.error("Error while displaying LA Agent List ",e);
		return new DataTableResultModel(listOfLaAgentMaster, iTotalRecords,iTotalDisplayRecords);
		}
	}

	@Override
	public DataTableResultModel paginGetListOfCycleMaster(DataTableModel model) {
		List<CycleMaster> listOfCycleMaster = new ArrayList<CycleMaster>();
		int iTotalRecords = 0;
		int iTotalDisplayRecords =0;
		try{
			String SQL ="SELECT * FROM ( "
					+ "SELECT ROW_NUMBER() OVER(ORDER BY TEMP10.CYCLE_ID DESC) AS ROW_CO, TEMP10.* FROM "
					+ "( "
					+ " SELECT * FROM TB_CYCLE_MASTER "
					+ " ) AS TEMP10 ";
				
				if(model.getSearchTearm()!=null&&model.getSearchTearm().length()>0){
 					SQL = SQL+" "+" WHERE TEMP10.CYCLE_NAME LIKE '%"+model.getSearchTearm()+
		 							"%' OR TEMP10.FROM_DATE LIKE '%"+model.getSearchTearm()+
		 							"%' OR TEMP10.TO_DATE LIKE '%"+model.getSearchTearm()+
		 							"%' OR TEMP10.CREATED_BY LIKE '%"+model.getSearchTearm()+
		 							"%' OR TEMP10.CREATED_ON LIKE '%"+model.getSearchTearm()+"%'";
					
				}
				
				SQL = SQL+" "+") AS temp11 "
						+ "WHERE ROW_CO BETWEEN '"+model.getStart()+"' AND '"+(model.getStart()+model.getAmount())+"'";
				listOfCycleMaster =jdbcTemplateObject.query(SQL, new CycleMasterMapper());
			
			
			
				String COUNTSQL = "SELECT COUNT(*) FROM ( "
						+ "SELECT ROW_NUMBER() OVER(ORDER BY TEMP10.CYCLE_ID DESC) AS ROW_CO, TEMP10.* FROM "
						+ "( "
						+ " SELECT * FROM TB_CYCLE_MASTER  "
						+ " ) AS TEMP10 "
						+" "+") AS temp11 ";
				 iTotalRecords = jdbcTemplateObject.queryForInt(COUNTSQL);
				
				String COUNTDISPLAYSQL = "SELECT COUNT(*) FROM ( "
						+ "SELECT ROW_NUMBER() OVER(ORDER BY TEMP10.CYCLE_ID DESC) AS ROW_CO, TEMP10.* FROM "
						+ "( "
						+ " SELECT * FROM TB_CYCLE_MASTER "
						+ " ) AS TEMP10 ";
				

					if(model.getSearchTearm()!=null&&model.getSearchTearm().length()>0){
						
						COUNTDISPLAYSQL = COUNTDISPLAYSQL+" "+"WHERE TEMP10.CYCLE_NAME LIKE '%"+model.getSearchTearm()+
																"%' OR TEMP10.FROM_DATE LIKE '%"+model.getSearchTearm()+
																"%' OR TEMP10.TO_DATE LIKE '%"+model.getSearchTearm()+ 
																"%' OR TEMP10.CREATED_BY LIKE '%"+model.getSearchTearm()+
																"%' OR TEMP10.CREATED_ON LIKE '%"+model.getSearchTearm()+"%'";
								
					}
					COUNTDISPLAYSQL = COUNTDISPLAYSQL+" "+") AS temp11 ";
					
					if(model.getSearchTearm()!=null&&model.getSearchTearm().length()>0){
						iTotalDisplayRecords = jdbcTemplateObject.queryForInt(COUNTDISPLAYSQL);
					}else{
						iTotalDisplayRecords=iTotalRecords;
					}	
			return new DataTableResultModel(listOfCycleMaster, iTotalRecords,iTotalDisplayRecords);
		}catch(Exception e){
			e.printStackTrace();
			logger.error("Error while displaying List OF Cycles ",e);
			return new DataTableResultModel(listOfCycleMaster, iTotalRecords,iTotalDisplayRecords);
		}
		
	}

	@Override
	public DataTableResultModel paginGetValidAndInvalideViewAgentMaster(DataTableModel model, long cycleId, int i) {
		List<LaAgentMaster> listOfLaAgentMaster = new ArrayList<LaAgentMaster>();
		int iTotalRecords = 0;
		int iTotalDisplayRecords =0;
		try{
			
		
			String SQL ="SELECT * FROM ( "
					+ "SELECT ROW_NUMBER() OVER(ORDER BY TEMP10.AGNTNUM ASC) AS ROW_CO, TEMP10.* FROM "
					+ "( "
					+ " SELECT * FROM [dbo].[ITVFAgentEligibility] ("+cycleId+") WHERE ELIGIBLE='"+i+"'"
					+ " ) AS TEMP10 ";
				
				if(model.getSearchTearm()!=null&&model.getSearchTearm().length()>0){
 					SQL = SQL+" "+" WHERE TEMP10.AGNTNUM LIKE '%"+model.getSearchTearm()+
		 							"%' OR TEMP10.AGENTNAME LIKE '%"+model.getSearchTearm()+
		 							"%' OR TEMP10.AGTYPE LIKE '%"+model.getSearchTearm()+
		 							"%' OR TEMP10.PANNo LIKE '%"+model.getSearchTearm()+"%'";
					
				}
				
				SQL = SQL+" "+") AS temp11 "
						+ "WHERE ROW_CO BETWEEN '"+model.getStart()+"' AND '"+(model.getStart()+model.getAmount())+"'";
				listOfLaAgentMaster =jdbcTemplateObject.query(SQL, new AgentEligibilityMapper());
			
			
			
				String COUNTSQL = "SELECT COUNT(*) FROM ( "
						+ "SELECT ROW_NUMBER() OVER(ORDER BY TEMP10.AGNTNUM ASC) AS ROW_CO, TEMP10.* FROM "
						+ "( "
						+ " SELECT * FROM [dbo].[ITVFAgentEligibility] ("+cycleId+") WHERE ELIGIBLE='"+i+"'"
						+ " ) AS TEMP10 "
						+" "+") AS temp11 ";
				 iTotalRecords = jdbcTemplateObject.queryForInt(COUNTSQL);
				
				String COUNTDISPLAYSQL = "SELECT COUNT(*) FROM ( "
						+ "SELECT ROW_NUMBER() OVER(ORDER BY TEMP10.AGNTNUM ASC) AS ROW_CO, TEMP10.* FROM "
						+ "( "
						+ " SELECT * FROM [dbo].[ITVFAgentEligibility] ("+cycleId+") WHERE ELIGIBLE='"+i+"'"
						+ " ) AS TEMP10 ";
				

					if(model.getSearchTearm()!=null&&model.getSearchTearm().length()>0){
						
						COUNTDISPLAYSQL = COUNTDISPLAYSQL+" "+" WHERE TEMP10.AGNTNUM LIKE '%"+model.getSearchTearm()+
															"%' OR TEMP10.AGENTNAME LIKE '%"+model.getSearchTearm()+
															"%' OR TEMP10.AGTYPE LIKE '%"+model.getSearchTearm()+
															"%' OR TEMP10.PANNo LIKE '%"+model.getSearchTearm()+"%'";
					}
					COUNTDISPLAYSQL = COUNTDISPLAYSQL+" "+") AS temp11 ";
					
					if(model.getSearchTearm()!=null&&model.getSearchTearm().length()>0){
						iTotalDisplayRecords = jdbcTemplateObject.queryForInt(COUNTDISPLAYSQL);
					}else{
						iTotalDisplayRecords=iTotalRecords;
					}	
			return new DataTableResultModel(listOfLaAgentMaster, iTotalRecords,iTotalDisplayRecords);
		}catch(Exception e){
			e.printStackTrace();
			logger.error("Error while displaying ValidAndInvalide LA Agent List ",e);
			return new DataTableResultModel(listOfLaAgentMaster, iTotalRecords,iTotalDisplayRecords);
		}
		
	}

	@Override
	public CycleMaster getcurrentCycleMaster() {
		String SQL = "SELECT TOP(1) * FROM TB_CYCLE_MASTER ORDER BY CYCLE_ID DESC";
		List<CycleMaster> cycleMaster = new ArrayList<CycleMaster>();
		cycleMaster = jdbcTemplateObject.query(SQL, new CycleMasterMapper());
		
		return cycleMaster.get(0);
	}

	@Override
	public List<LaAgentMaster> getListOfAgentMaster() {
		
		String SQL = "SELECT * FROM ViewAgentMaster";
		try {
			return jdbcTemplateObject.query(SQL, new ViewAgentMasterMapper());
		}catch (Exception e) {
			logger.error("Error While Downloding Excel For La Agent Master ",e);
			return new ArrayList<LaAgentMaster>();
		}
		
	}

	@Override
	public List<LaAgentMaster> getListOfValidAndInvalidAgentsListByCycleId(long cycleId, int eligibility) {
		String SQL = " SELECT * FROM [dbo].[ITVFAgentEligibility] ("+cycleId+") WHERE ELIGIBLE="+eligibility;
		try {
			return jdbcTemplateObject.query(SQL, new AgentEligibilityMapper());
		}catch (Exception e) {
			// TODO: handle exception
			logger.error("Error While getting data for valid and invalid agent list by cycle id",e);
			return new ArrayList<LaAgentMaster>();
		}
	}

	@Override
	public void saveAgentDataToTransactionMasterByCycleId(long cycleId) {
		
		try {
			List<LaAgentMaster> laAgentMastersList = getListOfAgentMaster();
			
			 String SQL ="INSERT INTO TB_TRANSACTION_AGENT_CYCLE( "
						+ "[AGNTNUM]," 
						+ "[DTEAPP]," 
						+ "[CLTSEX]," 
						+ "[SALUTL]," 
						+ "[AGENTNAME]," 
						+ "[AGENTADDRESS]," 
						+ "[ZBROKAGT],"  
						+ "[ZDATELIC]," 
						+ "[CLTDOB],"
						+ "[AGENT_BRANCH_CODE]," 
						+ "[AGENT_UNIT_CODE]," 
						+ "[AGTYPE],"
						+ "[ARACDE]," 
						+ "[AGENT_CLASS]," 
						+ "[DTETRM],"
						+ "[PAYCLT]," 
						+ "[CLNTNUM]," 
						+ "[PHONENO1]," 
						+ "[PHONENO2]," 
						+ "[REPORTAG]," 
						+ "[NBUSALLW],"
						+ "[MINSTA]," 
						+ "[REASONCD]," 
						+ "[EFFDATE]," 
						+ "[PROPTION]," 
						+ "[MOBILEPHONE]," 
						+ "[EMPLOYEECODE],"
						+ "[MAILID]," 
						+ "[PANNo]," 
						+ "[CHANNEL]," 
						+ "[PAYMETHOD]," 
						+ "[BANKACCNO],"
						+ "[IFSC],"
						+ "[CYCLE_ID])" 
						+ "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
			 
			/* String values ="";
			 
			 for (LaAgentMaster laAgentMaster : laAgentMastersList) {
				
					 values = values+"('"+laAgentMaster.getAgentNum()+"',";
					 values = values+"'"+laAgentMaster.getDateApp()+"',";
					 values = values+"'"+laAgentMaster.getClientSex()+"',";
					 values = values+"'"+laAgentMaster.getSalutl()+"',";
					 values = values+"'"+laAgentMaster.getAgentName()+"',";
					 values = values+"'"+laAgentMaster.getAgentAddr()+"',";
					 values = values+"'"+laAgentMaster.getzBrokAgnt()+"',";
					 values = values+"'"+laAgentMaster.getzDateLic()+"',";
					 values = values+"'"+laAgentMaster.getClientDob()+"',";
					 values = values+"'"+laAgentMaster.getAgentBranchCode()+"',";
					 values = values+"'"+laAgentMaster.getAgentUnitCode()+"',";
					 values = values+"'"+laAgentMaster.getAgentType()+"',";
					 values = values+"'"+laAgentMaster.getAracde()+"',";
					 values = values+"'"+laAgentMaster.getAgentClass()+"',";
					 values = values+"'"+laAgentMaster.getDateTrm()+"',";
					 values = values+"'"+laAgentMaster.getPayClient()+"',";
					 values = values+"'"+laAgentMaster.getClientNumber()+"',";
					 values = values+"'"+laAgentMaster.getPhoneNum1()+"',";
					 values = values+"'"+laAgentMaster.getPhoneNum2()+"',";
					 values = values+"'"+laAgentMaster.getReporting()+"',";
					 values = values+"'"+laAgentMaster.getNbusallw()+"',";
					 values = values+"'"+laAgentMaster.getMinsta()+"',";
					 values = values+"'"+laAgentMaster.getReasoncd()+"',";
					 values = values+"'"+laAgentMaster.getEffectiveDate()+"',";
					 values = values+"'"+laAgentMaster.getProption()+"',";
					 values = values+"'"+laAgentMaster.getMobilePhone()+"',";
					 values = values+"'"+laAgentMaster.getMailId()+"',";
					 values = values+"'"+laAgentMaster.getPanNum()+"',";
					 values = values+"'"+laAgentMaster.getChannel()+"',";
					 values = values+"'"+laAgentMaster.getPaymentMethod()+"',";
					 values = values+"'"+cycleId+"')";
				
			}*/
			 
			 List<Object[]> inputList = new ArrayList<Object[]>();
			 
			 for (LaAgentMaster objects : laAgentMastersList) {
				Object[] obj = {objects.getAgentNum(),
								objects.getDateApp(),
								objects.getClientSex(),
								objects.getSalutl(),
								objects.getAgentName(),
								objects.getAgentAddr(),
								objects.getzBrokAgnt(),
								objects.getzDateLic(),
								objects.getClientDob(),
								objects.getAgentBranchCode(),
								objects.getAgentUnitCode(),
								objects.getAgentType(),
								objects.getAracde(),
								objects.getAgentClass(),
								objects.getDateTrm(),
								objects.getPayClient(),
								objects.getClientNumber(),
								objects.getPhoneNum1(),
								objects.getPhoneNum2(),
								objects.getReporting(),
								objects.getNbusallw(),
								objects.getMinsta(),
								objects.getReasoncd(),
								objects.getEffectiveDate(),
								objects.getProption(),
								objects.getMobilePhone(),
								objects.getEmployeeCode(),
								objects.getMailId(),
								objects.getPanNum(),
								objects.getChannel(),
								objects.getPaymentMethod(),
								objects.getBankAccNumber(),
								objects.getIfsc(),
								cycleId};
				
								inputList.add(obj);
			}
			 
			 jdbcTemplateObject.batchUpdate(SQL, inputList);
			 
			 String sql1 = "  UPDATE TB_CYCLE_MASTER SET AGENT_SYNC_STATUS = 2 WHERE CYCLE_ID ="+cycleId;
			 jdbcTemplateObject.update(sql1);
			 
		}catch (Exception e) {
			// TODO: handle exception
			logger.error("Error While saving data to transaction master",e);
			
		}
		
		 

	}

	@Override
	public DataTableResultModel paginGetListOfAgPayUploadByCycleId(DataTableModel model, long cycleId) {
		// TODO Auto-generated method stub
		return null;
	}


	

}
