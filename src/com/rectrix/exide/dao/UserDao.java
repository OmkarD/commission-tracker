package com.rectrix.exide.dao;

import java.util.List;

import com.rectrix.exide.form.models.CycleMasterForm;
import com.rectrix.exide.form.models.DataTableModel;
import com.rectrix.exide.form.models.DataTableResultModel;
import com.rectrix.exide.models.CycleMaster;
import com.rectrix.exide.models.LaAgentMaster;
import com.rectrix.exide.models.Zanc;

public interface UserDao {

	/**
	 * saving new cycle details to cycle master
	 * @return
	 */
	void createNewCycleMaster(CycleMaster cycleMaster);

	/**
	 * return List of agent list details using viewagentmaster for data table
	 * @return
	 */
	DataTableResultModel paginGetViewAgentMaster(DataTableModel model, long cycleId);

	/**
	 * return List of cycles created by user for data table
	 * @return
	 */
	DataTableResultModel paginGetListOfCycleMaster(DataTableModel model);

	/**
	 * return current cycle details
	 * @return
	 */
	CycleMaster getcurrentCycleMaster();

	/**
	 * return List of agent master using ViewAgentMaster
	 * @return
	 */
	List<LaAgentMaster> getListOfAgentMaster();

	/**
	 * return List of valid and invalid agent master by cycleId for data table using sp 
	 * @return
	 */
	DataTableResultModel paginGetValidAndInvalideViewAgentMaster(DataTableModel model, long cycleId, int i);

	/**
	 * return List of  valid and invalid agent master by cycleId
	 * @return
	 */
	List<LaAgentMaster> getListOfValidAndInvalidAgentsListByCycleId(long cycleId, int eligibility);

	/**
	 * saving List of agent details to transaction table cycleId wise
	 * @return
	 */
	void saveAgentDataToTransactionMasterByCycleId(long cycleId);

	/**
	 * return Ag pay uploaded by user for per cycle
	 * @return
	 */
	DataTableResultModel paginGetListOfAgPayUploadByCycleId(DataTableModel model, long cycleId);

	


	
}
