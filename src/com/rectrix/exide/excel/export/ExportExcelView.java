package com.rectrix.exide.excel.export;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;

import com.rectrix.exide.models.LaAgentMaster;
import com.rectrix.exide.utils.Impl.AbstractExcelView;

public class ExportExcelView extends AbstractExcelView {

	private static final Logger logger = Logger.getLogger(ExportExcelView.class);
	@Override
	protected void buildExcelDocument(Map<String, Object> model,
			Workbook workbook, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		String value=(String) model.get("excel");
		
		if(value.equalsIgnoreCase("agentlist")) {
			List<LaAgentMaster> list = (List<LaAgentMaster>) model.get("agentdata");
			XSSFSheet excelSheet =  (XSSFSheet) workbook.createSheet("LA Agent List");
			 setExcelDownlodForAgentList(excelSheet,list);
			 
		}else if(value.equalsIgnoreCase("validagentlist")) {
			List<LaAgentMaster> list = (List<LaAgentMaster>) model.get("agentvaliddata");
			XSSFSheet excelSheet =  (XSSFSheet) workbook.createSheet("Valid Agent List");
			 setExcelDownlodForValidAgentList(excelSheet,list);
			 
		}else if(value.equalsIgnoreCase("invalidagentlist")) {
			List<LaAgentMaster> list = (List<LaAgentMaster>) model.get("agentinvaliddata");
			XSSFSheet excelSheet =  (XSSFSheet) workbook.createSheet("Invalid Agent List");
			 setExcelDownlodForInvalidAgentList(excelSheet,list);
			 
		}
		
		
	}
	
	private void setExcelDownlodForAgentList(XSSFSheet excelSheet, List<LaAgentMaster> list) {
		 XSSFRow excelHeader = excelSheet.createRow(0);
		  int tempCount=0;
		 excelHeader.createCell(tempCount++).setCellValue("AGNTNUM");
		 excelHeader.createCell(tempCount++).setCellValue("AGENTNAME");
		 excelHeader.createCell(tempCount++).setCellValue("AGENTADDRESS");
		 excelHeader.createCell(tempCount++).setCellValue("AGENT_BRANCH_CODE");
		 excelHeader.createCell(tempCount++).setCellValue("AGENT_CLASS");
		 excelHeader.createCell(tempCount++).setCellValue("AGTYPE");
		 excelHeader.createCell(tempCount++).setCellValue("AGENT_UNIT_CODE");
		 excelHeader.createCell(tempCount++).setCellValue("ARACDE");
		 excelHeader.createCell(tempCount++).setCellValue("CHANNEL");
		 excelHeader.createCell(tempCount++).setCellValue("CLTDOB");
		 excelHeader.createCell(tempCount++).setCellValue("CLNTNUM");
		 excelHeader.createCell(tempCount++).setCellValue("CLTSEX");
		 excelHeader.createCell(tempCount++).setCellValue("DTEAPP");
		 excelHeader.createCell(tempCount++).setCellValue("DTETRM");
		 excelHeader.createCell(tempCount++).setCellValue("EFFDATE");
		 excelHeader.createCell(tempCount++).setCellValue("EMPLOYEECODE");
		 excelHeader.createCell(tempCount++).setCellValue("MAILID");
		 excelHeader.createCell(tempCount++).setCellValue("MINSTA");
		 excelHeader.createCell(tempCount++).setCellValue("MOBILEPHONE");
		 excelHeader.createCell(tempCount++).setCellValue("NBUSALLW");
		 excelHeader.createCell(tempCount++).setCellValue("PANNo");
		 excelHeader.createCell(tempCount++).setCellValue("PAYCLT");
		 excelHeader.createCell(tempCount++).setCellValue("PAYMETHOD");
		 excelHeader.createCell(tempCount++).setCellValue("PHONENO1");
		 excelHeader.createCell(tempCount++).setCellValue("PHONENO2");
		 excelHeader.createCell(tempCount++).setCellValue("PROPTION");
		 excelHeader.createCell(tempCount++).setCellValue("REASONCD");
		 excelHeader.createCell(tempCount++).setCellValue("REPORTAG");
		 excelHeader.createCell(tempCount++).setCellValue("SALUTL");
		 excelHeader.createCell(tempCount++).setCellValue("ZBROKAGT");
		 excelHeader.createCell(tempCount++).setCellValue("ZDATELIC");
		 
		 try {
			 
			 int row = 1;
			 
			 for (LaAgentMaster agentMaster : list) {
					 excelHeader = excelSheet.createRow(row);
					 int temp1Count=0;
					 excelHeader.createCell(temp1Count++).setCellValue(agentMaster.getAgentNum() == null ? "" : agentMaster.getAgentNum());
					 excelHeader.createCell(temp1Count++).setCellValue(agentMaster.getAgentName() == null ? "" : agentMaster.getAgentName());
					 excelHeader.createCell(temp1Count++).setCellValue(agentMaster.getAgentAddr() == null ? "" : agentMaster.getAgentAddr());
					 excelHeader.createCell(temp1Count++).setCellValue(agentMaster.getAgentBranchCode() == null ? "" : agentMaster.getAgentBranchCode());
					 excelHeader.createCell(temp1Count++).setCellValue(agentMaster.getAgentClass() == null ? "" : agentMaster.getAgentClass());
					 excelHeader.createCell(temp1Count++).setCellValue(agentMaster.getAgentType() == null ? "" : agentMaster.getAgentType());
					 excelHeader.createCell(temp1Count++).setCellValue(agentMaster.getAgentUnitCode() == null ? "" : agentMaster.getAgentUnitCode());
					 excelHeader.createCell(temp1Count++).setCellValue(agentMaster.getAracde() == null ? "" : agentMaster.getAracde());
					 excelHeader.createCell(temp1Count++).setCellValue(agentMaster.getChannel() == null ? "" : agentMaster.getChannel());
					 
					if(agentMaster.getClientDob() == null) {
						 excelHeader.createCell(temp1Count++).setCellValue("");
					 }else {
						 excelHeader.createCell(temp1Count++).setCellValue(agentMaster.getClientDob());
					 }
					
					 excelHeader.createCell(temp1Count++).setCellValue(agentMaster.getClientNumber() == null ? "" : agentMaster.getClientNumber());
					 excelHeader.createCell(temp1Count++).setCellValue(agentMaster.getClientSex() == null ? "" : agentMaster.getClientSex());
					 
					 if(agentMaster.getDateApp() == null) {
						 excelHeader.createCell(temp1Count++).setCellValue("");
					 }else {
						 excelHeader.createCell(temp1Count++).setCellValue(agentMaster.getDateApp());
					 }
					 
					 if(agentMaster.getDateTrm() == null) {
						 excelHeader.createCell(temp1Count++).setCellValue("");
					 }else {
						 excelHeader.createCell(temp1Count++).setCellValue(agentMaster.getDateTrm());
					 }
					 
					 if(agentMaster.getEffectiveDate() == null) {
						 excelHeader.createCell(temp1Count++).setCellValue("");
					 }else {
						 excelHeader.createCell(temp1Count++).setCellValue(agentMaster.getEffectiveDate());
					 }
					 
					 excelHeader.createCell(temp1Count++).setCellValue(agentMaster.getEmployeeCode() == null ? "" : agentMaster.getEmployeeCode());
					 excelHeader.createCell(temp1Count++).setCellValue(agentMaster.getMailId() == null ? "" : agentMaster.getMailId());
					 excelHeader.createCell(temp1Count++).setCellValue(agentMaster.getMinsta() == null ? "" : agentMaster.getMinsta());
					 excelHeader.createCell(temp1Count++).setCellValue(agentMaster.getMobilePhone() == null ? "" : agentMaster.getMobilePhone());
					 excelHeader.createCell(temp1Count++).setCellValue(agentMaster.getNbusallw() == null ? "" : agentMaster.getNbusallw());
					 excelHeader.createCell(temp1Count++).setCellValue(agentMaster.getPanNum() == null ? "" : agentMaster.getPanNum() );
					 excelHeader.createCell(temp1Count++).setCellValue(agentMaster.getPayClient() == null ? "" : agentMaster.getPayClient());
					 excelHeader.createCell(temp1Count++).setCellValue(agentMaster.getPaymentMethod() == null ? "" : agentMaster.getPaymentMethod());
					 excelHeader.createCell(temp1Count++).setCellValue(agentMaster.getPhoneNum1() == null ? "" : agentMaster.getPhoneNum1());
					 excelHeader.createCell(temp1Count++).setCellValue(agentMaster.getPhoneNum2() == null ? "" : agentMaster.getPhoneNum2());
					 excelHeader.createCell(temp1Count++).setCellValue(agentMaster.getProption() == null ? "" : agentMaster.getProption());
					 excelHeader.createCell(temp1Count++).setCellValue(agentMaster.getReasoncd() == null ? "" : agentMaster.getReasoncd());
					 excelHeader.createCell(temp1Count++).setCellValue(agentMaster.getReporting() == null ? "" : agentMaster.getReporting());
					 excelHeader.createCell(temp1Count++).setCellValue(agentMaster.getSalutl() == null ? "" : agentMaster.getSalutl());
					 excelHeader.createCell(temp1Count++).setCellValue(agentMaster.getzBrokAgnt() == null ? "" : agentMaster.getzBrokAgnt());
					 excelHeader.createCell(temp1Count++).setCellValue(agentMaster.getzDateLic() == null ? "" : agentMaster.getzDateLic());
					 row++;
			 }
			 logger.info("Excel Downloding Success");
		 }catch (Exception e) {
			logger.error("Error While Downloading Excel For La Agent List",e);
		}
		 
	}
	
	private void setExcelDownlodForValidAgentList(XSSFSheet excelSheet, List<LaAgentMaster> list) {
		 XSSFRow excelHeader = excelSheet.createRow(0);
		  int tempCount=0;
		 excelHeader.createCell(tempCount++).setCellValue("AGNTNUM");
		 excelHeader.createCell(tempCount++).setCellValue("AGENTNAME");
		 excelHeader.createCell(tempCount++).setCellValue("AGENTADDRESS");
		 excelHeader.createCell(tempCount++).setCellValue("AGENT_BRANCH_CODE");
		 excelHeader.createCell(tempCount++).setCellValue("AGENT_CLASS");
		 excelHeader.createCell(tempCount++).setCellValue("AGTYPE");
		 excelHeader.createCell(tempCount++).setCellValue("AGENT_UNIT_CODE");
		 excelHeader.createCell(tempCount++).setCellValue("ARACDE");
		 excelHeader.createCell(tempCount++).setCellValue("CHANNEL");
		 excelHeader.createCell(tempCount++).setCellValue("CLTDOB");
		 excelHeader.createCell(tempCount++).setCellValue("CLNTNUM");
		 excelHeader.createCell(tempCount++).setCellValue("CLTSEX");
		 excelHeader.createCell(tempCount++).setCellValue("DTEAPP");
		 excelHeader.createCell(tempCount++).setCellValue("DTETRM");
		 excelHeader.createCell(tempCount++).setCellValue("EFFDATE");
		 excelHeader.createCell(tempCount++).setCellValue("EMPLOYEECODE");
		 excelHeader.createCell(tempCount++).setCellValue("MAILID");
		 excelHeader.createCell(tempCount++).setCellValue("MINSTA");
		 excelHeader.createCell(tempCount++).setCellValue("MOBILEPHONE");
		 excelHeader.createCell(tempCount++).setCellValue("NBUSALLW");
		 excelHeader.createCell(tempCount++).setCellValue("PANNo");
		 excelHeader.createCell(tempCount++).setCellValue("PAYCLT");
		 excelHeader.createCell(tempCount++).setCellValue("PAYMETHOD");
		 excelHeader.createCell(tempCount++).setCellValue("PHONENO1");
		 excelHeader.createCell(tempCount++).setCellValue("PHONENO2");
		 excelHeader.createCell(tempCount++).setCellValue("PROPTION");
		 excelHeader.createCell(tempCount++).setCellValue("REASONCD");
		 excelHeader.createCell(tempCount++).setCellValue("REPORTAG");
		 excelHeader.createCell(tempCount++).setCellValue("SALUTL");
		 excelHeader.createCell(tempCount++).setCellValue("ZBROKAGT");
		 excelHeader.createCell(tempCount++).setCellValue("ZDATELIC");
		 
		 try {
			 
			 int row = 1;
			 
			 for (LaAgentMaster agentMaster : list) {
					 excelHeader = excelSheet.createRow(row);
					 int temp1Count=0;
					 excelHeader.createCell(temp1Count++).setCellValue(agentMaster.getAgentNum() == null ? "" : agentMaster.getAgentNum());
					 excelHeader.createCell(temp1Count++).setCellValue(agentMaster.getAgentName() == null ? "" : agentMaster.getAgentName());
					 excelHeader.createCell(temp1Count++).setCellValue(agentMaster.getAgentAddr() == null ? "" : agentMaster.getAgentAddr());
					 excelHeader.createCell(temp1Count++).setCellValue(agentMaster.getAgentBranchCode() == null ? "" : agentMaster.getAgentBranchCode());
					 excelHeader.createCell(temp1Count++).setCellValue(agentMaster.getAgentClass() == null ? "" : agentMaster.getAgentClass());
					 excelHeader.createCell(temp1Count++).setCellValue(agentMaster.getAgentType() == null ? "" : agentMaster.getAgentType());
					 excelHeader.createCell(temp1Count++).setCellValue(agentMaster.getAgentUnitCode() == null ? "" : agentMaster.getAgentUnitCode());
					 excelHeader.createCell(temp1Count++).setCellValue(agentMaster.getAracde() == null ? "" : agentMaster.getAracde());
					 excelHeader.createCell(temp1Count++).setCellValue(agentMaster.getChannel() == null ? "" : agentMaster.getChannel());
					 
					if(agentMaster.getClientDob() == null) {
						 excelHeader.createCell(temp1Count++).setCellValue("");
					 }else {
						 excelHeader.createCell(temp1Count++).setCellValue(agentMaster.getClientDob());
					 }
					
					 excelHeader.createCell(temp1Count++).setCellValue(agentMaster.getClientNumber() == null ? "" : agentMaster.getClientNumber());
					 excelHeader.createCell(temp1Count++).setCellValue(agentMaster.getClientSex() == null ? "" : agentMaster.getClientSex());
					 
					 if(agentMaster.getDateApp() == null) {
						 excelHeader.createCell(temp1Count++).setCellValue("");
					 }else {
						 excelHeader.createCell(temp1Count++).setCellValue(agentMaster.getDateApp());
					 }
					 
					 if(agentMaster.getDateTrm() == null) {
						 excelHeader.createCell(temp1Count++).setCellValue("");
					 }else {
						 excelHeader.createCell(temp1Count++).setCellValue(agentMaster.getDateTrm());
					 }
					 
					 if(agentMaster.getEffectiveDate() == null) {
						 excelHeader.createCell(temp1Count++).setCellValue("");
					 }else {
						 excelHeader.createCell(temp1Count++).setCellValue(agentMaster.getEffectiveDate());
					 }
					 
					 excelHeader.createCell(temp1Count++).setCellValue(agentMaster.getEmployeeCode() == null ? "" : agentMaster.getEmployeeCode());
					 excelHeader.createCell(temp1Count++).setCellValue(agentMaster.getMailId() == null ? "" : agentMaster.getMailId());
					 excelHeader.createCell(temp1Count++).setCellValue(agentMaster.getMinsta() == null ? "" : agentMaster.getMinsta());
					 excelHeader.createCell(temp1Count++).setCellValue(agentMaster.getMobilePhone() == null ? "" : agentMaster.getMobilePhone());
					 excelHeader.createCell(temp1Count++).setCellValue(agentMaster.getNbusallw() == null ? "" : agentMaster.getNbusallw());
					 excelHeader.createCell(temp1Count++).setCellValue(agentMaster.getPanNum() == null ? "" : agentMaster.getPanNum() );
					 excelHeader.createCell(temp1Count++).setCellValue(agentMaster.getPayClient() == null ? "" : agentMaster.getPayClient());
					 excelHeader.createCell(temp1Count++).setCellValue(agentMaster.getPaymentMethod() == null ? "" : agentMaster.getPaymentMethod());
					 excelHeader.createCell(temp1Count++).setCellValue(agentMaster.getPhoneNum1() == null ? "" : agentMaster.getPhoneNum1());
					 excelHeader.createCell(temp1Count++).setCellValue(agentMaster.getPhoneNum2() == null ? "" : agentMaster.getPhoneNum2());
					 excelHeader.createCell(temp1Count++).setCellValue(agentMaster.getProption() == null ? "" : agentMaster.getProption());
					 excelHeader.createCell(temp1Count++).setCellValue(agentMaster.getReasoncd() == null ? "" : agentMaster.getReasoncd());
					 excelHeader.createCell(temp1Count++).setCellValue(agentMaster.getReporting() == null ? "" : agentMaster.getReporting());
					 excelHeader.createCell(temp1Count++).setCellValue(agentMaster.getSalutl() == null ? "" : agentMaster.getSalutl());
					 excelHeader.createCell(temp1Count++).setCellValue(agentMaster.getzBrokAgnt() == null ? "" : agentMaster.getzBrokAgnt());
					 excelHeader.createCell(temp1Count++).setCellValue(agentMaster.getzDateLic() == null ? "" : agentMaster.getzDateLic());
					 row++;
			 }
			 logger.info("Excel Downloding Success");
		 }catch (Exception e) {
			logger.error("Error While Downloading Excel For La Agent List",e);
		}
		 
	}
	
	private void setExcelDownlodForInvalidAgentList(XSSFSheet excelSheet, List<LaAgentMaster> list) {
		 XSSFRow excelHeader = excelSheet.createRow(0);
		  int tempCount=0;
		 excelHeader.createCell(tempCount++).setCellValue("AGNTNUM");
		 excelHeader.createCell(tempCount++).setCellValue("AGENTNAME");
		 excelHeader.createCell(tempCount++).setCellValue("AGENTADDRESS");
		 excelHeader.createCell(tempCount++).setCellValue("AGENT_BRANCH_CODE");
		 excelHeader.createCell(tempCount++).setCellValue("AGENT_CLASS");
		 excelHeader.createCell(tempCount++).setCellValue("AGTYPE");
		 excelHeader.createCell(tempCount++).setCellValue("AGENT_UNIT_CODE");
		 excelHeader.createCell(tempCount++).setCellValue("ARACDE");
		 excelHeader.createCell(tempCount++).setCellValue("CHANNEL");
		 excelHeader.createCell(tempCount++).setCellValue("CLTDOB");
		 excelHeader.createCell(tempCount++).setCellValue("CLNTNUM");
		 excelHeader.createCell(tempCount++).setCellValue("CLTSEX");
		 excelHeader.createCell(tempCount++).setCellValue("DTEAPP");
		 excelHeader.createCell(tempCount++).setCellValue("DTETRM");
		 excelHeader.createCell(tempCount++).setCellValue("EFFDATE");
		 excelHeader.createCell(tempCount++).setCellValue("EMPLOYEECODE");
		 excelHeader.createCell(tempCount++).setCellValue("MAILID");
		 excelHeader.createCell(tempCount++).setCellValue("MINSTA");
		 excelHeader.createCell(tempCount++).setCellValue("MOBILEPHONE");
		 excelHeader.createCell(tempCount++).setCellValue("NBUSALLW");
		 excelHeader.createCell(tempCount++).setCellValue("PANNo");
		 excelHeader.createCell(tempCount++).setCellValue("PAYCLT");
		 excelHeader.createCell(tempCount++).setCellValue("PAYMETHOD");
		 excelHeader.createCell(tempCount++).setCellValue("PHONENO1");
		 excelHeader.createCell(tempCount++).setCellValue("PHONENO2");
		 excelHeader.createCell(tempCount++).setCellValue("PROPTION");
		 excelHeader.createCell(tempCount++).setCellValue("REASONCD");
		 excelHeader.createCell(tempCount++).setCellValue("REPORTAG");
		 excelHeader.createCell(tempCount++).setCellValue("SALUTL");
		 excelHeader.createCell(tempCount++).setCellValue("ZBROKAGT");
		 excelHeader.createCell(tempCount++).setCellValue("ZDATELIC");
		 excelHeader.createCell(tempCount++).setCellValue("REMARKS");
		 
		 try {
			 
			 int row = 1;
			 
			 for (LaAgentMaster agentMaster : list) {
					 excelHeader = excelSheet.createRow(row);
					 int temp1Count=0;
					 excelHeader.createCell(temp1Count++).setCellValue(agentMaster.getAgentNum() == null ? "" : agentMaster.getAgentNum());
					 excelHeader.createCell(temp1Count++).setCellValue(agentMaster.getAgentName() == null ? "" : agentMaster.getAgentName());
					 excelHeader.createCell(temp1Count++).setCellValue(agentMaster.getAgentAddr() == null ? "" : agentMaster.getAgentAddr());
					 excelHeader.createCell(temp1Count++).setCellValue(agentMaster.getAgentBranchCode() == null ? "" : agentMaster.getAgentBranchCode());
					 excelHeader.createCell(temp1Count++).setCellValue(agentMaster.getAgentClass() == null ? "" : agentMaster.getAgentClass());
					 excelHeader.createCell(temp1Count++).setCellValue(agentMaster.getAgentType() == null ? "" : agentMaster.getAgentType());
					 excelHeader.createCell(temp1Count++).setCellValue(agentMaster.getAgentUnitCode() == null ? "" : agentMaster.getAgentUnitCode());
					 excelHeader.createCell(temp1Count++).setCellValue(agentMaster.getAracde() == null ? "" : agentMaster.getAracde());
					 excelHeader.createCell(temp1Count++).setCellValue(agentMaster.getChannel() == null ? "" : agentMaster.getChannel());
					 
					if(agentMaster.getClientDob() == null) {
						 excelHeader.createCell(temp1Count++).setCellValue("");
					 }else {
						 excelHeader.createCell(temp1Count++).setCellValue(agentMaster.getClientDob());
					 }
					
					 excelHeader.createCell(temp1Count++).setCellValue(agentMaster.getClientNumber() == null ? "" : agentMaster.getClientNumber());
					 excelHeader.createCell(temp1Count++).setCellValue(agentMaster.getClientSex() == null ? "" : agentMaster.getClientSex());
					 
					 if(agentMaster.getDateApp() == null) {
						 excelHeader.createCell(temp1Count++).setCellValue("");
					 }else {
						 excelHeader.createCell(temp1Count++).setCellValue(agentMaster.getDateApp());
					 }
					 
					 if(agentMaster.getDateTrm() == null) {
						 excelHeader.createCell(temp1Count++).setCellValue("");
					 }else {
						 excelHeader.createCell(temp1Count++).setCellValue(agentMaster.getDateTrm());
					 }
					 
					 if(agentMaster.getEffectiveDate() == null) {
						 excelHeader.createCell(temp1Count++).setCellValue("");
					 }else {
						 excelHeader.createCell(temp1Count++).setCellValue(agentMaster.getEffectiveDate());
					 }
					 
					 excelHeader.createCell(temp1Count++).setCellValue(agentMaster.getEmployeeCode() == null ? "" : agentMaster.getEmployeeCode());
					 excelHeader.createCell(temp1Count++).setCellValue(agentMaster.getMailId() == null ? "" : agentMaster.getMailId());
					 excelHeader.createCell(temp1Count++).setCellValue(agentMaster.getMinsta() == null ? "" : agentMaster.getMinsta());
					 excelHeader.createCell(temp1Count++).setCellValue(agentMaster.getMobilePhone() == null ? "" : agentMaster.getMobilePhone());
					 excelHeader.createCell(temp1Count++).setCellValue(agentMaster.getNbusallw() == null ? "" : agentMaster.getNbusallw());
					 excelHeader.createCell(temp1Count++).setCellValue(agentMaster.getPanNum() == null ? "" : agentMaster.getPanNum() );
					 excelHeader.createCell(temp1Count++).setCellValue(agentMaster.getPayClient() == null ? "" : agentMaster.getPayClient());
					 excelHeader.createCell(temp1Count++).setCellValue(agentMaster.getPaymentMethod() == null ? "" : agentMaster.getPaymentMethod());
					 excelHeader.createCell(temp1Count++).setCellValue(agentMaster.getPhoneNum1() == null ? "" : agentMaster.getPhoneNum1());
					 excelHeader.createCell(temp1Count++).setCellValue(agentMaster.getPhoneNum2() == null ? "" : agentMaster.getPhoneNum2());
					 excelHeader.createCell(temp1Count++).setCellValue(agentMaster.getProption() == null ? "" : agentMaster.getProption());
					 excelHeader.createCell(temp1Count++).setCellValue(agentMaster.getReasoncd() == null ? "" : agentMaster.getReasoncd());
					 excelHeader.createCell(temp1Count++).setCellValue(agentMaster.getReporting() == null ? "" : agentMaster.getReporting());
					 excelHeader.createCell(temp1Count++).setCellValue(agentMaster.getSalutl() == null ? "" : agentMaster.getSalutl());
					 excelHeader.createCell(temp1Count++).setCellValue(agentMaster.getzBrokAgnt() == null ? "" : agentMaster.getzBrokAgnt());
					 excelHeader.createCell(temp1Count++).setCellValue(agentMaster.getzDateLic() == null ? "" : agentMaster.getzDateLic());
					 excelHeader.createCell(temp1Count++).setCellValue(agentMaster.getRemarks() == null ? "" : agentMaster.getRemarks());
					 row++;
			 }
			 logger.info("Excel Downloding Success");
		 }catch (Exception e) {
			logger.error("Error While Downloading Excel For La Agent List",e);
		}
		 
	}

	public static BigDecimal round(float d, int decimalPlace) {
		BigDecimal bd = new BigDecimal(Float.toString(d));
		bd = bd.setScale(decimalPlace, BigDecimal.ROUND_HALF_UP);
		return bd;
	}

	

}
