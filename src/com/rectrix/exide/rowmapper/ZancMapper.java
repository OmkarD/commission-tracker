package com.rectrix.exide.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.rectrix.exide.models.Zanc;

public class ZancMapper implements RowMapper<Zanc> {

	@Override
	public Zanc mapRow(ResultSet rs, int rn) throws SQLException {
		Zanc zancDetails = new Zanc();
		zancDetails.setZancId(rs.getLong("ZancId"));
		zancDetails.setAgentNumber(rs.getString("AGNTNUM"));
		zancDetails.setClientNumber(rs.getString("CLNTNUM"));
		zancDetails.setAgentNumber1(rs.getString("AGNTNUM1"));
		zancDetails.setAgentName(rs.getString("AGENTNAME"));
		zancDetails.setClientName(rs.getString("CLNTSNAME"));
		zancDetails.setClientAddress1(rs.getString("CLTADDR01"));
		zancDetails.setClientAddress2(rs.getString("CLTADDR02"));
		zancDetails.setClientAddress3(rs.getString("CLTADDR03"));
		zancDetails.setClientAddress4(rs.getString("CLTADDR04"));
		zancDetails.setClientAddress5(rs.getString("CLTADDR05"));
		zancDetails.setPayOfMonth(rs.getString("PAYMTH"));
		zancDetails.setBankAccKey(rs.getString("BANKACCKEY"));
		zancDetails.setzCommPaid(rs.getFloat("ZCOMMPAID"));
		zancDetails.setzTaxWhld(rs.getFloat("ZTAXWHLD"));
		zancDetails.setzTotAmtPd(rs.getFloat("ZTOTAMTPD"));
		zancDetails.setAgentClass(rs.getString("AGENT_CLASS"));
		zancDetails.setwTaxRate(rs.getFloat("WTAXRATE"));
		zancDetails.setzAdjust(rs.getFloat("ZADJUST"));
		zancDetails.setzDocNum(rs.getString("ZDOCNO"));
		zancDetails.setIRDNum(rs.getString("IRDNO"));
		zancDetails.setAgentBranchCode(rs.getString("AGENT_BRANCH_CODE"));
		zancDetails.setAgentUnitCode(rs.getString("AGENT_UNIT_CODE"));
		zancDetails.setFromDate(rs.getDate("FRMDT"));
		zancDetails.setBankKey(rs.getString("BankKey"));
		zancDetails.setzIfscCode(rs.getString("ZIFSCCODE"));
		zancDetails.setBankDesc(rs.getString("BANKDESC"));
		zancDetails.setCycleName(rs.getString("CYCLE_NAME"));
		return null;
	}

}
