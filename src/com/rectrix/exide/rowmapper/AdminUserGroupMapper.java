package com.rectrix.exide.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.rectrix.exide.models.AdminUserGroups;

public class AdminUserGroupMapper implements RowMapper<AdminUserGroups> {

	@Override
	public AdminUserGroups mapRow(ResultSet rs, int rn) throws SQLException {
		
		AdminUserGroups user = new AdminUserGroups();
		user.setGroupId(rs.getString("GROUP_ID"));
		user.setStatus(rs.getInt("STATUS"));
		user.setType(rs.getInt("TYPE"));
		return user;
	}

}
