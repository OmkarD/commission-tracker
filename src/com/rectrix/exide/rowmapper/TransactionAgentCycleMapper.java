package com.rectrix.exide.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.rectrix.exide.models.LaAgentMaster;
import com.rectrix.exide.models.TransactionAgentCycle;

public class TransactionAgentCycleMapper implements RowMapper<TransactionAgentCycle> {

	@Override
	public TransactionAgentCycle mapRow(ResultSet rs, int rn) throws SQLException {
		TransactionAgentCycle agentMaster = new TransactionAgentCycle();
		
		agentMaster.setAgentAddr(rs.getString("AGENTADDRESS"));
		agentMaster.setAgentBranchCode(rs.getString("AGENT_BRANCH_CODE"));
		agentMaster.setAgentClass(rs.getString("AGENT_CLASS"));
		agentMaster.setAgentName(rs.getString("AGENTNAME"));
		agentMaster.setAgentNum(rs.getString("AGNTNUM"));
		agentMaster.setAgentType(rs.getString("AGTYPE"));
		agentMaster.setAgentUnitCode(rs.getString("AGENT_UNIT_CODE"));
		agentMaster.setAracde(rs.getString("ARACDE"));
		agentMaster.setChannel(rs.getString("CHANNEL"));
		agentMaster.setClientDob(rs.getDate("CLTDOB"));
		agentMaster.setClientNumber(rs.getString("CLNTNUM"));
		agentMaster.setClientSex(rs.getString("CLTSEX"));
		agentMaster.setDateApp(rs.getDate("DTEAPP"));
		agentMaster.setDateTrm(rs.getDate("DTETRM"));
		agentMaster.setEffectiveDate(rs.getDate("EFFDATE"));
		agentMaster.setEmployeeCode(rs.getString("EMPLOYEECODE"));
		agentMaster.setMailId(rs.getString("MAILID"));
		agentMaster.setMinsta(rs.getString("MINSTA"));
		agentMaster.setMobilePhone(rs.getString("MOBILEPHONE"));
		agentMaster.setNbusallw(rs.getString("NBUSALLW"));
		agentMaster.setPanNum(rs.getString("PANNo"));
		agentMaster.setPayClient(rs.getString("PAYCLT"));
		agentMaster.setPaymentMethod(rs.getString("PAYMETHOD"));
		agentMaster.setPhoneNum1(rs.getString("PHONENO1"));
		agentMaster.setPhoneNum2(rs.getString("PHONENO2"));
		agentMaster.setProption(rs.getString("PROPTION"));
		agentMaster.setReasoncd(rs.getString("REASONCD"));
		agentMaster.setReporting(rs.getString("REPORTAG"));
		agentMaster.setSalutl(rs.getString("SALUTL"));
		agentMaster.setzBrokAgnt(rs.getString("ZBROKAGT"));
		agentMaster.setzDateLic(rs.getString("ZDATELIC"));
		agentMaster.setBankAccNumber(rs.getString("BANKACCNO"));
		agentMaster.setIfsc(rs.getString("IFSC"));
		agentMaster.setCycleId(rs.getInt("CYCLE_ID"));
		
		return agentMaster;
	}

}
