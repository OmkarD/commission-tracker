package com.rectrix.exide.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.rectrix.exide.models.CycleMaster;

public class CycleMasterMapper implements RowMapper<CycleMaster> {

	@Override
	public CycleMaster mapRow(ResultSet rs, int rn) throws SQLException {
		
		CycleMaster cycleMaster = new CycleMaster();
		cycleMaster.setCycleId(rs.getLong("CYCLE_ID"));
		cycleMaster.setCycleName(rs.getString("CYCLE_NAME"));
		cycleMaster.setFromDate(rs.getDate("FROM_DATE"));
		cycleMaster.setToDate(rs.getDate("TO_DATE"));
		cycleMaster.setCycleDescription(rs.getString("CYCLE_DESC"));
		cycleMaster.setCreatedBy(rs.getString("CREATED_BY"));
		cycleMaster.setCriatedOn(rs.getDate("CREATED_ON"));
		cycleMaster.setValidflag(rs.getInt("VALID_FLAG"));
		cycleMaster.setAgentSyncStatus(rs.getInt("AGENT_SYNC_STATUS"));
		return cycleMaster;
	}

}
