package com.rectrix.exide.controller;


import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;


@Controller
@RequestMapping(value = "/")
@Scope(value = "request")
public class HomeController {
	private static final Logger logger = Logger.getLogger(HomeController.class);
	
	       
	public HomeController() {
		// TODO Auto-generated constructor stub
	}
	
	@RequestMapping(value = "user")
	public String user(){
		return "user/user";
	}
	
	@RequestMapping(value = "admin")
	public String admin(){
		return "admin/admin";
	}
	
	@RequestMapping(value = "login", method = RequestMethod.GET)
	public String login(){		
		return "home/login";
	}
	
	@RequestMapping(value = "loginsuccess")
	public String loginSuccess(){		
		return "loginsucccess";
	}
	
	@RequestMapping(value = "logindenied")
	public ModelAndView loginAccessDenied(){	
		/*Map map=new HashMap();
		SessionMaster sessionMaster = sessionMasterDao.getActiveSessionMaster();
		if(sessionMaster!=null){
		Date date=new Date();
		Date currentDate = new Date(date.getYear(), date.getMonth(), date.getDate());
				
		Date sessionEndDate =new Date(sessionMaster.getEndTime().getYear(), sessionMaster.getEndTime().getMonth(), sessionMaster.getEndTime().getDate());
		Date sessionStartDate = new Date(sessionMaster.getStartTime().getYear(), sessionMaster.getStartTime().getMonth(), sessionMaster.getStartTime().getDate());
		int result = currentDate.compareTo(sessionEndDate);
		int ewsult2= currentDate.compareTo(sessionStartDate);
		if(result>0){
			map.put("errormessage", "Currently there is no Active session, please contact Administrator");
		}else if(result<=0){
			map.put("errormessage", "Some Problem While Login");
		}
		}else{
			map.put("errormessage", "Currently there is no Active session, please contact Administrator");
		}
		return new ModelAndView("home/accessdenied",map);*/
		
		Map map=new HashMap();
		map.put("errormessage", "Don't have access for this user name .");
		return new ModelAndView( "home/login",map);
	}
	
	@RequestMapping(value = "loginfailed")
	public ModelAndView loginFailed(){		
		Map map=new HashMap();
		map.put("errormessage", "Invalid Userid or Password");
		return new ModelAndView("home/login",map);
	}	

	/**
	 * Logout
	 * 
	 * 
	 * 
	 */
	@RequestMapping(value = "logout", method = RequestMethod.GET)
	public String logout(HttpServletRequest request, HttpServletResponse response){		
		/*Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();
		if (auth != null) {
			new SecurityContextLogoutHandler().logout(request, response, auth);
		}
		
		return "home/login";*/
		Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();
		if (auth != null) {
			new SecurityContextLogoutHandler().logout(request, response, auth);
		}
		SecurityContextHolder.getContext().setAuthentication(null);
		return "home/login";
	}
	
	
	
	public UserDetails getUserDetails(){
		UserDetails userDetails=null;
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (!(auth instanceof AnonymousAuthenticationToken)) {
		       userDetails = (UserDetails) auth.getPrincipal();
		}
		return userDetails;
	}
	
	
}

