package com.rectrix.exide.controller;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpRequest;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import com.rectrix.exide.form.models.CycleMasterForm;
import com.rectrix.exide.form.models.DataTableModel;
import com.rectrix.exide.form.models.DataTableResultModel;
import com.rectrix.exide.models.CycleMaster;
import com.rectrix.exide.models.LaAgentMaster;
import com.rectrix.exide.models.Zanc;
import com.rectrix.exide.service.UserService;
import com.rectrix.exide.utils.Impl.DataTableRequestPcrocess;

@Controller
@RequestMapping(value = "/user/")
@Scope(value = "request")
public class UserController {
	private static final Logger logger = Logger.getLogger(UserController.class);
	   
	@Autowired
	UserService userService;
	
	@Autowired
	private DataTableRequestPcrocess dataTableRequestPcrocess;
	
	public UserController() {
		logger.info("UserController Calling ");
	}
	
	/*
	 * User Details 
	 */
	public UserDetails getUserDetailsWithRoles(){
		UserDetails userDetails=null;
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (!(auth instanceof AnonymousAuthenticationToken)) {
		       userDetails = (UserDetails) auth.getPrincipal();
		}
		
		return userDetails;
	}
	
	/*
	 * User Roles 
	 */
	@RequestMapping(value="userroles", method = RequestMethod.GET)
	public @ResponseBody String getUserRoles(HttpSession httpSession){
		String userRoles = "";
		
		userRoles = (String) httpSession.getAttribute("admin");
		return userRoles;
	}
	
	/*
	 * Adding cycle id to session
	 */
	
	@RequestMapping(value="addvariable", method = RequestMethod.POST)
	public @ResponseBody String addVariable(@RequestBody long cycleId,HttpSession httpSession){
		try{
			httpSession.setAttribute("cycleId",cycleId+"");
			return "1";
		}catch(Exception e){
			logger.error("Error While saving cycle id in session ",e);
			return "2";
		}
		
	}
	
	/*
	 *  Viewing List of Cycles created, sorted by created first
	 * 
	 */
	
	@RequestMapping(value = "listofcycles", method = {RequestMethod.GET,RequestMethod.POST})
	public @ResponseBody DataTableResultModel getListOfCyclesCreated(HttpSession httpSession,HttpServletRequest request){
		
		
		try {
			httpSession.removeAttribute("cycleId");
			
			DataTableModel model = dataTableRequestPcrocess.geDataTableModel(request);
			DataTableResultModel result =userService.paginGetListOfCyclesCreated(model);
			
			return result;
			
		}
		catch(Exception e) {
			logger.error("Error While Getting Cycle Master For User", e);
			return new DataTableResultModel();
		}
	}
	
	/*
	 *  saving data for create new cycle
	 */
	
	@RequestMapping(value = "createcycle", method = RequestMethod.POST )
	public @ResponseBody String newCycle(@RequestBody CycleMasterForm cycleMasterForm){
		
		try {
			
			CycleMaster CurrentCycleMaster = userService.getCurrentCycleDetails();
			
			UserDetails userDetails = getUserDetailsWithRoles();
				CycleMaster cycleMaster = new CycleMaster();
				cycleMaster.setCycleName(cycleMasterForm.getCycleName());
				cycleMaster.setFromDate(cycleMasterForm.getFromDate());
				cycleMaster.setToDate(cycleMasterForm.getToDate());
				cycleMaster.setCreatedBy(userDetails.getUsername());
				cycleMaster.setCriatedOn(new Date());
				cycleMaster.setCycleDescription(cycleMasterForm.getCycleDescription());
				cycleMaster.setValidflag(1);
				cycleMaster.setAgentSyncStatus(1);
				
				userService.saveNewCycle(cycleMaster);
				
			return "1";
		}catch(Exception e) {
			logger.error("Error While Creating New Cycle", e);
		}
		return "2";
	}
	
	
	/*
	 * List of agents from LA400 (View Created in CT)
	 */
	
	@RequestMapping(value = "listofagents", method = RequestMethod.GET )
	public @ResponseBody DataTableResultModel getListOfAgentDetails(HttpServletRequest request){
		
		try {
			
			String cycleId =  (String) request.getSession().getAttribute("cycleId");
			DataTableModel model = dataTableRequestPcrocess.geDataTableModel(request);
			DataTableResultModel result = userService.paginGetListOfLaAgentDetails(model,Long.parseLong(cycleId));
			return result;
			
		}catch(Exception e) {
			logger.error("Error While Getting Zanc For User", e);
			return new DataTableResultModel();
		}
	}
	
	/*
	 * Download list of agents for excel
	 */
	
	@RequestMapping(value="excelforagentlist", method=RequestMethod.GET)
	public ModelAndView getExcelForAgentList( HttpSession httpSession){
		
		Map map = new HashMap();
		map.put("excel", "agentlist");
		List<LaAgentMaster> list = new ArrayList<LaAgentMaster>();

		try {
			list = userService.getListOfAgents();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error("Error While Downloding data for Excel ",e);
		}
		map.put("agentdata",list);
		return new ModelAndView("ExportExcel", map);
		}
	
	/*
	 * validate LA400 agents list by sp 
	 */
	
	@RequestMapping(value = "validagentslist", method = RequestMethod.GET )
	public @ResponseBody DataTableResultModel getValidAgentDetals(HttpServletRequest request,HttpSession session){
		
		try {
			
			String cycleId =  (String) session.getAttribute("cycleId");
			
			DataTableModel model = dataTableRequestPcrocess.geDataTableModel(request);
			//passing 1 to get valid agents
			DataTableResultModel result = userService.paginGetValidAgentDetals(model,Long.parseLong(cycleId.toString()),1);
			return result;
			
			
		}catch(Exception e) {
			logger.error("Error While Getting valid data for agents", e);
			return new DataTableResultModel();
		}
	}
	
	/*
	 * invalidate LA400 agents list by sp 
	 */
	
	@RequestMapping(value = "invalidagentslist", method = RequestMethod.GET )
	public @ResponseBody DataTableResultModel getInvalidAgentDetals(HttpServletRequest request, HttpSession session){
		
		try {
			
			String cycleId =  (String) session.getAttribute("cycleId");
			
			DataTableModel model = dataTableRequestPcrocess.geDataTableModel(request);
			//passing 2 to get invalid agents
			DataTableResultModel result = userService.paginGetValidAgentDetals(model,Long.parseLong(cycleId.toString()),2);
			return result;
			
			
		}catch(Exception e) {
			logger.error("Error While Getting valid and invalid data for agents", e);
			return new DataTableResultModel();
		}
	}
	
	/*
	 * excel for valid agent list
	 */
	
	@RequestMapping(value="excelforvalidagentlist", method=RequestMethod.GET)
	public ModelAndView getExcelForValidAgentList( HttpSession session){
		
		Map map = new HashMap();
		map.put("excel", "validagentlist");
		List<LaAgentMaster> list = new ArrayList<LaAgentMaster>();

		try {
			String cycleId =  (String) session.getAttribute("cycleId");
			list = userService.getListOfValidAgentsListByCycleId(Long.parseLong(cycleId),1);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error("Error While Downloding valid agent data for Excel ",e);
		}
		map.put("agentvaliddata",list);
		return new ModelAndView("ExportExcel", map);
		}
	
	/*
	 * excel for invalid agent list
	 */
	
	@RequestMapping(value="excelforinvalidagentlist", method=RequestMethod.GET)
	public ModelAndView getExcelForIndalidAgentList( HttpSession session){
		
		Map map = new HashMap();
		map.put("excel", "invalidagentlist");
		List<LaAgentMaster> list = new ArrayList<LaAgentMaster>();

		try {
			String cycleId =  (String) session.getAttribute("cycleId");
			list = userService.getListOfValidAgentsListByCycleId(Long.parseLong(cycleId),2);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error("Error While Downloding invalid agent data for Excel ",e);
		}
		map.put("agentinvaliddata",list);
		return new ModelAndView("ExportExcel", map);
		}
	
	/*
	 * Sinking LA Agent List To CT
	 */
	
	@RequestMapping(value = "syncagentlist", method = RequestMethod.POST )
	public @ResponseBody String sinkingAgentListFromLa(HttpServletRequest request, HttpSession session){
		
		try {
			
			String cycleId =  (String) session.getAttribute("cycleId");
			userService.saveAgentListForCycle(Long.parseLong(cycleId));
			return "1";
			
		}catch(Exception e) {
			logger.error("Error While Sinking agent list to ct ", e);
			return "2";
		}
	}
	

	/*
	 * list of commission
	 */
	
	@RequestMapping(value = "listofagpay", method = RequestMethod.GET )
	public @ResponseBody DataTableResultModel getListOfCommission(HttpServletRequest request, HttpSession session){
		
		try {
			
			String cycleId =  (String) session.getAttribute("cycleId");
			
			DataTableModel model = dataTableRequestPcrocess.geDataTableModel(request);
			DataTableResultModel result = userService.paginGetListOfAgPayUploadByCycleId(model,Long.parseLong(cycleId.toString()));
			return result;
			
			
		}catch(Exception e) {
			logger.error("Error While Getting list of Ag Pay upload data cycle wise ", e);
			return new DataTableResultModel();
		}
	}
	
	
	/*
	 * Upload Ag Pay File
	 */
	
	@RequestMapping(value = "uploadagpay", method = RequestMethod.POST )
	public @ResponseBody String getListOfCommission(MultipartHttpServletRequest request, HttpSession session){
		
		try {
			
			String cycleId =  (String) session.getAttribute("cycleId");
			
			MultipartFile multipartFile = request.getFile("filename");
			String [] fullNameWithExt=multipartFile.getOriginalFilename().toString().split(Pattern.quote("."));
				if(fullNameWithExt[fullNameWithExt.length-1].equalsIgnoreCase("xlsx")){
					XSSFWorkbook workbook=new XSSFWorkbook(multipartFile.getInputStream());
					XSSFSheet sheet = workbook.getSheetAt(0);
					Iterator<Row> rowIterator = sheet.iterator();
					if(rowIterator.hasNext()){
						rowIterator.next();
					}
					
					while(rowIterator.hasNext()){
						Row row = rowIterator.next();
						/*TrainerBranchMapForm branch = new TrainerBranchMapForm();*/
						Cell cell;
					/*	if(row.getCell(0)!=null){
							cell = row.getCell(0);
							cell.setCellType(Cell.CELL_TYPE_STRING);
							String eid=null;
							if(cell.getStringCellValue().trim().length()>0)
								eid = cell.getStringCellValue();
							
							if(eid!=null && eid.length()>0){
								cell.setCellType(Cell.CELL_TYPE_STRING);
								if(cell.getStringCellValue().trim().length()>0)
									branch.setEmployeeId(cell.getStringCellValue());
								if(row.getCell(1)!=null){
									cell = row.getCell(1);
									cell.setCellType(Cell.CELL_TYPE_STRING);
									if(cell.getStringCellValue().trim().length()>0)
										branch.setBranchCode(cell.getStringCellValue());
								}
								list.add(branch);
							}
							}*/
						}
				}else if(fullNameWithExt[fullNameWithExt.length-1].equalsIgnoreCase("xls")){
					Workbook workbook= WorkbookFactory.create(multipartFile.getInputStream());
					Sheet sheet = workbook.getSheetAt(0);
					Iterator<Row> rowIterator = sheet.iterator();
					if(rowIterator.hasNext()){
						rowIterator.next();
					}
					while(rowIterator.hasNext()){
						Row row = rowIterator.next();
						/*TrainerBranchMapForm branch = new TrainerBranchMapForm();*/
						Cell cell;
					/*	if(row.getCell(0)!=null){
							cell = row.getCell(0);
							cell.setCellType(Cell.CELL_TYPE_STRING);
							String eid=null;
							if(cell.getStringCellValue().trim().length()>0)
								eid = cell.getStringCellValue();
							
							if(eid!=null && eid.length()>0){
								cell.setCellType(Cell.CELL_TYPE_STRING);
								if(cell.getStringCellValue().trim().length()>0)
									branch.setEmployeeId(cell.getStringCellValue());
								if(row.getCell(1)!=null){
									cell = row.getCell(1);
									cell.setCellType(Cell.CELL_TYPE_STRING);
									if(cell.getStringCellValue().trim().length()>0)
										branch.setBranchCode(cell.getStringCellValue());
								}
								list.add(branch);
							}
							}*/
						}
				}
			
			DataTableModel model = dataTableRequestPcrocess.geDataTableModel(request);
			DataTableResultModel result = userService.paginGetListOfAgPayUploadByCycleId(model,Long.parseLong(cycleId.toString()));
			return "1";
			
			
		}catch(Exception e) {
			logger.error("Error While Uploading Excel For Ag Pay ", e);
			return "2";
		}
	}
	
	/*
	 * Calculate AgPay
	 */
	
	@RequestMapping(value = "calculateagpay", method = RequestMethod.GET )
	public @ResponseBody DataTableResultModel getCalculatedAgPay(HttpServletRequest request, HttpSession session){
		
		try {
			
			String cycleId =  (String) session.getAttribute("cycleId");
			
			DataTableModel model = dataTableRequestPcrocess.geDataTableModel(request);
			DataTableResultModel result = userService.paginGetListOfAgPayUploadByCycleId(model,Long.parseLong(cycleId.toString()));
			return result;
			
			
		}catch(Exception e) {
			logger.error("Error While Calculating AgPay per cycle ", e);
			return new DataTableResultModel();
		}
	}
	
}

