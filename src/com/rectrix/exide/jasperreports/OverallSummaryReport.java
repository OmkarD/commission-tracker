package com.rectrix.exide.jasperreports;

public class OverallSummaryReport {
	
	private String colOne;
	private String colTwo;
	private String colThree;
	private String colFour;
	private String colFive;
	private String colSix;
	private String colSeven;
	private String colEight;
	private String colNine;
	private String colTen;
	private String col11;
	private String col12;
	
	
	
	public OverallSummaryReport() {
		// TODO Auto-generated constructor stub
	}
	
	
	/**
	 * @return the col12
	 */
	public String getCol12() {
		return col12;
	}
	/**
	 * @param col12 the col12 to set
	 */
	public void setCol12(String col12) {
		this.col12 = col12;
	}
	/**
	 * @return the colEight
	 */
	public String getColEight() {
		return colEight;
	}
	/**
	 * @param colEight the colEight to set
	 */
	public void setColEight(String colEight) {
		this.colEight = colEight;
	}
	/**
	 * @return the colNine
	 */
	public String getColNine() {
		return colNine;
	}
	/**
	 * @param colNine the colNine to set
	 */
	public void setColNine(String colNine) {
		this.colNine = colNine;
	}
	/**
	 * @return the colTen
	 */
	public String getColTen() {
		return colTen;
	}
	/**
	 * @param colTen the colTen to set
	 */
	public void setColTen(String colTen) {
		this.colTen = colTen;
	}
	/**
	 * @return the col11
	 */
	public String getCol11() {
		return col11;
	}
	/**
	 * @param col11 the col11 to set
	 */
	public void setCol11(String col11) {
		this.col11 = col11;
	}
	/**
	 * @return the colOne
	 */
	public String getColOne() {
		return colOne;
	}
	/**
	 * @param colOne the colOne to set
	 */
	public void setColOne(String colOne) {
		this.colOne = colOne;
	}
	/**
	 * @return the colTwo
	 */
	public String getColTwo() {
		return colTwo;
	}
	/**
	 * @param colTwo the colTwo to set
	 */
	public void setColTwo(String colTwo) {
		this.colTwo = colTwo;
	}
	/**
	 * @return the colThree
	 */
	public String getColThree() {
		return colThree;
	}
	/**
	 * @param colThree the colThree to set
	 */
	public void setColThree(String colThree) {
		this.colThree = colThree;
	}
	/**
	 * @return the colFour
	 */
	public String getColFour() {
		return colFour;
	}
	/**
	 * @param colFour the colFour to set
	 */
	public void setColFour(String colFour) {
		this.colFour = colFour;
	}
	/**
	 * @return the colFive
	 */
	public String getColFive() {
		return colFive;
	}
	/**
	 * @param colFive the colFive to set
	 */
	public void setColFive(String colFive) {
		this.colFive = colFive;
	}
	/**
	 * @return the colSix
	 */
	public String getColSix() {
		return colSix;
	}
	/**
	 * @param colSix the colSix to set
	 */
	public void setColSix(String colSix) {
		this.colSix = colSix;
	}
	/**
	 * @return the colSeven
	 */
	public String getColSeven() {
		return colSeven;
	}
	/**
	 * @param colSeven the colSeven to set
	 */
	public void setColSeven(String colSeven) {
		this.colSeven = colSeven;
	}
	
	
	
	

}
