package com.rectrix.exide.jasperreports;

import java.math.BigDecimal;

import com.itextpdf.text.pdf.PdfStructTreeController.returnType;

public class OverAllSummaryReportWithOthers {

	private String driver;
	private String maxScore;
	private String ques;
	private String selfScore;
	private String dmScore;
	private String drScore;
	private String peScore;
	private String shScore;
	private String avgScore;
	private String mergeScore;
	
	private String dmScoreStyle;
	private String selfScoreStyle;
	private String drScoreStyle;
	private String peScoreStyle;
	private String shScoreStyle;
	private String avgScoreStyle;
	private String mergeScoreStyle;
	
	
	
	
	/**
	 * @return the selfScoreStyle
	 */
	public String getSelfScoreStyle() {
		return selfScoreStyle;
	}
	/**
	 * @param selfScoreStyle the selfScoreStyle to set
	 */
	public void setSelfScoreStyle(String selfScoreStyle) {
		this.selfScoreStyle = selfScoreStyle;
	}
	/**
	 * @return the drScoreStyle
	 */
	public String getDrScoreStyle() {
		return drScoreStyle;
	}
	/**
	 * @param drScoreStyle the drScoreStyle to set
	 */
	public void setDrScoreStyle(String drScoreStyle) {
		this.drScoreStyle = drScoreStyle;
	}
	/**
	 * @return the peScoreStyle
	 */
	public String getPeScoreStyle() {
		return peScoreStyle;
	}
	/**
	 * @param peScoreStyle the peScoreStyle to set
	 */
	public void setPeScoreStyle(String peScoreStyle) {
		this.peScoreStyle = peScoreStyle;
	}
	/**
	 * @return the shScoreStyle
	 */
	public String getShScoreStyle() {
		return shScoreStyle;
	}
	/**
	 * @param shScoreStyle the shScoreStyle to set
	 */
	public void setShScoreStyle(String shScoreStyle) {
		this.shScoreStyle = shScoreStyle;
	}
	/**
	 * @return the avgScoreStyle
	 */
	public String getAvgScoreStyle() {
		Float value =0F;
		Float maxvalue = 1F;
		try{
			value=Float.parseFloat(getAvgScore());
			maxvalue = Float.parseFloat(getMaxScore());
		}catch(Exception e){
			return "WHITE";
		}
		return ""+(getRGB(value, maxvalue));
	}
	/**
	 * @param avgScoreStyle the avgScoreStyle to set
	 */
	public void setAvgScoreStyle(String avgScoreStyle) {
		this.avgScoreStyle = avgScoreStyle;
	}
	/**
	 * @return the mergeScoreStyle
	 */
	public String getMergeScoreStyle() {
		return mergeScoreStyle;
	}
	/**
	 * @param mergeScoreStyle the mergeScoreStyle to set
	 */
	public void setMergeScoreStyle(String mergeScoreStyle) {
		this.mergeScoreStyle = mergeScoreStyle;
	}
	/**
	 * @return the dmScoreStyle
	 */
	public String getDmScoreStyle() {
		return dmScoreStyle;
	}
	/**
	 * @param dmScoreStyle the dmScoreStyle to set
	 */
	public void setDmScoreStyle(String dmScoreStyle) {
		this.dmScoreStyle = dmScoreStyle;
	}
	/**
	 * @return the driver
	 */
	public String getDriver() {
		return driver;
	}
	/**
	 * @param driver the driver to set
	 */
	public void setDriver(String driver) {
		this.driver = driver;
	}
	/**
	 * @return the maxScore
	 */
	public String getMaxScore() {
		return maxScore;
	}
	/**
	 * @param maxScore the maxScore to set
	 */
	public void setMaxScore(String maxScore) {
		this.maxScore = maxScore;
	}
	/**
	 * @return the ques
	 */
	public String getQues() {
		return ques;
	}
	/**
	 * @param ques the ques to set
	 */
	public void setQues(String ques) {
		this.ques = ques;
	}
	/**
	 * @return the selfScore
	 */
	public String getSelfScore() {
		return selfScore;
	}
	/**
	 * @param selfScore the selfScore to set
	 */
	public void setSelfScore(String selfScore) {
		this.selfScore = selfScore;
	}
	/**
	 * @return the dmScore
	 */
	public String getDmScore() {
		return dmScore;
	}
	/**
	 * @param dmScore the dmScore to set
	 */
	public void setDmScore(String dmScore) {
		this.dmScore = dmScore;
	}
	/**
	 * @return the drScore
	 */
	public String getDrScore() {
		return drScore;
	}
	/**
	 * @param drScore the drScore to set
	 */
	public void setDrScore(String drScore) {
		this.drScore = drScore;
	}
	/**
	 * @return the peScore
	 */
	public String getPeScore() {
		return peScore;
	}
	/**
	 * @param peScore the peScore to set
	 */
	public void setPeScore(String peScore) {
		this.peScore = peScore;
	}
	/**
	 * @return the shScore
	 */
	public String getShScore() {
		return shScore;
	}
	/**
	 * @param shScore the shScore to set
	 */
	public void setShScore(String shScore) {
		this.shScore = shScore;
	}
	/**
	 * @return the avgScore
	 */
	public String getAvgScore() {/*
		Float value=0F;
		int count=0;
		try{
			value+=Float.parseFloat(this.dmScore);
			count++;
		}catch(Exception e){
			value+=0;
		}
		try{
			value+=Float.parseFloat(this.drScore);
			count++;
		}catch(Exception e){
			value+=0;
		}
		try{
			value+=Float.parseFloat(this.peScore);
			count++;
		}catch(Exception e){
			value+=0;
		}
		try{
			value+=Float.parseFloat(this.shScore);
			count++;
		}catch(Exception e){
			value+=0;
		}
		try{
			value+=Float.parseFloat(this.mergeScore);
			count++;
		}catch(Exception e){
			value+=0;
		}
		if(value==0)
			return null;
		else
		return ""+round(value/count, 1);
	*/
	return avgScore;	
	}
	/**
	 * @param avgScore the avgScore to set
	 */
	public void setAvgScore(String avgScore) {
		this.avgScore = avgScore;
	}
	/**
	 * @return the mergeScore
	 */
	public String getMergeScore() {
		return mergeScore;
	}
	/**
	 * @param mergeScore the mergeScore to set
	 */
	public void setMergeScore(String mergeScore) {
		this.mergeScore = mergeScore;
	}
	
	public static BigDecimal round(float d, int decimalPlace) {
		BigDecimal bd = new BigDecimal(Float.toString(d));
		bd = bd.setScale(decimalPlace, BigDecimal.ROUND_HALF_UP);
		return bd;
	}
	
	public static String getRGB(Float val, Float max){
		float per =((val*100)/max);
		if(per<50){
			return "RED";
		}else if(per==75){
			return "PALE";
		}else if(per<75){
			return "AMBER";
		}else if(per<101){
			return "GREEN";
		}else{
			return "WHITE";
		}
		}



}
