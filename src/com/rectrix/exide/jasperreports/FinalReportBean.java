package com.rectrix.exide.jasperreports;

import java.util.List;

public class FinalReportBean {
	
	private List<OverallSummaryReport> overallSummaryReports;
	private List<OverAllSummaryReportWithOthers> overAllSummaryReportWithOthers;
	private List<OverAllComparator> overAllComparators;
	private List<DetailedBehavior> detailedBehaviors;
	private List<SpiderBean> spiderBeans;
	

	

	/**
	 * @return the detailedBehaviors
	 */
	public List<DetailedBehavior> getDetailedBehaviors() {
		return detailedBehaviors;
	}


	/**
	 * @param detailedBehaviors the detailedBehaviors to set
	 */
	public void setDetailedBehaviors(List<DetailedBehavior> detailedBehaviors) {
		this.detailedBehaviors = detailedBehaviors;
	}


	/**
	 * @return the overAllComparators
	 */
	public List<OverAllComparator> getOverAllComparators() {
		return overAllComparators;
	}


	/**
	 * @param overAllComparators the overAllComparators to set
	 */
	public void setOverAllComparators(List<OverAllComparator> overAllComparators) {
		this.overAllComparators = overAllComparators;
	}


	/**
	 * @return the overallSummaryReports
	 */
	public List<OverallSummaryReport> getOverallSummaryReports() {
		return overallSummaryReports;
	}

	
	/**
	 * @return the overAllSummaryReportWithOthers
	 */
	public List<OverAllSummaryReportWithOthers> getOverAllSummaryReportWithOthers() {
		return overAllSummaryReportWithOthers;
	}


	/**
	 * @param overAllSummaryReportWithOthers the overAllSummaryReportWithOthers to set
	 */
	public void setOverAllSummaryReportWithOthers(
			List<OverAllSummaryReportWithOthers> overAllSummaryReportWithOthers) {
		this.overAllSummaryReportWithOthers = overAllSummaryReportWithOthers;
	}


	/**
	 * @param overallSummaryReports the overallSummaryReports to set
	 */
	public void setOverallSummaryReports(
			List<OverallSummaryReport> overallSummaryReports) {
		this.overallSummaryReports = overallSummaryReports;
	}


	public List<SpiderBean> getSpiderBeans() {
		return spiderBeans;
	}


	public void setSpiderBeans(List<SpiderBean> spiderBeans) {
		this.spiderBeans = spiderBeans;
	}
	
}
