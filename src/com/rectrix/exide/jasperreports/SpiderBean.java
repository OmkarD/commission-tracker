/**
 * 
 */
package com.rectrix.exide.jasperreports;

/**
 * @author rectrix
 *
 */
public class SpiderBean {

	private String series;
	private Float value;
	private String catagory;
	
	
	
	public SpiderBean(String series, Float value, String catagory) {
		super();
		this.series = series;
		this.value = value;
		this.catagory = catagory;
	}
	/**
	 * @return the series
	 */
	public String getSeries() {
		return series;
	}
	/**
	 * @param series the series to set
	 */
	public void setSeries(String series) {
		this.series = series;
	}
	/**
	 * @return the value
	 */
	public Float getValue() {
		return value;
	}
	/**
	 * @param value the value to set
	 */
	public void setValue(Float value) {
		this.value = value;
	}
	/**
	 * @return the catagory
	 */
	public String getCatagory() {
		return catagory;
	}
	/**
	 * @param catagory the catagory to set
	 */
	public void setCatagory(String catagory) {
		this.catagory = catagory;
	}

	
	
	
	
}
