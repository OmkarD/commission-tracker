/**
 * 
 */
package com.rectrix.exide.jasperreports;

/**
 * @author rectrix
 *
 */
public class DetailedBehavior {
	
	private String driver;
	private String maxScore;
	private String question;
	private String selfScore;
	private String dmScore;
	private String drScore;
	private String peScore;
	private String shScore;
	private String mergeScore;
	
	private String dmScoreStyle2;
	private String selfScoreStyle2;
	private String drScoreStyle2;
	private String peScoreStyle2;
	private String shScoreStyle2;
	private String mergeScoreStyle2;
	/**
	 * @return the driver
	 */
	public String getDriver() {
		return driver;
	}
	/**
	 * @param driver the driver to set
	 */
	public void setDriver(String driver) {
		this.driver = driver;
	}
	/**
	 * @return the maxScore
	 */
	public String getMaxScore() {
		return maxScore;
	}
	/**
	 * @param maxScore the maxScore to set
	 */
	public void setMaxScore(String maxScore) {
		this.maxScore = maxScore;
	}
	/**
	 * @return the question
	 */
	public String getQuestion() {
		return question;
	}
	/**
	 * @param question the question to set
	 */
	public void setQuestion(String question) {
		this.question = question;
	}
	/**
	 * @return the selfScore
	 */
	public String getSelfScore() {
		return selfScore;
	}
	/**
	 * @param selfScore the selfScore to set
	 */
	public void setSelfScore(String selfScore) {
		this.selfScore = selfScore;
	}
	/**
	 * @return the dmScore
	 */
	public String getDmScore() {
		return dmScore;
	}
	/**
	 * @param dmScore the dmScore to set
	 */
	public void setDmScore(String dmScore) {
		this.dmScore = dmScore;
	}
	/**
	 * @return the drScore
	 */
	public String getDrScore() {
		return drScore;
	}
	/**
	 * @param drScore the drScore to set
	 */
	public void setDrScore(String drScore) {
		this.drScore = drScore;
	}
	/**
	 * @return the peScore
	 */
	public String getPeScore() {
		return peScore;
	}
	/**
	 * @param peScore the peScore to set
	 */
	public void setPeScore(String peScore) {
		this.peScore = peScore;
	}
	/**
	 * @return the shScore
	 */
	public String getShScore() {
		return shScore;
	}
	/**
	 * @param shScore the shScore to set
	 */
	public void setShScore(String shScore) {
		this.shScore = shScore;
	}
	/**
	 * @return the mergeScore
	 */
	public String getMergeScore() {
		return mergeScore;
	}
	/**
	 * @param mergeScore the mergeScore to set
	 */
	public void setMergeScore(String mergeScore) {
		this.mergeScore = mergeScore;
	}
	/**
	 * @return the dmScoreStyle2
	 */
	public String getDmScoreStyle2() {
		return dmScoreStyle2;
	}
	/**
	 * @param dmScoreStyle2 the dmScoreStyle2 to set
	 */
	public void setDmScoreStyle2(String dmScoreStyle2) {
		this.dmScoreStyle2 = dmScoreStyle2;
	}
	/**
	 * @return the selfScoreStyle2
	 */
	public String getSelfScoreStyle2() {
		return selfScoreStyle2;
	}
	/**
	 * @param selfScoreStyle2 the selfScoreStyle2 to set
	 */
	public void setSelfScoreStyle2(String selfScoreStyle2) {
		this.selfScoreStyle2 = selfScoreStyle2;
	}
	/**
	 * @return the drScoreStyle2
	 */
	public String getDrScoreStyle2() {
		return drScoreStyle2;
	}
	/**
	 * @param drScoreStyle2 the drScoreStyle2 to set
	 */
	public void setDrScoreStyle2(String drScoreStyle2) {
		this.drScoreStyle2 = drScoreStyle2;
	}
	/**
	 * @return the peScoreStyle2
	 */
	public String getPeScoreStyle2() {
		return peScoreStyle2;
	}
	/**
	 * @param peScoreStyle2 the peScoreStyle2 to set
	 */
	public void setPeScoreStyle2(String peScoreStyle2) {
		this.peScoreStyle2 = peScoreStyle2;
	}
	/**
	 * @return the shScoreStyle2
	 */
	public String getShScoreStyle2() {
		return shScoreStyle2;
	}
	/**
	 * @param shScoreStyle2 the shScoreStyle2 to set
	 */
	public void setShScoreStyle2(String shScoreStyle2) {
		this.shScoreStyle2 = shScoreStyle2;
	}
	/**
	 * @return the mergeScoreStyle2
	 */
	public String getMergeScoreStyle2() {
		return mergeScoreStyle2;
	}
	/**
	 * @param mergeScoreStyle2 the mergeScoreStyle2 to set
	 */
	public void setMergeScoreStyle2(String mergeScoreStyle2) {
		this.mergeScoreStyle2 = mergeScoreStyle2;
	}

	
	
}
