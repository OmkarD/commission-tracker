/**
 * 
 */
package com.rectrix.exide.jasperreports;

/**
 * @author rectrix
 *
 */
public class OverAllComparator {
	
	private String driver;
	private String maxScore;
	private String earlierSession;
	private String previousSession;
	private String nextSession;
	private String incDecScore;
	
	private String earlierSessionStyle;
	private String previousSessionStyle;
	private String nextSessionStyle;
	private String incDecScoreStyle;
	
	private String earlierSessionValue;
	private String previousSessionValue;
	private String nextSessionValue;
	
	
	
	
	public String getEarlierSessionValue() {
		return earlierSessionValue;
	}

	public void setEarlierSessionValue(String earlierSessionValue) {
		this.earlierSessionValue = earlierSessionValue;
	}

	public String getPreviousSessionValue() {
		return previousSessionValue;
	}

	public void setPreviousSessionValue(String previousSessionValue) {
		this.previousSessionValue = previousSessionValue;
	}

	public String getNextSessionValue() {
		return nextSessionValue;
	}

	public void setNextSessionValue(String nextSessionValue) {
		this.nextSessionValue = nextSessionValue;
	}

	public String getEarlierSession() {
		return earlierSession;
	}
	
	public void setEarlierSession(String earlierSession) {
		this.earlierSession = earlierSession;
	}
	
	public String getEarlierSessionStyle() {
		return earlierSessionStyle;
	}
	
	public void setEarlierSessionStyle(String earlierSessionStyle) {
		this.earlierSessionStyle = earlierSessionStyle;
	}
	/**
	 * @return the driver
	 */
	public String getDriver() {
		return driver;
	}
	/**
	 * @param driver the driver to set
	 */
	public void setDriver(String driver) {
		this.driver = driver;
	}
	/**
	 * @return the maxScore
	 */
	public String getMaxScore() {
		return maxScore;
	}
	/**
	 * @param maxScore the maxScore to set
	 */
	public void setMaxScore(String maxScore) {
		this.maxScore = maxScore;
	}
	/**
	 * @return the previousSession
	 */
	public String getPreviousSession() {
		return previousSession;
	}
	/**
	 * @param previousSession the previousSession to set
	 */
	public void setPreviousSession(String previousSession) {
		this.previousSession = previousSession;
	}
	/**
	 * @return the nextSession
	 */
	public String getNextSession() {
		return nextSession;
	}
	/**
	 * @param nextSession the nextSession to set
	 */
	public void setNextSession(String nextSession) {
		this.nextSession = nextSession;
	}
	/**
	 * @return the incDecScore
	 */
	public String getIncDecScore() {
		return incDecScore;
	}
	/**
	 * @param incDecScore the incDecScore to set
	 */
	public void setIncDecScore(String incDecScore) {
		this.incDecScore = incDecScore;
	}
	/**
	 * @return the previousSessionStyle
	 */
	public String getPreviousSessionStyle() {
		return previousSessionStyle;
	}
	/**
	 * @param previousSessionStyle the previousSessionStyle to set
	 */
	public void setPreviousSessionStyle(String previousSessionStyle) {
		this.previousSessionStyle = previousSessionStyle;
	}
	/**
	 * @return the nextSessionStyle
	 */
	public String getNextSessionStyle() {
		return nextSessionStyle;
	}
	/**
	 * @param nextSessionStyle the nextSessionStyle to set
	 */
	public void setNextSessionStyle(String nextSessionStyle) {
		this.nextSessionStyle = nextSessionStyle;
	}
	/**
	 * @return the incDecScoreStyle
	 */
	public String getIncDecScoreStyle() {
		try{
			Float val = Float.parseFloat(getIncDecScore());
			if(val>0){
				return "GREEN";
			}else if(val==0){
				return "CEMENT";
			}else{
				return "AMBER";
			}
		}catch(Exception e){
			return "WHITE";
		}
		
	}
	/**
	 * @param incDecScoreStyle the incDecScoreStyle to set
	 */
	public void setIncDecScoreStyle(String incDecScoreStyle) {
		this.incDecScoreStyle = incDecScoreStyle;
	}
	
	
	

}
