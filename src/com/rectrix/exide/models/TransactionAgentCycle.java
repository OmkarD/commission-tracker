package com.rectrix.exide.models;

import java.io.Serializable;
import java.util.Date;

public class TransactionAgentCycle implements Serializable {

	private String agentNum;
	private Date dateApp;
	private String clientSex;
	private String salutl;
	private String agentName;
	private String agentAddr;
	private String zBrokAgnt;
	private String zDateLic;
	private Date ClientDob; 
	private String agentBranchCode;	
	private String agentUnitCode;
	private String agentType;
	private String aracde; 
	private String agentClass;
	private Date dateTrm;
	private String payClient;
	private String clientNumber;
	private String phoneNum1;
	private String phoneNum2;
	private String reporting;
	private String nbusallw;
	private String minsta;
	private String reasoncd;
	private Date effectiveDate; 
	private String proption;
	private String mobilePhone;
	private String employeeCode;
	private String mailId;
	private String panNum;
	private String channel;
	private String paymentMethod;
	private String bankAccNumber;
	private String ifsc;
	private int cycleId;
	
	
	
	
	public String getBankAccNumber() {
		return bankAccNumber;
	}
	public void setBankAccNumber(String bankAccNumber) {
		this.bankAccNumber = bankAccNumber;
	}
	public String getIfsc() {
		return ifsc;
	}
	public void setIfsc(String ifsc) {
		this.ifsc = ifsc;
	}
	public int getCycleId() {
		return cycleId;
	}
	public void setCycleId(int cycleId) {
		this.cycleId = cycleId;
	}
	public String getAgentNum() {
		return agentNum;
	}
	public void setAgentNum(String agentNum) {
		this.agentNum = agentNum;
	}
	public Date getDateApp() {
		return dateApp;
	}
	public void setDateApp(Date dateApp) {
		this.dateApp = dateApp;
	}
	public String getClientSex() {
		return clientSex;
	}
	public void setClientSex(String clientSex) {
		this.clientSex = clientSex;
	}
	public String getSalutl() {
		return salutl;
	}
	public void setSalutl(String salutl) {
		this.salutl = salutl;
	}
	public String getAgentName() {
		return agentName;
	}
	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}
	public String getAgentAddr() {
		return agentAddr;
	}
	public void setAgentAddr(String agentAddr) {
		this.agentAddr = agentAddr;
	}
	public String getzBrokAgnt() {
		return zBrokAgnt;
	}
	public void setzBrokAgnt(String zBrokAgnt) {
		this.zBrokAgnt = zBrokAgnt;
	}
	public String getzDateLic() {
		return zDateLic;
	}
	public void setzDateLic(String zDateLic) {
		this.zDateLic = zDateLic;
	}
	public Date getClientDob() {
		return ClientDob;
	}
	public void setClientDob(Date clientDob) {
		ClientDob = clientDob;
	}
	public String getAgentBranchCode() {
		return agentBranchCode;
	}
	public void setAgentBranchCode(String agentBranchCode) {
		this.agentBranchCode = agentBranchCode;
	}
	public String getAgentUnitCode() {
		return agentUnitCode;
	}
	public void setAgentUnitCode(String agentUnitCode) {
		this.agentUnitCode = agentUnitCode;
	}
	public String getAgentType() {
		return agentType;
	}
	public void setAgentType(String agentType) {
		this.agentType = agentType;
	}
	public String getAracde() {
		return aracde;
	}
	public void setAracde(String aracde) {
		this.aracde = aracde;
	}
	public String getAgentClass() {
		return agentClass;
	}
	public void setAgentClass(String agentClass) {
		this.agentClass = agentClass;
	}
	public Date getDateTrm() {
		return dateTrm;
	}
	public void setDateTrm(Date dateTrm) {
		this.dateTrm = dateTrm;
	}
	public String getPayClient() {
		return payClient;
	}
	public void setPayClient(String payClient) {
		this.payClient = payClient;
	}
	public String getClientNumber() {
		return clientNumber;
	}
	public void setClientNumber(String clientNumber) {
		this.clientNumber = clientNumber;
	}
	public String getPhoneNum1() {
		return phoneNum1;
	}
	public void setPhoneNum1(String phoneNum1) {
		this.phoneNum1 = phoneNum1;
	}
	public String getPhoneNum2() {
		return phoneNum2;
	}
	public void setPhoneNum2(String phoneNum2) {
		this.phoneNum2 = phoneNum2;
	}
	public String getReporting() {
		return reporting;
	}
	public void setReporting(String reporting) {
		this.reporting = reporting;
	}
	public String getNbusallw() {
		return nbusallw;
	}
	public void setNbusallw(String nbusallw) {
		this.nbusallw = nbusallw;
	}
	public String getMinsta() {
		return minsta;
	}
	public void setMinsta(String minsta) {
		this.minsta = minsta;
	}
	public String getReasoncd() {
		return reasoncd;
	}
	public void setReasoncd(String reasoncd) {
		this.reasoncd = reasoncd;
	}
	public Date getEffectiveDate() {
		return effectiveDate;
	}
	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}
	public String getProption() {
		return proption;
	}
	public void setProption(String proption) {
		this.proption = proption;
	}
	public String getMobilePhone() {
		return mobilePhone;
	}
	public void setMobilePhone(String mobilePhone) {
		this.mobilePhone = mobilePhone;
	}
	public String getEmployeeCode() {
		return employeeCode;
	}
	public void setEmployeeCode(String employeeCode) {
		this.employeeCode = employeeCode;
	}
	public String getMailId() {
		return mailId;
	}
	public void setMailId(String mailId) {
		this.mailId = mailId;
	}
	public String getPanNum() {
		return panNum;
	}
	public void setPanNum(String panNum) {
		this.panNum = panNum;
	}
	public String getChannel() {
		return channel;
	}
	public void setChannel(String channel) {
		this.channel = channel;
	}
	public String getPaymentMethod() {
		return paymentMethod;
	}
	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}
	
	
	
	
	

}
