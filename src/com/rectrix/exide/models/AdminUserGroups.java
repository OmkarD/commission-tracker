package com.rectrix.exide.models;

import java.io.Serializable;

public class AdminUserGroups implements Serializable {
	private String groupId;
	private int status; // based on status active or inactive
	private int type; // for multiple groups defined type : if type 1 : admin , if type 2 : user
	public String getGroupId() {
		return groupId;
	}
	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	
	
}
