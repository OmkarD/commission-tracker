package com.rectrix.exide.models;

import java.io.Serializable;
import java.util.Date;

public class CycleMaster implements Serializable {
	private Long cycleId;
	private String cycleName;
	private Date fromDate;
	private Date toDate;
	private String cycleDescription;
	private String createdBy;
	private Date criatedOn;
	private int validflag;
	private int agentSyncStatus;
	
	
	
	public int getAgentSyncStatus() {
		return agentSyncStatus;
	}
	public void setAgentSyncStatus(int agentSyncStatus) {
		this.agentSyncStatus = agentSyncStatus;
	}
	public int getValidflag() {
		return validflag;
	}
	public void setValidflag(int validflag) {
		this.validflag = validflag;
	}
	public Long getCycleId() {
		return cycleId;
	}
	public void setCycleId(Long cycleId) {
		this.cycleId = cycleId;
	}
	public String getCycleName() {
		return cycleName;
	}
	public void setCycleName(String cycleName) {
		this.cycleName = cycleName;
	}
	public Date getFromDate() {
		return fromDate;
	}
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}
	public Date getToDate() {
		return toDate;
	}
	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}
	public String getCycleDescription() {
		return cycleDescription;
	}
	public void setCycleDescription(String cycleDescription) {
		this.cycleDescription = cycleDescription;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCriatedOn() {
		return criatedOn;
	}
	public void setCriatedOn(Date criatedOn) {
		this.criatedOn = criatedOn;
	}
	
	
	
	

}
