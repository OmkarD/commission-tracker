package com.rectrix.exide.form.models;

import java.io.Serializable;
import java.util.Date;

public class CycleMasterForm implements Serializable {
	private Long cycleId;
	private String cycleName;
	private Date fromDate;
	private Date toDate;
	private String cycleDescription;
	private String createdBy;
	private Date criatedOn;
	private int validFlag;
	private int agentSyncStatus;
	
	
	public int getValidFlag() {
		return validFlag;
	}
	public void setValidFlag(int validFlag) {
		this.validFlag = validFlag;
	}
	public int getAgentSyncStatus() {
		return agentSyncStatus;
	}
	public void setAgentSyncStatus(int agentSyncStatus) {
		this.agentSyncStatus = agentSyncStatus;
	}
	public Long getCycleId() {
		return cycleId;
	}
	public void setCycleId(Long cycleId) {
		this.cycleId = cycleId;
	}
	public String getCycleName() {
		return cycleName;
	}
	public void setCycleName(String cycleName) {
		this.cycleName = cycleName;
	}
	public Date getFromDate() {
		return fromDate;
	}
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}
	public Date getToDate() {
		return toDate;
	}
	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}
	public String getCycleDescription() {
		return cycleDescription;
	}
	public void setCycleDescription(String cycleDescription) {
		this.cycleDescription = cycleDescription;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCriatedOn() {
		return criatedOn;
	}
	public void setCriatedOn(Date criatedOn) {
		this.criatedOn = criatedOn;
	}
	
	
	
	

}
