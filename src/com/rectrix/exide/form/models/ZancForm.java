package com.rectrix.exide.form.models;

import java.io.Serializable;
import java.util.Date;

public class ZancForm implements Serializable {

	
	private Long zancId;
	private String agentNumber;
	private String clientNumber;
	private String agentNumber1; // same DATA as agentNumber
	private String agentName;
	private String clientName;
	private String clientAddress1;
	private String clientAddress2;
	private String clientAddress3;
	private String clientAddress4;
	private String clientAddress5;
	private String payOfMonth;
	private String bankAccKey;
	private Float zCommPaid;
	private Float zTaxWhld;
	private Float zTotAmtPd;
	private String agentClass;
	private Float wTaxRate;
	private Float zAdjust;
	private String zDocNum;
	private String IRDNum;
	private String agentBranchCode;
	private String agentUnitCode;
	private Date fromDate;
	private String bankKey;
	private String zIfscCode;
	private String bankDesc;
	private String cycleName;
	
	
	public Long getZancId() {
		return zancId;
	}
	public void setZancId(Long zancId) {
		this.zancId = zancId;
	}
	public String getAgentNumber() {
		return agentNumber;
	}
	public void setAgentNumber(String agentNumber) {
		this.agentNumber = agentNumber;
	}
	public String getClientNumber() {
		return clientNumber;
	}
	public void setClientNumber(String clientNumber) {
		this.clientNumber = clientNumber;
	}
	public String getAgentNumber1() {
		return agentNumber1;
	}
	public void setAgentNumber1(String agentNumber1) {
		this.agentNumber1 = agentNumber1;
	}
	public String getAgentName() {
		return agentName;
	}
	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}
	public String getClientName() {
		return clientName;
	}
	public void setClientName(String clientName) {
		this.clientName = clientName;
	}
	public String getClientAddress1() {
		return clientAddress1;
	}
	public void setClientAddress1(String clientAddress1) {
		this.clientAddress1 = clientAddress1;
	}
	public String getClientAddress2() {
		return clientAddress2;
	}
	public void setClientAddress2(String clientAddress2) {
		this.clientAddress2 = clientAddress2;
	}
	public String getClientAddress3() {
		return clientAddress3;
	}
	public void setClientAddress3(String clientAddress3) {
		this.clientAddress3 = clientAddress3;
	}
	public String getClientAddress4() {
		return clientAddress4;
	}
	public void setClientAddress4(String clientAddress4) {
		this.clientAddress4 = clientAddress4;
	}
	public String getClientAddress5() {
		return clientAddress5;
	}
	public void setClientAddress5(String clientAddress5) {
		this.clientAddress5 = clientAddress5;
	}
	public String getPayOfMonth() {
		return payOfMonth;
	}
	public void setPayOfMonth(String payOfMonth) {
		this.payOfMonth = payOfMonth;
	}
	public String getBankAccKey() {
		return bankAccKey;
	}
	public void setBankAccKey(String bankAccKey) {
		this.bankAccKey = bankAccKey;
	}
	public Float getzCommPaid() {
		return zCommPaid;
	}
	public void setzCommPaid(Float zCommPaid) {
		this.zCommPaid = zCommPaid;
	}
	public Float getzTaxWhld() {
		return zTaxWhld;
	}
	public void setzTaxWhld(Float zTaxWhld) {
		this.zTaxWhld = zTaxWhld;
	}
	public Float getzTotAmtPd() {
		return zTotAmtPd;
	}
	public void setzTotAmtPd(Float zTotAmtPd) {
		this.zTotAmtPd = zTotAmtPd;
	}
	public String getAgentClass() {
		return agentClass;
	}
	public void setAgentClass(String agentClass) {
		this.agentClass = agentClass;
	}
	public Float getwTaxRate() {
		return wTaxRate;
	}
	public void setwTaxRate(Float wTaxRate) {
		this.wTaxRate = wTaxRate;
	}
	public Float getzAdjust() {
		return zAdjust;
	}
	public void setzAdjust(Float zAdjust) {
		this.zAdjust = zAdjust;
	}
	public String getzDocNum() {
		return zDocNum;
	}
	public void setzDocNum(String zDocNum) {
		this.zDocNum = zDocNum;
	}
	public String getIRDNum() {
		return IRDNum;
	}
	public void setIRDNum(String iRDNum) {
		IRDNum = iRDNum;
	}
	public String getAgentBranchCode() {
		return agentBranchCode;
	}
	public void setAgentBranchCode(String agentBranchCode) {
		this.agentBranchCode = agentBranchCode;
	}
	public String getAgentUnitCode() {
		return agentUnitCode;
	}
	public void setAgentUnitCode(String agentUnitCode) {
		this.agentUnitCode = agentUnitCode;
	}
	public Date getFromDate() {
		return fromDate;
	}
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}
	public String getBankKey() {
		return bankKey;
	}
	public void setBankKey(String bankKey) {
		this.bankKey = bankKey;
	}
	public String getzIfscCode() {
		return zIfscCode;
	}
	public void setzIfscCode(String zIfscCode) {
		this.zIfscCode = zIfscCode;
	}
	public String getBankDesc() {
		return bankDesc;
	}
	public void setBankDesc(String bankDesc) {
		this.bankDesc = bankDesc;
	}
	public String getCycleName() {
		return cycleName;
	}
	public void setCycleName(String cycleName) {
		this.cycleName = cycleName;
	}
	
	



}
