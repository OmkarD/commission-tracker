package com.rectrix.exide.form.models;


import com.rectrix.exide.security.XSSCleaner;

public class DataTableModel {

	private XSSCleaner xssCleaner = new XSSCleaner();
	
	Integer start;
	Integer amount;
	String col;
	String dire;
	String searchTearm;
	public DataTableModel() {
		// TODO Auto-generated constructor stub
	}
	public Integer getStart() {
		return start;
	}
	public void setStart(Integer start) {
		this.start = start;
	}
	public Integer getAmount() {
		return amount;
	}
	public void setAmount(Integer amount) {
		this.amount = amount;
	}
	public String getCol() {
		return col;
	}
	public void setCol(String col) {
		this.col = col;
	}
	public String getDire() {
		return dire;
	}
	public void setDire(String dire) {
		this.dire = dire;
	}
	public String getSearchTearm() {
		return searchTearm;
	}
	public void setSearchTearm(String searchTearm) {
		this.searchTearm = xssCleaner.stripXSS(searchTearm.trim());
	}

	
}
