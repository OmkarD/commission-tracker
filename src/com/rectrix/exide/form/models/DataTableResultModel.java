package com.rectrix.exide.form.models;

import java.util.Collection;

public class DataTableResultModel  {

	Integer iTotalRecords;
	Integer iTotalDisplayRecords;

	Collection aaData;
	public DataTableResultModel() {
		// TODO Auto-generated constructor stub
	}
	public DataTableResultModel(Collection aaData,Integer iTotalRecords,Integer iTotalDisplayRecords) {
		// TODO Auto-generated constructor stub
		this.aaData=aaData;
		this.iTotalDisplayRecords=iTotalDisplayRecords;
		this.iTotalRecords=iTotalRecords;
	}
	
	
	public Integer getiTotalRecords() {
		return iTotalRecords;
	}

	public void setiTotalRecords(Integer iTotalRecords) {
		this.iTotalRecords = iTotalRecords;
	}

	public Integer getiTotalDisplayRecords() {
		return iTotalDisplayRecords;
	}

	public void setiTotalDisplayRecords(Integer iTotalDisplayRecords) {
		this.iTotalDisplayRecords = iTotalDisplayRecords;
	}

	public Collection getAaData() {
		return aaData;
	}
	public void setAaData(Collection aaData) {
		this.aaData = aaData;
	}
	
}
