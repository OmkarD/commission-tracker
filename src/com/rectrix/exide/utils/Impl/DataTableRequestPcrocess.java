package com.rectrix.exide.utils.Impl;

import java.util.Collection;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Component;

import com.rectrix.exide.form.models.DataTableModel;
import com.rectrix.exide.form.models.DataTableResultModel;


@Component
public class DataTableRequestPcrocess {

	public DataTableModel geDataTableModel(HttpServletRequest request){
		DataTableModel model=new DataTableModel();
		
			model.setStart(Integer.parseInt(request.getParameter("iDisplayStart")));
			model.setAmount(Integer.parseInt(request.getParameter("iDisplayLength")));
			model.setCol(request.getParameter("iSortCol_0"));
			model.setDire(request.getParameter("sSortDir_0"));
			model.setSearchTearm(request.getParameter("sSearch"));
			
		return model;
	}
	
	
	public DataTableResultModel geDataTableModelResult(Collection details,Integer total,Integer afterFilter){
		DataTableResultModel modelResult=new DataTableResultModel();
		modelResult.setAaData(details);
		modelResult.setiTotalRecords(afterFilter);
		modelResult.setiTotalDisplayRecords(total);
		return modelResult;
		
	}
	
	


}
