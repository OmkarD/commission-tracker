package com.rectrix.exide.security;

import java.io.IOException;
import java.util.Collection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.WebAttributes;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Service;



public class MySimpleUrlAuthenticationSuccessHandler implements AuthenticationSuccessHandler {
	 
    private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();
 
    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, 
      HttpServletResponse response, Authentication authentication) throws IOException {
        
    	handle(request, response, authentication);
        clearAuthenticationAttributes(request);
    }
 
    protected void handle(HttpServletRequest request, 
      HttpServletResponse response, Authentication authentication) throws IOException {
        String targetUrl = determineTargetUrl(authentication,request);
 
        if (response.isCommitted()) {
            return;
        }
 
        redirectStrategy.sendRedirect(request, response, targetUrl);
    }
 
    /** Builds the target URL according to the logic defined in the main class Javadoc. */
    protected String determineTargetUrl(Authentication authentication,HttpServletRequest request) {
        int reDir=0;
    	HttpSession session = request.getSession(true);
    	Collection<? extends GrantedAuthority> authorities = authentication.getAuthorities();
        for (GrantedAuthority grantedAuthority : authorities) {
        	if(grantedAuthority.getAuthority().split("_").length>1){
	        	if(grantedAuthority.getAuthority().split("_")[1].toString().equalsIgnoreCase("ADMIN")){
	        		
	        		if(reDir != 0) {
	        			session.setAttribute("admin", "2");//For both roles
	        		}else {
	        			session.setAttribute("admin", "1");//For single roles
	        		}
	        		reDir=1;
	        	}else if(grantedAuthority.getAuthority().split("_")[1].toString().equalsIgnoreCase("USER")){
	        		
	        		if(reDir==1) {
	        			session.setAttribute("admin", "2");//For both roles
	        		}else {
	        			session.setAttribute("admin", "1");//For single roles
	        		}
	        			reDir=2;
	        	}	        	
        	}
        }
        
        
        if(reDir==1){
    		return "/admin/";
        }else if(reDir==2){
    		return "/user/";
        }else {
			return "/logindenied/";
		}
 
    }
 
    protected void clearAuthenticationAttributes(HttpServletRequest request) {
        HttpSession session = request.getSession(false);
        if (session == null) {
            return;
        }
        session.removeAttribute(WebAttributes.AUTHENTICATION_EXCEPTION);
    }
 
    public void setRedirectStrategy(RedirectStrategy redirectStrategy) {
        this.redirectStrategy = redirectStrategy;
    }
    protected RedirectStrategy getRedirectStrategy() {
        return redirectStrategy;
    }
}