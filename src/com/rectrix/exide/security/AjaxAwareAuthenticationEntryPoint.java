package com.rectrix.exide.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.LoginUrlAuthenticationEntryPoint;

public class AjaxAwareAuthenticationEntryPoint extends
		LoginUrlAuthenticationEntryPoint {

	public AjaxAwareAuthenticationEntryPoint(String loginUrl) {
		super(loginUrl);
	}

	@Override
	public void commence(HttpServletRequest request,
			HttpServletResponse response, AuthenticationException authException)
			throws IOException, ServletException {

		boolean isAjax=false;
		if(request.getRequestURI().startsWith("/commission-tracker/admin/")){
			isAjax = request.getRequestURI().toString().matches("^/commission-tracker/admin/[a-z A-Z 0-9]+");
		}else if(request.getRequestURI().startsWith("/commission-tracker/user/")){
			isAjax = request.getRequestURI().toString().matches("^/commission-tracker/user/[a-z A-Z 0-9]+");
		}

		if (isAjax) {
			response.sendError(403, "session time out");
		} else {
			super.commence(request, response, authException);
		}
	}
}