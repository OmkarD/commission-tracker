package com.rectrix.exide.security;


import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.rectrix.exide.dao.AdminUserGroupDao;
import com.rectrix.exide.models.AdminUserGroups;



public class UserDetailsServiceImpl implements UserDetailsService {

	
	
	@Autowired
	private AdminUserGroupDao adminUserGroupDao;
	
	
	
	public UserDetails loadUserByUsername(String username)
			throws UsernameNotFoundException {
		UserDetailsImpl matchingUser = null;
		if(username.equalsIgnoreCase("admin1")){
			matchingUser =new UserDetailsImpl("admin1","admin1",new GrantedAuthorityImpl("ROLE_ADMIN"));
		}else if(username.equalsIgnoreCase("user1")){
			matchingUser =new UserDetailsImpl("user1","user1",new GrantedAuthorityImpl("ROLE_USER"));
		}else{
			List<AdminUserGroups> adminUser= adminUserGroupDao.getAdminUserGroups();
			for (AdminUserGroups adminUserGroups : adminUser) {
				if(adminUserGroups!=null){
					if(adminUserGroups.getStatus()!=2 & adminUserGroups.getType() == 1) {
						matchingUser.getAuthorities().add(new GrantedAuthorityImpl("ROLE_ADMIN"));
					}else if(adminUserGroups.getStatus()!=2 & adminUserGroups.getType() == 2) {
						matchingUser.getAuthorities().add(new GrantedAuthorityImpl("ROLE_USER"));
					}
						
				}
			}
			
		}
		
		
		
		return matchingUser;
	}

}