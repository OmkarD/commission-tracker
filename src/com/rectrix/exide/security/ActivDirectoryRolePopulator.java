package com.rectrix.exide.security;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ldap.core.DirContextAdapter;
import org.springframework.ldap.core.DirContextOperations;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.ldap.userdetails.UserDetailsContextMapper;

import com.rectrix.exide.dao.AdminUserGroupDao;
import com.rectrix.exide.models.AdminUserGroups;


public class ActivDirectoryRolePopulator implements UserDetailsContextMapper {
	private static final Logger logger = Logger.getLogger(ActivDirectoryRolePopulator.class);

	
	@Autowired
	private AdminUserGroupDao adminUserGroupDao;
	
	
	@Override
	public UserDetails mapUserFromContext(DirContextOperations ctx,
			String username, Collection<? extends GrantedAuthority> authorities) {
	/*	List<SimpleGrantedAuthority> allAuthorities = new ArrayList<SimpleGrantedAuthority>();
	      for (GrantedAuthority auth : authorities) {
	          if (auth != null && !auth.getAuthority().isEmpty()) {
	             allAuthorities.add((SimpleGrantedAuthority) auth);
	          }
	        }
	        // add additional roles from the database table
	        allAuthorities.addAll(loadRolesFromDatabase(username, authorities));
	        return new User(username, "", true, true, true, true, allAuthorities);*/
		

		// TODO Auto-generated method stub
		List<SimpleGrantedAuthority> allAuthorities = new ArrayList<SimpleGrantedAuthority>();
		
		List<AdminUserGroups> adminUserGroups=adminUserGroupDao.getAdminUserGroups();
		boolean sa=false;
		for (GrantedAuthority auth : authorities) {
        if (auth != null && !auth.getAuthority().isEmpty()) {
        	
        	
        		for (AdminUserGroups groupMaster : adminUserGroups) {
        			if(auth.getAuthority().equalsIgnoreCase(groupMaster.getGroupId()))
                	{
        				if(groupMaster.getType() == 1 & groupMaster.getStatus() == 1) {
        					allAuthorities.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
                    		allAuthorities.add((SimpleGrantedAuthority) auth);
        				}
        				if(groupMaster.getType() == 2 & groupMaster.getStatus() == 1) {
        					allAuthorities.add(new SimpleGrantedAuthority("ROLE_USER"));
                    		allAuthorities.add((SimpleGrantedAuthority) auth);
        				}
        					
                	}else{
                		allAuthorities.add((SimpleGrantedAuthority) auth);
                	}
				}
        		
        }
      }
		
		
	    return new User(username, "", true, true, true, true, allAuthorities);
	
		
	}

	@Override
	   public void mapUserToContext(UserDetails user, DirContextAdapter ctx) {
	   }
	/*private List<SimpleGrantedAuthority> loadRolesFromDatabase(String username, Collection<? extends GrantedAuthority> authoritiesOld ) {
		List<SimpleGrantedAuthority> authorities=new ArrayList<SimpleGrantedAuthority>();
		try{
			SessionMaster sessionMaster = sessionMasterDao.getActiveSessionMaster();
			if(sessionMaster != null){
			if(employeeMasterDao.getEmployeeMasterByWinId(username, sessionMaster.getId())!=null){
				
				Date date=new Date();
				Date currentDate = new Date(date.getYear(), date.getMonth(), date.getDate());
				Date startDate = sessionMaster.getStartTime();
				Date endDate = sessionMaster.getEndTime();
			if((startDate.compareTo(currentDate)<=0)&&(endDate.compareTo(currentDate)>=0)){
				Boolean driverActive = false;
				List<DriverMaster> driverMaster = driverMasterDao.listDriverMasters();
				for (DriverMaster driverMaster2 : driverMaster) {
					if(driverMaster2.getStatus()==1){
						driverActive = true;
					}
					}
				
					if(driverActive){
						authorities.add(new SimpleGrantedAuthority("ROLE_EMPLOYEE"));
						}
					}
				}
				}
		}catch(Exception e){
			logger.error("logging error when LDAP Authentication", e);
			e.printStackTrace();
		}
		Boolean temp = false;
		try{
			AdminUser adminUser= adminUserDao.getAdminByUserId(username);
			if(adminUser!=null){
				authorities.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
				temp=true;
			}
		}catch(Exception e){
			logger.error("logging error when LDAP Authentication", e);
			e.printStackTrace();
		}
		if(!temp){
		try {
			List<AdminGroup> adminGroup = adminGroupDao.listAdminGroups();
			for (GrantedAuthority auth : authoritiesOld) {
		          if (auth != null && !auth.getAuthority().isEmpty()) {
		        	  AdminGroup adminGroup23 = new AdminGroup(auth.getAuthority());
		        	  if(adminGroup.contains(adminGroup23)){
		        		  authorities.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
		  					return authorities;
		        	  }
		        	  
		        	  
		        	  
		        	  for (AdminGroup adminGroup2 : adminGroup) {
		  				if(auth.getAuthority().equalsIgnoreCase(adminGroup2.getAppAdminGroup())){
		  					authorities.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
		  					return authorities;
		  				}
		  			}
		          }
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}
		return authorities;
	   }*/
}
