package com.rectrix.exide.security;

import java.util.Collection;
import java.util.HashSet;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

public class UserDetailsImpl implements UserDetails {
	private static final long serialVersionUID = -6509897037222767090L;

	private Collection<GrantedAuthority> authorities = new HashSet<GrantedAuthority>();
	private String password;
	private String username;

	public UserDetailsImpl(String username, String password,
			Collection<GrantedAuthority> authorities) {
		this.username = username;
		this.password = password;
		this.authorities = authorities;
	}

	public UserDetailsImpl(String username, String password,GrantedAuthority authorities) {
		this.username = username;
		this.password = password;
		this.authorities.add(authorities);
	}

	public UserDetailsImpl(String username, String password,GrantedAuthority authorities,GrantedAuthority authorities2) {
		this.username = username;
		this.password = password;
		this.authorities.add(authorities);
		this.authorities.add(authorities2);
		
	}
	
	public Collection getAuthorities() {
		return this.authorities;
	}

	public String getPassword() {
		return this.password;
	}

	public String getUsername() {
		return this.username;
	}

	public boolean isAccountNonExpired() {
		return true;
	}

	public boolean isAccountNonLocked() {
		return true;
	}

	public boolean isCredentialsNonExpired() {
		return true;
	}

	public boolean isEnabled() {
		return true;
	}
	@Override
	public String toString() {
		return getUsername() ;
	}

	public void setAuthorities(Collection<GrantedAuthority> authorities) {
		this.authorities = authorities;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setUsername(String username) {
		this.username = username;
	}
	


}
