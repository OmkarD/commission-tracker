package com.rectrix.exide.security;

import java.io.Serializable;

import org.springframework.security.core.GrantedAuthority;

public class GrantedAuthorityImpl implements GrantedAuthority, Serializable {
	public final static String admin="ROLE_ADMIN",user="ROLE_USER";

	
	private String rolename;

	public GrantedAuthorityImpl() {
		// TODO Auto-generated constructor stub
		this.rolename="ROLE_MANAGER";
	}
	public GrantedAuthorityImpl(String rolename) {
		this.rolename = rolename;
	}

	public String getAuthority() {
		return this.rolename;
	}

	public String getRolename() {
		return rolename;
	}

	public void setRolename(String rolename) {
		this.rolename = rolename;
	}

	
	
}